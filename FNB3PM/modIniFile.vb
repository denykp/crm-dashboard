﻿Imports System.Runtime.InteropServices
Imports System.Text
Module modIniFile
    Public lpSectionName1 As String
    Public lpKeyName1 As String
    Public lpValue1 As String
    Public lpSectionName2 As String
    Public lpKeyName2 As String
    Public lpValue2 As String
    Public lpSectionName3 As String
    Public lpKeyName3 As String
    Public lpValue3 As String
    Public lpSectionName4 As String
    Public lpKeyName4 As String
    Public lpValue4 As String
    Public lpSectionName5 As String
    Public lpKeyName5 As String
    Public lpValue5 As String
    Public lpFileName As String
    Public lpReturnedString1 As String
    Public lpReturnedString2 As String
    Public lpReturnedString3 As String
    Public lpReturnedString4 As String
    Public lpReturnedString5 As String
    Public nSize1 As Long
    Public nSize2 As Long
    Public nSize3 As Long
    Public nSize4 As Long
    Public nSize5 As Long
    'Public fso As New FileSystemObject
    Public LabelKu As String

    'API DECLARATIONS
    Declare Auto Function GetPrivateProfileString Lib "kernel32" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As String, ByVal lpDefault _
    As String, ByVal lpReturnedString As System.Text.StringBuilder, ByVal _
    nSize As Integer, ByVal lpFileName As String) As Integer

    Declare Auto Function WritePrivateProfileString Lib "kernel32" (ByVal lpAppName As String, _
                        ByVal lpKeyName As String, _
                        ByVal lpString As String, _
                        ByVal lpFileName As String) As Boolean

    Public Function sGetINI(ByVal sINIFile As String, ByVal sSection As String, ByVal sKey _
    As String, ByVal sDefault As String) As String

        Dim result As Integer
        Dim sb As StringBuilder
        'Dim IniFileName As String = Application.StartupPath & "\ServerName.ini"
        Try
            sb = New StringBuilder(300)
            result = GetPrivateProfileString(sSection, sKey, sDefault, sb, sb.Capacity, sINIFile)
            'servername = sb.ToString()
            sGetINI = sb.ToString
            Return sGetINI
            Exit Try
        Catch ex As Exception
            Return sDefault
            MsgBox(ex.Message)
        End Try


    End Function

    Public Function writeINI(ByVal sINIFile As String, ByVal sSection As String, ByVal sKey _
    As String, ByVal sValue As String) As Boolean
        Try

            WritePrivateProfileString(sSection, sKey, sValue, sINIFile)
            Return True
            Exit Try

        Catch ex As Exception
            Return False
            MsgBox(ex.Message)
        End Try
    End Function

    Public Sub ProfileSaveItem(ByVal lpSectionName As String, _
                               ByVal lpKeyName As String, _
                               ByVal lpValue As String, _
                               ByVal lpFileName As String)

        Call WritePrivateProfileString(lpSectionName, _
                                       lpKeyName, _
                                       lpValue, _
                                       lpFileName)

    End Sub

    'Public Function sizeFlxGrid(ByVal query As String, ByRef flxGrid As MSFlexGrid, ByVal keyIni As String, ByVal Index As Integer) As Long
    '    Dim conn As New ADODB.Connection
    '    Dim rs As New ADODB.Recordset
    '    conn.Open(strcon)
    '    rs.Open(query, conn)


    '    For i = 0 To rs.fields.Count - 1
    '        If keyIni <> "" Then
    '            If sGetINI(App_Path() & "\size.ini", keyIni, rs(i).Name, 0) > 0 Then
    '                flxGrid.ColWidth(Index) = Format((sGetINI(App_Path() & "\size.ini", keyIni, rs(i).Name, 0)) * 170, "###0")
    '            Else
    '                flxGrid.ColWidth(Index) = IIf(rs(i).DefinedSize < 30, rs(i).DefinedSize, 30) * 170
    '                writeINI(App_Path() & "\size.ini", keyIni, rs(i).Name, Format(flxGrid.ColWidth(Index), "###0"))
    '            End If
    '            Index = Index + 1
    '        End If
    '    Next

    '    rs.Close()
    '    conn.Close()
    'End Function
    'Public Function resizeFlxGrid(ByVal query As String, ByRef flxGrid As MSFlexGrid, ByVal keyIni As String, ByVal Index As Integer) As Long
    '    If conn.State Then conn.Close()
    '    Conn.Open()
    '    rs.Open(query, conn)


    '    For i = 0 To rs.fields.Count - 1
    '        writeINI(App_Path() & "\size.ini", keyIni, rs(i).Name, Format(flxGrid.ColWidth(Index) / 170, "###0"))
    '        Index = Index + 1
    '    Next

    '    rs.Close()
    '    conn.Close()
    'End Function

End Module
