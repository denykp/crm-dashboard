﻿Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class frmMasterDealer
    Dim transaction As OleDbTransaction
    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        If txtKodeDealer.Text = "" Or txtNamaDealer.Text = "" Then
            MsgBox("Semua data bertanda * harus diisi")
            Exit Sub
        End If
        Dim conn2 As New OleDbConnection(strcon)

        conn2.Open()
        transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)

        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        Try
            cmd = New OleDbCommand("select * from M_Dealer where Dealer_Kode = '" & txtKodeDealer.Text & "'", conn2, transaction)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                cmd = New OleDbCommand(update_dataheader(), conn2, transaction)
                cmd.ExecuteNonQuery()
            Else
                cmd = New OleDbCommand(add_dataheader(), conn2, transaction)
                cmd.ExecuteNonQuery()
            End If
            reader.Close()


            transaction.Commit()
            conn2.Close()
            MsgBox("Data sudah tersimpan")
            reset_form()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()
            transaction.Rollback()
            If conn2.State Then conn2.Close()
        End Try
    End Sub

    Private Function add_dataheader() As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(20)
        ReDim nilai(20)

        table_name = "M_Dealer"
        fields(0) = "Dealer_Kode"
        fields(1) = "Dealer_Name"
        fields(2) = "Dealer_KodeBisnis"
        fields(3) = "Dealer_MD_YN"
        fields(4) = "Dealer_MDKode"
        fields(5) = "Dealer_Alamat"
        fields(6) = "Dealer_Kec"
        fields(7) = "Dealer_Kota"
        fields(8) = "Dealer_Prov"
        fields(9) = "Dealer_KodePos"
        fields(10) = "Dealer_Tlp1"
        fields(11) = "Dealer_Tlp2"
        fields(12) = "Dealer_Tlp3"
        fields(13) = "Dealer_Fax"
        fields(14) = "Dealer_Mail"
        fields(15) = "Dealer_AktifYN"
        fields(16) = "Dealer_DateEdit"
        fields(17) = "Dealer_DateCreate"
        fields(18) = "Dealer_Username"
        fields(19) = "Dealer_batch"

        nilai(0) = txtKodeDealer.Text
        nilai(1) = txtNamaDealer.Text
        nilai(2) = cmbKodeBisnis.Text
        nilai(3) = "0"
        nilai(4) = txtKodeMD.Text
        nilai(5) = txtAlamat.Text
        nilai(6) = txtKecamatan.Text
        nilai(7) = txtKota.Text
        nilai(8) = txtProvinsi.Text
        nilai(9) = txtKodePos.Text
        nilai(10) = txtTelp1.Text
        nilai(11) = txtTelp2.Text
        nilai(12) = txtTelp3.Text
        nilai(13) = txtFax.Text
        nilai(14) = txtEmail.Text
        nilai(15) = IIf(chkAktif.Checked, "1", "0")
        nilai(16) = Format(Now, "yyyy/MM/dd HH:mm:ss")
        nilai(17) = Format(Now, "yyyy/MM/dd HH:mm:ss")
        nilai(18) = user
        nilai(19) = cmbBatch.Text

        add_dataheader = tambah_data2(table_name, fields, nilai)
    End Function
    Private Function update_dataheader() As String
        Dim fields() As String
        Dim nilai() As String
        Dim fieldsPK() As String
        Dim nilaiPK() As String
        Dim table_name As String

        ReDim fields(19)
        ReDim nilai(19)
        ReDim fieldsPK(1)
        ReDim nilaiPK(1)

        table_name = "M_Dealer"
        fields(0) = "Dealer_Kode"
        fields(1) = "Dealer_Name"
        fields(2) = "Dealer_KodeBisnis"
        fields(3) = "Dealer_MD_YN"
        fields(4) = "Dealer_MDKode"
        fields(5) = "Dealer_Alamat"
        fields(6) = "Dealer_Kec"
        fields(7) = "Dealer_Kota"
        fields(8) = "Dealer_Prov"
        fields(9) = "Dealer_KodePos"
        fields(10) = "Dealer_Tlp1"
        fields(11) = "Dealer_Tlp2"
        fields(12) = "Dealer_Tlp3"
        fields(13) = "Dealer_Fax"
        fields(14) = "Dealer_Mail"
        fields(15) = "Dealer_AktifYN"
        fields(16) = "Dealer_DateEdit"
        fields(17) = "Dealer_Username"
        fields(18) = "Dealer_batch"

        nilai(0) = txtKodeDealer.Text
        nilai(1) = txtNamaDealer.Text
        nilai(2) = cmbKodeBisnis.Text
        nilai(3) = "0"
        nilai(4) = txtKodeMD.Text
        nilai(5) = txtAlamat.Text
        nilai(6) = txtKecamatan.Text
        nilai(7) = txtKota.Text
        nilai(8) = txtProvinsi.Text
        nilai(9) = txtKodePos.Text
        nilai(10) = txtTelp1.Text
        nilai(11) = txtTelp2.Text
        nilai(12) = txtTelp3.Text
        nilai(13) = txtFax.Text
        nilai(14) = txtEmail.Text
        nilai(15) = IIf(chkAktif.Checked, "1", "0")
        nilai(16) = Format(Now, "yyyy/MM/dd HH:mm:ss")
        nilai(17) = user
        nilai(18) = cmbBatch.Text
        fieldsPK(0) = fields(0)
        nilaiPK(0) = nilai(0)
        update_dataheader = update_data2(table_name, fields, nilai, fieldsPK, nilaiPK)
    End Function
    Private Sub reset_form()
        txtKodeDealer.Text = ""
        txtNamaDealer.Text = ""
        txtBatch.Text = ""
        txtKodeMD.Text = ""
        txtAlamat.Text = ""
        txtKecamatan.Text = ""
        txtKota.Text = ""
        txtProvinsi.Text = ""
        txtKodePos.Text = ""
        txtTelp1.Text = ""
        txtTelp2.Text = ""
        txtTelp3.Text = ""
        txtFax.Text = ""
        txtEmail.Text = ""
        chkAktif.Checked = True
        load_dataBatch()
    End Sub

    Private Sub btnSearchSupplier_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("M_Dealer")
        f.setColumns("*")

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtKodeDealer.Text = f.value
        End If
        f.Dispose()
        load_data()
    End Sub

    Private Sub load_data()
        Dim conn2 As New OleDbConnection(strcon)
        conn2.Open()
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        Try
            cmd = New OleDbCommand("select * from M_Dealer where Dealer_Kode = '" & txtKodeDealer.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                txtNamaDealer.Text = reader("Dealer_Name")
                txtBatch.Text = reader("Dealer_batch")
                cmbBatch.Items.Add(reader("Dealer_batch"))
                cmbBatch.Text = reader("Dealer_batch")
                txtKodeMD.Text = reader("Dealer_MDKode")
                txtAlamat.Text = reader("Dealer_Alamat")
                txtKecamatan.Text = IIf(IsDBNull(reader("Dealer_Kec")), "", reader("Dealer_Kec"))
                txtKota.Text = reader("Dealer_Kota")
                txtProvinsi.Text = reader("Dealer_Prov")
                txtKodePos.Text = reader("Dealer_KodePos")
                txtTelp1.Text = reader("Dealer_Tlp1")
                txtTelp2.Text = IIf(IsDBNull(reader("Dealer_Tlp2")), "", reader("Dealer_Tlp2"))
                txtTelp3.Text = IIf(IsDBNull(reader("Dealer_Tlp2")), "", reader("Dealer_Tlp2"))
                txtFax.Text = IIf(IsDBNull(reader("Dealer_Tlp2")), "", reader("Dealer_Tlp2"))
                txtEmail.Text = reader("Dealer_Mail")
                cmbKodeBisnis.Text = reader("dealer_kodebisnis")
                chkAktif.Checked = reader("Dealer_AktifYN")

            End If
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub
    Private Sub load_dataMD()
        Dim conn2 As New OleDbConnection(strcon)
        conn2.Open()
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        Try
            cmd = New OleDbCommand("select * from M_MainDealer where MD_Kode = '" & txtKodeMD.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                lblNamaMainDealer.Text = reader("MD_Name")
            End If
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub
    Private Sub load_dataBatch()
        Dim conn2 As New OleDbConnection(strcon)
        conn2.Open()
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        Try
            cmbBatch.Items.Clear()
            cmd = New OleDbCommand("select distinct dealer_batch from M_Dealer", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                cmbBatch.Items.Add(reader("dealer_batch"))
            End While
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        reset_form()
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        If txtNamaDealer.Text = "" Or txtKodeDealer.Text = "" Then
            MsgBox("Pilih MD yang akan dihapus dulu")
            Exit Sub
        End If

        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing
        Dim conn2 As New OleDbConnection(strcon)
        Try
            conn2.Open()

            cmd = New OleDbCommand("select * from t_h1toh2 where MD_Kode = '" & txtKodeDealer.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                MsgBox("Supplier tidak bisa dihapus karena sudah terpakai")
                reader.Close()
                conn2.Close()
                Exit Sub
            Else
                cmd = New OleDbCommand("delete from M_MainDealer where MD_Kode = '" & txtKodeDealer.Text & "'", conn2)
                cmd.ExecuteNonQuery()
            End If
            reader.Close()
            conn2.Close()
            MsgBox("Data sudah dihapus")
            reset_form()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()

            If conn2.State Then conn2.Close()
        End Try
    End Sub

    Private Sub frmMasterMD_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F2 Then btnSimpan.PerformClick()
        If e.KeyCode = Keys.F5 Then btnSearch.PerformClick()
        If e.Control And e.KeyCode = Keys.R Then btnReset.PerformClick()
        If e.KeyCode = Keys.Escape Then Me.Close()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub


    Private Sub frmMasterMD_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnSimpan_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSimpan.MouseHover
        btnSimpan.Font = New Font(btnSimpan.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnSimpan_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSimpan.MouseLeave
        btnSimpan.Font = New Font(btnSimpan.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub btnReset_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.MouseHover
        btnReset.Font = New Font(btnReset.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnReset_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.MouseLeave
        btnReset.Font = New Font(btnReset.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub btnHapus_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHapus.MouseHover
        btnHapus.Font = New Font(btnHapus.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnHapus_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHapus.MouseLeave
        btnHapus.Font = New Font(btnHapus.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub btnKeluar_Click(sender As Object, e As EventArgs) Handles btnKeluar.Click
        Me.Close()
    End Sub

    Private Sub txtKodeDealer_Leave(sender As Object, e As System.EventArgs) Handles txtKodeDealer.Leave
        load_data()
    End Sub
    Private Sub txtKodeMainDealer_Leave(sender As Object, e As System.EventArgs) Handles txtKodeMD.Leave
        load_dataMD()
    End Sub

    Private Sub frmMasterDealer_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        load_dataBatch()
    End Sub

    Private Sub cmbBatch_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbBatch.SelectedIndexChanged

    End Sub
End Class
