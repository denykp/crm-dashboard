﻿Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class frmMasterMD
    Dim transaction As OleDbTransaction
    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        If txtKodeMD.Text = "" Or txtNamaMD.Text = "" Then
            MsgBox("Semua data bertanda * harus diisi")
            Exit Sub
        End If
        Dim conn2 As New OleDbConnection(strcon)

        conn2.Open()
        transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)

        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        Try
            cmd = New OleDbCommand("select * from M_MainDealer where MD_Kode = '" & txtKodeMD.Text & "'", conn2, transaction)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                cmd = New OleDbCommand(update_dataheader(), conn2, transaction)
                cmd.ExecuteNonQuery()
            Else
                cmd = New OleDbCommand(add_dataheader(), conn2, transaction)
                cmd.ExecuteNonQuery()
            End If
            reader.Close()

            
            transaction.Commit()
            conn2.Close()
            MsgBox("Data sudah tersimpan")
            reset_form()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()
            transaction.Rollback()
            If conn2.State Then conn2.Close()
        End Try
    End Sub

    Private Function add_dataheader() As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(6)
        ReDim nilai(6)

        table_name = "M_MainDealer"
        fields(0) = "MD_Kode"
        fields(1) = "MD_Name"
        fields(2) = "MD_AktifYN"
        fields(3) = "MD_DateCreate"
        fields(4) = "MD_DateEdit"
        fields(5) = "MD_Username"

        nilai(0) = txtKodeMD.Text
        nilai(1) = txtNamaMD.Text
        nilai(2) = IIf(chkAktif.Checked, "1", "0")
        nilai(3) = Format(Now, "yyyy/MM/dd HH:mm:ss")
        nilai(4) = Format(Now, "yyyy/MM/dd HH:mm:ss")
        nilai(5) = user

        add_dataheader = tambah_data2(table_name, fields, nilai)
    End Function
    Private Function update_dataheader() As String
        Dim fields() As String
        Dim nilai() As String
        Dim fieldsPK() As String
        Dim nilaiPK() As String
        Dim table_name As String

        ReDim fields(5)
        ReDim nilai(5)
        ReDim fieldsPK(1)
        ReDim nilaiPK(1)

        table_name = "M_MainDealer"
        fields(0) = "MD_Kode"
        fields(1) = "MD_Name"
        fields(2) = "MD_AktifYN"

        fields(3) = "MD_DateEdit"
        fields(4) = "MD_Username"

        nilai(0) = txtKodeMD.Text
        nilai(1) = txtNamaMD.Text
        nilai(2) = IIf(chkAktif.Checked, "1", "0")

        nilai(3) = Format(Now, "yyyy/MM/dd HH:mm:ss")
        nilai(4) = user
        fieldsPK(0) = fields(0)
        nilaiPK(0) = nilai(0)
        update_dataheader = update_data2(table_name, fields, nilai, fieldsPK, nilaiPK)
    End Function
    Private Sub reset_form()
        txtKodeMD.Text = ""
        txtNamaMD.Text = ""
        chkAktif.Checked = True
    End Sub

    Private Sub btnSearchSupplier_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchMD.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("M_MainDealer")
        f.setColumns("*")

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtKodeMD.Text = f.value
        End If
        f.Dispose()
        load_data()
    End Sub

    Private Sub load_data()
        Dim conn2 As New OleDbConnection(strcon)
        conn2.Open()
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        Try
            cmd = New OleDbCommand("select * from M_MainDealer where MD_Kode = '" & txtKodeMD.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                txtNamaMD.Text = reader("MD_Name")
                chkAktif.Checked = reader("MD_AktifYN")

            End If
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        reset_form()
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        If txtNamaMD.Text = "" Or txtKodeMD.Text = "" Then
            MsgBox("Pilih MD yang akan dihapus dulu")
            Exit Sub
        End If

        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing
        Dim conn2 As New OleDbConnection(strcon)
        Try
            conn2.Open()

            cmd = New OleDbCommand("select * from t_h1toh2 where MD_Kode = '" & txtKodeMD.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                MsgBox("Supplier tidak bisa dihapus karena sudah terpakai")
                reader.Close()
                conn2.Close()
                Exit Sub
            Else
                cmd = New OleDbCommand("delete from M_MainDealer where MD_Kode = '" & txtKodeMD.Text & "'", conn2)
                cmd.ExecuteNonQuery()
            End If
            reader.Close()
            conn2.Close()
            MsgBox("Data sudah dihapus")
            reset_form()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()

            If conn2.State Then conn2.Close()
        End Try
    End Sub

    Private Sub frmMasterMD_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F2 Then btnSimpan.PerformClick()
        If e.KeyCode = Keys.F5 Then btnSearchMD.PerformClick()
        If e.Control And e.KeyCode = Keys.R Then btnReset.PerformClick()
        If e.KeyCode = Keys.Escape Then Me.Close()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub


    Private Sub frmMasterMD_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnSimpan_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSimpan.MouseHover
        btnSimpan.Font = New Font(btnSimpan.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnSimpan_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSimpan.MouseLeave
        btnSimpan.Font = New Font(btnSimpan.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub btnReset_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.MouseHover
        btnReset.Font = New Font(btnReset.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnReset_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.MouseLeave
        btnReset.Font = New Font(btnReset.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub btnHapus_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHapus.MouseHover
        btnHapus.Font = New Font(btnHapus.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnHapus_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHapus.MouseLeave
        btnHapus.Font = New Font(btnHapus.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub btnKeluar_Click(sender As Object, e As EventArgs) Handles btnKeluar.Click
        Me.Close()
    End Sub

    Private Sub txtKodeMD_Leave(sender As Object, e As System.EventArgs) Handles txtKodeMD.Leave
        load_data()
    End Sub

End Class
