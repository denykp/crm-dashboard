﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.IO

Public Class frmMasterBarang
    Dim transaction As SqlTransaction
    Dim hpp As Double
    Dim stock As Double
    Dim kode_gudang As String
    Dim kodebarang As String = ""
    Private Sub reset_form()
        loadComboBoxAll(cmbKategoriGeneral, "nama, kode_kategori", "ms_kategori", "where '' = ''", "nama", "kode_kategori")
        loadComboBoxAll(cmbTipe, "nama_tipe, kode_tipe", "ms_tipe", "where '' = ''", "nama_tipe", "kode_tipe")
        loadComboBoxAll(cmbSatuanGeneral, "satuan", "var_satuan", "where '' = ''", "satuan", "satuan")
        loadComboBoxAll(cmbDapur, "dapur", "ms_dapur", "where '' = '' ", "dapur", "dapur")

        loadComboBoxAll(cmbPrinter1, "*", "vw_printer", "where '' = '' ", "nama", "kode")
        loadComboBoxAll(cmbPrinter2, "*", "vw_printer", "where '' = '' ", "nama", "kode")
        cek_tipe()
        txtKodeBarangGeneral.Text = ""
        txtNamaBarangGeneral.Text = ""
        txtHargaGeneral.Text = "0"
        txtQtyHasil.Text = "0"
        txtMinQty.Text = "0"
        txtAddCost1.Text = "0"
        txtAddCost2.Text = "0"
        txtAddCost3.Text = "0"
        txtLostFactor.Text = "0"
        txtPriceList.Text = "0"
        lbltotal_bom.Text = 0
        lblTotal.Text = 0
        dgvResep.Rows.Clear()
        dgvSatuan.Rows.Clear()
        dgvTambahan.Rows.Clear()
        chkAktifGeneral.Checked = True
        hpp = 0
        stock = 0
        kode_gudang = ""
        txtFoto.Text = ""
        PictureBox1.BackgroundImage = Nothing
    End Sub

    Private Sub frmMasterBarang_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reset_form()
    End Sub

    Private Sub btnSearchBarangGeneral_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchBarangGeneral.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("ms_barang")
        f.setColumns("kode_barang, nama, kode_kategori,kode_tipe, harga, active_fg, satuan, hpp, min_qty, dapur, printer1, printer2")

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtKodeBarangGeneral.Text = f.value
            load_dataBarang()
        End If
        f.Dispose()

    End Sub

    Private Sub load_bom()
        Dim conn2 As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim da As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim dt As DataTable

        Try
            conn2.Open()
            cmd = New SqlCommand("select m.kode_bahan,b.nama,m.qty,B.satuan,(select isnull(max(hpp),b.harga) from hpp where kode_barang=m.kode_bahan) from ms_barang_bom m left join ms_barang b on m.kode_bahan=b.kode_barang where m.kode_barang = '" & txtKodeBarangGeneral.Text & "' order by no_urut asc", conn2)
            da.SelectCommand = cmd
            da.Fill(ds, "ms_barang_bom")
            dt = ds.Tables("ms_barang_bom")
            For i As Integer = 0 To dt.Rows.Count - 1
                dgvBom.Rows.Add(New String() {dt.Rows(i).Item(0), dt.Rows(i).Item(1), dt.Rows(i).Item(2), dt.Rows(i).Item(3), dt.Rows(i).Item(4)})
            Next
            conn2.Close()
            hitung_totalbom()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
        validasi_resep()
    End Sub
    Private Sub hitung_totalbom()
        Try
            Dim total As Double = 0
            For row As Integer = 0 To dgvBom.RowCount - 2
                total = total + (dgvBom.Item("hpp_bom", row).Value * dgvBom.Item("qty_bom", row).Value)
            Next
            lbltotal_bom.Text = FormatNumber(total, 2)
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub
    Private Sub load_barang_resep()
        Dim conn2 As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim da As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim dt As DataTable
        Dim hpp As Double
        Try
            conn2.Open()
            cmd = New SqlCommand("select m.kode_bahan,b.nama,m.qty,B.satuan,harga,stock,b.lost_factor,b.price_list from ms_barang_resep m left join ms_barang b on m.kode_bahan=b.kode_barang where m.kode_barang = '" & txtKodeBarangGeneral.Text & "' order by no_urut asc", conn2)
            da.SelectCommand = cmd
            da.Fill(ds, "ms_barang_resep")
            dt = ds.Tables("ms_barang_resep")

            For i As Integer = 0 To dt.Rows.Count - 1
                hpp = 0
                
                dgvResep.Rows.Add(New String() {dt.Rows(i).Item(0), dt.Rows(i).Item(1), dt.Rows(i).Item(2), dt.Rows(i).Item(3), dt.Rows(i).Item(5), hpp, dt.Rows(i).Item(6), 0})
            Next
            conn2.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
        validasi_resep()
        hitung_totalbahan()
    End Sub

    Private Sub load_barang_satuan()
        Dim conn2 As New SqlConnection(strcon)
        Try
            conn2.Open()
            Dim cmd As SqlCommand
            Dim da As New SqlDataAdapter()
            Dim ds As New DataSet()
            Dim dt As DataTable
            cmd = New SqlCommand("select * from ms_barang_satuan where kode_barang = '" & txtKodeBarangGeneral.Text & "'", conn2)
            da.SelectCommand = cmd
            da.Fill(ds, "ms_barang_satuan")
            dt = ds.Tables("ms_barang_satuan")
            For i As Integer = 0 To dt.Rows.Count - 1
                dgvSatuan.Rows.Add(New String() {dt.Rows(i).Item(1), dt.Rows(i).Item(2)})
            Next
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

    Private Sub load_barang_tambahan()
        Dim conn2 As New SqlConnection(strcon)
        Try
            conn2.Open()
            Dim cmd As SqlCommand
            Dim cmdsatuan As SqlCommand
            Dim dr As SqlDataReader
            Dim da As New SqlDataAdapter()
            Dim ds As New DataSet()
            Dim dt As DataTable
            Dim dgc As DataGridViewComboBoxCell
            cmd = New SqlCommand("select * from ms_barang_tambahan where kode_barang = '" & txtKodeBarangGeneral.Text & "'", conn2)
            da.SelectCommand = cmd
            da.Fill(ds, "ms_barang_tambahan")
            dt = ds.Tables("ms_barang_tambahan")
            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    dgvTambahan.Rows.Add(New String() {dt.Rows(i).Item(1), "", dt.Rows(i).Item(2), dt.Rows(i).Item(3), dt.Rows(i).Item(4)})
                    cmdsatuan = New SqlCommand("select * from ms_barang_satuan where kode_barang = '" & dt.Rows(i).Item(1) & "'", conn2)
                    dr = cmdsatuan.ExecuteReader
                    dgc = dgvTambahan.Rows(i).Cells(3)
                    dgc.Items.Clear()
                    While dr.Read
                        dgc.Items.Add(dr.Item("satuan"))
                    End While
                    dr.Close()
                Next
            End If
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
        validasi_tambahan()
    End Sub

    Private Sub load_stock()
        Dim conn2 As New SqlConnection(strcon)
        Try

            conn2.Open()
            Dim cmd As SqlCommand
            Dim reader As SqlDataReader = Nothing
            cmd = New SqlCommand("select * from stock where kode_barang = '" & txtKodeBarangGeneral.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                stock = CDbl(reader("stock"))
                kode_gudang = reader("kode_gudang")
            End While
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try

    End Sub

    Private Sub load_hpp()
        Dim conn2 As New SqlConnection(strcon)
        Try
            conn2.Open()
            Dim cmd As SqlCommand
            Dim reader As SqlDataReader = Nothing
            cmd = New SqlCommand("select * from hpp where kode_barang = '" & txtKodeBarangGeneral.Text & "' and batch=''", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                hpp = CDbl(reader("hpp"))
            End While
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try

    End Sub

    Private Sub load_dataBarang()
        Dim conn2 As New SqlConnection(strcon)
        Try

            conn2.Open()
            Dim cmd As SqlCommand
            Dim reader As SqlDataReader = Nothing
            cmd = New SqlCommand("select * from ms_barang where kode_barang = '" & txtKodeBarangGeneral.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                txtNamaBarangGeneral.Text = reader("nama")
                cmbKategoriGeneral.SelectedValue = reader("kode_kategori")
                cmbTipe.SelectedValue = reader("kode_tipe")
                txtHargaGeneral.Text = reader("harga")
                txtMinQty.Text = reader("min_qty")
                txtLostFactor.Text = reader("lost_factor")
                txtPriceList.Text = reader("price_list")
                txtAddCost1.Text = reader("add_cost1")
                txtAddCost2.Text = reader("add_cost2")
                txtAddCost3.Text = reader("add_cost3")
                txtQtyHasil.Text = reader("qty_hasilproduksi")
                hpp = reader("hpp")
                cmbSatuanGeneral.SelectedValue = reader("satuan")
                cmbDapur.SelectedValue = reader("dapur")
                cmbPrinter1.SelectedValue = reader("printer1")
                cmbPrinter2.SelectedValue = reader("printer2")
                cmd = New SqlCommand("select photo from ms_barang where kode_barang='" & txtKodeBarangGeneral.Text & "'", conn2)
                Try
                    Dim imageData As Byte() = DirectCast(cmd.ExecuteScalar(), Byte())
                    If Not imageData Is Nothing Then
                        Using ms As New MemoryStream(imageData, 0, imageData.Length)
                            ms.Write(imageData, 0, imageData.Length)
                            PictureBox1.BackgroundImage = Image.FromStream(ms, True)
                        End Using
                    End If
                Catch e As Exception
                End Try
            End While
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try

        dgvResep.Rows.Clear()
        load_barang_resep()
        dgvSatuan.Rows.Clear()
        load_barang_satuan()
        dgvTambahan.Rows.Clear()
        dgvBom.Rows.Clear()
        load_barang_tambahan()
        load_hpp()
        load_stock()
        load_bom()
    End Sub

#Region "RESEP"
    Private Sub dgvResep_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles dgvResep.KeyDown
        If e.KeyCode = Keys.Delete Then
            Dim index As Integer
            index = dgvResep.CurrentRow.Index
            dgvResep.Rows.RemoveAt(index)
        End If
        If e.KeyCode = Keys.F3 Then
            Dim f As New frmSearch
            f.setCol(0)
            f.setTableName("vw_inventory")
            f.setColumns("*")

            If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                dgvResep.Item(0, dgvResep.CurrentRow.Index).Value = f.value
                validasi_resep()
                hitung_totalbahan()
            End If
            f.Dispose()
            If f.value <> "" Then
            End If
        End If
    End Sub

    Private Sub btnResep_Click(ByVal sender As Object, ByVal e As EventArgs)
        dgvResep.Focus()

    End Sub

    Private Sub dgvResep_CellEndEdit(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles dgvResep.CellEndEdit
        If e.ColumnIndex = 0 Then cek_barangresep()
        If IsNumeric(dgvResep.Item(dgvResep.Columns("Qty").Index, e.RowIndex).Value) = False Then
            dgvResep.Item(dgvResep.Columns("Qty").Index, e.RowIndex).Value = 0
        End If
        If e.ColumnIndex = 2 Or e.ColumnIndex = 4 Then hitung_totalbahan()
    End Sub

    Private Sub cek_barangresep()
        Dim r As DataGridViewRow
        Dim conn2 As New SqlConnection(strcon)
        Dim kodebarang As String
        r = dgvResep.CurrentRow
        kodebarang = r.Cells(0).Value

        conn2.Open()
        Dim cmd As SqlCommand
        Dim reader As SqlDataReader = Nothing
        Try
            cmd = New SqlCommand("select nama,satuan,lost_factor,(case when max(ms_barang.hpp)=0 then max(price_list) else max(ms_barang.hpp) end ) as hpp from ms_barang left join hpp on ms_barang.kode_barang=hpp.kode_barang where ms_barang.kode_barang = '" & kodebarang & "' group by nama,satuan,lost_factor", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                r.Cells(1).Value = reader("nama")
                r.Cells(3).Value = reader("satuan")
                r.Cells("LF").Value = reader("lost_factor")
                r.Cells("colhpp").Value = reader("hpp")
            End If
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

#End Region

#Region "Produksi"
    Private Sub dgBom_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvBom.KeyDown
        If e.KeyCode = Keys.Delete Then
            Dim index As Integer
            index = dgvResep.CurrentRow.Index
            dgvBom.Rows.RemoveAt(index)
        End If
        If e.KeyCode = Keys.F3 Then
            Dim f As New frmSearch
            f.setCol(0)
            f.setTableName("vw_inventory")
            f.setColumns("*")

            If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                dgvBom.Item(0, dgvBom.CurrentRow.Index).Value = f.value
                'load_bom()
                cek_bom()
            End If
            f.Dispose()


            If f.value <> "" Then
            End If
        End If
    End Sub
    Private Sub dgBom_CellEndEdit(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles dgvBom.CellEndEdit
        If e.ColumnIndex = 0 Then cek_bom()
        If IsNumeric(dgvBom.Item(dgvBom.Columns("qty_bom").Index, e.RowIndex).Value) = False Then
            dgvBom.Item(dgvBom.Columns("qty_bom").Index, e.RowIndex).Value = 0
        End If
        If e.ColumnIndex = 2 Or e.ColumnIndex = 4 Then hitung_totalbom()
    End Sub
    Private Sub cek_bom()
        Dim r As DataGridViewRow
        Dim conn2 As New SqlConnection(strcon)
        Dim kodebarang As String
        r = dgvBom.CurrentRow
        kodebarang = r.Cells(0).Value

        conn2.Open()
        Dim cmd As SqlCommand
        Dim reader As SqlDataReader = Nothing
        Try
            cmd = New SqlCommand("select * from ms_barang where kode_barang = '" & kodebarang & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                r.Cells(1).Value = reader("nama")
                r.Cells(3).Value = reader("satuan")
                r.Cells(4).Value = reader("hpp")
            End If
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try


    End Sub
#End Region
#Region "SATUAN"
    Private Sub dgvSatuan_CellEndEdit(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles dgvSatuan.CellEndEdit
        If IsNumeric(dgvSatuan.Item(dgvSatuan.Columns("satuanQty").Index, e.RowIndex).Value) = False Then
            dgvSatuan.Item(dgvSatuan.Columns("satuanQty").Index, e.RowIndex).Value = 0
        End If
    End Sub

    Private Sub dgvSatuan_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles dgvSatuan.KeyDown
        If e.KeyCode = Keys.Delete Then
            Dim index As Integer
            index = dgvSatuan.CurrentRow.Index
            dgvSatuan.Rows.RemoveAt(index)
        End If
    End Sub
#End Region

#Region "TAMBAHAN"
    Private Function cek_barangTambahan() As Boolean
        Dim conn As New SqlConnection(strcon)
        Dim kodebarang As String
        cek_barangTambahan = False
        kodebarang = dgvTambahan.Item(0, dgvTambahan.CurrentRow.Index).Value
        If kodebarang <> "" Then
            Dim ada As Boolean
            Dim baris As Double
            For row As Integer = 0 To dgvTambahan.RowCount - 2
                If kodebarang = dgvTambahan.Item(0, row).Value And row <> dgvTambahan.CurrentRow.Index Then
                    ada = True
                    baris = row + 1
                End If
            Next

            If ada = True Then
                MsgBox("Barang sudah ada pada baris ke " & baris)
                Exit Function
            End If
        End If

        Try
            With dgvTambahan.Rows(dgvTambahan.CurrentRow.Index)
                conn.Open()
                Dim cmd As SqlCommand
                Dim reader As SqlDataReader = Nothing
                Dim dr As SqlDataReader
                cmd = New SqlCommand("select * from ms_barang where kode_barang = '" & kodebarang & "'", conn)
                reader = cmd.ExecuteReader()
                If reader.Read() Then
                    cek_barangTambahan = True
                    .Cells(1).Value = reader("nama")
                    If Not IsNumeric(.Cells(2).Value) Then .Cells(2).Value = 0
                    If Not IsNumeric(.Cells(4).Value) Then .Cells(4).Value = 1
                    Dim dgc As DataGridViewComboBoxCell
                    cmd = New SqlCommand("select * from ms_barang_satuan where kode_barang = '" & kodebarang & "'", conn)
                    dr = cmd.ExecuteReader
                    dgc = .Cells(3)
                    dgc.Items.Clear()
                    dgc.Items.Add(reader.Item("satuan"))
                    While dr.Read
                        dgc.Items.Add(dr.Item("satuan"))
                    End While
                    dr.Close()
                Else
                    .Cells(1).Value = ""
                End If
                reader.Close()
                conn.Close()
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try


    End Function

    Private Sub dgvTambahan_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvTambahan.CellValidating
        'If e.ColumnIndex = 0 Then
        '    If Not cek_barangTambahan() And e.FormattedValue <> "" Then e.Cancel = True
        'End If
    End Sub

    Private Sub dgvTambahan_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles dgvTambahan.KeyDown
        If e.KeyCode = Keys.Delete Then
            Dim index As Integer
            index = dgvTambahan.CurrentRow.Index
            dgvTambahan.Rows.RemoveAt(index)
        End If
        If e.KeyCode = Keys.F3 Then
            Dim f As New frmSearch
            f.setCol(0)
            f.setTableName("vw_tambahan")
            f.setColumns("*")

            If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                dgvTambahan.Item(0, dgvTambahan.CurrentRow.Index).Value = f.value
                load_barang_tambahan()
            End If
            f.Dispose()


            If f.value <> "" Then
            End If
        End If
    End Sub

    Private Sub btnClearTambahan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClearTambahan.Click
        dgvTambahan.Rows.Clear()
    End Sub

    Private Sub dgvTambahan_CellEndEdit(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles dgvTambahan.CellEndEdit
        If e.ColumnIndex = 0 Then cek_barangTambahan()
        If IsNumeric(dgvTambahan.Item(dgvTambahan.Columns("tambahIsi").Index, e.RowIndex).Value) = False Then
            dgvTambahan.Item(dgvTambahan.Columns("tambahIsi").Index, e.RowIndex).Value = 1
        ElseIf IsNumeric(dgvTambahan.Item(dgvTambahan.Columns("tambahQty").Index, e.RowIndex).Value) = False Then
            dgvTambahan.Item(dgvTambahan.Columns("tambahQty").Index, e.RowIndex).Value = 1
        End If
    End Sub

#End Region

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        If txtKodeBarangGeneral.Text = "" Or txtNamaBarangGeneral.Text = "" Or cmbKategoriGeneral.Text = "" Then
            MsgBox("Semua data bertanda * harus diisi")
            Exit Sub
        End If
        Dim conn2 As New SqlConnection(strcon)
        Try

            conn2.Open()
            transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)

            Dim cmd As SqlCommand
            Dim reader As SqlDataReader = Nothing
            cmd = New SqlCommand("select * from ms_barang where kode_barang = '" & txtKodeBarangGeneral.Text & "'", conn2, transaction)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                cmd = New SqlCommand(add_dataheaderBarangGeneral("update"), conn2, transaction)
                cmd.ExecuteNonQuery()
            Else
                cmd = New SqlCommand(add_dataheaderBarangGeneral("insert"), conn2, transaction)
                cmd.ExecuteNonQuery()
            End If
            reader.Close()

            If PictureBox1.BackgroundImage Is Nothing Then
            Else
                Dim adapter As New SqlDataAdapter
                Dim sql As String = "update ms_barang set photo = @photo where kode_barang = '" & txtKodeBarangGeneral.Text & "'"
                Dim ms As New MemoryStream()
                '     PictureBox1.BackgroundImage.Save(ms, PictureBox1.BackgroundImage.RawFormat)
                Using bmp As New Bitmap(Me.PictureBox1.BackgroundImage)
                    bmp.Save(ms, PictureBox1.BackgroundImage.RawFormat)
                End Using
                'PictureBox1.BackgroundImage.Save("photo")
                Dim data As Byte() = ms.GetBuffer()
                Dim cmd1 As New SqlCommand
                cmd1 = New SqlCommand(sql, conn2, transaction)

                Dim p As New SqlParameter("@photo", SqlDbType.Image)
                p.Value = data
                cmd1.Parameters.Add(p)
                adapter.InsertCommand = cmd1
                adapter.InsertCommand.ExecuteNonQuery()
                ms.Close()
            End If

            ' insert table var_satuan >. hapus nilai yang sama kemudian entri baru
            cmd = New SqlCommand("select * from var_satuan where satuan = '" & cmbSatuanGeneral.Text & "'", conn2, transaction)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                cmd = New SqlCommand("delete from var_satuan where satuan = '" & cmbSatuanGeneral.Text & "'", conn2, transaction)
                cmd.ExecuteNonQuery()
            End If
            reader.Close()
            cmd = New SqlCommand("insert into var_satuan values ('" & cmbSatuanGeneral.Text & "')", conn2, transaction)
            cmd.ExecuteNonQuery()

            ' insert table barang resep
            If dgvResep.Rows.Count > 0 Then
                cmd = New SqlCommand("delete from ms_barang_resep where kode_barang = '" & txtKodeBarangGeneral.Text & "'", conn2, transaction)
                cmd.ExecuteNonQuery()
                For Each r As DataGridViewRow In dgvResep.Rows
                    If r.Cells(0).Value <> "" Then
                        cmd = New SqlCommand(add_dataheaderResep(r.Index), conn2, transaction)
                        cmd.ExecuteNonQuery()
                    End If
                Next
            End If
            ' insert table bahan BOM
            If dgvBom.Rows.Count > 0 Then
                cmd = New SqlCommand("delete from ms_barang_bom where kode_barang = '" & txtKodeBarangGeneral.Text & "'", conn2, transaction)
                cmd.ExecuteNonQuery()
                For Each r As DataGridViewRow In dgvBom.Rows
                    If r.Cells(0).Value <> "" Then
                        cmd = New SqlCommand(add_BOM(r.Index), conn2, transaction)
                        cmd.ExecuteNonQuery()
                    End If
                Next
            End If
            ' inser table barang satuan
            If dgvSatuan.Rows.Count > 0 Then
                cmd = New SqlCommand("delete from ms_barang_satuan where kode_barang = '" & txtKodeBarangGeneral.Text & "'", conn2, transaction)
                cmd.ExecuteNonQuery()
                For Each r As DataGridViewRow In dgvSatuan.Rows
                    If r.Cells(0).Value <> "" Then
                        cmd = New SqlCommand(add_dataheaderSatuan(r.Index), conn2, transaction)
                        cmd.ExecuteNonQuery()
                    End If
                Next
            End If

            ' insert table barang tambahan
            If dgvTambahan.Rows.Count > 0 Then
                cmd = New SqlCommand("delete from ms_barang_tambahan where kode_barang = '" & txtKodeBarangGeneral.Text & "'", conn2, transaction)
                cmd.ExecuteNonQuery()
                For Each r As DataGridViewRow In dgvTambahan.Rows
                    If r.Cells(0).Value <> "" Then
                        cmd = New SqlCommand(add_dataheaderTambahan(r.Index), conn2, transaction)
                        cmd.ExecuteNonQuery()
                    End If
                Next
            End If

            ' pengecekan apakah kode_barang tersebut sudah tersedia di table stock atau belum.
            Dim readerstock As SqlDataReader = Nothing
            cmd = New SqlCommand("select * from stock where kode_barang = '" & txtKodeBarangGeneral.Text & "'", conn2, transaction)
            readerstock = cmd.ExecuteReader()
            If Not readerstock.HasRows Then
                cmd = New SqlCommand("insert into stock select '" & txtKodeBarangGeneral.Text & "', 0, kode_gudang,'' from ms_gudang", conn2, transaction)
                cmd.ExecuteNonQuery()
            End If
            readerstock.Close()

            transaction.Commit()
            conn2.Close()
            If MsgBox("Data sudah tersimpan, input data baru?", vbYesNo) = vbYes Then btnReset.PerformClick()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()
            transaction.Rollback()
            If conn2.State Then conn2.Close()
        End Try
    End Sub
    Private Function add_dataheaderBarangGeneral(ByVal jenis As String) As String
        add_dataheaderBarangGeneral = ""
        Dim fields() As String
        Dim nilai() As String

        Dim table_name As String

        ReDim fields(18)
        ReDim nilai(18)
        Dim fieldPK() As String
        Dim nilaiPK() As String
        ReDim fieldPK(1)
        ReDim nilaiPK(1)

        table_name = "ms_barang"
        fields(0) = "kode_barang"
        fields(1) = "nama"
        fields(2) = "kode_kategori"
        fields(3) = "harga"
        fields(4) = "active_fg"
        fields(5) = "satuan"
        fields(6) = "hpp"
        fields(7) = "dapur"
        fields(8) = "printer1"
        fields(9) = "printer2"
        fields(10) = "kode_tipe"
        fields(11) = "min_qty"
        fields(12) = "add_cost1"
        fields(13) = "add_cost2"
        fields(14) = "add_cost3"
        fields(15) = "lost_factor"
        fields(16) = "price_list"
        fields(17) = "qty_hasilproduksi"

        nilai(0) = txtKodeBarangGeneral.Text
        nilai(1) = txtNamaBarangGeneral.Text
        nilai(2) = cmbKategoriGeneral.SelectedValue
        nilai(3) = CDbl(txtHargaGeneral.Text)
        If chkAktifGeneral.Checked = True Then
            nilai(4) = "1"
        Else
            nilai(4) = "0"
        End If
        nilai(5) = cmbSatuanGeneral.Text
        If Tab1.TabPages(0).Text <> "Resep" Then
            nilai(6) = hpp
        Else
            nilai(6) = CDec(lblTotal.Text)
        End If
        nilai(7) = cmbDapur.SelectedValue
        nilai(8) = cmbPrinter1.SelectedValue
        nilai(9) = cmbPrinter2.SelectedValue
        nilai(10) = cmbTipe.SelectedValue
        nilai(11) = CDec(txtMinQty.Text)
        nilai(12) = CDec(txtAddCost1.Text)
        nilai(13) = CDec(txtAddCost2.Text)
        nilai(14) = CDec(txtAddCost3.Text)
        nilai(15) = CDec(txtLostFactor.Text)
        nilai(16) = CDec(txtPriceList.Text)
        nilai(17) = CDec(txtQtyHasil.Text)

        fieldPK(0) = "kode_barang"
        nilaiPK(0) = txtKodeBarangGeneral.Text

        If jenis = "insert" Then
            add_dataheaderBarangGeneral = tambah_data2(table_name, fields, nilai)
        ElseIf jenis = "update" Then
            add_dataheaderBarangGeneral = update_data2(table_name, fields, nilai, fieldPK, nilaiPK)
        End If
    End Function
    Private Function add_dataheaderResep(ByVal row As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(6)
        ReDim nilai(6)

        table_name = "ms_barang_resep"
        fields(0) = "kode_barang"
        fields(1) = "kode_bahan"
        fields(2) = "qty"
        fields(3) = "satuan"
        fields(4) = "no_urut"
        fields(5) = "stock"

        nilai(0) = txtKodeBarangGeneral.Text
        nilai(1) = dgvResep.Item(0, row).Value
        nilai(2) = CDbl(dgvResep.Item(2, row).Value)
        nilai(3) = dgvResep.Item(3, row).Value
        nilai(4) = row + 1
        nilai(5) = dgvResep.Item("potong_stock", row).Value

        add_dataheaderResep = tambah_data2(table_name, fields, nilai)
    End Function
    Private Function add_BOM(ByVal row As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(5)
        ReDim nilai(5)

        table_name = "ms_barang_bom"
        fields(0) = "kode_barang"
        fields(1) = "kode_bahan"
        fields(2) = "qty"
        fields(3) = "satuan"
        fields(4) = "no_urut"

        nilai(0) = txtKodeBarangGeneral.Text
        nilai(1) = dgvBom.Item(0, row).Value
        nilai(2) = CDbl(dgvBom.Item(2, row).Value)
        nilai(3) = dgvBom.Item(3, row).Value
        nilai(4) = row + 1

        add_BOM = tambah_data2(table_name, fields, nilai)
    End Function
    Private Function add_dataheaderSatuan(ByVal row As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(3)
        ReDim nilai(3)

        table_name = "ms_barang_satuan"
        fields(0) = "kode_barang"
        fields(1) = "satuan"
        fields(2) = "qty"

        nilai(0) = txtKodeBarangGeneral.Text
        nilai(1) = dgvSatuan.Item(0, row).Value
        nilai(2) = CDbl(dgvSatuan.Item(1, row).Value)
        add_dataheaderSatuan = tambah_data2(table_name, fields, nilai)
    End Function

    Private Function add_dataheaderTambahan(ByVal row As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(6)
        ReDim nilai(6)

        table_name = "ms_barang_tambahan"
        fields(0) = "kode_barang"
        fields(1) = "kode_tambahan"
        fields(2) = "qty"
        fields(3) = "satuan"
        fields(4) = "isi"
        fields(5) = "hpp"

        nilai(0) = txtKodeBarangGeneral.Text
        nilai(1) = dgvTambahan.Item(0, row).Value
        nilai(2) = CDbl(dgvTambahan.Item(2, row).Value)
        nilai(3) = dgvTambahan.Item(3, row).Value
        nilai(4) = CDbl(dgvTambahan.Item(4, row).Value)
        nilai(5) = 0
        add_dataheaderTambahan = tambah_data2(table_name, fields, nilai)
    End Function

    Private Sub validasi_resep()
        Dim conn2 As New SqlConnection(strcon)
        Dim hpp As Decimal
        For Each r As DataGridViewRow In dgvResep.Rows
            Try
                conn2.Open()
                Dim cmd As SqlCommand
                Dim reader As SqlDataReader = Nothing
                cmd = New SqlCommand("select * from ms_barang where kode_barang = '" & r.Cells(0).Value & "'", conn2)
                reader = cmd.ExecuteReader()
                If reader.Read() Then
                    r.Cells(1).Value = reader("nama")
                    r.Cells(3).Value = reader("satuan")
                    hpp = getMaxHpp(r.Cells(0).Value)
                    If hpp = 0 Then hpp = reader("price_list")
                    r.Cells(5).Value = hpp
                    r.Cells(7).Value = r.Cells(2).Value * hpp * (1 / ((100 - r.Cells(6).Value) / 100))
                End If
                reader.Close()
                conn2.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message.ToString)
            End Try
        Next
    End Sub

    Private Sub validasi_tambahan()
        Dim conn2 As New SqlConnection(strcon)
        For Each r As DataGridViewRow In dgvTambahan.Rows
            Try
                conn2.Open()
                Dim cmd As SqlCommand
                Dim reader As SqlDataReader = Nothing
                cmd = New SqlCommand("select * from ms_barang where kode_barang = '" & r.Cells(0).Value & "'", conn2)
                reader = cmd.ExecuteReader()
                If reader.Read() Then
                    r.Cells(1).Value = reader("nama")
                End If
                reader.Close()
                conn2.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message.ToString)
            End Try
        Next
    End Sub

    Private Sub btnKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnKeluar.Click
        Me.Close()
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBrowse.Click
        OpenFileDialog1.Filter = "Image File (*.jpg;*.bmp;*.gif)|*.jpg;*.bmp;*.gif"
        If OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Dim bmp As New Bitmap(OpenFileDialog1.FileName)
            'SaveImage(bmp, New Size(50, 50))
            'PictureBox1.BackgroundImage = Image.FromFile(App_Path() & "\temp.jpg")
            PictureBox1.BackgroundImage = Image.FromFile(OpenFileDialog1.FileName)
            txtFoto.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub frmMasterBarang_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F2 Then btnSimpan.PerformClick()
        If e.KeyCode = Keys.F5 Then btnSearchBarangGeneral.PerformClick()
        If e.Control And e.KeyCode = Keys.R Then btnReset.PerformClick()
        If e.KeyCode = Keys.Escape Then Me.Close()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReset.Click
        reset_form()
    End Sub

    Private Sub btnClearSatuan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClearSatuan.Click
        dgvSatuan.Rows.Clear()
    End Sub

    Private Sub btnClearResep_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClearResep.Click
        dgvResep.Rows.Clear()
    End Sub

    Private Sub txtHargaGeneral_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles txtHargaGeneral.KeyPress
        NumericOnly(e)
    End Sub

    Private Sub btnNewKategori_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNewKategori.Click
        frmMasterKategori.ShowDialog()
        loadComboBoxAll(cmbKategoriGeneral, "nama, kode_kategori", "ms_kategori", "where '' = ''", "nama", "kode_kategori")
    End Sub

    Private Sub btnTipe_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnTipe.Click
        frmMasterTipe.ShowDialog()
        loadComboBoxAll(cmbTipe, "kode_tipe, nama_tipe", "ms_tipe", "where '' = ''", "nama_tipe", "kode_tipe")
    End Sub

    Private Sub cmbTipe_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTipe.SelectedValueChanged
        cek_tipe()
    End Sub

    Private Sub cek_tipe()
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader

        Tab1.TabPages.Remove(TabResep)
        Tab1.TabPages.Remove(TabSatuan)
        Tab1.TabPages.Remove(TabProduksi)
        grpPriceList.Visible = False
        conn.Open()
        cmd = New SqlCommand("select * from ms_tipe where kode_tipe='" & cmbTipe.SelectedValue.ToString & "'", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            If dr.Item("resep") = True Then Tab1.TabPages.Insert(0, TabResep)
            If dr.Item("produksi") = True Then Tab1.TabPages.Insert(1, TabProduksi)
            If dr.Item("inventory") = True Then
                Tab1.TabPages.Insert(0, TabSatuan)
                grpPriceList.Visible = True
            End If

        End If
            conn.Close()
    End Sub

    Private Sub btnHapus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnHapus.Click
        Dim conn2 As New SqlConnection(strcon)
        Dim reader As SqlDataReader = Nothing
        Try
            conn2.Open()
            transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)
            cmd = New SqlCommand("select * from ms_barang where kode_barang = '" & txtKodeBarangGeneral.Text & "' ", conn2, transaction)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                cmd = New SqlCommand("delete from ms_barang_resep where kode_barang = '" & reader("kode_barang") & "' ", conn2, transaction)
                cmd.ExecuteNonQuery()

                cmd = New SqlCommand("delete from ms_barang_tambahan where kode_barang = '" & reader("kode_barang") & "' ", conn2, transaction)
                cmd.ExecuteNonQuery()

                cmd = New SqlCommand("delete from ms_barang_satuan where kode_barang = '" & reader("kode_barang") & "' ", conn2, transaction)
                cmd.ExecuteNonQuery()

                cmd = New SqlCommand("delete from ms_barang where kode_barang = '" & reader("kode_barang") & "' ", conn2, transaction)
                cmd.ExecuteNonQuery()
            End If
            reader.Close()

            transaction.Commit()
            MsgBox("Data berhasil dihapus . . .", MsgBoxStyle.Information)
            conn2.Close()
            reset_form()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()
            transaction.Rollback()
            If conn2.State Then conn2.Close()
        End Try
    End Sub

    Private Sub txtKodeBarangGeneral_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKodeBarangGeneral.Leave
        load_dataBarang()
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        conn.Open()
        'load next bill
        cmd = New SqlCommand("select kode_barang from ms_barang where kode_barang>'" & txtKodeBarangGeneral.Text & "' order by kode_barang asc", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            txtKodeBarangGeneral.Text = dr.Item("kode_barang")
            load_dataBarang()
        End If
        conn.Close()
    End Sub

    Private Sub btnPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrev.Click
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        conn.Open()
        'load next bill
        cmd = New SqlCommand("select kode_barang from ms_barang where kode_barang<'" & txtKodeBarangGeneral.Text & "' order by kode_barang desc", conn)
        dr = cmd.ExecuteReader
        If dr.Read Then
            txtKodeBarangGeneral.Text = dr.Item("kode_barang")
            load_dataBarang()
        End If
        conn.Close()
    End Sub

    Private Sub hitung_totalbahan()
        Try
            Dim total As Double = 0
            For row As Integer = 0 To dgvResep.RowCount - 2
                total = total + (dgvResep.Item("colHPP", row).Value * dgvResep.Item("qty", row).Value)
            Next
            lblTotal.Text = FormatNumber(total, 2)
            lblTambah1.Text = pct(total, txtAddCost1.Text)
            lblTambah2.Text = pct(total + lblTambah1.Text, txtAddCost2.Text)
            lblTambah3.Text = pct(total + lblTambah1.Text + lblTambah2.Text, txtAddCost3.Text)
            lblGrandTotal.Text = FormatNumber(add_pct(add_pct(add_pct(lblTotal.Text, txtAddCost1.Text), txtAddCost2.Text), txtAddCost3.Text), 2)
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub
    Private Function add_pct(jumlah As Decimal, pct As Decimal) As Decimal
        add_pct = jumlah * (100 + pct) / 100
    End Function
    Private Function pct(jumlah As Decimal, pctg As Decimal) As Decimal
        pct = jumlah * pctg / 100
    End Function
    Private Sub btnPrintList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintList.Click
        Dim f As New frmReportView
        With f
            .filename = "\List Pricelist.rpt"
            .formula = ""
            .param(1) = ""
            .param(2) = ""
            .param(0) = user
            .Show()
        End With
    End Sub

    Private Sub btnMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMenu.Click
        Dim f As New frmReportView
        With f
            .filename = "\menu.rpt"
            .formula = ""
            .param(1) = ""
            .param(2) = ""
            .param(0) = user
            .Show()
        End With
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        dgvBom.Rows.Clear()
        lbltotal_bom.Text = 0
    End Sub

    Private Sub dgvResep_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvResep.CellContentClick

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim MDIChild As New frmBulk
        MDIChild.ShowDialog()
    End Sub

    Private Sub txtAddCost1_LostFocus(sender As Object, e As System.EventArgs) Handles txtAddCost1.LostFocus, txtAddCost2.LostFocus, txtAddCost3.LostFocus
        hitung_totalbahan()
    End Sub

End Class
