﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMasterBarang
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Tab1 = New System.Windows.Forms.TabControl()
        Me.TabResep = New System.Windows.Forms.TabPage()
        Me.lblTambah3 = New System.Windows.Forms.Label()
        Me.lblTambah2 = New System.Windows.Forms.Label()
        Me.lblTambah1 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtAddCost3 = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtAddCost2 = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.lblGrandTotal = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtAddCost1 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.btnClearResep = New System.Windows.Forms.Button()
        Me.dgvResep = New System.Windows.Forms.DataGridView()
        Me.kode_bahan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nama_bahan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Qty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Satuan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.potong_stock = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colhpp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LF = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Jumlah = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabSatuan = New System.Windows.Forms.TabPage()
        Me.btnClearSatuan = New System.Windows.Forms.Button()
        Me.dgvSatuan = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.satuanQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabTambahan = New System.Windows.Forms.TabPage()
        Me.btnClearTambahan = New System.Windows.Forms.Button()
        Me.dgvTambahan = New System.Windows.Forms.DataGridView()
        Me.DataGridViewButtonColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tambahQty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmbSatuan_tambahan = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.tambahIsi = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabProduksi = New System.Windows.Forms.TabPage()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.lbltotal_bom = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dgvBom = New System.Windows.Forms.DataGridView()
        Me.kodebahan_bom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.namabahan_bom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.qty_bom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.satuan_bom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hpp_bom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chkAktifGeneral = New System.Windows.Forms.CheckBox()
        Me.cmbSatuanGeneral = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtHargaGeneral = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmbKategoriGeneral = New System.Windows.Forms.ComboBox()
        Me.btnSearchBarangGeneral = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNamaBarangGeneral = New System.Windows.Forms.TextBox()
        Me.txtKodeBarangGeneral = New System.Windows.Forms.TextBox()
        Me.btnHapus = New System.Windows.Forms.Button()
        Me.btnKeluar = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnSimpan = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbPrinter2 = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cmbPrinter1 = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txtFoto = New System.Windows.Forms.TextBox()
        Me.cmbDapur = New System.Windows.Forms.ComboBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnNewKategori = New System.Windows.Forms.Button()
        Me.btnTipe = New System.Windows.Forms.Button()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.cmbTipe = New System.Windows.Forms.ComboBox()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnPrintList = New System.Windows.Forms.Button()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtMinQty = New System.Windows.Forms.TextBox()
        Me.btnMenu = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtLostFactor = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txtPriceList = New System.Windows.Forms.TextBox()
        Me.grpPriceList = New System.Windows.Forms.GroupBox()
        Me.txtQtyHasil = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Tab1.SuspendLayout()
        Me.TabResep.SuspendLayout()
        CType(Me.dgvResep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabSatuan.SuspendLayout()
        CType(Me.dgvSatuan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabTambahan.SuspendLayout()
        CType(Me.dgvTambahan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabProduksi.SuspendLayout()
        CType(Me.dgvBom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPriceList.SuspendLayout()
        Me.SuspendLayout()
        '
        'Tab1
        '
        Me.Tab1.Controls.Add(Me.TabResep)
        Me.Tab1.Controls.Add(Me.TabSatuan)
        Me.Tab1.Controls.Add(Me.TabTambahan)
        Me.Tab1.Controls.Add(Me.TabProduksi)
        Me.Tab1.Location = New System.Drawing.Point(412, 15)
        Me.Tab1.Name = "Tab1"
        Me.Tab1.SelectedIndex = 0
        Me.Tab1.Size = New System.Drawing.Size(674, 440)
        Me.Tab1.TabIndex = 14
        '
        'TabResep
        '
        Me.TabResep.Controls.Add(Me.lblTambah3)
        Me.TabResep.Controls.Add(Me.lblTambah2)
        Me.TabResep.Controls.Add(Me.lblTambah1)
        Me.TabResep.Controls.Add(Me.Label30)
        Me.TabResep.Controls.Add(Me.txtAddCost3)
        Me.TabResep.Controls.Add(Me.Label27)
        Me.TabResep.Controls.Add(Me.txtAddCost2)
        Me.TabResep.Controls.Add(Me.Label29)
        Me.TabResep.Controls.Add(Me.lblGrandTotal)
        Me.TabResep.Controls.Add(Me.Label28)
        Me.TabResep.Controls.Add(Me.txtAddCost1)
        Me.TabResep.Controls.Add(Me.Label12)
        Me.TabResep.Controls.Add(Me.lblTotal)
        Me.TabResep.Controls.Add(Me.btnClearResep)
        Me.TabResep.Controls.Add(Me.dgvResep)
        Me.TabResep.Location = New System.Drawing.Point(4, 22)
        Me.TabResep.Name = "TabResep"
        Me.TabResep.Padding = New System.Windows.Forms.Padding(3)
        Me.TabResep.Size = New System.Drawing.Size(666, 414)
        Me.TabResep.TabIndex = 1
        Me.TabResep.Text = "Resep"
        Me.TabResep.UseVisualStyleBackColor = True
        '
        'lblTambah3
        '
        Me.lblTambah3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTambah3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTambah3.Location = New System.Drawing.Point(496, 327)
        Me.lblTambah3.Name = "lblTambah3"
        Me.lblTambah3.Size = New System.Drawing.Size(145, 24)
        Me.lblTambah3.TabIndex = 202
        Me.lblTambah3.Text = "0"
        Me.lblTambah3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTambah2
        '
        Me.lblTambah2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTambah2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTambah2.Location = New System.Drawing.Point(496, 297)
        Me.lblTambah2.Name = "lblTambah2"
        Me.lblTambah2.Size = New System.Drawing.Size(145, 24)
        Me.lblTambah2.TabIndex = 201
        Me.lblTambah2.Text = "0"
        Me.lblTambah2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTambah1
        '
        Me.lblTambah1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTambah1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTambah1.Location = New System.Drawing.Point(496, 268)
        Me.lblTambah1.Name = "lblTambah1"
        Me.lblTambah1.Size = New System.Drawing.Size(145, 24)
        Me.lblTambah1.TabIndex = 200
        Me.lblTambah1.Text = "0"
        Me.lblTambah1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.BackColor = System.Drawing.Color.Transparent
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label30.Location = New System.Drawing.Point(251, 324)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(116, 24)
        Me.Label30.TabIndex = 199
        Me.Label30.Text = "Tambahan 3"
        '
        'txtAddCost3
        '
        Me.txtAddCost3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddCost3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtAddCost3.Location = New System.Drawing.Point(393, 327)
        Me.txtAddCost3.Name = "txtAddCost3"
        Me.txtAddCost3.Size = New System.Drawing.Size(66, 23)
        Me.txtAddCost3.TabIndex = 198
        Me.txtAddCost3.Text = "0"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label27.Location = New System.Drawing.Point(251, 295)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(116, 24)
        Me.Label27.TabIndex = 197
        Me.Label27.Text = "Tambahan 2"
        '
        'txtAddCost2
        '
        Me.txtAddCost2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddCost2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtAddCost2.Location = New System.Drawing.Point(393, 298)
        Me.txtAddCost2.Name = "txtAddCost2"
        Me.txtAddCost2.Size = New System.Drawing.Size(66, 23)
        Me.txtAddCost2.TabIndex = 196
        Me.txtAddCost2.Text = "0"
        '
        'Label29
        '
        Me.Label29.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(251, 360)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(51, 24)
        Me.Label29.TabIndex = 195
        Me.Label29.Text = "Total"
        '
        'lblGrandTotal
        '
        Me.lblGrandTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblGrandTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrandTotal.Location = New System.Drawing.Point(496, 360)
        Me.lblGrandTotal.Name = "lblGrandTotal"
        Me.lblGrandTotal.Size = New System.Drawing.Size(145, 24)
        Me.lblGrandTotal.TabIndex = 194
        Me.lblGrandTotal.Text = "0"
        Me.lblGrandTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label28.Location = New System.Drawing.Point(251, 264)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(116, 24)
        Me.Label28.TabIndex = 192
        Me.Label28.Text = "Tambahan 1"
        '
        'txtAddCost1
        '
        Me.txtAddCost1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddCost1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtAddCost1.Location = New System.Drawing.Point(393, 267)
        Me.txtAddCost1.Name = "txtAddCost1"
        Me.txtAddCost1.Size = New System.Drawing.Size(66, 23)
        Me.txtAddCost1.TabIndex = 191
        Me.txtAddCost1.Text = "0"
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(253, 235)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(90, 24)
        Me.Label12.TabIndex = 190
        Me.Label12.Text = "Sub Total"
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(496, 240)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(145, 24)
        Me.lblTotal.TabIndex = 189
        Me.lblTotal.Text = "0"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnClearResep
        '
        Me.btnClearResep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnClearResep.Location = New System.Drawing.Point(13, 240)
        Me.btnClearResep.Name = "btnClearResep"
        Me.btnClearResep.Size = New System.Drawing.Size(98, 32)
        Me.btnClearResep.TabIndex = 90
        Me.btnClearResep.Text = "Clear"
        Me.btnClearResep.UseVisualStyleBackColor = True
        '
        'dgvResep
        '
        Me.dgvResep.AllowDrop = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dgvResep.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvResep.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvResep.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvResep.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvResep.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.kode_bahan, Me.Nama_bahan, Me.Qty, Me.Satuan, Me.potong_stock, Me.colhpp, Me.LF, Me.Jumlah})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvResep.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvResep.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke
        Me.dgvResep.Location = New System.Drawing.Point(13, 10)
        Me.dgvResep.Name = "dgvResep"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvResep.RowHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvResep.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvResep.Size = New System.Drawing.Size(647, 221)
        Me.dgvResep.TabIndex = 87
        '
        'kode_bahan
        '
        Me.kode_bahan.HeaderText = "Kode Bahan"
        Me.kode_bahan.Name = "kode_bahan"
        Me.kode_bahan.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.kode_bahan.Width = 90
        '
        'Nama_bahan
        '
        Me.Nama_bahan.HeaderText = "Nama Bahan"
        Me.Nama_bahan.Name = "Nama_bahan"
        Me.Nama_bahan.ReadOnly = True
        Me.Nama_bahan.Width = 200
        '
        'Qty
        '
        Me.Qty.HeaderText = "Qty"
        Me.Qty.Name = "Qty"
        Me.Qty.Width = 50
        '
        'Satuan
        '
        Me.Satuan.HeaderText = "Satuan"
        Me.Satuan.Name = "Satuan"
        Me.Satuan.ReadOnly = True
        Me.Satuan.Width = 70
        '
        'potong_stock
        '
        Me.potong_stock.HeaderText = "Potong Stock"
        Me.potong_stock.Name = "potong_stock"
        Me.potong_stock.Width = 60
        '
        'colhpp
        '
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.colhpp.DefaultCellStyle = DataGridViewCellStyle3
        Me.colhpp.HeaderText = "HPP"
        Me.colhpp.Name = "colhpp"
        Me.colhpp.Width = 80
        '
        'LF
        '
        Me.LF.HeaderText = "LF"
        Me.LF.Name = "LF"
        Me.LF.ReadOnly = True
        Me.LF.Width = 40
        '
        'Jumlah
        '
        Me.Jumlah.HeaderText = "Jumlah"
        Me.Jumlah.Name = "Jumlah"
        Me.Jumlah.ReadOnly = True
        '
        'TabSatuan
        '
        Me.TabSatuan.Controls.Add(Me.btnClearSatuan)
        Me.TabSatuan.Controls.Add(Me.dgvSatuan)
        Me.TabSatuan.Location = New System.Drawing.Point(4, 22)
        Me.TabSatuan.Name = "TabSatuan"
        Me.TabSatuan.Size = New System.Drawing.Size(666, 414)
        Me.TabSatuan.TabIndex = 2
        Me.TabSatuan.Text = "Satuan"
        Me.TabSatuan.UseVisualStyleBackColor = True
        '
        'btnClearSatuan
        '
        Me.btnClearSatuan.Location = New System.Drawing.Point(7, 275)
        Me.btnClearSatuan.Name = "btnClearSatuan"
        Me.btnClearSatuan.Size = New System.Drawing.Size(98, 32)
        Me.btnClearSatuan.TabIndex = 89
        Me.btnClearSatuan.Text = "Clear"
        Me.btnClearSatuan.UseVisualStyleBackColor = True
        '
        'dgvSatuan
        '
        Me.dgvSatuan.AllowDrop = True
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dgvSatuan.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvSatuan.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSatuan.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvSatuan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSatuan.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.satuanQty})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvSatuan.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgvSatuan.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke
        Me.dgvSatuan.Location = New System.Drawing.Point(7, 7)
        Me.dgvSatuan.Name = "dgvSatuan"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSatuan.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvSatuan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvSatuan.Size = New System.Drawing.Size(307, 263)
        Me.dgvSatuan.TabIndex = 88
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Satuan"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 200
        '
        'satuanQty
        '
        Me.satuanQty.HeaderText = "Qty"
        Me.satuanQty.Name = "satuanQty"
        Me.satuanQty.Width = 50
        '
        'TabTambahan
        '
        Me.TabTambahan.Controls.Add(Me.btnClearTambahan)
        Me.TabTambahan.Controls.Add(Me.dgvTambahan)
        Me.TabTambahan.Location = New System.Drawing.Point(4, 22)
        Me.TabTambahan.Name = "TabTambahan"
        Me.TabTambahan.Size = New System.Drawing.Size(666, 414)
        Me.TabTambahan.TabIndex = 3
        Me.TabTambahan.Text = "Tambahan"
        Me.TabTambahan.UseVisualStyleBackColor = True
        '
        'btnClearTambahan
        '
        Me.btnClearTambahan.Location = New System.Drawing.Point(8, 271)
        Me.btnClearTambahan.Name = "btnClearTambahan"
        Me.btnClearTambahan.Size = New System.Drawing.Size(82, 34)
        Me.btnClearTambahan.TabIndex = 89
        Me.btnClearTambahan.Text = "Clear"
        Me.btnClearTambahan.UseVisualStyleBackColor = True
        '
        'dgvTambahan
        '
        Me.dgvTambahan.AllowDrop = True
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dgvTambahan.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvTambahan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTambahan.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgvTambahan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTambahan.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewButtonColumn1, Me.DataGridViewTextBoxColumn1, Me.tambahQty, Me.cmbSatuan_tambahan, Me.tambahIsi})
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTambahan.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgvTambahan.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke
        Me.dgvTambahan.Location = New System.Drawing.Point(11, 7)
        Me.dgvTambahan.Name = "dgvTambahan"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTambahan.RowHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvTambahan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvTambahan.Size = New System.Drawing.Size(574, 258)
        Me.dgvTambahan.TabIndex = 88
        '
        'DataGridViewButtonColumn1
        '
        Me.DataGridViewButtonColumn1.HeaderText = "Kode Tambahan"
        Me.DataGridViewButtonColumn1.Name = "DataGridViewButtonColumn1"
        Me.DataGridViewButtonColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Nama Tambahan"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 200
        '
        'tambahQty
        '
        Me.tambahQty.HeaderText = "Qty"
        Me.tambahQty.Name = "tambahQty"
        Me.tambahQty.Width = 50
        '
        'cmbSatuan_tambahan
        '
        Me.cmbSatuan_tambahan.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.cmbSatuan_tambahan.HeaderText = "Satuan"
        Me.cmbSatuan_tambahan.Name = "cmbSatuan_tambahan"
        Me.cmbSatuan_tambahan.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cmbSatuan_tambahan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.cmbSatuan_tambahan.Width = 120
        '
        'tambahIsi
        '
        Me.tambahIsi.HeaderText = "Isi"
        Me.tambahIsi.Name = "tambahIsi"
        Me.tambahIsi.ReadOnly = True
        Me.tambahIsi.Width = 50
        '
        'TabProduksi
        '
        Me.TabProduksi.Controls.Add(Me.Label38)
        Me.TabProduksi.Controls.Add(Me.txtQtyHasil)
        Me.TabProduksi.Controls.Add(Me.Label26)
        Me.TabProduksi.Controls.Add(Me.lbltotal_bom)
        Me.TabProduksi.Controls.Add(Me.Button1)
        Me.TabProduksi.Controls.Add(Me.dgvBom)
        Me.TabProduksi.Location = New System.Drawing.Point(4, 22)
        Me.TabProduksi.Name = "TabProduksi"
        Me.TabProduksi.Size = New System.Drawing.Size(666, 414)
        Me.TabProduksi.TabIndex = 4
        Me.TabProduksi.Text = "BOM Produksi"
        Me.TabProduksi.UseVisualStyleBackColor = True
        '
        'Label26
        '
        Me.Label26.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(412, 374)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(94, 24)
        Me.Label26.TabIndex = 192
        Me.Label26.Text = "Total HPP"
        '
        'lbltotal_bom
        '
        Me.lbltotal_bom.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbltotal_bom.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltotal_bom.Location = New System.Drawing.Point(508, 374)
        Me.lbltotal_bom.Name = "lbltotal_bom"
        Me.lbltotal_bom.Size = New System.Drawing.Size(145, 24)
        Me.lbltotal_bom.TabIndex = 191
        Me.lbltotal_bom.Text = "0"
        Me.lbltotal_bom.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(21, 355)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(98, 32)
        Me.Button1.TabIndex = 91
        Me.Button1.Text = "Clear"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'dgvBom
        '
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.dgvBom.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle14
        Me.dgvBom.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBom.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.dgvBom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBom.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.kodebahan_bom, Me.namabahan_bom, Me.qty_bom, Me.satuan_bom, Me.hpp_bom})
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvBom.DefaultCellStyle = DataGridViewCellStyle16
        Me.dgvBom.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke
        Me.dgvBom.Location = New System.Drawing.Point(3, 7)
        Me.dgvBom.Name = "dgvBom"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBom.RowHeadersDefaultCellStyle = DataGridViewCellStyle17
        Me.dgvBom.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvBom.Size = New System.Drawing.Size(650, 319)
        Me.dgvBom.TabIndex = 88
        '
        'kodebahan_bom
        '
        Me.kodebahan_bom.HeaderText = "Kode Bahan"
        Me.kodebahan_bom.Name = "kodebahan_bom"
        Me.kodebahan_bom.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.kodebahan_bom.Width = 90
        '
        'namabahan_bom
        '
        Me.namabahan_bom.HeaderText = "Nama Bahan"
        Me.namabahan_bom.Name = "namabahan_bom"
        Me.namabahan_bom.ReadOnly = True
        Me.namabahan_bom.Width = 200
        '
        'qty_bom
        '
        Me.qty_bom.HeaderText = "Qty"
        Me.qty_bom.Name = "qty_bom"
        Me.qty_bom.Width = 50
        '
        'satuan_bom
        '
        Me.satuan_bom.HeaderText = "Satuan"
        Me.satuan_bom.Name = "satuan_bom"
        Me.satuan_bom.ReadOnly = True
        Me.satuan_bom.Width = 80
        '
        'hpp_bom
        '
        Me.hpp_bom.HeaderText = "HPP"
        Me.hpp_bom.Name = "hpp_bom"
        '
        'chkAktifGeneral
        '
        Me.chkAktifGeneral.AutoSize = True
        Me.chkAktifGeneral.Location = New System.Drawing.Point(245, 231)
        Me.chkAktifGeneral.Name = "chkAktifGeneral"
        Me.chkAktifGeneral.Size = New System.Drawing.Size(47, 17)
        Me.chkAktifGeneral.TabIndex = 14
        Me.chkAktifGeneral.Text = "Aktif"
        Me.chkAktifGeneral.UseVisualStyleBackColor = True
        '
        'cmbSatuanGeneral
        '
        Me.cmbSatuanGeneral.FormattingEnabled = True
        Me.cmbSatuanGeneral.Location = New System.Drawing.Point(120, 175)
        Me.cmbSatuanGeneral.MaxDropDownItems = 16
        Me.cmbSatuanGeneral.Name = "cmbSatuanGeneral"
        Me.cmbSatuanGeneral.Size = New System.Drawing.Size(121, 21)
        Me.cmbSatuanGeneral.TabIndex = 10
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label10.Location = New System.Drawing.Point(101, 178)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(11, 13)
        Me.Label10.TabIndex = 73
        Me.Label10.Text = ":"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label11.Location = New System.Drawing.Point(12, 178)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(41, 13)
        Me.Label11.TabIndex = 72
        Me.Label11.Text = "Satuan"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label8.Location = New System.Drawing.Point(101, 122)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(11, 13)
        Me.Label8.TabIndex = 71
        Me.Label8.Text = ":"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label9.Location = New System.Drawing.Point(12, 122)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 13)
        Me.Label9.TabIndex = 70
        Me.Label9.Text = "Harga Jual"
        '
        'txtHargaGeneral
        '
        Me.txtHargaGeneral.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHargaGeneral.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtHargaGeneral.Location = New System.Drawing.Point(120, 119)
        Me.txtHargaGeneral.Name = "txtHargaGeneral"
        Me.txtHargaGeneral.Size = New System.Drawing.Size(121, 21)
        Me.txtHargaGeneral.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label5.Location = New System.Drawing.Point(84, 71)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(13, 13)
        Me.Label5.TabIndex = 68
        Me.Label5.Text = "*"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label6.Location = New System.Drawing.Point(101, 71)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(11, 13)
        Me.Label6.TabIndex = 67
        Me.Label6.Text = ":"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label7.Location = New System.Drawing.Point(12, 71)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(74, 13)
        Me.Label7.TabIndex = 66
        Me.Label7.Text = "Kode Kategori"
        '
        'cmbKategoriGeneral
        '
        Me.cmbKategoriGeneral.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbKategoriGeneral.FormattingEnabled = True
        Me.cmbKategoriGeneral.Location = New System.Drawing.Point(120, 68)
        Me.cmbKategoriGeneral.Name = "cmbKategoriGeneral"
        Me.cmbKategoriGeneral.Size = New System.Drawing.Size(121, 21)
        Me.cmbKategoriGeneral.TabIndex = 4
        '
        'btnSearchBarangGeneral
        '
        Me.btnSearchBarangGeneral.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnSearchBarangGeneral.Location = New System.Drawing.Point(247, 13)
        Me.btnSearchBarangGeneral.Name = "btnSearchBarangGeneral"
        Me.btnSearchBarangGeneral.Size = New System.Drawing.Size(32, 23)
        Me.btnSearchBarangGeneral.TabIndex = 2
        Me.btnSearchBarangGeneral.TabStop = False
        Me.btnSearchBarangGeneral.Text = "F5"
        Me.btnSearchBarangGeneral.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label17.Location = New System.Drawing.Point(84, 44)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(13, 13)
        Me.Label17.TabIndex = 63
        Me.Label17.Text = "*"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label16.Location = New System.Drawing.Point(84, 18)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(13, 13)
        Me.Label16.TabIndex = 62
        Me.Label16.Text = "*"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label3.Location = New System.Drawing.Point(101, 44)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(11, 13)
        Me.Label3.TabIndex = 61
        Me.Label3.Text = ":"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label4.Location = New System.Drawing.Point(12, 44)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 13)
        Me.Label4.TabIndex = 60
        Me.Label4.Text = "Nama"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label2.Location = New System.Drawing.Point(101, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(11, 13)
        Me.Label2.TabIndex = 59
        Me.Label2.Text = ":"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label1.Location = New System.Drawing.Point(12, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 13)
        Me.Label1.TabIndex = 58
        Me.Label1.Text = "Kode Barang"
        '
        'txtNamaBarangGeneral
        '
        Me.txtNamaBarangGeneral.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNamaBarangGeneral.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtNamaBarangGeneral.Location = New System.Drawing.Point(120, 41)
        Me.txtNamaBarangGeneral.MaxLength = 40
        Me.txtNamaBarangGeneral.Name = "txtNamaBarangGeneral"
        Me.txtNamaBarangGeneral.Size = New System.Drawing.Size(199, 21)
        Me.txtNamaBarangGeneral.TabIndex = 3
        '
        'txtKodeBarangGeneral
        '
        Me.txtKodeBarangGeneral.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKodeBarangGeneral.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtKodeBarangGeneral.Location = New System.Drawing.Point(120, 15)
        Me.txtKodeBarangGeneral.MaxLength = 7
        Me.txtKodeBarangGeneral.Name = "txtKodeBarangGeneral"
        Me.txtKodeBarangGeneral.Size = New System.Drawing.Size(121, 21)
        Me.txtKodeBarangGeneral.TabIndex = 1
        '
        'btnHapus
        '
        Me.btnHapus.BackColor = System.Drawing.Color.Transparent
        Me.btnHapus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHapus.FlatAppearance.BorderSize = 0
        Me.btnHapus.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnHapus.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHapus.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnHapus.Location = New System.Drawing.Point(624, 517)
        Me.btnHapus.Name = "btnHapus"
        Me.btnHapus.Size = New System.Drawing.Size(98, 32)
        Me.btnHapus.TabIndex = 19
        Me.btnHapus.Text = "Hapus"
        Me.btnHapus.UseVisualStyleBackColor = False
        '
        'btnKeluar
        '
        Me.btnKeluar.BackColor = System.Drawing.Color.Transparent
        Me.btnKeluar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnKeluar.FlatAppearance.BorderSize = 0
        Me.btnKeluar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnKeluar.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnKeluar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnKeluar.Location = New System.Drawing.Point(728, 517)
        Me.btnKeluar.Name = "btnKeluar"
        Me.btnKeluar.Size = New System.Drawing.Size(98, 32)
        Me.btnKeluar.TabIndex = 20
        Me.btnKeluar.Text = "Keluar(Esc)"
        Me.btnKeluar.UseVisualStyleBackColor = False
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.Transparent
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnReset.Location = New System.Drawing.Point(520, 517)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(98, 32)
        Me.btnReset.TabIndex = 18
        Me.btnReset.Text = "Reset(Ctrl+R)"
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'btnSimpan
        '
        Me.btnSimpan.BackColor = System.Drawing.Color.Transparent
        Me.btnSimpan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSimpan.FlatAppearance.BorderSize = 0
        Me.btnSimpan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnSimpan.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSimpan.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnSimpan.Location = New System.Drawing.Point(416, 517)
        Me.btnSimpan.Name = "btnSimpan"
        Me.btnSimpan.Size = New System.Drawing.Size(98, 32)
        Me.btnSimpan.TabIndex = 17
        Me.btnSimpan.Text = "Simpan(F2)"
        Me.btnSimpan.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbPrinter2)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.cmbPrinter1)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Location = New System.Drawing.Point(11, 251)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(306, 85)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Printer"
        '
        'cmbPrinter2
        '
        Me.cmbPrinter2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPrinter2.FormattingEnabled = True
        Me.cmbPrinter2.Location = New System.Drawing.Point(107, 52)
        Me.cmbPrinter2.Name = "cmbPrinter2"
        Me.cmbPrinter2.Size = New System.Drawing.Size(188, 21)
        Me.cmbPrinter2.TabIndex = 16
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label13.Location = New System.Drawing.Point(86, 55)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(11, 13)
        Me.Label13.TabIndex = 81
        Me.Label13.Text = ":"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label14.Location = New System.Drawing.Point(6, 55)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(48, 13)
        Me.Label14.TabIndex = 80
        Me.Label14.Text = "Printer 2"
        '
        'cmbPrinter1
        '
        Me.cmbPrinter1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPrinter1.FormattingEnabled = True
        Me.cmbPrinter1.Location = New System.Drawing.Point(107, 25)
        Me.cmbPrinter1.Name = "cmbPrinter1"
        Me.cmbPrinter1.Size = New System.Drawing.Size(188, 21)
        Me.cmbPrinter1.TabIndex = 15
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label15.Location = New System.Drawing.Point(86, 28)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(11, 13)
        Me.Label15.TabIndex = 78
        Me.Label15.Text = ":"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label18.Location = New System.Drawing.Point(6, 28)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(48, 13)
        Me.Label18.TabIndex = 77
        Me.Label18.Text = "Printer 1"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label20.Location = New System.Drawing.Point(99, 233)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(11, 13)
        Me.Label20.TabIndex = 96
        Me.Label20.Text = ":"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label21.Location = New System.Drawing.Point(12, 232)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(36, 13)
        Me.Label21.TabIndex = 95
        Me.Label21.Text = "Dapur"
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(333, 340)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(58, 23)
        Me.btnBrowse.TabIndex = 16
        Me.btnBrowse.Text = "Browse"
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.BackColor = System.Drawing.Color.Transparent
        Me.Label36.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label36.Location = New System.Drawing.Point(99, 345)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(11, 13)
        Me.Label36.TabIndex = 152
        Me.Label36.Text = ":"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.BackColor = System.Drawing.Color.Transparent
        Me.Label37.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label37.Location = New System.Drawing.Point(17, 345)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(29, 13)
        Me.Label37.TabIndex = 151
        Me.Label37.Text = "Foto"
        '
        'txtFoto
        '
        Me.txtFoto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFoto.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtFoto.Location = New System.Drawing.Point(118, 342)
        Me.txtFoto.Name = "txtFoto"
        Me.txtFoto.Size = New System.Drawing.Size(199, 21)
        Me.txtFoto.TabIndex = 17
        '
        'cmbDapur
        '
        Me.cmbDapur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDapur.FormattingEnabled = True
        Me.cmbDapur.Location = New System.Drawing.Point(118, 229)
        Me.cmbDapur.MaxDropDownItems = 16
        Me.cmbDapur.Name = "cmbDapur"
        Me.cmbDapur.Size = New System.Drawing.Size(121, 21)
        Me.cmbDapur.TabIndex = 13
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox1.Location = New System.Drawing.Point(118, 369)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(273, 153)
        Me.PictureBox1.TabIndex = 153
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.WaitOnLoad = True
        '
        'btnNewKategori
        '
        Me.btnNewKategori.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnNewKategori.Location = New System.Drawing.Point(247, 66)
        Me.btnNewKategori.Name = "btnNewKategori"
        Me.btnNewKategori.Size = New System.Drawing.Size(43, 23)
        Me.btnNewKategori.TabIndex = 5
        Me.btnNewKategori.TabStop = False
        Me.btnNewKategori.Text = "New"
        Me.btnNewKategori.UseVisualStyleBackColor = True
        '
        'btnTipe
        '
        Me.btnTipe.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnTipe.Location = New System.Drawing.Point(247, 92)
        Me.btnTipe.Name = "btnTipe"
        Me.btnTipe.Size = New System.Drawing.Size(43, 23)
        Me.btnTipe.TabIndex = 7
        Me.btnTipe.TabStop = False
        Me.btnTipe.Text = "New"
        Me.btnTipe.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label19.Location = New System.Drawing.Point(84, 97)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(13, 13)
        Me.Label19.TabIndex = 158
        Me.Label19.Text = "*"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label22.Location = New System.Drawing.Point(101, 97)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(11, 13)
        Me.Label22.TabIndex = 157
        Me.Label22.Text = ":"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label23.Location = New System.Drawing.Point(12, 97)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(54, 13)
        Me.Label23.TabIndex = 156
        Me.Label23.Text = "Kode Tipe"
        '
        'cmbTipe
        '
        Me.cmbTipe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipe.FormattingEnabled = True
        Me.cmbTipe.Location = New System.Drawing.Point(120, 94)
        Me.cmbTipe.Name = "cmbTipe"
        Me.cmbTipe.Size = New System.Drawing.Size(121, 21)
        Me.cmbTipe.TabIndex = 6
        '
        'btnPrev
        '
        Me.btnPrev.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnPrev.Location = New System.Drawing.Point(314, 13)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(32, 23)
        Me.btnPrev.TabIndex = 159
        Me.btnPrev.TabStop = False
        Me.btnPrev.Text = "<<"
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnNext.Location = New System.Drawing.Point(352, 13)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(32, 23)
        Me.btnNext.TabIndex = 160
        Me.btnNext.TabStop = False
        Me.btnNext.Text = ">>"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPrintList
        '
        Me.btnPrintList.BackColor = System.Drawing.Color.Transparent
        Me.btnPrintList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnPrintList.FlatAppearance.BorderSize = 0
        Me.btnPrintList.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnPrintList.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrintList.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnPrintList.Location = New System.Drawing.Point(832, 517)
        Me.btnPrintList.Name = "btnPrintList"
        Me.btnPrintList.Size = New System.Drawing.Size(98, 32)
        Me.btnPrintList.TabIndex = 161
        Me.btnPrintList.Text = "Price List"
        Me.btnPrintList.UseVisualStyleBackColor = False
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label24.Location = New System.Drawing.Point(101, 205)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(11, 13)
        Me.Label24.TabIndex = 164
        Me.Label24.Text = ":"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label25.Location = New System.Drawing.Point(12, 205)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(44, 13)
        Me.Label25.TabIndex = 163
        Me.Label25.Text = "Min Qty"
        '
        'txtMinQty
        '
        Me.txtMinQty.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMinQty.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtMinQty.Location = New System.Drawing.Point(120, 202)
        Me.txtMinQty.Name = "txtMinQty"
        Me.txtMinQty.Size = New System.Drawing.Size(121, 21)
        Me.txtMinQty.TabIndex = 11
        '
        'btnMenu
        '
        Me.btnMenu.BackColor = System.Drawing.Color.Transparent
        Me.btnMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMenu.FlatAppearance.BorderSize = 0
        Me.btnMenu.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnMenu.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMenu.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnMenu.Location = New System.Drawing.Point(936, 517)
        Me.btnMenu.Name = "btnMenu"
        Me.btnMenu.Size = New System.Drawing.Size(65, 32)
        Me.btnMenu.TabIndex = 165
        Me.btnMenu.Text = "Menu"
        Me.btnMenu.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Transparent
        Me.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.Button2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Button2.Location = New System.Drawing.Point(325, 37)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(65, 25)
        Me.Button2.TabIndex = 166
        Me.Button2.Text = "Edit Kode"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.BackColor = System.Drawing.Color.Transparent
        Me.Label31.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label31.Location = New System.Drawing.Point(283, 208)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(11, 13)
        Me.Label31.TabIndex = 169
        Me.Label31.Text = ":"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.BackColor = System.Drawing.Color.Transparent
        Me.Label32.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label32.Location = New System.Drawing.Point(244, 208)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(18, 13)
        Me.Label32.TabIndex = 168
        Me.Label32.Text = "LF"
        '
        'txtLostFactor
        '
        Me.txtLostFactor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLostFactor.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtLostFactor.Location = New System.Drawing.Point(302, 205)
        Me.txtLostFactor.Name = "txtLostFactor"
        Me.txtLostFactor.Size = New System.Drawing.Size(57, 21)
        Me.txtLostFactor.TabIndex = 12
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.BackColor = System.Drawing.Color.Transparent
        Me.Label33.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label33.Location = New System.Drawing.Point(365, 208)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(18, 13)
        Me.Label33.TabIndex = 170
        Me.Label33.Text = "%"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.BackColor = System.Drawing.Color.Transparent
        Me.Label34.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label34.Location = New System.Drawing.Point(94, 5)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(11, 13)
        Me.Label34.TabIndex = 173
        Me.Label34.Text = ":"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.Color.Transparent
        Me.Label35.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label35.Location = New System.Drawing.Point(5, 5)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(55, 13)
        Me.Label35.TabIndex = 172
        Me.Label35.Text = "Harga Beli"
        '
        'txtPriceList
        '
        Me.txtPriceList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPriceList.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtPriceList.Location = New System.Drawing.Point(110, 5)
        Me.txtPriceList.Name = "txtPriceList"
        Me.txtPriceList.Size = New System.Drawing.Size(121, 21)
        Me.txtPriceList.TabIndex = 9
        '
        'grpPriceList
        '
        Me.grpPriceList.Controls.Add(Me.Label34)
        Me.grpPriceList.Controls.Add(Me.txtPriceList)
        Me.grpPriceList.Controls.Add(Me.Label35)
        Me.grpPriceList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.grpPriceList.Location = New System.Drawing.Point(8, 143)
        Me.grpPriceList.Name = "grpPriceList"
        Me.grpPriceList.Size = New System.Drawing.Size(298, 31)
        Me.grpPriceList.TabIndex = 174
        Me.grpPriceList.TabStop = False
        '
        'txtQtyHasil
        '
        Me.txtQtyHasil.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQtyHasil.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtQtyHasil.Location = New System.Drawing.Point(532, 338)
        Me.txtQtyHasil.Name = "txtQtyHasil"
        Me.txtQtyHasil.Size = New System.Drawing.Size(121, 21)
        Me.txtQtyHasil.TabIndex = 175
        '
        'Label38
        '
        Me.Label38.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(412, 337)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(84, 24)
        Me.Label38.TabIndex = 193
        Me.Label38.Text = "Qty Hasil"
        '
        'frmMasterBarang
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1104, 561)
        Me.Controls.Add(Me.grpPriceList)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.txtLostFactor)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.btnMenu)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.txtMinQty)
        Me.Controls.Add(Me.btnPrintList)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btnPrev)
        Me.Controls.Add(Me.btnTipe)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.cmbTipe)
        Me.Controls.Add(Me.btnNewKategori)
        Me.Controls.Add(Me.cmbDapur)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.btnBrowse)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.Label37)
        Me.Controls.Add(Me.txtFoto)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnHapus)
        Me.Controls.Add(Me.btnKeluar)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnSimpan)
        Me.Controls.Add(Me.chkAktifGeneral)
        Me.Controls.Add(Me.cmbSatuanGeneral)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtHargaGeneral)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cmbKategoriGeneral)
        Me.Controls.Add(Me.btnSearchBarangGeneral)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtNamaBarangGeneral)
        Me.Controls.Add(Me.txtKodeBarangGeneral)
        Me.Controls.Add(Me.Tab1)
        Me.KeyPreview = True
        Me.Name = "frmMasterBarang"
        Me.Text = "frmMasterBarang"
        Me.Tab1.ResumeLayout(False)
        Me.TabResep.ResumeLayout(False)
        Me.TabResep.PerformLayout()
        CType(Me.dgvResep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabSatuan.ResumeLayout(False)
        CType(Me.dgvSatuan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabTambahan.ResumeLayout(False)
        CType(Me.dgvTambahan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabProduksi.ResumeLayout(False)
        Me.TabProduksi.PerformLayout()
        CType(Me.dgvBom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPriceList.ResumeLayout(False)
        Me.grpPriceList.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Tab1 As System.Windows.Forms.TabControl
    Friend WithEvents TabResep As System.Windows.Forms.TabPage
    Friend WithEvents TabSatuan As System.Windows.Forms.TabPage
    Friend WithEvents TabTambahan As System.Windows.Forms.TabPage
    Friend WithEvents dgvResep As System.Windows.Forms.DataGridView
    Friend WithEvents chkAktifGeneral As System.Windows.Forms.CheckBox
    Friend WithEvents cmbSatuanGeneral As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtHargaGeneral As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmbKategoriGeneral As System.Windows.Forms.ComboBox
    Friend WithEvents btnSearchBarangGeneral As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtNamaBarangGeneral As System.Windows.Forms.TextBox
    Friend WithEvents txtKodeBarangGeneral As System.Windows.Forms.TextBox
    Friend WithEvents dgvTambahan As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSatuan As System.Windows.Forms.DataGridView
    Friend WithEvents btnClearSatuan As System.Windows.Forms.Button
    Friend WithEvents btnClearTambahan As System.Windows.Forms.Button
    Friend WithEvents btnHapus As System.Windows.Forms.Button
    Friend WithEvents btnKeluar As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnSimpan As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbPrinter2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cmbPrinter1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtFoto As System.Windows.Forms.TextBox
    Friend WithEvents cmbDapur As System.Windows.Forms.ComboBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnClearResep As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents satuanQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnNewKategori As System.Windows.Forms.Button
    Friend WithEvents btnTipe As System.Windows.Forms.Button
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents cmbTipe As System.Windows.Forms.ComboBox
    Friend WithEvents btnPrev As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents DataGridViewButtonColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tambahQty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmbSatuan_tambahan As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents tambahIsi As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents btnPrintList As System.Windows.Forms.Button
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtMinQty As System.Windows.Forms.TextBox
    Friend WithEvents btnMenu As System.Windows.Forms.Button
    Friend WithEvents TabProduksi As System.Windows.Forms.TabPage
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents lbltotal_bom As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents dgvBom As System.Windows.Forms.DataGridView
    Friend WithEvents kodebahan_bom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents namabahan_bom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents qty_bom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents satuan_bom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hpp_bom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents lblGrandTotal As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtAddCost1 As System.Windows.Forms.TextBox
    Friend WithEvents lblTambah3 As System.Windows.Forms.Label
    Friend WithEvents lblTambah2 As System.Windows.Forms.Label
    Friend WithEvents lblTambah1 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtAddCost3 As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtAddCost2 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtLostFactor As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtPriceList As System.Windows.Forms.TextBox
    Friend WithEvents kode_bahan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nama_bahan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Qty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Satuan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents potong_stock As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhpp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Jumlah As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grpPriceList As System.Windows.Forms.GroupBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents txtQtyHasil As System.Windows.Forms.TextBox
End Class
