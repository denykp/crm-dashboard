﻿Imports System.Data.OleDb
Module ModConnection
    Public Conn As OleDbConnection
    Public da As OleDbDataAdapter
    Public ds As DataSet
    Public cmd As OleDbCommand
    Public rd As OleDbDataReader
    Public reader As OleDbDataReader
    Public strcon As String
    Public provider As String
    Public servername As String
    Public fileH1toH2_1 As String, fileH1toH2_2 As String, fileH1toH2_3 As String, fileH1toH2_4 As String
    Public fileH2toH1 As String
    Public sourcefolder As String
    Public pRowMax As Long
    Public pStr As String

    Public Sub CatatLog(ByVal vNama As String, ByVal vAktifitas As String, ByVal vObjek As String, ByVal vCmd As String)
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing
        Dim vConn As New OleDbConnection()

        vConn.ConnectionString = strcon
        vConn.Open()

        Try
            pStr = "insert into T_UserLog (UL_UserName, UL_DateTime, UL_Activity, UL_Object, UL_Cmd) values ('" _
                                   & vNama & "', now(), '" & vAktifitas & "', '" & vObjek & "', '" & removeapos(vCmd) & "')"

            cmd = New OleDbCommand(pStr, vConn)
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            MessageBox.Show("Warning Level.01" & vbCrLf & ex.Message.ToString)
        End Try

        If vConn.State Then vConn.Close()
    End Sub

    Public Function Get_RowMax(ByVal vUser As String) As Long
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader
        Dim vConn As New OleDbConnection
        vConn.ConnectionString = strcon
        vConn.Open()

        pStr = "SELECT TOP 1 Set_MaxRows FROM M_UserSetting WHERE User_Name = '" & vUser & "' ORDER BY ID DESC "
        cmd = New OleDbCommand(pStr, vConn)
        reader = cmd.ExecuteReader()
        If reader.Read() Then
            Get_RowMax = reader.Item("Set_MaxRows")
        Else
            Get_RowMax = 50
        End If
        reader.Close()
        vConn.Close()
    End Function

    Public Function RemoveApos(ByVal vStr As String) As String
        Return Replace(vStr, "'", "`")
    End Function

    Public Sub setconnectionstring(ByVal path As String, ByVal dbname As String)
        Dim constrbuilder As New OleDbConnectionStringBuilder
        constrbuilder.Provider = "Microsoft.ACE.OLEDB.12.0" 'Microsoft.ACE.OLEDB.12.0
        constrbuilder.PersistSecurityInfo = False
        constrbuilder.DataSource = path + dbname
        constrbuilder.Add("Jet OLEDB:Database Password", "denik12345")

        '        constrbuilder.password = pwd
        strcon = constrbuilder.ConnectionString
    End Sub

    Public Function getfields(ByVal tablename As String, columns As String, conditions As String) As String()
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader
        Dim conn As New oledbConnection
        Dim str As String = ""
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New oledbCommand("select " & columns & " from " & tablename & " " & conditions & "", conn)
        reader = cmd.ExecuteReader()
        If reader.Read() Then
            For i As Integer = 0 To reader.FieldCount - 1
                str += reader(i) & ","
            Next
            str = Strings.Left(str, Len(str) - 1)
        Else
        End If
        Return Split(Str, ",")
        reader.Close()
        conn.Close()
    End Function
    Public Sub executeSQL(query As String)
        Dim conn As New oledbConnection(strcon)
        Dim cmd As New oledbCommand(query, conn)
        Dim da As New oledbDataAdapter(cmd)
        Dim ds As New DataSet
        Try

            conn.Open()
            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conn.Close()
        End Try

    End Sub
    Public Sub executeSQL(ByVal query As String, ByRef conn As oledbConnection, ByRef trans As oledbTransaction)
        Dim cmd As New oledbCommand(query, conn, trans)
        cmd.ExecuteNonQuery()
    End Sub
    Public Function getQuery(query As String) As DataSet
        Dim conn As New oledbConnection(strcon)
        Dim cmd As New oledbCommand(query, conn)
        Dim da As New oledbDataAdapter(cmd)
        Dim ds As New DataSet
        Try
            conn.Open()
            da.Fill(ds)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conn.Close()
        End Try
        Return ds
    End Function
    Public Function getQuery(query As String, conn As oledbConnection) As DataSet

        Dim cmd As New oledbCommand(query, conn)
        Dim da As New oledbDataAdapter(cmd)
        Dim ds As New DataSet
        Try

            da.Fill(ds)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return ds
    End Function
    Public Function getQuery(query As String, ByRef conn As oledbConnection, ByRef trans As oledbTransaction) As DataSet

        Dim cmd As New oledbCommand(query, conn, trans)
        Dim da As New oledbDataAdapter(cmd)
        Dim ds As New DataSet
        Try
            da.Fill(ds)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return ds
    End Function
End Module