﻿Imports Microsoft.Win32

Module Module1
    Public dbdir As String
    Public odbc As String
    Public tablename As String
    Public transtablename As String
    Public fdkodebarang As String
    Public fdnamabarang As String
    Public fdharga As String
    Public fddisc As String

    Public fdtransnotans As String
    Public fdtranskodebarang As String
    Public fdtransnamabarang As String
    Public fdtransharga As String
    Public fdtransqty As String
    Public fdtransdisc As String
    Public namaview As String
    Public x3 As Boolean
    Public msgboxTitle As String = "3PM-Solutions"

    Public connstr As String
    ' provider As String
    Public serverna As String
    Public dbname As String
    Public userid As String
    'Public pwd As String
    Public Function GetAppPath() As String
        Dim i As Integer
        Dim strAppPath As String
        strAppPath = System.Reflection.Assembly.GetExecutingAssembly.Location()
        i = strAppPath.Length - 1
        Do Until strAppPath.Substring(i, 1) = "\"
            i = i - 1
        Loop
        strAppPath = strAppPath.Substring(0, i)
        Return strAppPath
    End Function
    'Public Function getSupplier(ByVal kodeproduk As String) As String

    '    Dim query As String
    '    db1.openConnection(db.getConnectionString)
    '    getSupplier = "U"
    '    query = "select kode_supplier from ms_produk where kode_produk='" & kodeproduk & "'"
    '    db1.query(query, rs)
    '    If Not rs.EOF Then getSupplier = rs(0).Value
    '    rs.Close()
    '    db1.closeConnection()
    'End Function
    'Public Function getStock2(ByVal kodeproduk As String, ByVal db As DBClass) As Double
    '    Dim db1 As New DBClass
    '    Dim rs As New ADODB.Recordset
    '    Dim query As String

    '    getStock2 = 0
    '    query = "select isnull(stock,0) from stock where kode_produk='" & kodeproduk & "'"
    '    db.query(query, rs)
    '    If Not rs.EOF Then getStock2 = rs(0).Value
    '    rs.Close()

    'End Function
    Public Class CalendarColumn
        Inherits DataGridViewColumn

        Public Sub New()
            MyBase.New(New CalendarCell())
        End Sub

        Public Overrides Property CellTemplate() As DataGridViewCell
            Get
                Return MyBase.CellTemplate
            End Get
            Set(ByVal value As DataGridViewCell)

                ' Ensure that the cell used for the template is a CalendarCell. 
                If (value IsNot Nothing) AndAlso _
                    Not value.GetType().IsAssignableFrom(GetType(CalendarCell)) _
                    Then
                    Throw New InvalidCastException("Must be a CalendarCell")
                End If
                MyBase.CellTemplate = value

            End Set
        End Property

    End Class
    Public Class CalendarCell
        Inherits DataGridViewTextBoxCell

        Public Sub New()
            ' Use the short date format. 
            Me.Style.Format = "dd/MM/yyyy"
        End Sub

        Public Overrides Sub InitializeEditingControl(ByVal rowIndex As Integer, _
            ByVal initialFormattedValue As Object, _
            ByVal dataGridViewCellStyle As DataGridViewCellStyle)

            ' Set the value of the editing control to the current cell value. 
            MyBase.InitializeEditingControl(rowIndex, initialFormattedValue, _
                dataGridViewCellStyle)

            Dim ctl As CalendarEditingControl = _
                CType(DataGridView.EditingControl, CalendarEditingControl)

            ' Use the default row value when Value property is null. 
            If (Me.Value Is Nothing) Then
                ctl.Value = CType(Me.DefaultNewRowValue, DateTime)
            Else
                ctl.Value = CType(Me.Value, DateTime)
            End If
        End Sub

        Public Overrides ReadOnly Property EditType() As Type
            Get
                ' Return the type of the editing control that CalendarCell uses. 
                Return GetType(CalendarEditingControl)
            End Get
        End Property

        Public Overrides ReadOnly Property ValueType() As Type
            Get
                ' Return the type of the value that CalendarCell contains. 
                Return GetType(DateTime)
            End Get
        End Property

        Public Overrides ReadOnly Property DefaultNewRowValue() As Object
            Get
                ' Use the current date and time as the default value. 
                Return DateTime.Now
            End Get
        End Property

    End Class
    Class CalendarEditingControl
        Inherits DateTimePicker
        Implements IDataGridViewEditingControl

        Private dataGridViewControl As DataGridView
        Private valueIsChanged As Boolean = False
        Private rowIndexNum As Integer

        Public Sub New()
            Me.Format = DateTimePickerFormat.Short
        End Sub

        Public Property EditingControlFormattedValue() As Object _
            Implements IDataGridViewEditingControl.EditingControlFormattedValue

            Get
                Return Me.Value.ToShortDateString()
            End Get

            Set(ByVal value As Object)
                Try
                    ' This will throw an exception of the string is  
                    ' null, empty, or not in the format of a date. 
                    Me.Value = DateTime.Parse(CStr(value))
                Catch
                    ' In the case of an exception, just use the default 
                    ' value so we're not left with a null value. 
                    Me.Value = DateTime.Now
                End Try
            End Set

        End Property

        Public Function GetEditingControlFormattedValue(ByVal context _
            As DataGridViewDataErrorContexts) As Object _
            Implements IDataGridViewEditingControl.GetEditingControlFormattedValue

            Return Me.Value.ToShortDateString()

        End Function

        Public Sub ApplyCellStyleToEditingControl(ByVal dataGridViewCellStyle As  _
            DataGridViewCellStyle) _
            Implements IDataGridViewEditingControl.ApplyCellStyleToEditingControl

            Me.Font = dataGridViewCellStyle.Font
            Me.CalendarForeColor = dataGridViewCellStyle.ForeColor
            Me.CalendarMonthBackground = dataGridViewCellStyle.BackColor
            Me.Format = DateTimePickerFormat.Custom
            Me.CustomFormat = "dd/MM/yyyy"

        End Sub

        Public Property EditingControlRowIndex() As Integer _
            Implements IDataGridViewEditingControl.EditingControlRowIndex

            Get
                Return rowIndexNum
            End Get
            Set(ByVal value As Integer)
                rowIndexNum = value
            End Set

        End Property

        Public Function EditingControlWantsInputKey(ByVal key As Keys, _
            ByVal dataGridViewWantsInputKey As Boolean) As Boolean _
            Implements IDataGridViewEditingControl.EditingControlWantsInputKey

            ' Let the DateTimePicker handle the keys listed. 
            Select Case key And Keys.KeyCode
                Case Keys.Left, Keys.Up, Keys.Down, Keys.Right, _
                    Keys.Home, Keys.End, Keys.PageDown, Keys.PageUp

                    Return True

                Case Else
                    Return Not dataGridViewWantsInputKey
            End Select

        End Function

        Public Sub PrepareEditingControlForEdit(ByVal selectAll As Boolean) _
            Implements IDataGridViewEditingControl.PrepareEditingControlForEdit

            ' No preparation needs to be done. 

        End Sub

        Public ReadOnly Property RepositionEditingControlOnValueChange() _
            As Boolean Implements _
            IDataGridViewEditingControl.RepositionEditingControlOnValueChange

            Get
                Return False
            End Get

        End Property

        Public Property EditingControlDataGridView() As DataGridView _
            Implements IDataGridViewEditingControl.EditingControlDataGridView

            Get
                Return dataGridViewControl
            End Get
            Set(ByVal value As DataGridView)
                dataGridViewControl = value
            End Set

        End Property

        Public Property EditingControlValueChanged() As Boolean _
            Implements IDataGridViewEditingControl.EditingControlValueChanged

            Get
                Return valueIsChanged
            End Get
            Set(ByVal value As Boolean)
                valueIsChanged = value
            End Set

        End Property

        Public ReadOnly Property EditingControlCursor() As Cursor _
            Implements IDataGridViewEditingControl.EditingPanelCursor

            Get
                Return MyBase.Cursor
            End Get

        End Property

        Protected Overrides Sub OnValueChanged(ByVal eventargs As EventArgs)

            ' Notify the DataGridView that the contents of the cell have changed.
            valueIsChanged = True
            Me.EditingControlDataGridView.NotifyCurrentCellDirty(True)
            MyBase.OnValueChanged(eventargs)

        End Sub

    End Class
    Public Sub cekreg()
        Dim reg As RegistryKey
        reg = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Info", True)
        If reg Is Nothing Then
            ' Key doesn't exist; create it.
            reg = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Info")
        End If
        Dim intVersion As Integer = 0
        If (Not reg Is Nothing) Then
            dbdir = reg.GetValue("db", 0)
            If dbdir = "0" Then
                reg.SetValue("db", GetAppPath() & "\dbmanufacture.mdb")
            End If
            reg.Close()
        End If
    End Sub
    Public Sub savereg(ByVal str As String)
        Dim reg As RegistryKey
        reg = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Info", True)
        If reg Is Nothing Then
            ' Key doesn't exist; create it.
            reg = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Info")
        End If
        Dim intVersion As Integer = 0
        If (Not reg Is Nothing) Then
            reg.SetValue("db", str)
            reg.Close()
        End If
    End Sub
    Public Function App_Path() As String
        Return System.AppDomain.CurrentDomain.BaseDirectory()
    End Function
    Public Function getPaperIndex(ByVal printername As String, ByVal papername As String) As Integer
        Dim printDoc As New System.Drawing.Printing.PrintDocument
        Dim paperindex As Integer
        Dim i As Byte
        paperindex = 0
        printDoc.PrinterSettings.PrinterName = printername
        For i = 0 To printDoc.PrinterSettings.PaperSizes.Count
            If printDoc.PrinterSettings.PaperSizes(i).PaperName = papername Then
                paperindex = CInt(printDoc.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(printDoc.PrinterSettings.PaperSizes(i)))
                Exit For
            End If


        Next
        getPaperIndex = paperindex
    End Function
    
End Module
