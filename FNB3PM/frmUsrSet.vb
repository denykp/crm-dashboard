﻿Imports System.Data.OleDb
Imports System.IO

Public Class frmUserSet

    Private Sub frmUserSet_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub

    Private Sub frmUserSet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Load_UserSet()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub txtMaxRow_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMaxRow.KeyPress
        Select Case e.KeyChar
            Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ChrW(8)
            Case Else
                e.KeyChar = Strings.ChrW(0)
        End Select
    End Sub

    Private Sub btnBrow1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrow1.Click
        Dim myStream As Stream = Nothing
        Dim vDir As New OpenFileDialog()

        If Directory.Exists(txtPathSource.Text) Then
            vDir.InitialDirectory = txtPathSource.Text
        Else
            vDir.InitialDirectory = "C:\"
        End If

        vDir.Filter = "excel files (*.xlsx)|*.xls?|All files (*.*)|*.*"
        vDir.FilterIndex = 1
        vDir.RestoreDirectory = True
        vDir.CheckFileExists = False
        vDir.CheckPathExists = True

        If vDir.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                '                myStream = vDir.OpenFile()
                '                If (myStream IsNot Nothing) Then
                Dim fi As New FileInfo(vDir.FileName)
                txtPathSource.Text = fi.Directory.ToString
                '                End If
            Catch Ex As Exception
                MessageBox.Show("Invalid Directory " & vbCrLf & "Original error: " & Ex.Message)
            Finally
                ' Check this again, since we need to make sure we didn't throw an exception on open.
                If (myStream IsNot Nothing) Then
                    myStream.Close()
                End If
            End Try
        End If
    End Sub

    Private Sub btnBrow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBrow2.Click, btnBrow3.Click, btnBrow4.Click, btnBrow5.Click, btnBrow6.Click
        browse(CType(sender, Button).Name)
    End Sub
    Private Sub browse(pilihan As String)
        Dim myStream As Stream = Nothing
        Dim vDir As New OpenFileDialog()

        'If Directory.Exists(txtPathDest.Text) Then
        '    vDir.InitialDirectory = txtPathDest.Text
        'Else
        '    vDir.InitialDirectory = "C:\"
        'End If

        vDir.Filter = "excel files (*.xlsx)|*.xls?|All files (*.*)|*.*"
        vDir.FilterIndex = 1
        vDir.RestoreDirectory = True
        vDir.CheckFileExists = False
        vDir.CheckPathExists = True

        If vDir.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                '                myStream = vDir.OpenFile()
                '                If (myStream IsNot Nothing) Then
                Dim fi As New FileInfo(vDir.FileName)
                Select Case pilihan
                    Case "btnBrow2" : txtPathKPB1.Text = fi.FullName
                    Case "btnBrow3" : txtPathKPB2.Text = fi.FullName
                    Case "btnBrow4" : txtPathKPB3.Text = fi.FullName
                    Case "btnBrow5" : txtPathKPB4.Text = fi.FullName
                    Case "btnBrow6" : txtPathH2H1.Text = fi.FullName
                End Select

                '                End If
            Catch Ex As Exception
                MessageBox.Show("Invalid Directory " & vbCrLf & "Original error: " & Ex.Message)
            Finally
                ' Check this again, since we need to make sure we didn't throw an exception on open.
                If (myStream IsNot Nothing) Then
                    myStream.Close()
                End If
            End Try
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'cek source
        'If Not Directory.Exists(txtPathSource.Text) Then
        '    MsgBox("Invalid Source Directory", vbInformation, "Error")
        '    txtPathSource.Focus()
        '    Exit Sub
        'End If
        'cek maxrow
        'If Val(txtMaxRow.Text) > 500 Or Val(txtMaxRow.Text) < 5 Then
        '    txtMaxRow.Text = 50
        'End If
        Save_UserSet()
        Me.Close()
    End Sub

    Private Sub Load_UserSet()
        Dim vConn As New OleDbConnection
        vConn.ConnectionString = strcon
        vConn.Open()

        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        pStr = "SELECT TOP 1 * FROM M_UserSetting WHERE User_Name = '" & user & "' ORDER BY ID DESC"

        Try
            cmd = New OleDbCommand(pStr, vConn)
            reader = cmd.ExecuteReader()

            If reader.HasRows Then
                reader.Read()
                txtMaxRow.Text = reader.Item("Set_MaxRows")
            End If
            txtPathKPB1.Text = fileH1toH2_1
            txtPathKPB2.Text = fileH1toH2_2
            txtPathKPB3.Text = fileH1toH2_3
            txtPathKPB4.Text = fileH1toH2_4
            txtPathH2H1.Text = fileH2toH1
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
        reader.Close()
        vConn.Close()

    End Sub

    Private Sub Save_UserSet()

        Dim vConn As New OleDbConnection()
        Dim vStr As String = ""

        vConn.ConnectionString = strcon
        vConn.Open()

        Try
            Dim xm As New Ini(GetAppPath() & "\setting.ini")
            xm.WriteString("Tabulasi", "H1toH21", txtPathKPB1.Text)
            xm.WriteString("Tabulasi", "H1toH22", txtPathKPB2.Text)
            xm.WriteString("Tabulasi", "H1toH23", txtPathKPB3.Text)
            xm.WriteString("Tabulasi", "H1toH24", txtPathKPB4.Text)
            xm.WriteString("Tabulasi", "H2toH1", txtPathH2H1.Text)
            fileH1toH2_1 = txtPathKPB1.Text
            fileH1toH2_2 = txtPathKPB2.Text
            fileH1toH2_3 = txtPathKPB3.Text
            fileH1toH2_4 = txtPathKPB4.Text
            fileH2toH1 = txtPathH2H1.Text
            'vStr = "insert into M_UserSetting (User_Name, Set_SourcePath, Set_DestPath, Set_MaxRows) values ('" _
            '                       & user & "', '" & txtPathSource.Text & "', '" & txtPathDest.Text & "', " & txtMaxRow.Text & ")"

            'cmd = New OleDbCommand(vStr, vConn)
            'cmd.ExecuteNonQuery()

            CatatLog(user, "USER SETTING", "SAVE SUKSES", vStr)
        Catch ex As Exception
            MsgBox("Error Found" & vbCrLf & ex.Message.ToString, vbCritical, "Save Failed")
        End Try

        If vConn.State Then vConn.Close()


    End Sub

End Class