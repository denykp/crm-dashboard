﻿Imports System.Data.OleDb
Module modProperties
    Public servnameasli As String
    Public Const ServerType As String = "mssql"
    Public Const katasandi As String = "ikan"

    Public servname As String
    Public group, user, gudang, pwd, userchallenge As String
    Public printername As String
    Public right_stock, right_hpp, right_alert, right_harga As Boolean
    Public right_hargaManual As Boolean
    Public right_TipeHarga As String
    Public right_export As String

    Public dir As String
    Public printer1 As String, printer2 As String, printer3 As String, printer4 As String
    Public printerkasir As String, printeradmin As String
    Public reportLocation As String
    Public default_kasbesar As String
    Public default_PPN As String
    Public default_servicecharge As String
    Public default_namaperusahaan As String
    Public default_alamatperusahaan As String
    Public default_billfooter As String
    Public default_displaygambarmenu As Boolean
    Public default_orderpassword As Boolean
    Public default_printbill As Boolean
    Public default_maxpembulatan As String
    Public default_pembulatan As String

    Public Function load_setting(keterangan As String) As String
        load_setting = ""
        Using conn As New oledbConnection(strcon)
            conn.Open()
            Using cmd As oledbCommand = New oledbCommand("select * from setting where keterangan='" & keterangan & "'", conn)

                Using reader As oledbDataReader = cmd.ExecuteReader()
                    If reader.Read() Then
                        load_setting = reader.Item(1)
                    End If
                    reader.Dispose()
                End Using
            End Using
            conn.Close()
        End Using


    End Function
End Module
