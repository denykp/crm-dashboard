﻿Imports System.Data.OleDb
Public Class frmLogin
    Private Sub frmLogin_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown ', txtServerName.KeyDown, txtUserName.KeyDown, txtPassword.KeyDown
        If e.KeyCode = Keys.Escape Then Me.Close()
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim conn2 As New oledbConnection()
        Try
            setconnectionstring(servername, dbname)
            conn2.ConnectionString = strcon
            conn2.Open()
            user = txtUserName.Text
            pwd = txtPassword.Text
            cmd = New OleDbCommand("select * from ms_user where username='" & user & "'", conn2)
            reader = cmd.ExecuteReader()


            If reader.Read() Then
                If pwd = reader.Item("password") Then
                    reader.Close()
                    conn2.Close()
                    frmMain.Show()
                    Me.Close()
                Else
                    reader.Close()
                    conn2.Close()
                    MsgBox("password salah")
                    txtPassword.Focus()
                End If
            Else
                reader.Close()
                conn2.Close()
                MsgBox("Username tidak ditemukan")
                txtUserName.Focus()
            End If

        Catch ex As Exception
            MsgBox("login failed, " & ex.Message)
            If conn2.State Then conn2.Close()
        End Try
    End Sub

    Private Sub loadUserRight()

        Dim conn2 As New oledbConnection(strcon)
        conn2.Open()
        Try
            cmd = New oledbCommand("select * from user_group where userid='" & txtUserName.Text & "' and modul='" & modul & "';select * from user_groupmenu gm left join user_group g on gm.nama_group = g.nama_group where g.userid = '" & txtUserName.Text & "'  and g.modul='" & modul & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read Then
                defaultkas = reader.Item("kode_kasbank")
            End If
            reader.NextResult()
            While reader.Read()

                'For Each ti As ToolStripMenuItem In frmMain.MenuStrip1.Items
                '    If ti.Name = reader("key") Then ti.Visible = CBool(reader("value"))
                '    If ti.HasDropDownItems Then enablemenu(ti, reader("key"), reader("value"))
                'Next

                'For Each ti In frmMain.MenuStrip1.Items
                '    If ti.HasDropDownItems Then
                '        reader.Read()
                '        For Each ts As ToolStripItem In ti.DropDownItems
                '            If ts.Name = reader("nama_menu") Then
                '                ts.Enabled = CBool(reader("value"))
                '            End If
                '            reader.Read()
                '        Next
                '    End If
                'Next
                For Each ti In frmMain.MenuStrip1.Items
                    If ti.Name = reader("key") Then
                        ti.visible = CBool(reader("value"))
                        Exit For
                    End If
                    If enablemenu(ti, reader("key"), reader("value")) Then Exit For

                Next
            End While
            reader.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        If conn2.State Then conn2.Close()
    End Sub
    Private Function enablemenu(ts As ToolStripMenuItem, namamenu As String, value As Boolean) As Boolean
        Dim ti As Object
        Dim res As Boolean
        res = False
        Try
            For Each ti In ts.DropDownItems
                If TypeOf (ti) Is ToolStripMenuItem Then
                    If ti.Name = namamenu Then
                        ti.Visible = value
                        res = True
                        Return res
                        'Exit Function
                    End If

                    If TryCast(ti, ToolStripMenuItem).HasDropDownItems Then
                        res = enablemenu(ti, namamenu, value)
                        If res Then
                            Return res
                            'Exit Function
                        End If
                    End If
                End If
                'loadmenu( ts)
            Next
        Catch ex As Exception

        End Try
        Return res
    End Function
    'Private Sub reset_form()
        
    '    txtGroup.Text = ""
    'End Sub

    'Private Sub loadmenu(ByVal ti As ToolStripMenuItem)
    '    For Each ts As ToolStripItem In ti.DropDownItems
    '        tvwMain.Nodes(ti.Name).Nodes.Add(ts.Name, ts.Text)
    '        'loadmenu( ts)
    '    Next
    'End Sub
    Private Sub frmLogin_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp, txtPassword.KeyUp, txtUserName.KeyUp
        If e.KeyCode = Keys.Enter Then e.SuppressKeyPress = True
    End Sub

    Private Sub frmLogin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim xm As New Ini(GetAppPath() & "\setting.ini")
        servername = xm.GetString("Server", "Server name", "")
        dbname = xm.GetString("Server", "Database name", "")
        reportLocation = xm.GetString("Report", "Location", "")

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub


    Private Sub btnSetting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetting.Click
        FrmSettingDB.ShowDialog()
    End Sub
End Class
