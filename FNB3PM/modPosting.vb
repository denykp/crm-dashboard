﻿Imports System.Data.OleDb

Module modPosting
    Public Sub Posting_Pembelian(ByVal no_transaksi As String, ByRef conn2 As oledbConnection, ByRef transaction As oledbTransaction)
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim gudang As String = Nothing
        Dim cara_bayar As Integer
        Dim tanggal As Date
        Dim keterangan As String = Nothing
        Dim kode_kasbank As String = Nothing
        Dim total As Double
        Dim supplier As String = Nothing
        Dim jatuh_tempo As Date
        Dim nofaktur As String = ""
        Dim da As New oledbDataAdapter()
        Dim ds As New DataSet()
        Dim dt As DataTable

        cmd = New oledbCommand("select h.*,m.nama,g.nama_gudang,s.nama_supplier from t_pembelianh h left join ms_kasbank m on h.kode_kasbank = m.kode_kasbank left join ms_gudang g on h.kode_gudang = g.kode_gudang " & _
                             " left join ms_supplier s on h.kode_supplier=s.kode_supplier where nomer_pembelian = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        While reader.Read()
            gudang = reader("kode_gudang")
            cara_bayar = CInt(reader("cara_bayar"))
            tanggal = reader("tanggal")
            keterangan = reader("nama_supplier")
            kode_kasbank = reader("kode_kasbank")
            total = reader("grand_total")
            supplier = reader("kode_supplier")
            jatuh_tempo = reader("jatuh_tempo")
            nofaktur = reader("faktur")
        End While
        reader.Close()

        cmd = New oledbCommand("select d.*,m.nama from t_pembeliand d left join ms_barang m on d.kode_barang = m.kode_barang where nomer_pembelian = '" & no_transaksi & "' order by no_urut", conn2, transaction)
        da.SelectCommand = cmd
        da.Fill(ds, "t_pembeliand")
        dt = ds.Tables("t_pembeliand")
        For i As Integer = 0 To dt.Rows.Count - 1
            insertKartustock(keterangan, no_transaksi, tanggal.ToString("yyyy/MM/dd HH:mm:ss"), dt.Rows(i).Item("kode_barang"), gudang, dt.Rows(i).Item("qty"), "Pembelian", "", conn2, transaction, i + 1, dt.Rows(i).Item("satuan"), dt.Rows(i).Item("qty_pcs"), dt.Rows(i).Item("isi"))
            updatestock(gudang, dt.Rows(i).Item("kode_barang").ToString, dt.Rows(i).Item("qty_pcs"), tanggal, "", conn2, transaction)
            updatehpp(no_transaksi, tanggal, dt.Rows(i).Item("kode_barang").ToString, dt.Rows(i).Item("qty_pcs"), dt.Rows(i).Item("netto_pcs"), dt.Rows(i).Item("no_urut"), "", conn2, transaction)
        Next
        'cmd = New oledbCommand("update hst_hpp set stock_awal =(select isnull(sum(masuk-keluar),0) from kartu_stock where kode_barang=hst_hpp.kode_barang and batch=hst_hpp.batch and tanggal<hst_hpp.tanggal)," & _
        '                     "hpp_awal=(select top 1 hpp from hst_hpp where kode_barang=hst_hpp.kode_barang and batch=hst_hpp.batch and tanggal<hst_hpp.tanggal order by tanggal desc,id desc) " & _
        '                     " where kode_barang in (select kode_barang from t_pembeliand where nomer_pembelian='" & no_transaksi & "') and convert(varchar(20),tanggal,120)>'" & Format(tanggal, "yyyy-MM-dd HH:mm:ss") & "'", conn2, transaction)
        'cmd.ExecuteNonQuery()
        'Dim query As String
        'query = "update ms_barang set hpp=(select top 1 hpp from hst_hpp where kode_barang=ms_barang.kode_barang order by tanggal desc,id desc) where kode_barang in (select kode_barang from t_pembeliand where nomer_pembelian='" & no_transaksi & "');"
        ' query += "update t_billd set hpp= " & _
        '     "isnull((select top 1 hpp from hst_hpp where kode_barang=t_billd.kode_barang and tanggal<(select tanggal from t_billh where nomer_bill=t_billd.nomer_bill)" & _
        '     "order by tanggal desc,id desc),0) where nomer_bill in (select nomer_bill from t_billh where  convert(varchar(20),tanggal,120)>'" & Format(tanggal, "yyyy-MM-dd HH:mm:ss") & "') and  kode_barang in (select kode_barang from t_pembeliand where nomer_pembelian='" & no_transaksi & "')"
        'cmd = New oledbCommand(query, conn2, transaction)
        'cmd.ExecuteNonQuery()

        If cara_bayar = 0 Then
            insertbukukas("Pembelian" & " " & keterangan, no_transaksi, tanggal.ToString("yyyy/MM/dd HH:mm:ss"), kode_kasbank, CDbl(total) * -1, "Pembelian", conn2, transaction)
            cmd = New oledbCommand("update ms_kasbank set saldo = saldo - " & CDbl(total) & " where kode_kasbank = '" & kode_kasbank & "' ", conn2, transaction)
            cmd.ExecuteNonQuery()
        Else
            cmd = New oledbCommand("insert into list_hutang(nomer_transaksi,nomer_faktur,kode_supplier,tipe,tanggal,tanggal_jatuhtempo,jumlah,uang_muka,sudah_bayar,[status],koreksi) values('" & no_transaksi & "','" & nofaktur & "','" & supplier & "','hutang','" & tanggal.ToString("yyyy/MM/dd HH:mm:ss") & "','" & jatuh_tempo.ToString("yyyy/MM/dd") & "','" & CDbl(total) & "',0,0,0,0)", conn2, transaction)
            cmd.ExecuteNonQuery()
            insertkartuHutang("Pembelian", no_transaksi, tanggal, supplier, CDbl(total), conn2, transaction, keterangan)
        End If
    End Sub

    Public Sub Posting_Opname(ByVal no_transaksi As String, ByRef conn2 As oledbConnection, ByRef transaction As oledbTransaction)

        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim gudang As String = Nothing
        Dim tanggal As Date
        Dim keterangan As String = Nothing
        Dim da As New oledbDataAdapter()
        Dim ds As New DataSet()
        Dim dt As DataTable

        cmd = New oledbCommand("select * from t_opnamestockh where nomer_opnamestock = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        While reader.Read()
            gudang = reader("kode_gudang")
            tanggal = reader("tanggal")
            keterangan = reader("keterangan")
        End While
        reader.Close()

        cmd = New oledbCommand("select d.*,m.satuan as satuankecil from t_opnamestockd d inner join ms_barang m  on d.kode_barang=m.kode_barang where nomer_opnamestock = '" & no_transaksi & "' order by no_urut", conn2, transaction)
        da.SelectCommand = cmd
        da.Fill(ds, "t_opnamestockd")
        dt = ds.Tables("t_opnamestockd")
        For i As Integer = 0 To dt.Rows.Count - 1
            insertKartustock(keterangan, no_transaksi, tanggal.ToString("yyyy/MM/dd HH:mm:ss"), dt.Rows(i).Item("kode_barang"), gudang, dt.Rows(i).Item("selisih"), "Opname", "", conn2, transaction, i + 1, dt.Rows(i).Item("satuankecil"), dt.Rows(i).Item("selisih"), 1)
            updatestock(gudang, dt.Rows(i).Item("kode_barang"), dt.Rows(i).Item("selisih"), tanggal, "", conn2, transaction)
        Next

        cmd = New oledbCommand("update t_opnamestockh set status_approve = '1' where nomer_opnamestock = '" & no_transaksi & "';" & _
                             "update stock set stock=(select isnull(sum(masukpcs-keluarpcs),0) from kartu_stock where kode_barang=stock.kode_barang and kode_gudang=stock.kode_gudang and batch=stock.batch)", conn2, transaction)
        cmd.ExecuteNonQuery()

        If reader IsNot Nothing Then reader.Close()
    End Sub

    Public Sub Posting_Koreksi(ByVal no_transaksi As String, ByRef conn2 As oledbConnection, ByRef transaction As oledbTransaction)
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim gudang As String = Nothing
        Dim tanggal As Date
        Dim keterangan As String = Nothing
        Dim da As New oledbDataAdapter()
        Dim ds As New DataSet()
        Dim dt As DataTable

        cmd = New oledbCommand("select * from t_koreksistockh where nomer_koreksi = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        While reader.Read()
            gudang = reader("kode_gudang")
            tanggal = reader("tanggal")
            keterangan = reader("keterangan")
        End While
        reader.Close()

        cmd = New oledbCommand("select * from t_koreksistockd where nomer_koreksi = '" & no_transaksi & "' order by no_urut", conn2, transaction)
        da.SelectCommand = cmd
        da.Fill(ds, "t_koreksistockd")
        dt = ds.Tables("t_koreksistockd")
        For i As Integer = 0 To dt.Rows.Count - 1
            insertKartustock(keterangan, no_transaksi, tanggal.ToString("yyyy/MM/dd HH:mm:ss"), dt.Rows(i).Item("kode_barang"), gudang, dt.Rows(i).Item("qty"), "Koreksi", "", conn2, transaction, i + 1, dt.Rows(i).Item("satuan"), dt.Rows(i).Item("qty_pcs"), dt.Rows(i).Item("isi"))
            updatestock(gudang, dt.Rows(i).Item("kode_barang"), dt.Rows(i).Item("qty_pcs"), tanggal, "", conn2, transaction)
        Next

    End Sub

    Public Sub Posting_ReturBeli(ByVal no_transaksi As String, ByRef conn2 As oledbConnection, ByRef transaction As oledbTransaction)
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim gudang As String = Nothing
        Dim tanggal As Date
        Dim keterangan As String = Nothing
        Dim tipe As String = Nothing
        Dim kode_kasbank As String = ""
        Dim total As Double
        Dim kode_supplier As String = ""
        Dim no_nota As String = ""
        Dim nota_lain As String = ""
        Dim da As New oledbDataAdapter()
        Dim ds As New DataSet()
        Dim dt As DataTable

        cmd = New oledbCommand("select h.*,m.nama_supplier from t_returbelih h inner join ms_supplier m on h.kode_supplier=m.kode_supplier where nomer_returbeli = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        While reader.Read()
            gudang = reader("kode_gudang")
            tanggal = reader("tanggal")
            keterangan = reader("nama_supplier")
            tipe = reader("tipe_tukar")
            kode_kasbank = reader("kode_kasbank")
            total = CDbl(reader("total"))
            gudang = reader("kode_gudang")
            no_nota = reader("nota")
            kode_supplier = reader("kode_supplier")
            nota_lain = reader("nota_lain")
        End While
        reader.Close()

        If tipe = "Tukar Tunai" Then
            insertbukukas("Retur Beli" & " " & keterangan, no_transaksi, tanggal.ToString("yyyy/MM/dd HH:mm:ss"), kode_kasbank, total, "Retur Beli", conn2, transaction)
            cmd = New oledbCommand("update ms_kasbank set saldo = saldo + " & total & " where kode_kasbank = '" & kode_kasbank & "' ", conn2, transaction)
            cmd.ExecuteNonQuery()
        ElseIf tipe = "Pengurangan Tagihan" Then
            cmd = New oledbCommand("update list_hutang set koreksi = -(select isnull(sum(total),0) from t_returbelih where nota=list_hutang.nomer_transaksi and tipe_tukar='Pengurangan Tagihan' ) where nomer_transaksi = '" & no_nota & "' ", conn2, transaction)
            cmd.ExecuteNonQuery()
            insertkartuHutang("Retur Beli", no_transaksi, tanggal, kode_supplier, -CDbl(total), conn2, transaction, keterangan)
        ElseIf tipe = "Pengurangan Tagihan Lain" Then
            'cmd = New oledbCommand("insert into list_piutang_retur values('" & kode_supplier & "','" & no_transaksi & "','" & total & "',0,0)", conn2, transaction)
            cmd = New oledbCommand("insert into list_hutang(nomer_transaksi,nomer_faktur,kode_supplier,tipe,tanggal,tanggal_jatuhtempo,jumlah,uang_muka,sudah_bayar,[status],koreksi) values " & _
                                 "('" & no_transaksi & "','','" & kode_supplier & "','Retur','" & tanggal.ToString("yyyy/MM/dd HH:mm:ss") & "','" & tanggal.ToString("yyyy/MM/dd HH:mm:ss") & "','-" & CDbl(total) & "',0,0,0,0)", conn2, transaction)
            cmd.ExecuteNonQuery()
            insertkartuHutang("Retur Beli", no_transaksi, tanggal, kode_supplier, -CDbl(total), conn2, transaction, keterangan)
        End If

        cmd = New oledbCommand("select * from t_returbelid where nomer_returbeli = '" & no_transaksi & "' order by no_urut", conn2, transaction)
        da.SelectCommand = cmd
        da.Fill(ds, "t_returbelid")
        dt = ds.Tables("t_returbelid")
        For i As Integer = 0 To dt.Rows.Count - 1
            insertKartustock(keterangan, no_transaksi, tanggal.ToString("yyyy/MM/dd HH:mm:ss"), dt.Rows(i).Item("kode_barang"), gudang, dt.Rows(i).Item("qty") * -1, "Retur Beli", "", conn2, transaction, i + 1, dt.Rows(i).Item("satuan"), dt.Rows(i).Item("qty_pcs") * -1, dt.Rows(i).Item("isi"))
            updatestock(gudang, dt.Rows(i).Item("kode_barang"), dt.Rows(i).Item("qty_pcs") * -1, tanggal, "", conn2, transaction)
        Next

    End Sub

    Public Sub Posting_MutasiStock(ByVal no_transaksi As String, ByRef conn2 As oledbConnection, ByRef transaction As oledbTransaction)
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim gudang_asal As String = Nothing
        Dim gudang_tujuan As String = Nothing
        Dim tanggal As Date
        Dim keterangan As String = Nothing
        Dim da As New oledbDataAdapter()
        Dim ds As New DataSet()
        Dim dt As DataTable

        cmd = New oledbCommand("select * from t_mutasistockh where nomer_mutasistock = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        While reader.Read()
            gudang_asal = reader("gudang_asal")
            gudang_tujuan = reader("gudang_tujuan")
            tanggal = reader("tanggal")
            keterangan = reader("keterangan")
        End While
        reader.Close()

        cmd = New oledbCommand("select * from t_mutasistockd where nomer_mutasistock = '" & no_transaksi & "' order by no_urut", conn2, transaction)
        da.SelectCommand = cmd
        da.Fill(ds, "t_mutasistockd")
        dt = ds.Tables("t_mutasistockd")
        For i As Integer = 0 To dt.Rows.Count - 1
            insertKartustock(keterangan, no_transaksi, tanggal.ToString("yyyy/MM/dd HH:mm:ss"), dt.Rows(i).Item("kode_barang"), gudang_asal, dt.Rows(i).Item("qty") * -1, "Mutasi", "", conn2, transaction, i + 1, dt.Rows(i).Item("satuan"), dt.Rows(i).Item("qty_pcs") * -1, dt.Rows(i).Item("isi"))
            insertKartustock(keterangan, no_transaksi, tanggal.ToString("yyyy/MM/dd HH:mm:ss"), dt.Rows(i).Item("kode_barang"), gudang_tujuan, dt.Rows(i).Item("qty"), "Mutasi", "", conn2, transaction, i + 1, dt.Rows(i).Item("satuan"), dt.Rows(i).Item("qty_pcs"), dt.Rows(i).Item("isi"))
            updatestock(gudang_asal, dt.Rows(i).Item("kode_barang"), dt.Rows(i).Item("qty_pcs") * -1, tanggal, "", conn2, transaction)
            updatestock(gudang_tujuan, dt.Rows(i).Item("kode_barang"), dt.Rows(i).Item("qty_pcs"), tanggal, "", conn2, transaction)
        Next

    End Sub

    Public Sub Posting_Pemakaian(ByVal no_transaksi As String, ByRef conn2 As oledbConnection, ByRef transaction As oledbTransaction)
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim gudang As String = Nothing
        Dim tanggal As Date
        Dim keterangan As String = Nothing
        Dim da As New oledbDataAdapter()
        Dim ds As New DataSet()
        Dim dt As DataTable

        cmd = New oledbCommand("select * from t_pemakaianstockh where nomer_pemakaianstock = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        While reader.Read()
            gudang = reader("kode_gudang")
            tanggal = reader("tanggal")
            keterangan = reader("keterangan")
        End While
        reader.Close()

        cmd = New oledbCommand("select * from t_pemakaianstockd where nomer_pemakaianstock = '" & no_transaksi & "' order by no_urut", conn2, transaction)
        da.SelectCommand = cmd
        da.Fill(ds, "t_pemakaianstockd")
        dt = ds.Tables("t_pemakaianstockd")
        For i As Integer = 0 To dt.Rows.Count - 1
            insertKartustock(keterangan, no_transaksi, tanggal.ToString("yyyy/MM/dd HH:mm:ss"), dt.Rows(i).Item("kode_barang"), gudang, dt.Rows(i).Item("qty") * -1, "Pemakaian", "", conn2, transaction, i + 1, dt.Rows(i).Item("satuan"), dt.Rows(i).Item("qty_pcs") * -1, dt.Rows(i).Item("isi"))
            updatestock(gudang, dt.Rows(i).Item("kode_barang"), dt.Rows(i).Item("qty_pcs") * -1, tanggal, "", conn2, transaction)
        Next

    End Sub

    Public Sub Posting_Konversi(ByVal no_transaksi As String, ByRef conn2 As oledbConnection, ByRef transaction As oledbTransaction)
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim gudang_bahan As String = Nothing
        Dim gudang_hasil As String = Nothing
        Dim tanggal As Date
        Dim keterangan As String = Nothing
        Dim da As New oledbDataAdapter()
        Dim ds As New DataSet()
        Dim dt As DataTable

        cmd = New oledbCommand("select * from t_konversistockh where nomer_konversi = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        While reader.Read()
            gudang_bahan = reader("kode_gudang_bahan")
            gudang_hasil = reader("kode_gudang_hasil")
            tanggal = reader("tanggal")
            keterangan = reader("keterangan")
            updatestock(gudang_hasil, reader("kode_barang"), reader("qty_pcs"), tanggal, "", conn2, transaction)
            insertKartustock(keterangan, no_transaksi, tanggal.ToString("yyyy/MM/dd HH:mm:ss"), reader("kode_barang"), gudang_hasil, reader("qty"), "Hasil Konversi", "", conn2, transaction, 1, reader("satuan"), reader("qty_pcs"), reader("isi"))
        End While
        reader.Close()

        cmd = New oledbCommand("select * from t_konversistockd where nomer_konversi = '" & no_transaksi & "' order by urut", conn2, transaction)
        da.SelectCommand = cmd
        da.Fill(ds, "t_konversistockd")
        dt = ds.Tables("t_konversistockd")
        For i As Integer = 0 To dt.Rows.Count - 1
            insertKartustock(keterangan, no_transaksi, tanggal.ToString("yyyy/MM/dd HH:mm:ss"), dt.Rows(i).Item("kode_barang"), gudang_bahan, dt.Rows(i).Item("qty") * -1, "Bahan Konversi", "", conn2, transaction, i + 1, dt.Rows(i).Item("satuan"), dt.Rows(i).Item("qty_pcs") * -1, dt.Rows(i).Item("isi"))
            updatestock(gudang_bahan, dt.Rows(i).Item("kode_barang"), dt.Rows(i).Item("qty_pcs") * -1, tanggal, "", conn2, transaction)
        Next

    End Sub

    Public Sub Posting_StockAwal(ByVal no_transaksi As String, ByRef conn2 As oledbConnection, ByRef transaction As oledbTransaction)
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim gudang As String = Nothing
        Dim tanggal As Date
        Dim keterangan As String = Nothing
        Dim da As New oledbDataAdapter()
        Dim ds As New DataSet()
        Dim dt As DataTable

        cmd = New oledbCommand("select * from t_stockawalh where nomer_stockawal = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        While reader.Read()
            gudang = reader("kode_gudang")
            tanggal = reader("tanggal")
            keterangan = reader("keterangan")
        End While
        reader.Close()

        cmd = New oledbCommand("select * from t_stockawald where nomer_stockawal = '" & no_transaksi & "' order by no_urut", conn2, transaction)
        da.SelectCommand = cmd
        da.Fill(ds, "t_stockawald")
        dt = ds.Tables("t_stockawald")
        For i As Integer = 0 To dt.Rows.Count - 1
            insertKartustock(keterangan, no_transaksi, tanggal.ToString("yyyy/MM/dd HH:mm:ss"), dt.Rows(i).Item("kode_barang"), gudang, dt.Rows(i).Item("qty"), "Stock Awal", "", conn2, transaction, i + 1, dt.Rows(i).Item("satuan"), dt.Rows(i).Item("qty_pcs"), dt.Rows(i).Item("isi"))
            updatestock(gudang, dt.Rows(i).Item("kode_barang"), dt.Rows(i).Item("qty_pcs"), tanggal, "", conn2, transaction)
        Next

        cmd = New oledbCommand("update t_stockawalh set status_approve = 'true' where nomer_stockawal = '" & no_transaksi & "'", conn2, transaction)
        cmd.ExecuteNonQuery()
    End Sub
    Public Function Posting_kaskeluar(ByVal no_transaksi As String, ByRef conn As oledbConnection, ByRef transaction As oledbTransaction) As Boolean
        Dim cmd As oledbCommand
        Dim dr As oledbDataReader = Nothing

        Dim tanggal As Date
        Dim jenis As String = ""
        Dim carabayar As String
        Dim keterangan As String = ""

        Dim penerima As String

        Posting_kaskeluar = False
        cmd = New oledbCommand("select * from t_kaskeluarh where nomer_kaskeluar='" & no_transaksi & "'", conn, transaction)
        dr = cmd.ExecuteReader()
        If dr.Read() Then
            tanggal = dr("tanggal")
            carabayar = dr("carabayar")
            jenis = dr("jenis")
            penerima = dr("penerima")
            keterangan = dr("keterangan")
        End If
        dr.Close()
        cmd = New oledbCommand("select t.kode_kasbank,t.jumlah,m.saldo from t_kaskeluar_kas t inner join ms_kasbank m on t.kode_kasbank=m.kode_kasbank where nomer_kaskeluar='" & no_transaksi & "'", conn, transaction)
        dr = cmd.ExecuteReader
        While dr.Read
            'If dr("saldo") < dr("jumlah") Then
            '    MsgBox("Saldo kas " & dr("kode_kasbank") & " tidak mencukupi")
            '    dr.Close()
            '    Exit Function
            'End If

            updatekas(dr("kode_kasbank"), -dr("jumlah"), conn, transaction)
            insertbukukas(keterangan, no_transaksi, tanggal, dr("kode_kasbank"), -dr("jumlah"), jenis, conn, transaction)
            'insertkartuPiutang "Bayar piutang (Cash)", NoTransaksi, tanggal, Kodesupplier, -rs("jumlah"), conn

        End While
        dr.Close()
        cmd = New oledbCommand("select * from t_kaskeluar_hutang where nomer_kaskeluar='" & no_transaksi & "'", conn, transaction)
        dr = cmd.ExecuteReader
        If dr.Read Then
            insertkartuHutang(jenis, no_transaksi, tanggal, dr("kode_supplier"), dr("hutang"), conn, transaction, keterangan)
            cmd = New oledbCommand("insert into list_hutang (kode_supplier,nomer_transaksi,nomer_faktur,jumlah,uang_muka,sudah_bayar,koreksi,tanggal,tanggal_jatuhtempo,tipe) values " & _
                "('" & dr("kode_supplier") & "','" & no_transaksi & "','" & dr("nomer_faktur") & "','" & dr("hutang") & "',0,0,0,'" & Format(tanggal, "yyyy/MM/dd") & "','" & Format(dr("tanggal_jatuhtempo"), "yyyy/MM/dd") & "','" & jenis & "')", conn, transaction)
            cmd.ExecuteNonQuery()
            cmd = New oledbCommand("update ms_supplier set saldo_hutang=(select sum(sisa_hutang) from list_hutang where kode_supplier=ms_supplier.kode_supplier) where kode_supplier='" & dr("kode_supplier") & "'", conn, transaction)
            cmd.ExecuteNonQuery()
        End If
        dr.Close()

        Select Case jenis

            Case "Pembayaran Hutang"
                cmd = New oledbCommand("select * from t_kaskeluar_bayarhutang j inner join list_hutang t on j.nomer_beli=t.nomer_transaksi where nomer_kaskeluar='" & no_transaksi & "'", conn, transaction)
                dr = cmd.ExecuteReader
                While dr.Read
                    'updatehutangbayar(dr("nomer_beli, dr("kode_supplier, dr("jumlah_bayar, jenis, tanggal, conn, pc)
                    cmd = New oledbCommand("update list_hutang set sudah_bayar=(select isnull(sum(jumlah_bayar),0) from t_kaskeluar_bayarhutang where nomer_beli=list_hutang.nomer_transaksi) " & _
                            " where sudah_bayar<>(select isnull(sum(jumlah_bayar),0) from t_kaskeluar_bayarhutang where nomer_beli=list_hutang.nomer_transaksi)", conn, transaction)
                    cmd.ExecuteNonQuery()
                    insertkartuHutang(jenis, no_transaksi, tanggal, dr("kode_supplier"), dr("jumlah_bayar") * -1, conn, transaction)

                End While
                dr.Close()
            Case "Pembayaran Gaji"
                cmd = New oledbCommand("select * from t_kaskeluar_gaji j inner join ms_gaji t on j.kode_gaji=t.kode_gaji and t.kasbon='True' where nomer_kaskeluar='" & no_transaksi & "'", conn, transaction)
                dr = cmd.ExecuteReader
                While dr.Read
                    cmd = New oledbCommand("update ms_karyawan set kasbon=kasbon+" & dr("jumlah") & " where nik='" & dr("NIK") & "'", conn, transaction)
                    cmd.ExecuteNonQuery()
                    insertbukuKasbon(dr("keterangan"), no_transaksi, tanggal, dr("NIK"), CDbl(dr("jumlah")), conn, transaction)
                End While
            Case "Kasbon"
                'conn.Execute "update list_kasbon set total_bayar=total_bayar+"&&" where "
                cmd = New oledbCommand("select * from t_kaskeluar_kasbon  where nomer_kaskeluar='" & no_transaksi & "'", conn, transaction)
                dr = cmd.ExecuteReader
                If dr.Read Then
                    cmd = New oledbCommand("update ms_karyawan set kasbon=kasbon+" & dr("jumlah") & " where nik='" & dr("NIK") & "'", conn, transaction)
                    cmd.ExecuteNonQuery()
                    insertbukuKasbon(dr("keterangan"), no_transaksi, tanggal, dr("NIK"), CDbl(dr("jumlah")), conn, transaction)
                End If
                dr.Close()
            Case "Mutasi Kas"
                cmd = New oledbCommand("select * from t_kaskeluar_mutasi  where nomer_kaskeluar='" & no_transaksi & "'", conn, transaction)
                dr = cmd.ExecuteReader
                If dr.Read Then
                    updatekas(dr("kode_kasbank"), dr("jumlah"), conn, transaction)
                    insertbukukas(keterangan, no_transaksi, tanggal, dr("kode_kasbank"), dr("jumlah"), jenis, conn, transaction)
                End If
                dr.Close()
        End Select
        Posting_kaskeluar = True
    End Function
    Public Function Posting_kasmasuk(ByVal no_transaksi As String, ByRef conn As oledbConnection, ByRef transaction As oledbTransaction) As Boolean
        Dim cmd As oledbCommand
        Dim dr As oledbDataReader = Nothing

        Dim tanggal As Date
        Dim jenis As String = ""
        Dim carabayar As String
        Dim keterangan As String = ""

        Dim pembayar As String

        Posting_kasmasuk = False
        cmd = New oledbCommand("select * from t_kasmasukh where nomer_kasmasuk='" & no_transaksi & "'", conn, transaction)
        dr = cmd.ExecuteReader()
        If dr.Read() Then
            tanggal = dr("tanggal")
            carabayar = dr("carabayar")
            jenis = dr("jenis")
            pembayar = dr("pembayar")
            keterangan = dr("keterangan")
        End If
        dr.Close()
        cmd = New oledbCommand("select t.kode_kasbank,t.jumlah,m.saldo from t_kasmasuk_kas t inner join ms_kasbank m on t.kode_kasbank=m.kode_kasbank where nomer_kasmasuk='" & no_transaksi & "'", conn, transaction)
        dr = cmd.ExecuteReader
        While dr.Read
            'If dr("saldo") < dr("jumlah") Then
            '    MsgBox("Saldo kas " & dr("kode_kasbank") & " tidak mencukupi")
            '    dr.Close()
            '    Exit Function
            'End If

            updatekas(dr("kode_kasbank"), dr("jumlah"), conn, transaction)
            insertbukukas(keterangan, no_transaksi, tanggal, dr("kode_kasbank"), dr("jumlah"), jenis, conn, transaction)
            'insertkartuPiutang("Bayar piutang (Cash)", NoTransaksi, tanggal, Kodecustomer, -rs("jumlah"), conn)

        End While
        dr.Close()
        cmd = New oledbCommand("select * from t_kasmasuk_piutang where nomer_kasmasuk='" & no_transaksi & "'", conn, transaction)
        dr = cmd.ExecuteReader
        If dr.Read Then
            insertkartuPiutang(jenis, no_transaksi, tanggal, dr("kode_customer"), dr("piutang"), conn, transaction, keterangan)
            cmd = New oledbCommand("insert into list_piutang (kode_customer,nomer_transaksi,nomer_faktur,jumlah,uang_muka,sudah_bayar,koreksi,tanggal,tanggal_jatuhtempo,tipe) values " & _
                "('" & dr("kode_customer") & "','" & no_transaksi & "','" & dr("nomer_faktur") & "','" & dr("piutang") & "',0,0,0,'" & Format(tanggal, "yyyy/MM/dd") & "','" & Format(dr("tanggal_jatuhtempo"), "yyyy/MM/dd") & "','" & jenis & "')", conn, transaction)
            cmd.ExecuteNonQuery()
            'cmd = New oledbCommand("update ms_customer set saldo_piutang=(select sum(sisa_piutang) from list_piutang where kode_customer=ms_customer.kode_customer) where kode_customer='" & dr("kode_customer") & "'", conn, transaction)
            'cmd.ExecuteNonQuery()
        End If
        dr.Close()

        Select Case jenis

            Case "Pembayaran Piutang"
                cmd = New oledbCommand("select * from t_kasmasuk_bayarpiutang j inner join list_piutang t on j.nomer_jual=t.nomer_transaksi where nomer_kasmasuk='" & no_transaksi & "'", conn, transaction)
                dr = cmd.ExecuteReader
                While dr.Read
                    'updatepiutangbayar(dr("nomer_beli, dr("kode_customer, dr("jumlah_bayar, jenis, tanggal, conn, pc)

                    insertkartuPiutang(jenis, no_transaksi, tanggal, dr("kode_customer"), dr("jumlah_bayar") * -1, conn, transaction)
                    'cmd = New oledbCommand("update ms_customer set saldo_piutang=(select sum(sisa_piutang) from list_piutang where kode_customer=ms_customer.kode_customer) where kode_customer='" & dr("kode_customer") & "'", conn, transaction)
                    'cmd.ExecuteNonQuery()
                End While
                dr.Close()
                cmd = New oledbCommand("update list_piutang set sudah_bayar=(select isnull(sum(jumlah_bayar),0) from t_kasmasuk_bayarpiutang where nomer_jual=nomer_transaksi)", conn, transaction)
                cmd.ExecuteNonQuery()


            Case "Cicilan Kasbon"
                'conn.Execute "update list_kasbon set total_bayar=total_bayar+"&&" where "
                cmd = New oledbCommand("select * from t_kasmasuk_cicilankasbon  where nomer_kasmasuk='" & no_transaksi & "'", conn, transaction)
                dr = cmd.ExecuteReader
                While dr.Read
                    cmd = New oledbCommand("update ms_karyawan set kasbon=kasbon-" & dr("jumlah") & " where nik='" & dr("NIK") & "'", conn, transaction)
                    cmd.ExecuteNonQuery()
                    insertbukuKasbon(dr("keterangan"), no_transaksi, tanggal, dr("NIK"), -CDbl(dr("jumlah")), conn, transaction)
                End While
                dr.Close()
            Case "Mutasi Kas"
                cmd = New oledbCommand("select * from t_kasmasuk_mutasi  where nomer_kasmasuk='" & no_transaksi & "'", conn, transaction)
                dr = cmd.ExecuteReader
                If dr.Read Then
                    updatekas(dr("kode_kasbank"), -dr("jumlah"), conn, transaction)
                    insertbukukas(keterangan, no_transaksi, tanggal, dr("kode_kasbank"), -dr("jumlah"), jenis, conn, transaction)
                End If
                dr.Close()
        End Select
        Posting_kasmasuk = True
    End Function
    Public Sub urutkan_bukukas()
        Dim conn As New oledbConnection(strcon)
        Dim da As New oledbDataAdapter

        Dim cmd As oledbCommand
        Dim dr As oledbDataReader
        Dim saldoawal As Decimal
        conn.Open()
        Dim builder As New oledbCommandBuilder(da)
        cmd = New oledbCommand("select * from ms_kasbank", conn)
        dr = cmd.ExecuteReader
        While dr.Read
            'MsgBox("select * from kartu_kas where kode_kasbank='" & dr("kode_kasbank") & "' order by tanggal,urut")
            cmd = New oledbCommand("select * from kartu_kas where kode_kasbank='" & dr("kode_kasbank") & "' order by tanggal,urut", conn)
            da.SelectCommand = cmd
            Dim ds As New DataSet
            da.FillSchema(ds, SchemaType.Source, "kartu_kas")
            da.Fill(ds, "kartu_kas")

            saldoawal = 0


            For i = 0 To ds.Tables(0).Rows.Count - 1
                With ds.Tables(0).Rows(i)
                    .BeginEdit()
                    .Item("saldo_awal") = saldoawal
                    .EndEdit()
                    saldoawal = saldoawal + .Item("masuk") - .Item("keluar")

                End With
            Next
            builder.GetUpdateCommand()
            da.Update(ds, "kartu_kas")

        End While

        dr.Close()
        MsgBox("selesai")
        conn.Close()
    End Sub
    Public Sub urutkan_kartustock()
        Dim conn As New oledbConnection(strcon)
        Dim da As New oledbDataAdapter

        Dim cmd As oledbCommand
        Dim dr As oledbDataReader
        Dim saldoawal As Decimal
        conn.Open()
        Dim builder As New oledbCommandBuilder(da)
        cmd = New oledbCommand("select kode_gudang,m.kode_barang from ms_gudang, ms_barang m inner join ms_tipe t on m.kode_tipe=t.kode_tipe order by kode_barang", conn)
        dr = cmd.ExecuteReader
        While dr.Read
            'MsgBox("select * from kartu_kas where kode_kasbank='" & dr("kode_kasbank") & "' order by tanggal,urut")
            cmd = New oledbCommand("select * from kartu_stock where kode_barang='" & dr("kode_barang") & "' and kode_gudang='" & dr("kode_gudang") & "' order by tanggal,urut", conn)
            da.SelectCommand = cmd
            Dim ds As New DataSet
            da.FillSchema(ds, SchemaType.Source, "kartu_stock")
            da.Fill(ds, "kartu_stock")

            saldoawal = 0

            For i = 0 To ds.Tables(0).Rows.Count - 1
                With ds.Tables(0).Rows(i)
                    .BeginEdit()
                    .Item("stockawal") = saldoawal
                    .EndEdit()
                    saldoawal = saldoawal + .Item("masukpcs") - .Item("keluarpcs")

                End With
            Next
            builder.GetUpdateCommand()
            da.Update(ds, "kartu_stock")

        End While

        dr.Close()
        MsgBox("selesai")
        conn.Close()
    End Sub
    Public Sub urutkan_hpp()
        Dim conn As New oledbConnection(strcon)
        Dim da As New oledbDataAdapter

        Dim cmd As oledbCommand
        Dim dr As oledbDataReader

        Dim hpp As Decimal
        conn.Open()
        Dim builder As New oledbCommandBuilder(da)
        cmd = New oledbCommand("update hst_hpp set stock_awal =(select isnull(sum(masuk-keluar),0) from kartu_stock where kode_barang=hst_hpp.kode_barang and batch=hst_hpp.batch and tanggal<hst_hpp.tanggal)", conn)
        cmd.ExecuteNonQuery()
        cmd = New oledbCommand("select m.kode_barang from  ms_barang m inner join ms_tipe t on m.kode_tipe=t.kode_tipe order by kode_barang", conn)
        dr = cmd.ExecuteReader
        While dr.Read
            hpp = 0
            'MsgBox("select * from kartu_kas where kode_kasbank='" & dr("kode_kasbank") & "' order by tanggal,urut")
            cmd = New oledbCommand("select * from hst_hpp where kode_barang='" & dr("kode_barang") & "' order by tanggal,id,batch", conn)
            da.SelectCommand = cmd
            Dim ds As New DataSet
            da.FillSchema(ds, SchemaType.Source, "hst_hpp")
            da.Fill(ds, "hst_hpp")

            For i = 0 To ds.Tables(0).Rows.Count - 1
                With ds.Tables(0).Rows(i)
                    .BeginEdit()
                    .Item("hpp_awal") = hpp
                    .EndEdit()
                    If (.Item("qty_in") + .Item("stock_awal")) > 0 Then
                        hpp = ((.Item("stock_awal") * hpp) + (.Item("qty_in") * .Item("harga_in"))) / (.Item("qty_in") + .Item("stock_awal"))
                    Else
                        hpp = .Item("harga_in")
                    End If
                    cmd = New oledbCommand("update hpp set hpp ='" & hpp & "' where kode_barang='" & dr("kode_barang") & "' and batch='" & .Item("batch") & "'", conn)
                    cmd.ExecuteNonQuery()

                End With
            Next
            builder.GetUpdateCommand()
            da.Update(ds, "hst_hpp")

        End While

        dr.Close()

        MsgBox("selesai")
        conn.Close()
    End Sub
    Public Sub update_hppresep()
        executeSQL("UPDATE  hpp set hpp=(select isnull(sum(qty*h.hpp),0) from ms_barang_resep b inner join hpp h on b.kode_bahan=h.kode_barang where b.kode_barang=t1.kode_barang) " & _
                    "FROM hpp t1 left JOIN ms_barang t2 ON t1.kode_barang = t2.kode_barang left join ms_tipe t3 on t2.kode_tipe=t3.kode_tipe  " & _
                    "WHERE t1.kode_barang = t2.kode_barang and t3.kode_tipe in (select kode_tipe from ms_tipe where resep=1) ")
    End Sub

    Public Sub Posting_produksi(ByVal no_transaksi As String, ByRef conn2 As oledbConnection, ByRef transaction As oledbTransaction)
        Dim cmd, cmd2 As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim gudangbahan As String = Nothing
        Dim gudanghasil As String = Nothing
        Dim tanggal As Date
        Dim keterangan As String = Nothing
        Dim da As New oledbDataAdapter()
        Dim ds As New DataSet()
        Dim dt, dt1 As DataTable

        cmd = New oledbCommand("select * from t_produksih where nomer_produksihasil = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        While reader.Read()
            gudangbahan = reader("kode_gudangbahan")
            gudanghasil = reader("kode_gudanghasil")
            tanggal = reader("tanggal")
            keterangan = reader("keterangan")
        End While
        reader.Close()

        'nambah stock jadi
        cmd = New oledbCommand("select * from t_produksi_hasil a left join ms_barang m on a.kode_barang=m.kode_barang where a.nomer_produksihasil = '" & no_transaksi & "'", conn2, transaction)
        da.SelectCommand = cmd
        da.Fill(ds, "t_produksi_hasil")
        dt = ds.Tables("t_produksi_hasil")
        For i As Integer = 0 To dt.Rows.Count - 1
            insertKartustock(keterangan, no_transaksi, tanggal.ToString("yyyy/MM/dd HH:mm:ss"), dt.Rows(i).Item("kode_barang"), gudanghasil, dt.Rows(i).Item("qty"), "Produksi Hasil", dt.Rows(i).Item("batch"), conn2, transaction, i + 1, dt.Rows(i).Item("satuan"), dt.Rows(i).Item("qty_pcs"), dt.Rows(i).Item("isi"))
            updatestock(gudanghasil, dt.Rows(i).Item("kode_barang"), dt.Rows(i).Item("qty_pcs"), tanggal, dt.Rows(i).Item("batch"), conn2, transaction)
            updatehpp(no_transaksi, tanggal, dt.Rows(i).Item("kode_barang").ToString, dt.Rows(i).Item("qty_pcs"), dt.Rows(i).Item("harga"), dt.Rows(i).Item("no_urut"), dt.Rows(i).Item("batch"), conn2, transaction)
        Next
        'kurang stock bahan
        cmd2 = New oledbCommand("select * " & _
                              " from t_produksi_bahan a left join ms_barang m on a.kode_barang=m.kode_barang where a.nomer_produksihasil = '" & no_transaksi & "'", conn2, transaction)
        da.SelectCommand = cmd2
        da.Fill(ds, "t_produksi_bahan_batch")
        dt1 = ds.Tables("t_produksi_bahan_batch")
        For i As Integer = 0 To dt1.Rows.Count - 1
            insertKartustock(keterangan, no_transaksi, tanggal.ToString("yyyy/MM/dd HH:mm:ss"), dt1.Rows(i).Item("kode_barang"), gudangbahan, dt1.Rows(i).Item("qty") * -1, "Produksi Bahan", dt1.Rows(i).Item("batch"), conn2, transaction, i + 1, dt1.Rows(i).Item("satuan"), dt1.Rows(i).Item("qty_pcs") * -1, dt1.Rows(i).Item("isi"))
            updatestock(gudangbahan, dt1.Rows(i).Item("kode_barang"), dt1.Rows(i).Item("qty_pcs") * -1, tanggal, dt1.Rows(i).Item("batch"), conn2, transaction)
        Next
    End Sub

End Module
