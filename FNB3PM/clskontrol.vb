﻿Imports System.Data.SqlClient
Public Class clsKontrol
    Public Function Isi_DataGrid(ByVal perintahSql As String, ByVal NamaGrid As DataGridView, ByVal KolField As Collection) As Boolean
        Dim database As New clsDatabase
        Dim dr As sqldatareader
        Dim i As Integer = 0
        Dim isGridBerisi As Boolean = False
        database.Open()
        dr = database.GetReader(perintahSql)
        Try
            If dr.HasRows Then
                isGridBerisi = True
                With NamaGrid
                    .Rows.Clear()
                    While dr.Read
                        .Rows.Add()
                        For j As Integer = 1 To KolField.Count
                            .Rows.Item(i).Cells(j - 1).Value = database.Nilai_Dr(CStr(KolField.Item(j)))
                        Next
                        i = i + 1
                    End While
                End With
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            database.Close()
        End Try
        NamaGrid.AutoResizeColumns()
        Return isGridBerisi
    End Function

End Class
