﻿Imports System.Data.OleDb

Module modFunction
    Public Function getHpp1(ByVal kdbrg As String, ByVal batch As String) As Double
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader
        Dim hpp As Double
        Dim conn As New oledbConnection

        conn.ConnectionString = strcon
        conn.Open()

        cmd = New oledbCommand("select hpp from hpp where kode_barang='" & kdbrg & "' and batch ='" & batch & "'", conn)
        reader = cmd.ExecuteReader()
        If Not reader.HasRows Then
            While reader.Read()
                hpp = reader("hpp")
            End While
        Else
            hpp = 0
        End If
        reader.Close()
        conn.Close()
        getHpp1 = hpp
        'Return hpp

    End Function
    Public Function getMaxHpp(ByVal kdbrg As String) As Decimal
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader
        Dim hpp As Decimal
        Dim conn As New oledbConnection

        conn.ConnectionString = strcon
        conn.Open()

        cmd = New oledbCommand("select isnull(max(hpp),0) as hpp from hpp where kode_barang='" & kdbrg & "'", conn)
        reader = cmd.ExecuteReader()
        If reader.Read() Then
            hpp = reader("hpp")
        Else
            hpp = 0
        End If
        reader.Close()
        conn.Close()
        getMaxHpp = hpp
        'Return hpp

    End Function
    Public Function GetStock(ByVal gudang As String, ByVal kdbrg As String, ByVal batch As String) As Double
        Dim conn As New oledbConnection(strcon)
        Dim cmd10 As oledbCommand
        Dim reader10 As oledbDataReader
        Dim stock As Double

        conn.ConnectionString = strcon
        conn.Open()
        cmd10 = New oledbCommand("select * from  stock where kode_barang = '" & kdbrg & "' and kode_gudang = '" & gudang & "' and batch='" & batch & "'", conn)
        reader10 = cmd10.ExecuteReader()

        If reader10.Read Then
            stock = reader10("stock")
        Else
            stock = 0
        End If
        reader10.Close()
        conn.Close()
        GetStock = stock
    End Function
    Public Function GetStockperTanggal(ByVal gudang As String, ByVal kdbrg As String, tanggal As Date) As Double
        Dim conn As New oledbConnection(strcon)
        Dim cmd10 As oledbCommand
        Dim reader10 As oledbDataReader
        Dim stock As Double

        conn.ConnectionString = strcon
        conn.Open()
        cmd10 = New oledbCommand("select isnull(sum(masukpcs-keluarpcs),0) as stock from  kartu_stock where kode_barang = '" & kdbrg & "' and kode_gudang = '" & gudang & "' and replace(convert(varchar(20),tanggal,20),'/','-')<'" & Format(tanggal, "yyyy-MM-dd HH:mm:ss") & "'", conn)
        reader10 = cmd10.ExecuteReader()

        If reader10.Read Then
            stock = reader10("stock")
        Else
            stock = 0
        End If
        reader10.Close()
        conn.Close()
        GetStockperTanggal = stock
    End Function
    Public Function GetSaldokas(ByVal kodekas As String) As Double
        Dim conn As New oledbConnection(strcon)
        Dim cmd As oledbCommand
        Dim dr As oledbDataReader
        GetSaldokas = 0

        conn.ConnectionString = strcon
        conn.Open()
        cmd = New oledbCommand("select * from  ms_kasbank where kode_kasbank = '" & kodekas & "'", conn)
        dr = cmd.ExecuteReader()

        If dr.Read Then
            GetSaldokas = dr("saldo")
        End If
        dr.Close()
        conn.Close()

    End Function

    Public Function GetHargaBeliTerakhir(ByVal kodekas As String) As Double
        Dim conn As New oledbConnection(strcon)
        Dim cmd As oledbCommand
        Dim dr As oledbDataReader
        GetHargaBeliTerakhir = 0

        conn.ConnectionString = strcon
        conn.Open()
        cmd = New oledbCommand("select * from t_pembeliand d left join t_pembelianh h on d.nomer_pembelian=h.nomer_pembelian order by tanggal desc", conn)
        dr = cmd.ExecuteReader()

        If dr.Read Then
            GetHargaBeliTerakhir = dr("saldo")
        End If
        dr.Close()
        conn.Close()

    End Function

    Public Function GetSaldoKasperTanggal(ByVal kodekas As String, tanggal As Date, conn As oledbConnection, tr As oledbTransaction) As Double

        Dim cmd As oledbCommand
        Dim dr As oledbDataReader
        GetSaldoKasperTanggal = 0

        cmd = New oledbCommand("select isnull(sum(masuk-keluar),0) as saldo from  kartu_kas where kode_kasbank = '" & kodekas & "' and replace(convert(varchar(20),tanggal,120),'-','')<'" & Format(tanggal, "yyyyMMdd HH:mm:ss") & "'", conn, tr)
        dr = cmd.ExecuteReader()

        If dr.Read Then
            GetSaldoKasperTanggal = dr("saldo")
        End If
        dr.Close()

    End Function
    Public Function ResizeGrid(ByVal query As String, ByRef dgv As DataGridView, ByVal keyini As String, ByVal index As Integer, ByVal table_name As String)

        Dim str() As String

        str = query.Split(",")

        For i As Integer = 0 To str.Length - 1
            Dim xm As New Ini(Application.StartupPath & "\size.ini")
            xm.WriteString(keyini, str(i).ToString, dgv.Columns(str(i)).Width)
            index = index + 1
        Next
        Return 1
    End Function

    Public Function SizeGrid(ByVal query As String, ByRef dgv1 As DataGridView, ByVal keyini As String, ByVal index As Integer, ByVal table_name As String)

        Dim str() As String

        str = query.Split(",")

        For i As Integer = 0 To str.Length - 1
            Dim xm As New Ini(Application.StartupPath & "\setting.ini")
            dgv1.Columns(str(i)).Width = xm.GetString(keyini, str(i), "")
            index = index + 1
        Next

        Return 1
    End Function
    Public Function getdisc(ByVal kddisc As String) As Double
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader
        Dim conn As New oledbConnection

        conn.ConnectionString = strcon
        conn.Open()

        cmd = New oledbCommand("select diskon from ms_diskon where [kode_diskon]='" & kddisc & "'", conn)
        reader = cmd.ExecuteReader()

        If reader.Read() Then
            getdisc = reader("diskon")

        Else
            getdisc = 0
        End If
        reader.Close()
        conn.Close()
    End Function

    Public Function getcharge(kodebayar As String) As Double
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader
        Dim conn As New oledbConnection
        Dim charge As Double = 0
        conn.ConnectionString = strcon
        conn.Open()

        cmd = New oledbCommand("select charge from cara_bayar where [kode_carabayar]='" & kodebayar & "'", conn)
        reader = cmd.ExecuteReader()
        If reader.Read Then
            charge = reader("charge")
        Else
            charge = 0
        End If
        reader.Close()
        conn.Close()
        getcharge = charge
    End Function
    Public Function getds(ByVal tablename As String, columns As String, conditions As String, ByVal conn As oledbConnection, ByVal trans As oledbTransaction) As DataSet
        Dim cmd As oledbCommand
        Dim str As String = ""

        cmd = New oledbCommand("select " & columns & " from " & tablename & " " & conditions & "", conn, trans)
        Dim dataadaptor As New oledbDataAdapter(cmd)
        Dim dsDataSet As New DataSet
        Try
            dataadaptor.Fill(dsDataSet, tablename)
        Catch ex As Exception
            MsgBox("Failed to Fill " + tablename + ": " + ex.ToString)
        Finally
        End Try
        Return dsDataSet
    End Function
    Public Function getds(ByVal query As String, ByVal conn As oledbConnection) As DataSet
        Dim cmd As oledbCommand
        Dim str As String = ""

        cmd = New oledbCommand(query, conn)
        Dim dataadaptor As New oledbDataAdapter(cmd)
        Dim dsDataSet As New DataSet
        Try
            dataadaptor.Fill(dsDataSet, "a")
        Catch ex As Exception
            MsgBox("Failed to Fill " + tablename + ": " + ex.ToString)
        Finally
        End Try
        Return dsDataSet
    End Function
    Public Function getds(ByVal query As String, ByVal conn As oledbConnection, ByVal trans As oledbTransaction) As DataSet
        Dim cmd As oledbCommand
        Dim str As String = ""

        cmd = New oledbCommand(query, conn, trans)
        Dim dataadaptor As New oledbDataAdapter(cmd)
        Dim dsDataSet As New DataSet
        Try
            dataadaptor.Fill(dsDataSet)
        Catch ex As Exception
            MsgBox("Failed to Fill " + ": " + ex.ToString)
        Finally
        End Try
        Return dsDataSet
    End Function
    Public Function getfield(ByVal tablename As String, ByVal columns As String, ByVal conditions As String) As DataSet
        Dim cmd As oledbCommand
        Dim conn As New oledbConnection
        Dim str As String = ""

        cmd = New oledbCommand("select " & columns & " from " & tablename & " " & conditions & "", conn)
        Dim dataadaptor As New oledbDataAdapter(cmd)
        Dim dsDataSet As New DataSet
        Try
            conn.ConnectionString = strcon
            conn.Open()
            dataadaptor.Fill(dsDataSet, tablename)
        Catch ex As Exception
            MsgBox("Failed to Fill " + tablename + ": " + ex.ToString)
        Finally
            conn.Close()
        End Try
        Return dsDataSet


    End Function
End Module
