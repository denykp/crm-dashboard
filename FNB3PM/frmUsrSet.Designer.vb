﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUserSet
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtPathSource = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnBrow1 = New System.Windows.Forms.Button()
        Me.btnBrow2 = New System.Windows.Forms.Button()
        Me.txtPathKPB1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.txtMaxRow = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnBrow4 = New System.Windows.Forms.Button()
        Me.txtPathKPB3 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnBrow3 = New System.Windows.Forms.Button()
        Me.txtPathKPB2 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.btnBrow6 = New System.Windows.Forms.Button()
        Me.txtPathH2H1 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.btnBrow5 = New System.Windows.Forms.Button()
        Me.txtPathKPB4 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtPathSource
        '
        Me.txtPathSource.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPathSource.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtPathSource.Location = New System.Drawing.Point(152, 23)
        Me.txtPathSource.MaxLength = 80
        Me.txtPathSource.Name = "txtPathSource"
        Me.txtPathSource.Size = New System.Drawing.Size(366, 21)
        Me.txtPathSource.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Folder Source File"
        '
        'btnBrow1
        '
        Me.btnBrow1.Location = New System.Drawing.Point(524, 22)
        Me.btnBrow1.Name = "btnBrow1"
        Me.btnBrow1.Size = New System.Drawing.Size(30, 24)
        Me.btnBrow1.TabIndex = 10
        Me.btnBrow1.Text = "..."
        Me.btnBrow1.UseVisualStyleBackColor = True
        '
        'btnBrow2
        '
        Me.btnBrow2.Location = New System.Drawing.Point(524, 98)
        Me.btnBrow2.Name = "btnBrow2"
        Me.btnBrow2.Size = New System.Drawing.Size(30, 24)
        Me.btnBrow2.TabIndex = 13
        Me.btnBrow2.Text = "..."
        Me.btnBrow2.UseVisualStyleBackColor = True
        '
        'txtPathKPB1
        '
        Me.txtPathKPB1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPathKPB1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtPathKPB1.Location = New System.Drawing.Point(152, 99)
        Me.txtPathKPB1.MaxLength = 80
        Me.txtPathKPB1.Name = "txtPathKPB1"
        Me.txtPathKPB1.Size = New System.Drawing.Size(366, 21)
        Me.txtPathKPB1.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(31, 102)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 13)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Template H1 to H2"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(420, 231)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(98, 24)
        Me.btnCancel.TabIndex = 16
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'txtMaxRow
        '
        Me.txtMaxRow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtMaxRow.Location = New System.Drawing.Point(152, 161)
        Me.txtMaxRow.MaxLength = 3
        Me.txtMaxRow.Name = "txtMaxRow"
        Me.txtMaxRow.Size = New System.Drawing.Size(70, 21)
        Me.txtMaxRow.TabIndex = 15
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(31, 164)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 13)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Max Rows"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(151, 47)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(203, 15)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Tempat dimana file yang akan di-upload berada"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(151, 185)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(244, 15)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Jumlah maksimal data yang akan ditampilkan saat query"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(297, 231)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(98, 24)
        Me.btnSave.TabIndex = 20
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnBrow4
        '
        Me.btnBrow4.Location = New System.Drawing.Point(545, 399)
        Me.btnBrow4.Name = "btnBrow4"
        Me.btnBrow4.Size = New System.Drawing.Size(30, 24)
        Me.btnBrow4.TabIndex = 31
        Me.btnBrow4.Text = "..."
        Me.btnBrow4.UseVisualStyleBackColor = True
        Me.btnBrow4.Visible = False
        '
        'txtPathKPB3
        '
        Me.txtPathKPB3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPathKPB3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtPathKPB3.Location = New System.Drawing.Point(173, 400)
        Me.txtPathKPB3.MaxLength = 80
        Me.txtPathKPB3.Name = "txtPathKPB3"
        Me.txtPathKPB3.Size = New System.Drawing.Size(366, 21)
        Me.txtPathKPB3.TabIndex = 30
        Me.txtPathKPB3.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(52, 403)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(37, 13)
        Me.Label10.TabIndex = 29
        Me.Label10.Text = "KPB 3"
        Me.Label10.Visible = False
        '
        'btnBrow3
        '
        Me.btnBrow3.Location = New System.Drawing.Point(545, 372)
        Me.btnBrow3.Name = "btnBrow3"
        Me.btnBrow3.Size = New System.Drawing.Size(30, 24)
        Me.btnBrow3.TabIndex = 27
        Me.btnBrow3.Text = "..."
        Me.btnBrow3.UseVisualStyleBackColor = True
        Me.btnBrow3.Visible = False
        '
        'txtPathKPB2
        '
        Me.txtPathKPB2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPathKPB2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtPathKPB2.Location = New System.Drawing.Point(173, 373)
        Me.txtPathKPB2.MaxLength = 80
        Me.txtPathKPB2.Name = "txtPathKPB2"
        Me.txtPathKPB2.Size = New System.Drawing.Size(366, 21)
        Me.txtPathKPB2.TabIndex = 26
        Me.txtPathKPB2.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(52, 376)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(37, 13)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "KPB 2"
        Me.Label12.Visible = False
        '
        'btnBrow6
        '
        Me.btnBrow6.Location = New System.Drawing.Point(524, 125)
        Me.btnBrow6.Name = "btnBrow6"
        Me.btnBrow6.Size = New System.Drawing.Size(30, 24)
        Me.btnBrow6.TabIndex = 39
        Me.btnBrow6.Text = "..."
        Me.btnBrow6.UseVisualStyleBackColor = True
        '
        'txtPathH2H1
        '
        Me.txtPathH2H1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPathH2H1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtPathH2H1.Location = New System.Drawing.Point(152, 126)
        Me.txtPathH2H1.MaxLength = 80
        Me.txtPathH2H1.Name = "txtPathH2H1"
        Me.txtPathH2H1.Size = New System.Drawing.Size(366, 21)
        Me.txtPathH2H1.TabIndex = 38
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(31, 129)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(97, 13)
        Me.Label14.TabIndex = 37
        Me.Label14.Text = "Template H2 to H1"
        '
        'btnBrow5
        '
        Me.btnBrow5.Location = New System.Drawing.Point(545, 427)
        Me.btnBrow5.Name = "btnBrow5"
        Me.btnBrow5.Size = New System.Drawing.Size(30, 24)
        Me.btnBrow5.TabIndex = 35
        Me.btnBrow5.Text = "..."
        Me.btnBrow5.UseVisualStyleBackColor = True
        Me.btnBrow5.Visible = False
        '
        'txtPathKPB4
        '
        Me.txtPathKPB4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPathKPB4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtPathKPB4.Location = New System.Drawing.Point(173, 428)
        Me.txtPathKPB4.MaxLength = 80
        Me.txtPathKPB4.Name = "txtPathKPB4"
        Me.txtPathKPB4.Size = New System.Drawing.Size(366, 21)
        Me.txtPathKPB4.TabIndex = 34
        Me.txtPathKPB4.Visible = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(52, 431)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(37, 13)
        Me.Label16.TabIndex = 33
        Me.Label16.Text = "KPB 4"
        Me.Label16.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(31, 72)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(114, 13)
        Me.Label5.TabIndex = 40
        Me.Label5.Text = "File Hasil H1 to H2"
        Me.Label5.Visible = False
        '
        'frmUserSet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(609, 271)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnBrow6)
        Me.Controls.Add(Me.txtPathH2H1)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.btnBrow5)
        Me.Controls.Add(Me.txtPathKPB4)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.btnBrow4)
        Me.Controls.Add(Me.txtPathKPB3)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.btnBrow3)
        Me.Controls.Add(Me.txtPathKPB2)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.txtMaxRow)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnBrow2)
        Me.Controls.Add(Me.txtPathKPB1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnBrow1)
        Me.Controls.Add(Me.txtPathSource)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmUserSet"
        Me.Text = "User Setting"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtPathSource As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnBrow1 As System.Windows.Forms.Button
    Friend WithEvents btnBrow2 As System.Windows.Forms.Button
    Friend WithEvents txtPathKPB1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents txtMaxRow As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnBrow4 As System.Windows.Forms.Button
    Friend WithEvents txtPathKPB3 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnBrow3 As System.Windows.Forms.Button
    Friend WithEvents txtPathKPB2 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btnBrow6 As System.Windows.Forms.Button
    Friend WithEvents txtPathH2H1 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents btnBrow5 As System.Windows.Forms.Button
    Friend WithEvents txtPathKPB4 As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
