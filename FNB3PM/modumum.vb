﻿Imports System.Data.OleDb
'Imports CrystalDecisions.CrystalReports.Engine
'Imports CrystalDecisions.Shared

Module modUmum


    Public defaultGudang As String = "Dapur"
    Public defaultkas As String = ""
    Public filtertanggal As String
    Public filterkategori As String
    Public filterkodekasbank As String
    Public filterid As String
    Public filtergudang As String
    Public filterbarang As String
    Public formula As String
    Public filename As String


    Public Function GetAppPath1() As String
        Dim i As Integer
        Dim strAppPath As String
        strAppPath = System.Reflection.Assembly.GetExecutingAssembly.Location()
        i = strAppPath.Length - 1
        Do Until strAppPath.Substring(i, 1) = "\"
            i = i - 1
        Loop
        strAppPath = strAppPath.Substring(0, i)
        Return strAppPath
    End Function

    Sub print()
        
    End Sub

    Public Sub getallforms(ByVal sender As Object)
        Dim Forms As New List(Of Form)()
        Dim formType As Type = Type.GetType("System.Windows.Forms.Form")
        For Each t As Type In sender.GetType().Assembly.GetTypes()
            If UCase(t.BaseType.ToString) = "SYSTEM.WINDOWS.FORMS.FORM" Then
                MsgBox(t.Name)
            End If
        Next
    End Sub

    Public Function newID(table_name As String, key As String, prefix As String, dateformat As String, numlen As Integer, tanggal As Date)
        Dim conn As New oledbConnection(strcon)
        Dim cmd As oledbCommand
        Dim dr As oledbDataReader
        Dim numformat As String = ""
        numformat = numformat.PadLeft(numlen, "0")
        newID = ""
        conn.Open()
        Try
            Dim no As String = ""
            cmd = New oledbCommand("select top 1 " & key & " from " & table_name & " where substring(" & key & "," & Len(prefix) + 1 & "," & Len(dateformat) & ")=" & Val(Format(tanggal, dateformat)) & " order by " & key & " desc", conn)
            dr = cmd.ExecuteReader()
            If dr.Read() Then
                no = prefix & tanggal.ToString(dateformat) & Format((CLng(Strings.Right(dr.GetString(0).ToString, numlen)) + 1), numformat)
            Else
                no = prefix & tanggal.ToString(dateformat) & Format(1, numformat)
            End If
            dr.Close()
            newID = no
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Function
    
End Module
