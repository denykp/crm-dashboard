﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class frmTransKoreksiStock
    Dim transaction As SqlTransaction
    Dim itemsatuan As String

    Private Sub frmTransKoreksiStock_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
        If e.KeyCode = Keys.F5 Then btnSearchId.PerformClick()
        If e.KeyCode = Keys.Escape Then Me.Close()
    End Sub
    Private Sub frmKoreksiStock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Koneksi()
        load_combo()
    End Sub
    Private Sub load_combo()
        Dim cmd As New SqlCommand
        Dim reader As SqlDataReader = Nothing

        Dim ds As New DataSet()

        Try
            Dim da As New SqlDataAdapter("select * from ms_gudang order by kode_gudang", strcon)

            da.Fill(ds)

            cmbGudang.DataSource = ds.Tables(0)
            cmbGudang.DisplayMember = "nama_gudang"
            cmbGudang.ValueMember = "kode_gudang"

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

    Private Sub dgv1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv1.CellContentClick
        If e.ColumnIndex = dgv1.Columns("kode_barang").Index Then
            Dim f As New frmSearch
            f.setCol(0)
            f.setTableName("ms_barang")
            f.setColumns("*")
            f.setWhere("active_fg = '1'")

            If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                lblKodebarang.Text = f.value

                If f.value <> "" Then
                    Dim ada As Boolean
                    Dim baris As Double
                    For row As Integer = 0 To dgv1.RowCount - 2
                        If f.value = dgv1.Item(0, row).Value Then
                            ada = True
                            baris = row + 1
                        End If
                    Next

                    If ada = True Then
                        MsgBox("Barang sudah ada pada baris ke " & baris)
                        Exit Sub
                    End If

                    dgv1.Rows.Add(New String() {"", "", 0})
                    dgv1.Item(0, dgv1.CurrentRow.Index - 1).Value = f.value
                    dgv1.Rows(dgv1.CurrentRow.Index - 1).Selected = True
                    dgv1.CurrentCell = dgv1.Rows(dgv1.CurrentRow.Index - 1).Cells(2)
                    dgv1.BeginEdit(True)
                End If



            End If
            f.Dispose()
            load_barang()

        End If
    End Sub

    Private Sub load_barang()
        Dim conn2 As New SqlConnection
        conn2.ConnectionString = strcon
        conn2.Open()
        Dim cmd As SqlCommand
        Dim reader As SqlDataReader = Nothing

        Try
            cmd = New SqlCommand("select * from ms_barang where kode_barang = '" & dgv1.Item(0, dgv1.CurrentRow.Index).Value & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.HasRows Then
                While reader.Read()
                    lblNamaBarang.Text = reader.GetString(1).ToString
                    dgv1.Item(1, dgv1.CurrentRow.Index).Value = reader.GetString(1).ToString
                End While
                fill_satuan(dgv1.CurrentRow.Index)
            End If
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            reader.Close()
            conn2.Close()
        End Try
    End Sub

    Private Sub lblKodebarang_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblKodebarang.TextChanged
        'Dim ada As Boolean
        'Dim baris As Double
        'For row As Integer = 0 To dgv1.RowCount - 2
        '    If lblKodebarang.Text = dgv1.Item(0, row).Value Then
        '        ada = True
        '        baris = row + 1
        '    End If
        'Next
        'If ada = True Then
        '    MsgBox("Barang sudah ada pada baris ke " & baris)
        '    Exit Sub
        'End If
        'If lblKodebarang.Text <> "kode" Then
        '    load_barang()
        '    dgv1.Rows.Add(New String() {lblKodebarang.Text, lblNamaBarang.Text, 0})
        '    dgv1.Rows(dgv1.CurrentRow.Index - 1).Selected = True
        '    dgv1.CurrentCell = dgv1.Rows(dgv1.CurrentRow.Index - 1).Cells(2)
        '    dgv1.BeginEdit(True)
        'End If
    End Sub

    Private Sub dgv1_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv1.CellEndEdit
        If IsNumeric(dgv1.Item(dgv1.Columns("qty").Index, e.RowIndex).Value) = False Then
            dgv1.Item(dgv1.Columns("qty").Index, e.RowIndex).Value = 0
        ElseIf IsNumeric(dgv1.Item(dgv1.Columns("qty_pcs").Index, e.RowIndex).Value) = False Then
            dgv1.Item(dgv1.Columns("qty_pcs").Index, e.RowIndex).Value = 0
        End If
        dgv1.Item(dgv1.Columns("qty_pcs").Index, e.RowIndex).Value = dgv1.Item(dgv1.Columns("qty").Index, e.RowIndex).Value * dgv1.Item(dgv1.Columns("isi").Index, e.RowIndex).Value
    End Sub

    Private Sub dgv1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv1.KeyDown
        If e.KeyCode = Keys.Delete Then
            Dim index As Integer
            If dgv1.RowCount > 1 Then
                If dgv1.Rows.Count <> dgv1.CurrentRow.Index + 1 Then
                    index = dgv1.CurrentRow.Index
                    dgv1.Rows.RemoveAt(index)
                End If
            End If
        End If
    End Sub

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        If cmbGudang.Text = "" Then
            MsgBox("Kode gudang harus diisi")
            cmbGudang.Focus()
            Exit Sub
        End If

        If dgv1.RowCount < 1 Then
            MsgBox("Masukkan barang terlebih dahulu")
            dgv1.Focus()
            Exit Sub
        End If

        If simpan() Then
            MsgBox("Data sudah tersimpan")
            reset_form()
        End If

    End Sub

    Private Function simpan() As Boolean
        simpan = False
        Dim cmd As SqlCommand
        Dim conn2 As New SqlConnection(strcon)
        Try
            conn2.Open()
            transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)

            If lblNoTrans.Text <> "-" Then
                unposting_koreksi(lblNoTrans.Text, conn2, transaction)
            Else
                lblNoTrans.Text = newID("t_koreksistockh", "nomer_koreksi", "PS", "yyMM", 5, DateTimePicker1.Value)
            End If

            cmd = New SqlCommand(add_dataheader(), conn2, transaction)
            cmd.ExecuteNonQuery()

            For row As Integer = 0 To dgv1.RowCount - 2
                cmd = New SqlCommand(add_datadetail(row), conn2, transaction)
                cmd.ExecuteNonQuery()
            Next

            Posting_Koreksi(lblNoTrans.Text, conn2, transaction)
            transaction.Commit()
            conn2.Close()
            MsgBox("Data sudah tersimpan")
            reset_form()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()
            transaction.Rollback()
            If conn2.State Then conn2.Close()
        End Try
    End Function

    Private Sub reset_form()
        cmbGudang.Text = ""
        txtKeterangan.Text = ""
        dgv1.Rows.Clear()

        DateTimePicker1.Value = Now

        lblNoTrans.Text = "-"
    End Sub

    Private Function add_dataheader() As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(5)
        ReDim nilai(5)

        table_name = "t_koreksistockh"
        fields(0) = "nomer_koreksi"
        fields(1) = "tanggal"
        fields(2) = "keterangan"
        fields(3) = "kode_gudang"
        fields(4) = "userid"

        nilai(0) = lblNoTrans.Text
        nilai(1) = DateTimePicker1.Value.ToString("yyyy/MM/dd HH:mm:ss") 'Format(DateTimePicker1.Text, "yyyy/MM/dd HH:mm:ss") 
        nilai(2) = txtKeterangan.Text
        nilai(3) = cmbGudang.SelectedValue.ToString
        nilai(4) = user

        add_dataheader = tambah_data2(table_name, fields, nilai)

    End Function

    Private Function add_datadetail(ByVal row As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(8)
        ReDim nilai(8)

        table_name = "t_koreksistockd"
        fields(0) = "nomer_koreksi"
        fields(1) = "kode_barang"
        fields(2) = "qty"
        fields(3) = "hpp"
        fields(4) = "no_urut"
        fields(5) = "satuan"
        fields(6) = "isi"
        fields(7) = "qty_pcs"

        nilai(0) = lblNoTrans.Text
        nilai(1) = dgv1.Item(dgv1.Columns("kode_barang").Index, row).Value
        nilai(2) = CDbl(dgv1.Item(dgv1.Columns("qty").Index, row).Value)
        nilai(3) = getHpp1(nilai(1))
        nilai(4) = row + 1
        nilai(5) = dgv1.Item(dgv1.Columns("satuan").Index, row).Value
        nilai(6) = dgv1.Item(dgv1.Columns("isi").Index, row).Value
        nilai(7) = dgv1.Item(dgv1.Columns("qty_pcs").Index, row).Value

        add_datadetail = tambah_data2(table_name, fields, nilai)

    End Function

    Private Sub btnSearchId_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchId.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("t_koreksistockh")
        f.setColumns("*")

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            lblNoTrans.Text = f.value
        End If
        f.Dispose()
        load_trans()
    End Sub

    Private Sub load_trans()
        Dim conn2 As New SqlConnection
        conn2.ConnectionString = strcon
        conn2.Open()
        Dim cmd As SqlCommand
        Dim reader As SqlDataReader = Nothing
        Dim da As New SqlDataAdapter()
        Dim ds As New DataSet()
        Dim dt As DataTable
        Try
            cmd = New SqlCommand("select * from t_koreksistockh h left join ms_gudang m on h.kode_gudang = m.kode_gudang where nomer_koreksi = '" & lblNoTrans.Text & "'", conn2)
            reader = cmd.ExecuteReader()

            If reader.HasRows Then
                While reader.Read()
                    DateTimePicker1.Value = reader("tanggal")
                    cmbGudang.Text = reader("nama_gudang")
                    txtKeterangan.Text = reader("keterangan")
                End While
            End If
            reader.Close()

            cmd = New SqlCommand("select * from t_koreksistockd d left join ms_barang m on d.kode_barang = m.kode_barang where nomer_koreksi = '" & lblNoTrans.Text & "' order by no_urut", conn2)
            da.SelectCommand = cmd
            da.Fill(ds, "t_koreksistockd")
            dt = ds.Tables("t_koreksistockd")
            dgv1.Rows.Clear()
            For i As Integer = 0 To dt.Rows.Count - 1
                dgv1.Rows.Add()
                dgv1.Item(dgv1.Columns("kode_barang").Index, i).Value = dt.Rows(i).Item("kode_barang")
                dgv1.Item(dgv1.Columns("nama_barang").Index, i).Value = dt.Rows(i).Item("nama")
                dgv1.Item(dgv1.Columns("qty_pcs").Index, i).Value = dt.Rows(i).Item("qty_pcs")
                dgv1.Item(dgv1.Columns("qty").Index, i).Value = dt.Rows(i).Item("qty")
                dgv1.Item(dgv1.Columns("isi").Index, i).Value = CDbl(dt.Rows(i).Item("isi"))
                fill_satuan(i)
                Dim CBox As DataGridViewComboBoxCell = CType(dgv1.Rows(i).Cells("satuan"), DataGridViewComboBoxCell)
                Dim CCol As DataGridViewComboBoxColumn = CType(dgv1.Columns("satuan"), DataGridViewComboBoxColumn)
                CBox.Value = dt.Rows(i).Item("satuan").ToString
                dgv1.UpdateCellValue(dgv1.Columns("satuan").Index, i)
            Next
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            reader.Close()
            conn2.Close()
        End Try
    End Sub

    Private Sub fill_satuan(ByVal ROW As Integer)
        Dim cmd As New SqlCommand
        Dim reader As SqlDataReader = Nothing

        Dim ds As New DataSet()

        Dim dgvcc As DataGridViewComboBoxCell

        dgvcc = dgv1.Rows(ROW).Cells(dgv1.Columns("satuan").Index)

        Try
            Dim da As New SqlDataAdapter("select * from ms_barang_satuan where kode_barang = '" & dgv1.Item(dgv1.Columns("kode_barang").Index, ROW).Value & "' order by satuan", strcon)

            da.Fill(ds)
            dgvcc.DataSource = ds.Tables(0)
            dgvcc.DisplayMember = "satuan"

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)

        End Try
    End Sub

    Private Sub btnTambahkan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTambahkan.Click
        Dim row As Integer
        Dim conn As New SqlConnection

        If txtKodeBarang.Text <> "" And cmbSatuan1.Text <> "" Then
            dgv1.Rows.Add()
            row = dgv1.RowCount - 2
            dgv1.Item(dgv1.Columns("kode_barang").Index, row).Value = txtKodeBarang.Text
            dgv1.Item(dgv1.Columns("nama_barang").Index, row).Value = lblNamaBarang1.Text
            dgv1.Item(dgv1.Columns("qty").Index, row).Value = txtQty.Text
            dgv1.Item(dgv1.Columns("satuan").Index, row).Value = cmbSatuan1.Text
            fill_satuan(row)
            load_satuan(row)
            dgv1.Item(dgv1.Columns("qty_pcs").Index, row).Value = dgv1.Item(dgv1.Columns("qty").Index, row).Value * dgv1.Item(dgv1.Columns("isi").Index, row).Value
            txtKodeBarang.Text = ""
            cmbSatuan1.Text = ""
            lblNamaBarang1.Text = "-"
            txtQty.Text = 1
            txtHarga.Text = 0
            txtKodeBarang.Focus()
        End If
    End Sub

    Private Sub dgv1_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgv1.EditingControlShowing
        Try
            If dgv1.CurrentCell.ColumnIndex = dgv1.Columns("satuan").Index Then
                Dim selectedComboBox As ComboBox = DirectCast(e.Control, ComboBox)
                RemoveHandler selectedComboBox.SelectionChangeCommitted, AddressOf selectedComboBox_SelectionChangeCommitted
                AddHandler selectedComboBox.SelectionChangeCommitted, AddressOf selectedComboBox_SelectionChangeCommitted
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

    Private Sub selectedComboBox_SelectionChangeCommitted(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim selectedCombobox As ComboBox = DirectCast(sender, ComboBox)
            If selectedCombobox.SelectedItem IsNot Nothing Then
                dgv1(dgv1.Columns("satuan").Index, dgv1.CurrentCell.RowIndex).Value = selectedCombobox.Text
                itemsatuan = selectedCombobox.Text
                load_satuan(dgv1.CurrentRow.Index)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

    Private Sub load_satuan(ByVal row As Integer)
        Dim conn2 As New SqlConnection
        conn2.ConnectionString = strcon
        conn2.Open()
        Dim cmd As SqlCommand
        Dim reader As SqlDataReader = Nothing

        Try
            cmd = New SqlCommand("select * from ms_barang_satuan where kode_barang = '" & dgv1.Item(dgv1.Columns("kode_barang").Index, row).Value & "' and satuan = '" & dgv1.Item(dgv1.Columns("satuan").Index, row).Value & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.HasRows Then
                While reader.Read()
                    dgv1.Item(dgv1.Columns("isi").Index, row).Value = reader("qty")
                End While
            Else
                MsgBox("Satuan tidak ditemukan, periksa kode barang dan satuan")
                dgv1.Item(dgv1.Columns("satuan").Index, row).Value = ""
            End If
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            reader.Close()
            conn2.Close()
        End Try
    End Sub

    Private Sub btnSearchBarang_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchBarang.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("search_barangdgtipe")
        f.setColumns("*")

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtKodeBarang.Text = f.value
        End If
        f.Dispose()
        load_dataBarang()
    End Sub

    Private Sub load_dataBarang()
        Dim conn2 As New SqlConnection
        conn2.ConnectionString = strcon
        conn2.Open()
        Dim cmd As SqlCommand
        Dim reader As SqlDataReader = Nothing

        Try
            cmd = New SqlCommand("select * from ms_barang where kode_barang = '" & txtKodeBarang.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.HasRows Then
                While reader.Read()
                    lblNamaBarang1.Text = reader.GetString(1).ToString
                End While
                txtQty.Focus()
                load_comboSatuan()
            Else
                cmbSatuan1.Text = ""
            End If
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try

    End Sub

    Private Sub load_comboSatuan()
        Dim cmd As New SqlCommand
        Dim reader As SqlDataReader = Nothing

        Dim ds As New DataSet()

        Try
            Dim da As New SqlDataAdapter("select * from ms_barang_satuan where kode_barang = '" & txtKodeBarang.Text & "' order by satuan", strcon)

            da.Fill(ds)

            cmbSatuan1.DataSource = ds.Tables(0)
            cmbSatuan1.DisplayMember = "satuan"
            cmbSatuan1.ValueMember = "satuan"

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)

        End Try
    End Sub

    Private Sub txtKodeBarang_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtKodeBarang.KeyDown
        If e.KeyCode = Keys.F3 Then btnSearchBarang.PerformClick()
    End Sub

    Private Sub txtKodeBarang_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKodeBarang.LostFocus
        load_dataBarang()
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        reset_form()
    End Sub

    Private Sub dgv1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv1.CellValueChanged
        Dim indek As Integer
        Try
            If e.ColumnIndex = dgv1.Columns("isi").Index Then
                indek = dgv1.CurrentRow.Index
                dgv1.Item(dgv1.Columns("qty_pcs").Index, indek).Value = dgv1.Item(dgv1.Columns("qty").Index, indek).Value * dgv1.Item(dgv1.Columns("isi").Index, indek).Value
                dgv1.CurrentCell = dgv1.Rows(dgv1.CurrentRow.Index).Cells(dgv1.Columns("qty").Index)
                dgv1.BeginEdit(True)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgv1_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgv1.DataError
        dgv1.CommitEdit(DataGridViewDataErrorContexts.CurrentCellChange)
    End Sub

End Class
