﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMeja
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.grpStatusMeja = New System.Windows.Forms.GroupBox()
        Me.DBGrid = New System.Windows.Forms.DataGridView()
        Me.kode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Qty = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Item = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnAddOrder = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.btnPindah = New System.Windows.Forms.Button()
        Me.btnBill = New System.Windows.Forms.Button()
        Me.grpStatusMeja.SuspendLayout()
        CType(Me.DBGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grpStatusMeja
        '
        Me.grpStatusMeja.Controls.Add(Me.btnBill)
        Me.grpStatusMeja.Controls.Add(Me.DBGrid)
        Me.grpStatusMeja.Controls.Add(Me.btnCancel)
        Me.grpStatusMeja.Controls.Add(Me.btnAddOrder)
        Me.grpStatusMeja.Dock = System.Windows.Forms.DockStyle.Right
        Me.grpStatusMeja.Location = New System.Drawing.Point(256, 0)
        Me.grpStatusMeja.Name = "grpStatusMeja"
        Me.grpStatusMeja.Size = New System.Drawing.Size(338, 625)
        Me.grpStatusMeja.TabIndex = 0
        Me.grpStatusMeja.TabStop = False
        Me.grpStatusMeja.Visible = False
        '
        'DBGrid
        '
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DBGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DBGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DBGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.DBGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.kode, Me.Qty, Me.Item})
        Me.DBGrid.GridColor = System.Drawing.Color.White
        Me.DBGrid.Location = New System.Drawing.Point(6, 12)
        Me.DBGrid.Name = "DBGrid"
        Me.DBGrid.RowHeadersVisible = False
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DBGrid.RowsDefaultCellStyle = DataGridViewCellStyle3
        Me.DBGrid.Size = New System.Drawing.Size(304, 529)
        Me.DBGrid.TabIndex = 3
        '
        'kode
        '
        Me.kode.HeaderText = "kode"
        Me.kode.Name = "kode"
        Me.kode.Visible = False
        '
        'Qty
        '
        Me.Qty.HeaderText = "Qty"
        Me.Qty.Name = "Qty"
        Me.Qty.Width = 50
        '
        'Item
        '
        Me.Item.HeaderText = "Item"
        Me.Item.Name = "Item"
        Me.Item.ReadOnly = True
        Me.Item.Width = 220
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnCancel.Location = New System.Drawing.Point(116, 547)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(103, 39)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel Order"
        Me.btnCancel.UseVisualStyleBackColor = True
        Me.btnCancel.Visible = False
        '
        'btnAddOrder
        '
        Me.btnAddOrder.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnAddOrder.Location = New System.Drawing.Point(7, 547)
        Me.btnAddOrder.Name = "btnAddOrder"
        Me.btnAddOrder.Size = New System.Drawing.Size(103, 39)
        Me.btnAddOrder.TabIndex = 0
        Me.btnAddOrder.Text = "Add Order"
        Me.btnAddOrder.UseVisualStyleBackColor = True
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(12, 12)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(113, 32)
        Me.btnRefresh.TabIndex = 3
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'btnPindah
        '
        Me.btnPindah.Location = New System.Drawing.Point(131, 12)
        Me.btnPindah.Name = "btnPindah"
        Me.btnPindah.Size = New System.Drawing.Size(113, 32)
        Me.btnPindah.TabIndex = 4
        Me.btnPindah.Text = "Pindah Meja"
        Me.btnPindah.UseVisualStyleBackColor = True
        '
        'btnBill
        '
        Me.btnBill.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnBill.Location = New System.Drawing.Point(223, 547)
        Me.btnBill.Name = "btnBill"
        Me.btnBill.Size = New System.Drawing.Size(103, 39)
        Me.btnBill.TabIndex = 4
        Me.btnBill.Text = "Bill"
        Me.btnBill.UseVisualStyleBackColor = True
        Me.btnBill.Visible = False
        '
        'frmMeja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(594, 625)
        Me.Controls.Add(Me.btnPindah)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.grpStatusMeja)
        Me.Name = "frmMeja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmMeja"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.grpStatusMeja.ResumeLayout(False)
        CType(Me.DBGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grpStatusMeja As System.Windows.Forms.GroupBox
    Friend WithEvents btnAddOrder As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents DBGrid As System.Windows.Forms.DataGridView
    Friend WithEvents kode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Qty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Item As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents btnPindah As System.Windows.Forms.Button
    Friend WithEvents btnBill As System.Windows.Forms.Button
End Class
