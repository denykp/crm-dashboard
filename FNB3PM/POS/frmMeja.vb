﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Public Class frmMeja
    Public mejaAktif As String
    Private Sub frmMeja_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        reload_meja()
    End Sub
    Private Sub reload_meja()
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        Dim i As Integer
        Dim btnWidht As Integer = 90
        Dim btnHeight As Integer = 50
        Dim top As Integer = 70, left As Integer = 0, col As Integer = 10
        Dim fontbtn As New Font("Arial", 12)
        Dim fontbtn1 As New Font("Arial", 12, FontStyle.Bold)
        For i = Me.Controls.Count - 1 To 0 Step -1
            If TypeOf Me.Controls(i) Is Button Then
                Dim b As Button = TryCast(Me.Controls(i), Button)
                If Strings.Left(b.Name, 4) = "meja" Then b.Dispose()
            End If
        Next
        conn.Open()
        cmd = New SqlCommand("select * from ms_meja order by right(replicate('0',10)+kode_meja,10);select distinct meja from vw_rekappesan", conn)
        dr = cmd.ExecuteReader
        i = 0
        While dr.Read
            Dim btn As New Button
            btn.Name = "meja" + dr.Item("kode_meja")
            btn.Text = dr.Item("kode_meja")
            btn.Left = left + ((i Mod col) * btnWidht)
            btn.Top = top + ((i \ col) * btnHeight)
            btn.Height = btnHeight
            btn.Width = btnWidht
            btn.Font = fontbtn
            btn.BackColor = Color.White

            AddHandler btn.Click, AddressOf btnclick
            AddHandler btn.MouseEnter, AddressOf btnMouseOver
            AddHandler btn.MouseLeave, AddressOf btnMouseLeave
            Me.Controls.Add(btn)
            i += 1
        End While
        dr.NextResult()
        While dr.Read
            For i = Me.Controls.Count - 1 To 0 Step -1
                If TypeOf Me.Controls(i) Is Button Then
                    Dim b As Button = TryCast(Me.Controls(i), Button)
                    If b.Name = "meja" & dr.Item("meja") Then b.ForeColor = Color.DarkGoldenrod
                End If
            Next

        End While
        dr.Close()
        conn.Close()
        If mejaAktif <> "" Then
            Dim b As Button = Me.Controls("meja" & mejaAktif)
            b.PerformClick()
        End If
    End Sub
    Private Sub load_order(meja As String)
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        Dim row() As String
        conn.Open()
        DBGrid.Rows.Clear()

        cmd = New SqlCommand("select v.*,m.nama from vw_rekappesan v inner join ms_barang m on v.kode_barang=m.kode_barang where meja='" & meja & "' order by urut,kode_barang", conn)
        dr = cmd.ExecuteReader
        While dr.Read
            row = {dr.Item("kode_barang").ToString, dr.Item("qty").ToString, dr.Item("nama").ToString}
            DBGrid.Rows.Add(row)
            row = {dr.Item("kode_barang").ToString, "", dr.Item("tambahan").ToString}
            DBGrid.Rows.Add(row)
            If DBGrid.Item(2, DBGrid.Rows.Count - 2).Value <> "" Then DBGrid.Rows(DBGrid.Rows.Count - 2).Visible = True Else DBGrid.Rows(DBGrid.Rows.Count - 2).Visible = False
        End While
    End Sub
    Private Sub btnclick(sender As System.Object, e As System.EventArgs)
        For i = Me.Controls.Count - 1 To 0 Step -1
            If TypeOf Me.Controls(i) Is Button Then
                Dim b As Button = TryCast(Me.Controls(i), Button)
                b.UseVisualStyleBackColor = False
                b.BackColor = Color.White
            End If
        Next
        Dim btn As Button = sender
        btn.BackColor = Color.Red
        mejaAktif = btn.Text
        grpStatusMeja.Visible = True
        load_order(btn.Text)
        If btn.ForeColor = Color.DarkGoldenrod Then
            btnCancel.Visible = True
            btnPindah.Visible = True
        Else
            btnCancel.Visible = False
            btnPindah.Visible = False
        End If
    End Sub
    Private Sub btnswitch(sender As System.Object, e As System.EventArgs)
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim btn As Button = sender

        conn.Open()
        cmd = New SqlCommand("update t_pesanh set meja='" & btn.Text & "' where meja='" & mejaAktif & "' and status_bayar='0'", conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        mejaAktif = btn.Text
        reload_meja()
    End Sub
    Private Sub btnMouseOver(sender As System.Object, e As System.EventArgs)
        Dim btn As Button = sender
        btn.Font = New Font(btn.Font.FontFamily, btn.Font.Size, FontStyle.Bold)
    End Sub
    Private Sub btnMouseLeave(sender As System.Object, e As System.EventArgs)
        Dim btn As Button = sender
        btn.Font = New Font(btn.Font.FontFamily, btn.Font.Size, FontStyle.Regular)
    End Sub

    Private Sub btnAddOrder_Click(sender As System.Object, e As System.EventArgs) Handles btnAddOrder.Click
        If Not mejaAktif Is Nothing Then
            frmTransOrder.meja = mejaAktif
            frmTransOrder.ShowDialog()
        End If
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click
        reload_meja()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        If Not mejaAktif Is Nothing Then
            frmCancelOrder.ShowDialog()
        End If
    End Sub

    Private Sub btnPindah_Click(sender As System.Object, e As System.EventArgs) Handles btnPindah.Click
        For i = Me.Controls.Count - 1 To 0 Step -1
            If TypeOf Me.Controls(i) Is Button Then
                Dim b As Button = TryCast(Me.Controls(i), Button)
                If Strings.Left(b.Name, 4) = "meja" Then
                    If b.ForeColor = Color.DarkGoldenrod Then b.Enabled = False
                    RemoveHandler b.Click, AddressOf btnclick
                    AddHandler b.Click, AddressOf btnswitch
                End If
            End If
        Next
    End Sub
End Class