﻿Public Class frmQty
    Public maxqty As Integer
    'Private Sub btnOk_Click(sender As System.Object, e As System.EventArgs) Handles btnOk.Click
    '    Me.DialogResult = Windows.Forms.DialogResult.OK
    '    Me.Dispose()
    'End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click
        If CInt(txtQty.Text) < maxqty Then txtQty.Text = CInt(txtQty.Text) + 1
    End Sub

    Private Sub frmQty_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If e.KeyChar = vbBack And Len(txtQty.Text) > 0 Then txtQty.Text = Strings.Left(txtQty.Text, Len(txtQty.Text) - 1)
        If IsNumeric(e.KeyChar) Then txtQty.Text = txtQty.Text + e.KeyChar

    End Sub

    Private Sub btnSub_Click(sender As System.Object, e As System.EventArgs) Handles btnSub.Click
        If CInt(txtQty.Text) > 0 Then txtQty.Text = CInt(txtQty.Text) - 1
    End Sub
End Class