﻿Imports System.Data.SqlClient
Public Class frmTambahan
    Public tambahan As String

    Public Sub load_tambahan()
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader

        conn.Open()
        cmd = New SqlCommand("select b.kode_barang,b.nama,qty,m.satuan,isi from ms_barang_tambahan m inner join ms_barang b on m.kode_tambahan=b.kode_barang where m.kode_barang='" & lblKodeBarang.Text & "' order by kode_barang", conn)
        dr = cmd.ExecuteReader
        DBGridTambahan.Rows.Clear()
        If Not dr.HasRows Then Me.DialogResult = Windows.Forms.DialogResult.No
        While dr.Read
            Dim row() As String
            row = {dr.Item("kode_barang").ToString, dr.Item("nama"), False, dr.Item("qty").ToString, dr.Item("satuan").ToString, dr.Item("isi").ToString}
            DBGridTambahan.Rows.Add(row)
        End While
        If tambahan <> "" Then
            tambahan = Replace(tambahan, "(", "")
            tambahan = Replace(tambahan, ")", "")
            Dim val() As String = Split(tambahan, ",")
            For i As Integer = 0 To UBound(val)
                For j As Integer = 0 To DBGridTambahan.Rows.Count - 1
                    If LTrim(val(i)) = DBGridTambahan.Item(0, j).Value Then DBGridTambahan.Item(2, j).Value = True
                Next
            Next
        End If
    End Sub

    Private Sub btnTambahan_Click(sender As System.Object, e As System.EventArgs) Handles btnTambahan.Click
        rekap_tambahan()

    End Sub
    Private Sub rekap_tambahan()
        tambahan = ""
        If DBGridTambahan.Rows.Count > 1 Then
            For i As Integer = 0 To DBGridTambahan.Rows.Count - 1
                If DBGridTambahan.Item(2, i).Value = True Then tambahan += DBGridTambahan.Item(0, i).Value + ", "
            Next
            If Len(tambahan) > 0 Then tambahan = Strings.Left(tambahan, Len(tambahan) - 2)
            If Len(tambahan) > 0 Then tambahan = "(" & tambahan & ")"
        End If
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub frmTambahan_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'rekap_tambahan()
    End Sub
End Class