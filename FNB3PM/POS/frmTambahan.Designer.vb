﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTambahan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblKodeBarang = New System.Windows.Forms.Label()
        Me.btnTambahan = New System.Windows.Forms.Button()
        Me.DBGridTambahan = New System.Windows.Forms.DataGridView()
        Me.KodeTambahan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NamaTambahan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ya = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.QtyTambah = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SatuanTambah = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IsiTambah = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DBGridTambahan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblKodeBarang
        '
        Me.lblKodeBarang.AutoSize = True
        Me.lblKodeBarang.Location = New System.Drawing.Point(357, 69)
        Me.lblKodeBarang.Name = "lblKodeBarang"
        Me.lblKodeBarang.Size = New System.Drawing.Size(39, 13)
        Me.lblKodeBarang.TabIndex = 122
        Me.lblKodeBarang.Text = "Label1"
        '
        'btnTambahan
        '
        Me.btnTambahan.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnTambahan.Location = New System.Drawing.Point(360, 20)
        Me.btnTambahan.Name = "btnTambahan"
        Me.btnTambahan.Size = New System.Drawing.Size(70, 36)
        Me.btnTambahan.TabIndex = 121
        Me.btnTambahan.Text = "Ok"
        Me.btnTambahan.UseVisualStyleBackColor = True
        '
        'DBGridTambahan
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DBGridTambahan.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DBGridTambahan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DBGridTambahan.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.KodeTambahan, Me.NamaTambahan, Me.Ya, Me.QtyTambah, Me.SatuanTambah, Me.IsiTambah})
        Me.DBGridTambahan.Location = New System.Drawing.Point(24, 20)
        Me.DBGridTambahan.Name = "DBGridTambahan"
        Me.DBGridTambahan.RowHeadersVisible = False
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DBGridTambahan.RowsDefaultCellStyle = DataGridViewCellStyle2
        Me.DBGridTambahan.Size = New System.Drawing.Size(316, 265)
        Me.DBGridTambahan.TabIndex = 120
        '
        'KodeTambahan
        '
        Me.KodeTambahan.HeaderText = "KodeTambahan"
        Me.KodeTambahan.Name = "KodeTambahan"
        Me.KodeTambahan.Visible = False
        '
        'NamaTambahan
        '
        Me.NamaTambahan.HeaderText = "Nama"
        Me.NamaTambahan.Name = "NamaTambahan"
        Me.NamaTambahan.ReadOnly = True
        Me.NamaTambahan.Width = 200
        '
        'Ya
        '
        Me.Ya.HeaderText = "Ya"
        Me.Ya.Name = "Ya"
        Me.Ya.Width = 70
        '
        'QtyTambah
        '
        Me.QtyTambah.HeaderText = "Qty"
        Me.QtyTambah.Name = "QtyTambah"
        Me.QtyTambah.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.QtyTambah.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.QtyTambah.Visible = False
        '
        'SatuanTambah
        '
        Me.SatuanTambah.HeaderText = "Satuan"
        Me.SatuanTambah.Name = "SatuanTambah"
        Me.SatuanTambah.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SatuanTambah.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.SatuanTambah.Visible = False
        '
        'IsiTambah
        '
        Me.IsiTambah.HeaderText = "isi"
        Me.IsiTambah.Name = "IsiTambah"
        Me.IsiTambah.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.IsiTambah.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IsiTambah.Visible = False
        '
        'frmTambahan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(455, 304)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblKodeBarang)
        Me.Controls.Add(Me.btnTambahan)
        Me.Controls.Add(Me.DBGridTambahan)
        Me.Name = "frmTambahan"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Modifier"
        CType(Me.DBGridTambahan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblKodeBarang As System.Windows.Forms.Label
    Friend WithEvents btnTambahan As System.Windows.Forms.Button
    Friend WithEvents DBGridTambahan As System.Windows.Forms.DataGridView
    Friend WithEvents KodeTambahan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NamaTambahan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ya As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents QtyTambah As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SatuanTambah As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IsiTambah As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
