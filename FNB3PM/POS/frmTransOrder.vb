﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Public Class frmTransOrder
    Public meja As String
    Private Sub frmTranspesan_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        Dim i As Integer
        Dim btnWidht As Integer = 100
        Dim btnHeight As Integer = 50
        Dim top As Integer = 0, left As Integer = 400, col As Integer = 10

        conn.Open()
        cmd = New SqlCommand("select * from ms_kategori inner join ms_tipe on ms_kategori.kode_tipe=ms_tipe.kode_tipe where dijual='1' and tambahan='0' order by nama", conn)
        dr = cmd.ExecuteReader
        i = 0
        While dr.Read
            Dim btn As New Button
            'btn.Name = "btn" + i
            btn.Tag = dr.Item("kode_kategori")
            btn.Text = dr.Item("nama")
            btn.BackColor = Color.FromArgb(dr.Item("warna"))
            btn.Left = left + ((i Mod col) * btnWidht)
            btn.Top = top + ((i \ col) * btnHeight)
            btn.Height = btnHeight
            btn.Width = btnWidht
            AddHandler btn.Click, AddressOf btnclick
            AddHandler btn.MouseEnter, AddressOf btnMouseOver
            AddHandler btn.MouseLeave, AddressOf btnMouseLeave
            Me.Controls.Add(btn)
            i += 1
        End While
    End Sub
    Private Sub btnclick(sender As System.Object, e As System.EventArgs)
        For i = Me.Controls.Count - 1 To 0 Step -1
            If TypeOf Me.Controls(i) Is Button Then
                Dim b As Button = TryCast(Me.Controls(i), Button)
                b.Font = New Font(b.Font.FontFamily, 8)
            End If
        Next
        Dim btn As Button = sender
        btn.Font = New Font(btn.Font.FontFamily, 11)
        load_menu(btn.Tag)
    End Sub
    Private Sub btnMouseOver(sender As System.Object, e As System.EventArgs)
        Dim btn As Button = sender
        btn.Font = New Font(btn.Font.FontFamily, btn.Font.Size, FontStyle.Bold)
    End Sub
    Private Sub btnMouseLeave(sender As System.Object, e As System.EventArgs)
        Dim btn As Button = sender
        btn.Font = New Font(btn.Font.FontFamily, btn.Font.Size, FontStyle.Regular)
    End Sub

    Private Sub load_menu(kategori As String)
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        Dim i As Integer
        Dim btnWidht As Integer = 100
        Dim btnHeight As Integer = 50
        Dim top As Integer = 200, left As Integer = 400, col As Integer = 10
        For i = Me.Controls.Count - 1 To 0 Step -1
            Dim b As Button = TryCast(Me.Controls(i), Button)
            If b IsNot Nothing AndAlso b.Tag IsNot Nothing Then
                If Strings.Left(b.Name, 5) = "menu-" Then b.Dispose()
            End If
        Next
        conn.Open()
        cmd = New SqlCommand("select * from ms_barang where kode_kategori='" & kategori & "' order by kode_barang", conn)
        dr = cmd.ExecuteReader
        i = 0
        While dr.Read
            Dim btn As New Button
            'btn.Name = "btn" + i
            btn.Name = "menu-" & dr.Item("kode_barang")
            btn.Tag = dr.Item("kode_barang")
            btn.Text = dr.Item("nama")
            btn.Left = left + ((i Mod col) * btnWidht)
            btn.Top = top + ((i \ col) * btnHeight)
            btn.Height = btnHeight
            btn.Width = btnWidht
            AddHandler btn.Click, AddressOf btnmenuclick
            'AddHandler btn.MouseEnter, AddressOf btnMouseOver
            'AddHandler btn.MouseLeave, AddressOf btnMouseLeave
            Me.Controls.Add(btn)
            i += 1
        End While
    End Sub
    Private Sub btnmenuclick(sender As System.Object, e As System.EventArgs)
        Dim btn As Button = sender
        add_menu(btn.Tag)
    End Sub
    Private Sub txtCommand_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCommand.KeyDown
        If e.KeyCode = Keys.Enter Then
            add_menu(txtCommand.Text)
            txtCommand.Text = ""
        End If
    End Sub

    Private Function add_menu(kode As String) As Boolean
        Dim cmdMenu As SqlCommand
        Dim dr As SqlDataReader
        add_menu = False
        Dim conn As New SqlConnection(strcon)
        Dim tambahan As String
        conn.Open()
        tambahan = ""
        cmdMenu = New SqlCommand("select * from ms_barang_tambahan where kode_barang='" & kode & "';select * from ms_barang where kode_barang='" & kode & "'", conn)
        dr = cmdMenu.ExecuteReader
        If dr.Read Then
            Dim f As New frmTambahan
            f.lblKodeBarang.Text = kode
            f.load_tambahan()
            f.ShowDialog()
            tambahan = f.tambahan
        End If

        dr.NextResult()

        cmdMenu = New SqlCommand("select * from ms_barang where kode_barang='" & kode & "'", conn)
        dr = cmdMenu.ExecuteReader
        If dr.Read Then
            Dim row() As String
            row = {dr.Item("kode_barang").ToString, "1", dr.Item("nama").ToString}
            DBGrid.Rows.Add(row)
            row = {dr.Item("kode_barang").ToString, "", tambahan}
            DBGrid.Rows.Add(row)
            If DBGrid.Item(2, DBGrid.Rows.Count - 2).Value <> "" Then DBGrid.Rows(DBGrid.Rows.Count - 2).Visible = True Else DBGrid.Rows(DBGrid.Rows.Count - 2).Visible = False
            add_menu = True
        Else
            MsgBox("Kode yang anda cari tidak ditemukan")
        End If
keluar:
        dr.Close()
    End Function

    Private Sub btnUp_Click(sender As System.Object, e As System.EventArgs) Handles btnUp.Click
        Dim idxSource As Integer
        Dim idxTarget As Integer
        Dim value As String

        If DBGrid.CurrentRow.Index > 0 Then
            idxSource = DBGrid.CurrentRow.Index
            idxTarget = DBGrid.CurrentRow.Index - 1
            For i = 0 To DBGrid.Columns.Count - 1
                value = DBGrid.Item(i, idxSource).Value
                DBGrid.Item(i, idxSource).Value = DBGrid.Item(i, idxTarget).Value
                DBGrid.Item(i, idxTarget).Value = value
            Next
            DBGrid.CurrentCell = DBGrid.Item(1, idxTarget)
        End If
    End Sub

    Private Sub btnDown_Click(sender As System.Object, e As System.EventArgs) Handles btnDown.Click
        Dim idxSource As Integer
        Dim idxTarget As Integer
        Dim value As String

        If DBGrid.CurrentRow.Index < DBGrid.Rows.Count - 2 Then
            idxSource = DBGrid.CurrentRow.Index
            idxTarget = DBGrid.CurrentRow.Index + 1
            For i = 0 To DBGrid.Columns.Count - 1
                value = DBGrid.Item(i, idxSource).Value
                DBGrid.Item(i, idxSource).Value = DBGrid.Item(i, idxTarget).Value
                DBGrid.Item(i, idxTarget).Value = value
            Next
            DBGrid.CurrentCell = DBGrid.Item(1, idxTarget)
        End If
    End Sub

    Private Sub DBGrid_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DBGrid.CellDoubleClick
        Dim tambahan As String
        Dim f As New frmTambahan
        Dim row As Integer
        row = (DBGrid.CurrentRow.Index \ 2) * 2
        f.lblKodeBarang.Text = DBGrid.Item(0, row).Value
        f.tambahan = DBGrid.Item(2, row + 1).Value
        f.load_tambahan()
        f.ShowDialog()
        If f.DialogResult = Windows.Forms.DialogResult.OK Then
            tambahan = f.tambahan
            DBGrid.Item(2, row + 1).Value = tambahan
            If DBGrid.Item(2, row + 1).Value <> "" Then DBGrid.Rows(row + 1).Visible = True Else DBGrid.Rows(row + 1).Visible = False
        End If
    End Sub

    Private Sub btnSimpan_Click(sender As System.Object, e As System.EventArgs) Handles btnSimpan.Click
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader = Nothing
        Dim trans As SqlTransaction
        conn.Open()
        trans = conn.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            lblNoTrans.Text = newID("t_pesanh", "nomer_pesan", "OR", "yyMMdd", 4, Now)
            cmd = New SqlCommand(add_dataheader(), conn, trans)
            cmd.ExecuteNonQuery()
            For row As Integer = 0 To DBGrid.RowCount - 2 Step 2
                cmd = New SqlCommand(add_datadetail(row), conn, trans)
                cmd.ExecuteNonQuery()
                Dim tambahan() As String
                Dim querytambahan As String = "", desc As String = DBGrid.Item(2, row + 1).Value
                desc = Replace(desc, "(", "")
                desc = Replace(desc, ")", "")
                tambahan = Split(desc, ", ")
                querytambahan += add_tambahan(row, DBGrid.Item(0, row).Value)
                If desc <> "" Then
                    For i = 0 To UBound(tambahan)
                        querytambahan += add_tambahan(row, tambahan(i))
                    Next
                End If
                cmd = New SqlCommand(querytambahan, conn, trans)
                cmd.ExecuteNonQuery()
            Next
            trans.Commit()
            conn2.Close()
            MsgBox("Data sudah tersimpan")
            Me.Dispose()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            trans.Rollback()
            If conn.State Then conn.Close()
            'If reader.IsClosed = False Then

            conn.Close()

        Finally
            If reader IsNot Nothing Then reader.Close()
        End Try
    End Sub
    Private Function add_dataheader() As String
        Dim fields() As String
        Dim nilai() As String

        Dim table_name As String

        ReDim fields(6)
        ReDim nilai(6)

        table_name = "t_pesanh"
        fields(0) = "nomer_pesan"
        fields(1) = "tanggal"
        fields(2) = "userid"
        fields(3) = "type"
        fields(4) = "meja"
        fields(5) = "status_bayar"

        nilai(0) = lblNoTrans.Text
        nilai(1) = Now.ToString("yyyy/MM/dd HH:mm:ss") 'Format(DateTimePicker1.Text, "yyyy/MM/dd HH:mm:ss") 
        nilai(2) = user
        nilai(3) = "Tambah"
        nilai(4) = meja
        nilai(5) = "0"
        add_dataheader = tambah_data2(table_name, fields, nilai)

    End Function

    Private Function add_datadetail(ByVal row As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(5)
        ReDim nilai(5)

        table_name = "t_pesand"
        fields(0) = "nomer_pesan"
        fields(1) = "kode_barang"
        fields(2) = "qty"
        fields(3) = "urut"
        fields(4) = "tambahan"


        nilai(0) = lblNoTrans.Text
        nilai(1) = DBGrid.Item(0, row).Value
        nilai(2) = DBGrid.Item(1, row).Value
        nilai(3) = row
        nilai(4) = DBGrid.Item(2, row + 1).Value

        add_datadetail = tambah_data2(table_name, fields, nilai)

    End Function
    Private Function add_tambahan(ByVal row As Integer, kode_tambahan As String) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(4)
        ReDim nilai(4)

        table_name = "t_pesand_tambahan"
        fields(0) = "nomer_pesan"
        fields(1) = "kode_barang"
        fields(2) = "qty"
        fields(3) = "urut"

        nilai(0) = lblNoTrans.Text
        nilai(1) = kode_tambahan
        nilai(2) = DBGrid.Item(1, row).Value
        nilai(3) = row

        add_tambahan = tambah_data2(table_name, fields, nilai)

    End Function
End Class