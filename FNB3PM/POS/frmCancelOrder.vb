﻿Imports System.Data.SqlClient
Public Class frmCancelOrder


    Private Sub frmCancelOrder_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        Dim row() As String
        conn.Open()
        DBGrid.Rows.Clear()
        DBGridCancel.Rows.Clear()
        cmd = New SqlCommand("select v.*,m.nama from vw_rekappesan v inner join ms_barang m on v.kode_barang=m.kode_barang where meja='" & frmMeja.mejaAktif & "' order by urut,kode_barang", conn)
        dr = cmd.ExecuteReader
        While dr.Read
            row = {dr.Item("kode_barang").ToString, dr.Item("qty").ToString, dr.Item("nama").ToString}
            DBGrid.Rows.Add(row)
            row = {dr.Item("kode_barang").ToString, "", dr.Item("tambahan").ToString}
            DBGrid.Rows.Add(row)
            If DBGrid.Item(2, DBGrid.Rows.Count - 2).Value <> "" Then DBGrid.Rows(DBGrid.Rows.Count - 2).Visible = True Else DBGrid.Rows(DBGrid.Rows.Count - 2).Visible = False
        End While
    End Sub

    Private Sub DBGrid_SelectionChanged(sender As Object, e As System.EventArgs) Handles DBGrid.SelectionChanged
        btnCancel.Visible = True
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        cancel(DBGrid.CurrentRow.Index)
    End Sub
    Private Sub cancel(currrow As Integer, Optional qty As Integer = 0)
        Dim rowindex As Integer
        Dim row() As String

        Dim rowcancel As Integer
        rowcancel = -1
        rowindex = (currrow \ 2) * 2
        If qty = 0 Then
            qty = DBGrid.Item(1, rowindex).Value
            If qty > 1 Then
                Dim f As New frmQty
                f.maxqty = qty
                f.ShowDialog()
                If f.DialogResult = Windows.Forms.DialogResult.OK And IsNumeric(f.txtQty.Text) Then qty = f.txtQty.Text
            End If
        End If
        If qty > 0 Then
            With DBGridCancel
                If .Rows.Count > 1 Then
                    For i As Integer = 0 To .Rows.Count - 2 Step 2
                        If DBGrid.Item(0, rowindex).Value = .Item(0, i).Value And DBGrid.Item(2, rowindex + 1).Value = .Item(2, i + 1).Value Then
                            rowcancel = i
                        End If
                    Next
                End If
                If rowcancel >= 0 Then
                    .Item(1, rowcancel).Value = .Item(1, rowcancel).Value + qty
                Else
                    row = {DBGrid.Item(0, rowindex).Value, qty, DBGrid.Item(2, rowindex).Value}
                    .Rows.Add(row)
                    row = {DBGrid.Item(0, rowindex).Value, "", DBGrid.Item(2, rowindex + 1).Value}
                    .Rows.Add(row)
                    If .Item(2, .Rows.Count - 2).Value <> "" Then .Rows(.Rows.Count - 2).Visible = True Else .Rows(.Rows.Count - 2).Visible = False
                End If

            End With
            DBGrid.Item(1, rowindex).Value = DBGrid.Item(1, rowindex).Value - qty
            If DBGrid.Item(1, rowindex).Value <= 0 Then
                DBGrid.Rows.Remove(DBGrid.Rows(rowindex + 1))
                DBGrid.Rows.Remove(DBGrid.Rows(rowindex))
            End If
        End If
    End Sub
    Private Sub goback(currrow As Integer, Optional qty As Integer = 0)
        Dim rowindex As Integer
        Dim row() As String

        Dim rowcancel As Integer
        rowcancel = -1
        rowindex = (currrow \ 2) * 2
        If qty = 0 Then
            qty = DBGridCancel.Item(1, rowindex).Value
            If qty > 1 Then
                Dim f As New frmQty
                f.maxqty = qty
                f.ShowDialog()
                If f.DialogResult = Windows.Forms.DialogResult.OK And IsNumeric(f.txtQty.Text) Then qty = f.txtQty.Text
            End If
        End If
        If qty > 0 Then
            With DBGrid
                If .Rows.Count > 1 Then
                    For i As Integer = 0 To .Rows.Count - 2 Step 2
                        If DBGridCancel.Item(0, rowindex).Value = .Item(0, i).Value And DBGridCancel.Item(2, rowindex + 1).Value = .Item(2, i + 1).Value Then
                            rowcancel = i
                        End If
                    Next
                End If
                If rowcancel >= 0 Then
                    .Item(1, rowcancel).Value = .Item(1, rowcancel).Value + qty
                Else
                    row = {DBGridCancel.Item(0, rowindex).Value, qty, DBGridCancel.Item(2, rowindex).Value}
                    .Rows.Add(row)
                    row = {DBGridCancel.Item(0, rowindex).Value, "", DBGridCancel.Item(2, rowindex + 1).Value}
                    .Rows.Add(row)
                    If .Item(2, .Rows.Count - 2).Value <> "" Then .Rows(.Rows.Count - 2).Visible = True Else .Rows(.Rows.Count - 2).Visible = False
                End If

            End With
            DBGridCancel.Item(1, rowindex).Value = DBGridCancel.Item(1, rowindex).Value - qty
            If DBGridCancel.Item(1, rowindex).Value <= 0 Then
                DBGridCancel.Rows.Remove(DBGridCancel.Rows(rowindex + 1))
                DBGridCancel.Rows.Remove(DBGridCancel.Rows(rowindex))
            End If
        End If
    End Sub
    Private Function add_dataheader() As String
        Dim fields() As String
        Dim nilai() As String

        Dim table_name As String

        ReDim fields(6)
        ReDim nilai(6)

        table_name = "t_pesanh"
        fields(0) = "nomer_pesan"
        fields(1) = "tanggal"
        fields(2) = "userid"
        fields(3) = "type"
        fields(4) = "meja"
        fields(5) = "status_bayar"
        nilai(0) = lblNoTrans.Text
        nilai(1) = Now.ToString("yyyy/MM/dd HH:mm:ss") 'Format(DateTimePicker1.Text, "yyyy/MM/dd HH:mm:ss") 
        nilai(2) = user
        nilai(3) = "Batal"
        nilai(4) = frmMeja.mejaAktif
        nilai(5) = "False"
        add_dataheader = tambah_data2(table_name, fields, nilai)

    End Function

    Private Function add_datadetail(ByVal row As Integer) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(5)
        ReDim nilai(5)

        table_name = "t_pesand"
        fields(0) = "nomer_pesan"
        fields(1) = "kode_barang"
        fields(2) = "qty"
        fields(3) = "urut"
        fields(4) = "tambahan"


        nilai(0) = lblNoTrans.Text
        nilai(1) = DBGridCancel.Item(0, row).Value
        nilai(2) = DBGridCancel.Item(1, row).Value * -1
        nilai(3) = row
        nilai(4) = DBGridCancel.Item(2, row + 1).Value

        add_datadetail = tambah_data2(table_name, fields, nilai)

    End Function
    Private Function add_tambahan(ByVal row As Integer, kode_tambahan As String) As String
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String

        ReDim fields(4)
        ReDim nilai(4)

        table_name = "t_pesand_tambahan"
        fields(0) = "nomer_pesan"
        fields(1) = "kode_barang"
        fields(2) = "qty"
        fields(3) = "urut"

        nilai(0) = lblNoTrans.Text
        nilai(1) = kode_tambahan
        nilai(2) = DBGridCancel.Item(1, row).Value
        nilai(3) = row

        add_tambahan = tambah_data2(table_name, fields, nilai)

    End Function

    Private Sub btnSimpan_Click(sender As System.Object, e As System.EventArgs) Handles btnSimpan.Click
        Dim conn As New SqlConnection(strcon)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader = Nothing
        Dim trans As SqlTransaction
        conn.Open()
        trans = conn.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            lblNoTrans.Text = newID("t_pesanh", "nomer_pesan", "BT", "yyMMdd", 4, Now)
            cmd = New SqlCommand(add_dataheader(), conn, trans)
            cmd.ExecuteNonQuery()
            With DBGridCancel
                For row As Integer = 0 To .RowCount - 2 Step 2
                    cmd = New SqlCommand(add_datadetail(row), conn, trans)
                    cmd.ExecuteNonQuery()
                    Dim tambahan() As String
                    Dim querytambahan As String = "", desc As String = .Item(2, row + 1).Value
                    desc = Replace(desc, "(", "")
                    desc = Replace(desc, ")", "")
                    tambahan = Split(desc, ", ")
                    querytambahan += add_tambahan(row, .Item(0, row).Value)
                    If desc <> "" Then
                        For i = 0 To UBound(tambahan)
                            querytambahan += add_tambahan(row, tambahan(i))
                        Next
                    End If
                    cmd = New SqlCommand(querytambahan, conn, trans)
                    cmd.ExecuteNonQuery()
                Next
            End With
            trans.Commit()
            conn2.Close()
            MsgBox("Data sudah tersimpan")
            Me.Dispose()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            trans.Rollback()
            If conn.State Then conn.Close()
            'If reader.IsClosed = False Then

            conn.Close()

        Finally
            If reader IsNot Nothing Then reader.Close()
        End Try
    End Sub

    Private Sub btnBack_Click(sender As System.Object, e As System.EventArgs) Handles btnBack.Click
        goback(DBGridCancel.CurrentRow.Index)
    End Sub

    Private Sub btnCancelAll_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelAll.Click
        For i = DBGrid.Rows.Count - 3 To 0 Step -2
            cancel(i, DBGrid.Item(1, i).Value)
        Next
    End Sub

    Private Sub btnBackAll_Click(sender As System.Object, e As System.EventArgs) Handles btnBackAll.Click
        For i = DBGrid.Rows.Count - 3 To 0 Step -2
            goback(i, DBGrid.Item(1, i).Value)
        Next
    End Sub
End Class