﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.MasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMaster1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MasterMainDealerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMaster2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuMaster3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuMaster4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMaster5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCRMNig = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuUpload = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.ApprovalH1ToH2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ApprovalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ApprovalDealerInformationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportH1ToH2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportH2ToH1ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PetunjukPenggunaanAplikasiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TentangKamiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HistoryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HistorySoldOutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HistoryPrintBillToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.LaporanHistoryPesananToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanPesananToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanBatalPesanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanReservasiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripLabel3 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripLabel4 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripLabel5 = New System.Windows.Forms.ToolStripLabel()
        Me.StockOpnameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Font = New System.Drawing.Font("Arial Unicode MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MasterToolStripMenuItem, Me.mnuCRMNig, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1276, 29)
        Me.MenuStrip1.TabIndex = 9
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'MasterToolStripMenuItem
        '
        Me.MasterToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMaster1, Me.MasterMainDealerToolStripMenuItem, Me.mnuMaster2, Me.ToolStripSeparator4, Me.mnuMaster3, Me.ToolStripMenuItem3, Me.mnuMaster4, Me.mnuMaster5, Me.ToolStripMenuItem2, Me.mnuExit})
        Me.MasterToolStripMenuItem.Name = "MasterToolStripMenuItem"
        Me.MasterToolStripMenuItem.Size = New System.Drawing.Size(70, 25)
        Me.MasterToolStripMenuItem.Text = "Master"
        '
        'mnuMaster1
        '
        Me.mnuMaster1.Name = "mnuMaster1"
        Me.mnuMaster1.Size = New System.Drawing.Size(217, 26)
        Me.mnuMaster1.Text = "Master User Login"
        '
        'MasterMainDealerToolStripMenuItem
        '
        Me.MasterMainDealerToolStripMenuItem.Name = "MasterMainDealerToolStripMenuItem"
        Me.MasterMainDealerToolStripMenuItem.Size = New System.Drawing.Size(217, 26)
        Me.MasterMainDealerToolStripMenuItem.Text = "Master Main Dealer"
        '
        'mnuMaster2
        '
        Me.mnuMaster2.Name = "mnuMaster2"
        Me.mnuMaster2.Size = New System.Drawing.Size(217, 26)
        Me.mnuMaster2.Text = "Master Dealer"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(214, 6)
        '
        'mnuMaster3
        '
        Me.mnuMaster3.Name = "mnuMaster3"
        Me.mnuMaster3.Size = New System.Drawing.Size(217, 26)
        Me.mnuMaster3.Text = "Ubah Password"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(214, 6)
        '
        'mnuMaster4
        '
        Me.mnuMaster4.Name = "mnuMaster4"
        Me.mnuMaster4.Size = New System.Drawing.Size(217, 26)
        Me.mnuMaster4.Text = "Backup Database"
        '
        'mnuMaster5
        '
        Me.mnuMaster5.Name = "mnuMaster5"
        Me.mnuMaster5.Size = New System.Drawing.Size(217, 26)
        Me.mnuMaster5.Text = "User Setting"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(214, 6)
        '
        'mnuExit
        '
        Me.mnuExit.Name = "mnuExit"
        Me.mnuExit.Size = New System.Drawing.Size(217, 26)
        Me.mnuExit.Text = "E&xit"
        '
        'mnuCRMNig
        '
        Me.mnuCRMNig.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuUpload, Me.mnuReport, Me.ApprovalH1ToH2ToolStripMenuItem, Me.ApprovalToolStripMenuItem, Me.ApprovalDealerInformationToolStripMenuItem, Me.ExportH1ToH2ToolStripMenuItem, Me.ExportH2ToH1ToolStripMenuItem})
        Me.mnuCRMNig.Name = "mnuCRMNig"
        Me.mnuCRMNig.Size = New System.Drawing.Size(111, 25)
        Me.mnuCRMNig.Text = "CRM Report"
        '
        'mnuUpload
        '
        Me.mnuUpload.Name = "mnuUpload"
        Me.mnuUpload.Size = New System.Drawing.Size(277, 26)
        Me.mnuUpload.Text = "Upload File"
        '
        'mnuReport
        '
        Me.mnuReport.Name = "mnuReport"
        Me.mnuReport.Size = New System.Drawing.Size(277, 26)
        Me.mnuReport.Text = "Generate Report"
        '
        'ApprovalH1ToH2ToolStripMenuItem
        '
        Me.ApprovalH1ToH2ToolStripMenuItem.Name = "ApprovalH1ToH2ToolStripMenuItem"
        Me.ApprovalH1ToH2ToolStripMenuItem.Size = New System.Drawing.Size(277, 26)
        Me.ApprovalH1ToH2ToolStripMenuItem.Text = "Approval H1 to H2"
        '
        'ApprovalToolStripMenuItem
        '
        Me.ApprovalToolStripMenuItem.Name = "ApprovalToolStripMenuItem"
        Me.ApprovalToolStripMenuItem.Size = New System.Drawing.Size(277, 26)
        Me.ApprovalToolStripMenuItem.Text = "Approval H2 to H1"
        '
        'ApprovalDealerInformationToolStripMenuItem
        '
        Me.ApprovalDealerInformationToolStripMenuItem.Name = "ApprovalDealerInformationToolStripMenuItem"
        Me.ApprovalDealerInformationToolStripMenuItem.Size = New System.Drawing.Size(277, 26)
        Me.ApprovalDealerInformationToolStripMenuItem.Text = "Approval Dealer Information"
        '
        'ExportH1ToH2ToolStripMenuItem
        '
        Me.ExportH1ToH2ToolStripMenuItem.Name = "ExportH1ToH2ToolStripMenuItem"
        Me.ExportH1ToH2ToolStripMenuItem.Size = New System.Drawing.Size(277, 26)
        Me.ExportH1ToH2ToolStripMenuItem.Text = "Export H1 To H2"
        '
        'ExportH2ToH1ToolStripMenuItem
        '
        Me.ExportH2ToH1ToolStripMenuItem.Name = "ExportH2ToH1ToolStripMenuItem"
        Me.ExportH2ToH1ToolStripMenuItem.Size = New System.Drawing.Size(277, 26)
        Me.ExportH2ToH1ToolStripMenuItem.Text = "Export H2 To H1"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PetunjukPenggunaanAplikasiToolStripMenuItem, Me.TentangKamiToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(55, 25)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'PetunjukPenggunaanAplikasiToolStripMenuItem
        '
        Me.PetunjukPenggunaanAplikasiToolStripMenuItem.Name = "PetunjukPenggunaanAplikasiToolStripMenuItem"
        Me.PetunjukPenggunaanAplikasiToolStripMenuItem.Size = New System.Drawing.Size(296, 26)
        Me.PetunjukPenggunaanAplikasiToolStripMenuItem.Text = "Petunjuk Penggunaan Aplikasi"
        '
        'TentangKamiToolStripMenuItem
        '
        Me.TentangKamiToolStripMenuItem.Name = "TentangKamiToolStripMenuItem"
        Me.TentangKamiToolStripMenuItem.Size = New System.Drawing.Size(296, 26)
        Me.TentangKamiToolStripMenuItem.Text = "Tentang Kami"
        '
        'HistoryToolStripMenuItem
        '
        Me.HistoryToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HistorySoldOutToolStripMenuItem, Me.HistoryPrintBillToolStripMenuItem})
        Me.HistoryToolStripMenuItem.Name = "HistoryToolStripMenuItem"
        Me.HistoryToolStripMenuItem.Size = New System.Drawing.Size(71, 25)
        Me.HistoryToolStripMenuItem.Text = "History"
        '
        'HistorySoldOutToolStripMenuItem
        '
        Me.HistorySoldOutToolStripMenuItem.Name = "HistorySoldOutToolStripMenuItem"
        Me.HistorySoldOutToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.HistorySoldOutToolStripMenuItem.Text = "History Sold Out"
        '
        'HistoryPrintBillToolStripMenuItem
        '
        Me.HistoryPrintBillToolStripMenuItem.Name = "HistoryPrintBillToolStripMenuItem"
        Me.HistoryPrintBillToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.HistoryPrintBillToolStripMenuItem.Text = "History Print Bill"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(295, 6)
        '
        'LaporanHistoryPesananToolStripMenuItem
        '
        Me.LaporanHistoryPesananToolStripMenuItem.Name = "LaporanHistoryPesananToolStripMenuItem"
        Me.LaporanHistoryPesananToolStripMenuItem.Size = New System.Drawing.Size(298, 26)
        Me.LaporanHistoryPesananToolStripMenuItem.Text = "Laporan History Pesanan"
        '
        'LaporanPesananToolStripMenuItem
        '
        Me.LaporanPesananToolStripMenuItem.Name = "LaporanPesananToolStripMenuItem"
        Me.LaporanPesananToolStripMenuItem.Size = New System.Drawing.Size(298, 26)
        Me.LaporanPesananToolStripMenuItem.Text = "Laporan Pesanan"
        '
        'LaporanBatalPesanToolStripMenuItem
        '
        Me.LaporanBatalPesanToolStripMenuItem.Name = "LaporanBatalPesanToolStripMenuItem"
        Me.LaporanBatalPesanToolStripMenuItem.Size = New System.Drawing.Size(298, 26)
        Me.LaporanBatalPesanToolStripMenuItem.Text = "Laporan Batal Pesan"
        '
        'LaporanReservasiToolStripMenuItem
        '
        Me.LaporanReservasiToolStripMenuItem.Name = "LaporanReservasiToolStripMenuItem"
        Me.LaporanReservasiToolStripMenuItem.Size = New System.Drawing.Size(298, 26)
        Me.LaporanReservasiToolStripMenuItem.Text = "Laporan Reservasi"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.ToolStripSeparator1, Me.ToolStripLabel2, Me.ToolStripSeparator2, Me.ToolStripLabel3, Me.ToolStripSeparator3, Me.ToolStripLabel4, Me.ToolStripLabel5})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 490)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1276, 25)
        Me.ToolStrip1.TabIndex = 11
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(60, 22)
        Me.ToolStripLabel1.Text = "Username"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(39, 22)
        Me.ToolStripLabel2.Text = "Server"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripLabel3
        '
        Me.ToolStripLabel3.Name = "ToolStripLabel3"
        Me.ToolStripLabel3.Size = New System.Drawing.Size(42, 22)
        Me.ToolStripLabel3.Text = "Printer"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripLabel4
        '
        Me.ToolStripLabel4.Name = "ToolStripLabel4"
        Me.ToolStripLabel4.Size = New System.Drawing.Size(31, 22)
        Me.ToolStripLabel4.Text = "Date"
        '
        'ToolStripLabel5
        '
        Me.ToolStripLabel5.Name = "ToolStripLabel5"
        Me.ToolStripLabel5.Size = New System.Drawing.Size(34, 22)
        Me.ToolStripLabel5.Text = "Time"
        '
        'StockOpnameToolStripMenuItem
        '
        Me.StockOpnameToolStripMenuItem.Name = "StockOpnameToolStripMenuItem"
        Me.StockOpnameToolStripMenuItem.Size = New System.Drawing.Size(185, 26)
        Me.StockOpnameToolStripMenuItem.Text = "Stock Opname"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.DASHBOARD.My.Resources.Resources.background
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1276, 515)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DASHBOARD"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents MasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripLabel2 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripLabel3 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripLabel4 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel5 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents mnuCRMNig As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PetunjukPenggunaanAplikasiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TentangKamiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMaster2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMaster1 As System.Windows.Forms.ToolStripMenuItem
    '<<<<<<< HEAD
    '=======
    Friend WithEvents StockOpnameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LaporanHistoryPesananToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanPesananToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanBatalPesanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanReservasiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MasterDivisiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanHistoryPenjualanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HistoryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HistorySoldOutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HistoryPrintBillToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMaster3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuMaster4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUpload As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMaster5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ApprovalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MasterMainDealerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportH1ToH2ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ApprovalH1ToH2ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ApprovalDealerInformationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportH2ToH1ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

    '>>>>>>> 88eb1cf5786b85506e7799df8701c340c0427191
End Class
