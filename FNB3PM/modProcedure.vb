﻿Imports System.Data.OleDb
Imports System.Drawing.Printing
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.IO
Module modProcedure
    Public Function tambah_data2(ByVal table_name As String, ByVal field() As String, ByVal nilai() As String) As String
        Dim query As String
        query = "insert into " & table_name & " ("
        For i = 0 To UBound(field) - 1
            query = query & field(i) & ","
        Next
        query = Left(query, Len(query) - 1) & ") values ("
        For i = 0 To UBound(nilai) - 1
            query = query & "'" & nilai(i) & "',"
        Next
        query = Left(query, Len(query) - 1) & ")"

        tambah_data2 = query
        'MsgBox query
    End Function

    Public Function update_data2(ByVal table_name As String, ByVal field() As String, ByVal nilai() As String, ByVal fieldPK() As String, ByVal nilaiPK() As String) As String
        Dim query As String
        query = "update " & table_name & " set "
        For i = 0 To UBound(field) - 1
            query = query & field(i) & "='"
            query = query & nilai(i) & "',"
        Next
        query = Left(query, Len(query) - 1) & " where "
        For j = 0 To UBound(fieldPK) - 1
            If j < 1 Then
                query = query & fieldPK(j) & "='" & nilaiPK(j) & "' "
            Else
                query = query & fieldPK(j) & "='" & nilaiPK(j) & "' and "
            End If
        Next
        query = Left(query, Len(query) - 1)

        update_data2 = query
        'MsgBox query
    End Function

    Public Sub loadComboBoxAll(ByVal cb As ComboBox, ByVal columns As String, ByVal tables As String, ByVal where As String, ByVal displayMember As String, ByVal valueMember As String)
        Dim conn2 As New oledbConnection(strcon)
        Try
            Conn2.ConnectionString = strcon
            Conn2.Open()

            Dim da As New oledbDataAdapter()
            Dim dt As New DataTable
            cmd = New oledbCommand("select distinct " & columns & " from " & tables & " " & where & "", conn2)
            da.SelectCommand = cmd
            da.Fill(dt)
            Dim emptyrow As DataRow = dt.NewRow
            emptyrow(0) = ""

            dt.Rows.InsertAt(emptyrow, 0)
            cb.DataSource = dt
            cb.DisplayMember = displayMember
            cb.ValueMember = valueMember
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try

    End Sub
    Public Sub loadComboBoxdgv(ByRef dgv As DataGridView, ByVal col As Integer, ByVal columns As String, ByVal tables As String, ByVal where As String, ByVal displayMember As String, ByVal valueMember As String)
        Dim conn2 As New oledbConnection(strcon)
        Try
            conn2.ConnectionString = strcon
            conn2.Open()

            Dim da As New oledbDataAdapter()
            Dim dt As New DataTable
            Dim cbcol As DataGridViewComboBoxColumn
            cmd = New oledbCommand("select " & columns & " from " & tables & " " & where & "", conn2)
            da.SelectCommand = cmd
            da.Fill(dt)
            Dim emptyrow As DataRow = dt.NewRow
            emptyrow(0) = ""
            cbcol = dgv.Columns.Item(col)
            dt.Rows.InsertAt(emptyrow, 0)
            cbcol.DisplayMember = displayMember
            cbcol.ValueMember = valueMember
            cbcol.DataSource = dt

            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try

    End Sub

    Public Sub load_printer(ByVal cmbprinter As ComboBox)
        Dim pkInstalledPrinters As String
        cmbprinter.Items.Clear()
        cmbprinter.Items.Add("")
        ' Find all printers installed
        For Each pkInstalledPrinters In _
            PrinterSettings.InstalledPrinters()
            cmbprinter.Items.Add(pkInstalledPrinters)
        Next pkInstalledPrinters

        ' Set the combo to the first printer in the list
        cmbprinter.SelectedIndex = 0
    End Sub

    Public Sub NumericOnly(ByVal e As KeyPressEventArgs)
        If IsNumeric(e.KeyChar) Or e.KeyChar = "," Or e.KeyChar = "." Or e.KeyChar = ControlChars.Back Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub
    Public Sub syncExcelH1toH2(filename As String, kpb As String, batch As String, proses As Boolean, asli As Boolean)
        Dim conn2 As New OleDbConnection(strcon)
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing
        Dim appXL As Excel.Application
        Dim wbXl As Excel.Workbook
        Dim shXL As Excel.Worksheet = Nothing
        Dim x As Integer = 0, y As Integer = 0
        Dim row As Integer = 0, col As Integer = 0
        Dim md As String = ""
        Dim dealer As String = ""
        Dim bulan As String = "", tahun As String = ""
        Dim tableRev As String = IIf(asli = True, "", "_rev")
        Dim xlrng As Excel.Range

        conn2.Open()
        'Try
        appXL = CreateObject("Excel.Application")
        wbXl = appXL.Workbooks.Open(filename)
        If proses = True Then
            shXL = wbXl.Worksheets("Tabulasi H1 to H2 145 D")

            'delete sheet tidak terpakai
            wbXl.Application.DisplayAlerts = False
            wbXl.Sheets("Rekap&Grafik H1to H2 145 D").Delete()
            wbXl.Application.DisplayAlerts = True

            'Add kolom bulan & tahun
            cmd = New OleDbCommand("select distinct tahun,bulan from t_h1toh2" & tableRev & " where kpb='" & kpb & "' order by tahun desc,bulan desc", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                y = 0
                While IsDate(CType(shXL.Cells(2, 5 + (y * 2)), Excel.Range).Value())
                    If Year(CType(shXL.Cells(2, 5 + (y * 2)), Excel.Range).Value()) < reader.Item("tahun") Or _
                       (Year(CType(shXL.Cells(2, 5 + (y * 2)), Excel.Range).Value()) = reader.Item("tahun") And Month(CType(shXL.Cells(2, 5 + (y * 2)), Excel.Range).Value()) > CInt(reader.Item("bulan").ToString)) Then
                        With shXL
                            .Columns(5 + (y * 2)).Insert(Excel.XlInsertShiftDirection.xlShiftToRight, Excel.XlInsertFormatOrigin.xlFormatFromRightOrBelow)
                            .Columns(5 + (y * 2)).Insert(Excel.XlInsertShiftDirection.xlShiftToRight, Excel.XlInsertFormatOrigin.xlFormatFromRightOrBelow)
                            CType(.Cells(2, 5 + (y * 2)), Excel.Range).Value = reader.Item("bulan") & "/01/" & reader("tahun")
                            CType(.Cells(2, 5 + (y * 2)), Excel.Range).NumberFormat = "[$-409]mmm-yy;@"
                            xlrng = .Range(.Cells(2, 5 + (y * 2)), .Cells(2, 6 + (y * 2)))
                            mergecenter(xlrng)
                        End With
                        col += 2
                        GoTo sudahada
                    ElseIf (Year(CType(shXL.Cells(2, 5 + (y * 2)), Excel.Range).Value()) = reader.Item("tahun") And Month(CType(shXL.Cells(2, 5 + (y * 2)), Excel.Range).Value()) = CInt(reader.Item("bulan").ToString)) Then
                        GoTo sudahada
                    End If

                    y += 1
                End While
                With shXL
                    .Columns(5 + (y * 2)).Insert(Excel.XlInsertShiftDirection.xlShiftToRight, Excel.XlInsertFormatOrigin.xlFormatFromRightOrBelow)
                    .Columns(5 + (y * 2)).Insert(Excel.XlInsertShiftDirection.xlShiftToRight, Excel.XlInsertFormatOrigin.xlFormatFromRightOrBelow)
                    CType(.Cells(2, 5 + (y * 2)), Excel.Range).Value = reader.Item("bulan") & "/01/" & reader("tahun")
                    CType(.Cells(2, 5 + (y * 2)), Excel.Range).NumberFormat = "[$-409]mmm-yy;@"
                    xlrng = .Range(.Cells(2, 5 + (y * 2)), .Cells(2, 6 + (y * 2)))
                    mergecenter(xlrng)
                End With
sudahada:
            End While
            reader.Close()
            'Add baris dealer
            row = 3
            cmd = New OleDbCommand("select distinct m.dealer_batch,m.dealer_name,md.md_name from (t_h1toh2" & tableRev & " t inner join m_dealer m on t.dealer_kode=m.dealer_kode) inner join m_maindealer md on m.dealer_mdkode=md.md_kode where kpb='" & kpb & "' and m.dealer_batch = '" & batch & "' order by m.dealer_batch,m.dealer_name,md.md_name union all select distinct '" & batch & "','Total','Total' from m_dealer", conn2)
            reader = cmd.ExecuteReader
            While reader.Read
                With shXL
                    For i As Integer = 1 To 13
                        .Rows(row + i).Insert(Excel.XlInsertShiftDirection.xlShiftToRight, Excel.XlInsertFormatOrigin.xlFormatFromRightOrBelow)
                    Next

                    If row = 3 Then CType(.Cells(row, 1), Excel.Range).Value = reader("dealer_batch")
                    CType(.Cells(row, 2), Excel.Range).Value = reader("md_name")
                    CType(.Cells(row, 3), Excel.Range).Value = reader("dealer_name")

                    CType(.Cells(row, 4), Excel.Range).Value = "Total data source (sales m)"
                    CType(.Cells(row + 1, 4), Excel.Range).Value = "Phone No Available"
                    CType(.Cells(row + 2, 4), Excel.Range).Value = "SMS Sent"
                    CType(.Cells(row + 3, 4), Excel.Range).Value = "1st  result KPB (after SMS)"
                    CType(.Cells(row + 4, 4), Excel.Range).Value = "Contacted by Call"
                    CType(.Cells(row + 5, 4), Excel.Range).Value = "Unreachable"
                    CType(.Cells(row + 6, 4), Excel.Range).Value = "Rejected"
                    CType(.Cells(row + 7, 4), Excel.Range).Value = "Workload"
                    CType(.Cells(row + 8, 4), Excel.Range).Value = "2nd result (after call)"
                    CType(.Cells(row + 9, 4), Excel.Range).Value = "Total Visitor KPB"
                    CType(.Cells(row + 10, 4), Excel.Range).Value = "Too Far"
                    CType(.Cells(row + 11, 4), Excel.Range).Value = "No Time"
                    CType(.Cells(row + 12, 4), Excel.Range).Value = "Forget"
                    mergecenter(.Range(.Cells(row, 2), .Cells(row + 12, 2))) 'merge main dealer
                    mergecenter(.Range(.Cells(row, 3), .Cells(row + 12, 3))) 'merga dealer
                    'garis bawah
                    .Range(.Cells(row + 12, 1), .Cells(row + 12, 6 + col)).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous 'insert garis bawah
                    row += 13
                End With
            End While
            reader.Close()
            mergecenter(shXL.Range(shXL.Cells(3, 1), shXL.Cells(row - 1, 1))) 'merge batch
            With shXL
                .Rows(row + 1).delete()
                .Rows(row).delete()
                .Columns(8 + col).delete()
                .Columns(7 + col).delete()
            End With

            'Mulai entry data per bulan
            cmd = New OleDbCommand("select bulan, tahun, Total_DataSource, PhoneAvailable, SMSSent, [1stResult], Contacted, Unreachable, Rejected, Workload, [2ndResult], TotalVisitor, TooFar, NoTime, Forget, m.dealer_batch, m.dealer_name, md.md_name from (t_h1toh2" & tableRev & " t inner join m_dealer m on t.dealer_kode=m.dealer_kode) inner join m_maindealer md on m.dealer_mdkode=md.md_kode where kpb='" & kpb & "' and m.dealer_batch = '" & batch & "' order by m.dealer_batch,t.tahun,t.bulan,dealer_name,kpb union all " & _
                                    "select t.bulan, t.tahun, sum(t.Total_DataSource), sum(t.PhoneAvailable), sum(t.SMSSent), sum(t.[1stResult]), sum(t.Contacted), sum(t.Unreachable), sum(t.Rejected), sum(t.Workload), sum(t.[2ndResult]), sum(t.TotalVisitor), sum(t.TooFar), sum(t.NoTime), sum(t.Forget), '" & batch & "', 'Total', 'Total' from (t_h1toh2" & tableRev & " t inner join m_dealer m on t.dealer_kode=m.dealer_kode) inner join m_maindealer md on m.dealer_mdkode=md.md_kode where kpb='" & kpb & "' and m.dealer_batch = '" & batch & "' group by t.tahun,t.bulan", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                x = 0
                With shXL
                    md = CType(.Cells(3 + (x * 13), 2), Excel.Range).Value()
                    dealer = CType(.Cells(3 + (x * 13), 3), Excel.Range).Value()
                    While CType(.Cells(3 + (x * 13), 2), Excel.Range).Value() <> ""
                        If (CType(.Cells(3 + (x * 13), 2), Excel.Range).Value() = reader.Item("md_name").ToString And CType(.Cells(3 + (x * 13), 3), Excel.Range).Value() = reader.Item("dealer_name")) Then
                            md = CType(.Cells(3 + (x * 13), 2), Excel.Range).Value()
                            dealer = CType(.Cells(3 + (x * 13), 3), Excel.Range).Value()
                            row = x
                        End If
                        x += 1
                    End While
                    x -= 1
                    bulan = 0
                    tahun = 0
                    y = 0
                    While IsDate(CType(.Cells(2, 5 + (y * 2)), Excel.Range).Value())
                        If (Month(CType(.Cells(2, 5 + (y * 2)), Excel.Range).Value()) = CInt(reader.Item("bulan").ToString) And _
                             Year(CType(.Cells(2, 5 + (y * 2)), Excel.Range).Value()) = reader.Item("tahun")) Then
                            bulan = Month(CType(.Cells(2, 5 + (y * 2)), Excel.Range).Value())
                            tahun = Year(CType(.Cells(2, 5 + (y * 2)), Excel.Range).Value())
                            col = y
                        End If
                        y += 1
                    End While
                    If bulan <> CInt(reader.Item("bulan").ToString) Or tahun <> reader.Item("tahun") Or dealer <> reader.Item("dealer_name") Then GoTo lanjut
                    CType(.Cells(3 + (row * 13), 5 + (col * 2)), Excel.Range).Value = reader.Item("Total_DataSource")
                    CType(.Cells(4 + (row * 13), 5 + (col * 2)), Excel.Range).Value = reader.Item("PhoneAvailable").ToString
                    CType(.Cells(5 + (row * 13), 5 + (col * 2)), Excel.Range).Value = reader.Item("SMSSent").ToString
                    CType(.Cells(6 + (row * 13), 5 + (col * 2)), Excel.Range).Value = reader.Item("1stResult").ToString
                    CType(.Cells(7 + (row * 13), 5 + (col * 2)), Excel.Range).Value = reader.Item("Contacted").ToString
                    CType(.Cells(8 + (row * 13), 5 + (col * 2)), Excel.Range).Value = reader.Item("Unreachable").ToString
                    CType(.Cells(9 + (row * 13), 5 + (col * 2)), Excel.Range).Value = reader.Item("Rejected").ToString
                    CType(.Cells(10 + (row * 13), 5 + (col * 2)), Excel.Range).Value = reader.Item("Workload").ToString
                    CType(.Cells(11 + (row * 13), 5 + (col * 2)), Excel.Range).Value = reader.Item("2ndResult").ToString
                    CType(.Cells(12 + (row * 13), 5 + (col * 2)), Excel.Range).Value = reader.Item("TotalVisitor").ToString
                    CType(.Cells(13 + (row * 13), 5 + (col * 2)), Excel.Range).Value = reader.Item("TooFar").ToString
                    CType(.Cells(14 + (row * 13), 5 + (col * 2)), Excel.Range).Value = reader.Item("NoTime").ToString
                    CType(.Cells(15 + (row * 13), 5 + (col * 2)), Excel.Range).Value = reader.Item("Forget").ToString

                    CType(.Cells(4 + (row * 13), 6 + (col * 2)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-1]C[-1]"
                    CType(.Cells(5 + (row * 13), 6 + (col * 2)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-2]C[-1]"
                    CType(.Cells(6 + (row * 13), 6 + (col * 2)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-3]C[-1]"
                    CType(.Cells(7 + (row * 13), 6 + (col * 2)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-4]C[-1]"
                    CType(.Cells(8 + (row * 13), 6 + (col * 2)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-5]C[-1]"
                    CType(.Cells(9 + (row * 13), 6 + (col * 2)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-6]C[-1]"
                    CType(.Cells(10 + (row * 13), 6 + (col * 2)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-7]C[-1]"
                    CType(.Cells(11 + (row * 13), 6 + (col * 2)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-8]C[-1]"
                    CType(.Cells(12 + (row * 13), 6 + (col * 2)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-9]C[-1]"
                    CType(.Cells(13 + (row * 13), 6 + (col * 2)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-10]C[-1]"
                    CType(.Cells(14 + (row * 13), 6 + (col * 2)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-11]C[-1]"
                    CType(.Cells(15 + (row * 13), 6 + (col * 2)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-12]C[-1]"
                    CType(.Cells(4 + (row * 13), 6 + (col * 2)), Excel.Range).Style = "Percent"
                    CType(.Cells(5 + (row * 13), 6 + (col * 2)), Excel.Range).Style = "Percent"
                    CType(.Cells(6 + (row * 13), 6 + (col * 2)), Excel.Range).Style = "Percent"
                    CType(.Cells(7 + (row * 13), 6 + (col * 2)), Excel.Range).Style = "Percent"
                    CType(.Cells(8 + (row * 13), 6 + (col * 2)), Excel.Range).Style = "Percent"
                    CType(.Cells(9 + (row * 13), 6 + (col * 2)), Excel.Range).Style = "Percent"
                    CType(.Cells(10 + (row * 13), 6 + (col * 2)), Excel.Range).Style = "Percent"
                    CType(.Cells(11 + (row * 13), 6 + (col * 2)), Excel.Range).Style = "Percent"
                    CType(.Cells(12 + (row * 13), 6 + (col * 2)), Excel.Range).Style = "Percent"
                    CType(.Cells(13 + (row * 13), 6 + (col * 2)), Excel.Range).Style = "Percent"
                    CType(.Cells(14 + (row * 13), 6 + (col * 2)), Excel.Range).Style = "Percent"
                    CType(.Cells(15 + (row * 13), 6 + (col * 2)), Excel.Range).Style = "Percent"
                    
                End With
lanjut:
            End While
            reader.Close()
        Else

            shXL = wbXl.Worksheets("Rekap&Grafik H1to H2 145 D")

            wbXl.Application.DisplayAlerts = False
            wbXl.Sheets("Tabulasi H1 to H2 145 D").Delete()
            wbXl.Application.DisplayAlerts = True

            'Add kolom bulan & tahun
            cmd = New OleDbCommand("select distinct tahun,bulan from t_h1toh2" & tableRev & " where kpb='" & kpb & "' order by tahun desc,bulan desc", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                y = 0
                While IsDate(CType(shXL.Cells(2, 5 + y), Excel.Range).Value())
                    If Year(CType(shXL.Cells(2, 5 + y), Excel.Range).Value()) < reader.Item("tahun") Or _
                       (Year(CType(shXL.Cells(2, 5 + y), Excel.Range).Value()) = reader.Item("tahun") And Month(CType(shXL.Cells(2, 5 + y), Excel.Range).Value()) > CInt(reader.Item("bulan").ToString)) Then
                        With shXL
                            .Columns(5 + y).Insert(Excel.XlInsertShiftDirection.xlShiftToRight, Excel.XlInsertFormatOrigin.xlFormatFromRightOrBelow)
                            CType(.Cells(2, 5 + y), Excel.Range).Value = reader.Item("bulan") & "/01/" & reader("tahun")
                            CType(.Cells(2, 5 + y), Excel.Range).NumberFormat = "[$-409]mmm-yy;@"
                        End With
                        col += 1
                        GoTo sudahada1
                    ElseIf (Year(CType(shXL.Cells(2, 5 + y), Excel.Range).Value()) = reader.Item("tahun") And Month(CType(shXL.Cells(2, 5 + y), Excel.Range).Value()) = CInt(reader.Item("bulan").ToString)) Then
                        GoTo sudahada1
                    End If

                    y += 1
                End While
                With shXL
                    .Columns(5 + y).Insert(Excel.XlInsertShiftDirection.xlShiftToRight, Excel.XlInsertFormatOrigin.xlFormatFromRightOrBelow)
                    CType(.Cells(2, 5 + y), Excel.Range).Value = reader.Item("bulan") & "/01/" & reader("tahun")
                    CType(.Cells(2, 5 + y), Excel.Range).NumberFormat = "[$-409]mmm-yy;@"
                End With
sudahada1:
            End While
            reader.Close()
            'Add baris dealer
            row = 3
            cmd = New OleDbCommand("select distinct m.dealer_batch,m.dealer_name,md.md_name from ((t_h1toh2" & tableRev & " t inner join t_dealerinfo" & tableRev & " r on t.bulan=r.bulan and t.tahun=r.tahun and t.dealer_kode=r.dealer_kode) inner join m_dealer m on t.dealer_kode=m.dealer_kode) inner join m_maindealer md on m.dealer_mdkode=md.md_kode where kpb='" & kpb & "' and m.dealer_batch = '" & batch & "' order by m.dealer_name,m.dealer_name,md.md_name union all select distinct '" & batch & "', 'Total', 'Total' from m_dealer", conn2)
            reader = cmd.ExecuteReader
            While reader.Read
                With shXL
                    For i As Integer = 1 To 4
                        .Rows(row + i).Insert(Excel.XlInsertShiftDirection.xlShiftToRight, Excel.XlInsertFormatOrigin.xlFormatFromRightOrBelow)
                    Next

                    If row = 3 Then CType(.Cells(row, 1), Excel.Range).Value = reader("dealer_batch")
                    CType(.Cells(row, 2), Excel.Range).Value = reader("md_name")
                    CType(.Cells(row, 3), Excel.Range).Value = reader("dealer_name")

                    CType(.Cells(row, 4), Excel.Range).Value = "% KPB 1 Own Dealer / Sales"
                    CType(.Cells(row + 1, 4), Excel.Range).Value = "Total Sales"
                    CType(.Cells(row + 2, 4), Excel.Range).Value = "Total Unit Entry"
                    CType(.Cells(row + 3, 4), Excel.Range).Value = "Total KPB 1 Own Dealer"

                    mergecenter(.Range(.Cells(row, 2), .Cells(row + 3, 2))) 'merge main dealer
                    mergecenter(.Range(.Cells(row, 3), .Cells(row + 3, 3))) 'merge dealer
                    'garis bawah
                    .Range(.Cells(row + 3, 1), .Cells(row + 3, 5 + col)).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
                    row += 4
                End With
            End While
            reader.Close()
            mergecenter(shXL.Range(shXL.Cells(3, 1), shXL.Cells(row - 1, 1))) 'merge batch

            With shXL
                CType(.Cells(1, 5), Excel.Range).Value() = "After"
                mergecenter(.Range(.Cells(1, 5), .Cells(1, 5 + col)))

                .Rows(row + 1).delete()
                .Rows(row).delete()
                .Columns(6 + col).delete()
            End With

            cmd = New OleDbCommand("select t.bulan,t.tahun,t.totalvisitor,(select total from t_dealerinfo" & tableRev & " r where t.bulan=r.bulan and t.tahun=r.tahun and t.dealer_kode=r.dealer_kode and keterangan5='total sales' and keterangan2 = 'dealer piloting') as TotalSales,(select total from t_dealerinfo" & tableRev & " r where t.bulan=r.bulan and t.tahun=r.tahun and t.dealer_kode=r.dealer_kode and keterangan6='total unit entry' and keterangan2 = 'dealer piloting') as TotalUnitEntry,m.dealer_batch,m.dealer_name,md.md_name from (t_h1toh2" & tableRev & " t inner join m_dealer m on t.dealer_kode=m.dealer_kode) inner join m_maindealer md on m.dealer_mdkode=md.md_kode where kpb='" & kpb & "' and m.dealer_batch = '" & batch & "' order by t.bulan,t.tahun,dealer_name,kpb union all " & _
                                    "select t.bulan, t.tahun, sum(t.totalvisitor), sum(select total from t_dealerinfo" & tableRev & " r where t.bulan=r.bulan and t.tahun=r.tahun and t.dealer_kode=r.dealer_kode and keterangan5='total sales' and keterangan2 = 'dealer piloting'), sum(select total from t_dealerinfo" & tableRev & " r where t.bulan=r.bulan and t.tahun=r.tahun and t.dealer_kode=r.dealer_kode and keterangan6='total unit entry' and keterangan2 = 'dealer piloting'), '" & batch & "', 'Total', 'Total' from (t_h1toh2" & tableRev & " t inner join m_dealer m on t.dealer_kode=m.dealer_kode) inner join m_maindealer md on m.dealer_mdkode=md.md_kode where kpb='" & kpb & "' and m.dealer_batch = '" & batch & "' group by t.tahun,t.bulan,t.dealer_kode", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                x = 0
                With shXL
                    md = ""
                    dealer = ""
                    While CType(.Cells(3 + (x * 4), 2), Excel.Range).Value() <> ""
                        If UCase(CType(.Cells(3 + (x * 4), 2), Excel.Range).Value()) = UCase(reader.Item("md_name").ToString) And UCase(CType(.Cells(3 + (x * 4), 3), Excel.Range).Value()) = UCase(reader.Item("dealer_name")) Then
                            md = CType(.Cells(3 + (x * 4), 2), Excel.Range).Value()
                            dealer = CType(.Cells(3 + (x * 4), 3), Excel.Range).Value()
                            row = x
                        End If
                        x += 1
                    End While
                    x -= 1
                    bulan = 0
                    tahun = 0
                    y = 0
                    While IsDate(CType(.Cells(2, 5 + y), Excel.Range).Value())
                        If (Month(CType(.Cells(2, 5 + y), Excel.Range).Value()) = CInt(reader.Item("bulan").ToString) And _
                             Year(CType(.Cells(2, 5 + y), Excel.Range).Value()) = reader.Item("tahun")) Then
                            bulan = Month(CType(.Cells(2, 5 + y), Excel.Range).Value())
                            tahun = Year(CType(.Cells(2, 5 + y), Excel.Range).Value())
                            col = y
                        End If
                        y += 1
                    End While
                    If bulan <> CInt(reader.Item("bulan").ToString) Or tahun <> reader.Item("tahun") Or UCase(dealer) <> UCase(reader.Item("dealer_name")) Then GoTo lanjutrekap
                    CType(.Cells(4 + (row * 4), 5 + col), Excel.Range).Value = reader.Item("TotalSales")
                    CType(.Cells(5 + (row * 4), 5 + col), Excel.Range).Value = reader.Item("TotalUnitEntry").ToString
                    CType(.Cells(6 + (row * 4), 5 + col), Excel.Range).Value = reader.Item("TotalVisitor").ToString


                    CType(.Cells(3 + (row * 4), 5 + col), Excel.Range).FormulaR1C1 = "=R[3]C/R[1]C"
                    CType(.Cells(3 + (row * 4), 5 + col), Excel.Range).Style = "Percent"
                    
                End With


lanjutrekap:
            End While
            reader.Close()
        End If
        conn2.Close()

        wbXl.SaveAs(appXL.GetSaveAsFilename("D:", "Excel Files (*.xlsx), *.xlsx"), Excel.XlFileFormat.xlOpenXMLWorkbook)

        wbXl.Close(False)
        appXL.Quit()

        releaseObject(shXL)
        releaseObject(wbXl)
        appXL.Visible = True
        releaseObject(appXL)
        MsgBox("Done")
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message.ToString)
        'End Try
    End Sub
    Public Sub syncExcelH2toH1(batch As String, proses As Boolean, asli As Boolean)
        Dim conn2 As New OleDbConnection(strcon)
        conn2.Open()
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing
        Dim appXL As Excel.Application
        Dim wbXl As Excel.Workbook
        Dim shXL As Excel.Worksheet = Nothing
        Dim x As Integer = 0, y As Integer = 0
        Dim row As Integer = 0, col As Integer = 0
        Dim md As String = ""
        Dim dealer As String = ""
        Dim bulan As String = "", tahun As String = ""
        Dim tableRev As String = ""
        Dim xlrng As Excel.Range
        tableRev = IIf(asli = True, "", "" & tableRev & "")
        'Try
        appXL = CreateObject("Excel.Application")
        wbXl = appXL.Workbooks.Open(fileH2toH1)
        If proses = True Then
            shXL = wbXl.Worksheets("145 Dealer")
            'delete sheet tidak terpakai
            wbXl.Application.DisplayAlerts = False
            wbXl.Sheets("Result").Delete()
            wbXl.Application.DisplayAlerts = True

            cmd = New OleDbCommand("select distinct tahun,bulan from t_h2toh1" & tableRev & " order by tahun desc,bulan desc", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                y = 0
                Debug.Print("tahun : " & Year(CType(shXL.Cells(8, 5 + (y * 6)), Excel.Range).Value()))
                Debug.Print("bulan : " & Month(CType(shXL.Cells(8, 5 + (y * 6)), Excel.Range).Value()))
                While IsDate(CType(shXL.Cells(8, 5 + (y * 6)), Excel.Range).Value())
                    If Year(CType(shXL.Cells(8, 5 + (y * 6)), Excel.Range).Value()) < reader.Item("tahun") Or _
                       (Year(CType(shXL.Cells(8, 5 + (y * 6)), Excel.Range).Value()) = reader.Item("tahun") And Month(CType(shXL.Cells(8, 5 + (y * 6)), Excel.Range).Value()) > CInt(reader.Item("bulan").ToString)) Then
                        With shXL
                            For i As Integer = 0 To 5
                                .Columns(5 + (y * 6)).Insert(Excel.XlInsertShiftDirection.xlShiftToRight, Excel.XlInsertFormatOrigin.xlFormatFromRightOrBelow)
                                .Columns(5 + (y * 6)).NumberFormat = "General"
                            Next
                            'header tanggal
                            CType(.Cells(8, 5 + (y * 6)), Excel.Range).Value = reader.Item("bulan") & "/01/" & reader("tahun")
                            CType(.Cells(8, 5 + (y * 6)), Excel.Range).NumberFormat = "[$-409]mmm-yy;@"
                            xlrng = .Range(.Cells(8, 5 + (y * 6)), .Cells(8, 10 + (y * 6)))
                            mergecenter(xlrng)
                            'h1toh1 buy only
                            CType(.Cells(9, 5 + (y * 6)), Excel.Range).Value = "H1 To H1" & Chr(10) & "Buy(Only)"
                            CType(.Cells(9, 5 + (y * 6)), Excel.Range).ColumnWidth = 11
                            CType(.Cells(9, 6 + (y * 6)), Excel.Range).ColumnWidth = 11
                            xlrng = .Range(.Cells(9, 5 + (y * 6)), .Cells(9, 6 + (y * 6)))
                            mergecenter(xlrng)
                            CType(.Cells(9, 7 + (y * 6)), Excel.Range).Value = "H2 To H1" & Chr(10) & "Own Dealer"
                            CType(.Cells(9, 7 + (y * 6)), Excel.Range).ColumnWidth = 11
                            CType(.Cells(9, 8 + (y * 6)), Excel.Range).ColumnWidth = 11
                            xlrng = .Range(.Cells(9, 7 + (y * 6)), .Cells(9, 8 + (y * 6)))
                            mergecenter(xlrng)
                            CType(.Cells(9, 9 + (y * 6)), Excel.Range).Value = "H2 To H1" & Chr(10) & "Other Dealer"
                            CType(.Cells(9, 9 + (y * 6)), Excel.Range).ColumnWidth = 11
                            CType(.Cells(9, 10 + (y * 6)), Excel.Range).ColumnWidth = 11
                            xlrng = .Range(.Cells(9, 9 + (y * 6)), .Cells(9, 10 + (y * 6)))
                            mergecenter(xlrng)
                        End With
                        col += 6
                        GoTo sudahada
                    ElseIf (Year(CType(shXL.Cells(8, 5 + (y * 6)), Excel.Range).Value()) = reader.Item("tahun") And Month(CType(shXL.Cells(8, 5 + (y * 6)), Excel.Range).Value()) = CInt(reader.Item("bulan").ToString)) Then
                        GoTo sudahada
                    End If

                    y += 1
                End While
                With shXL
                    For i As Integer = 0 To 5
                        .Columns(5 + (y * 6)).Insert(Excel.XlInsertShiftDirection.xlShiftToRight, Excel.XlInsertFormatOrigin.xlFormatFromLeftOrAbove)
                        .Columns(5 + (y * 6)).NumberFormat = "General"
                    Next
                    'header tanggal
                    CType(.Cells(8, 5 + (y * 6)), Excel.Range).Value = reader.Item("bulan") & "/01/" & reader("tahun")
                    CType(.Cells(8, 5 + (y * 6)), Excel.Range).NumberFormat = "[$-409]mmm-yy;@"
                    xlrng = .Range(.Cells(8, 5 + (y * 6)), .Cells(8, 10 + (y * 6)))
                    mergecenter(xlrng)
                    'h1toh1 buy only
                    CType(.Cells(9, 5 + (y * 6)), Excel.Range).Value = "H1 To H1" & Chr(10) & "Buy(Only)"
                    CType(.Cells(9, 5 + (y * 6)), Excel.Range).ColumnWidth = 11
                    CType(.Cells(9, 6 + (y * 6)), Excel.Range).ColumnWidth = 11
                    xlrng = .Range(.Cells(9, 5 + (y * 6)), .Cells(9, 6 + (y * 6)))
                    mergecenter(xlrng)
                    CType(.Cells(9, 7 + (y * 6)), Excel.Range).Value = "H2 To H1" & Chr(10) & "Own Dealer"
                    CType(.Cells(9, 7 + (y * 6)), Excel.Range).ColumnWidth = 11
                    CType(.Cells(9, 8 + (y * 6)), Excel.Range).ColumnWidth = 11
                    xlrng = .Range(.Cells(9, 7 + (y * 6)), .Cells(9, 8 + (y * 6)))
                    mergecenter(xlrng)
                    CType(.Cells(9, 9 + (y * 6)), Excel.Range).Value = "H2 To H1" & Chr(10) & "Other Dealer"
                    CType(.Cells(9, 9 + (y * 6)), Excel.Range).ColumnWidth = 11
                    CType(.Cells(9, 10 + (y * 6)), Excel.Range).ColumnWidth = 11
                    xlrng = .Range(.Cells(9, 9 + (y * 6)), .Cells(9, 10 + (y * 6)))
                    mergecenter(xlrng)
                End With
sudahada:
            End While
            reader.Close()

            'Add baris dealer tab1
            row = 10
            cmd = New OleDbCommand("select distinct m.dealer_batch,m.dealer_name,md.md_name from (t_h2toh1" & tableRev & " t inner join m_dealer m on t.dealer_kode=m.dealer_kode) inner join m_maindealer md on m.dealer_mdkode=md.md_kode where m.dealer_batch = '" & batch & "' order by m.dealer_batch,m.dealer_name,md.md_name union all select distinct '" & batch & "','Total','Total' from m_dealer", conn2)
            reader = cmd.ExecuteReader
            While reader.Read
                With shXL
                    For i As Integer = 1 To 18
                        .Rows(row + i).Insert(Excel.XlInsertShiftDirection.xlShiftToRight, Excel.XlInsertFormatOrigin.xlFormatFromRightOrBelow)
                    Next

                    If row = 10 Then CType(.Cells(row, 1), Excel.Range).Value = reader("dealer_batch")
                    CType(.Cells(row, 2), Excel.Range).Value = reader("md_name")
                    CType(.Cells(row, 3), Excel.Range).Value = reader("dealer_name")

                    CType(.Cells(row, 4), Excel.Range).Value = "Total Sales This Month"
                    CType(.Cells(row + 1, 4), Excel.Range).Value = "Total Data Source "
                    CType(.Cells(row + 2, 4), Excel.Range).Value = "Data Filtering"
                    CType(.Cells(row + 3, 4), Excel.Range).Value = "SMS Sent"
                    CType(.Cells(row + 4, 4), Excel.Range).Value = "Workload from (M-1)"
                    CType(.Cells(row + 5, 4), Excel.Range).Value = "Contacted by Phone"
                    CType(.Cells(row + 6, 4), Excel.Range).Value = "Unreachable"
                    CType(.Cells(row + 7, 4), Excel.Range).Value = "Rejected"
                    CType(.Cells(row + 8, 4), Excel.Range).Value = "Workload"
                    CType(.Cells(row + 9, 4), Excel.Range).Value = "Prospect Customer (M-2)"
                    CType(.Cells(row + 10, 4), Excel.Range).Value = "Prospect Customer (M-1)"
                    CType(.Cells(row + 11, 4), Excel.Range).Value = "Prospect Customer (M)"
                    CType(.Cells(row + 12, 4), Excel.Range).Value = "Total Prospect"
                    CType(.Cells(row + 13, 4), Excel.Range).Value = "Hot Prospect"
                    CType(.Cells(row + 14, 4), Excel.Range).Value = "Low Prospect"
                    CType(.Cells(row + 15, 4), Excel.Range).Value = "Not Deal"
                    CType(.Cells(row + 16, 4), Excel.Range).Value = "Deal / Data analysis"
                    CType(.Cells(row + 17, 4), Excel.Range).Value = "Deal/ Sales"
                    mergecenter(.Range(.Cells(row, 2), .Cells(row + 17, 2))) 'merge main dealer
                    mergecenter(.Range(.Cells(row, 3), .Cells(row + 17, 3))) 'merga dealer
                    'garis bawah
                    .Range(.Cells(row + 17, 1), .Cells(row + 17, 10 + col)).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous 'insert garis bawah
                    row += 18
                End With
            End While
            reader.Close()
            mergecenter(shXL.Range(shXL.Cells(10, 1), shXL.Cells(row - 1, 1))) 'merge batch
            With shXL
                .Rows(row + 1).delete()
                .Rows(row).delete()
                For i As Integer = 1 To 6
                    .Columns(11 + col).delete()
                Next
            End With

            'get list kolom
            Dim column_name As String = ""
            Dim column_sum As String = ""
            Dim filterValues = {Nothing, Nothing, "T_H2toH1", Nothing}
            Dim columns = conn2.GetSchema("Columns", filterValues)
            For Each column As DataRow In columns.Rows
                'Console.WriteLine("{0,-20}{1}", column("column_name"), column("data_type"))
                If column("data_type") = 3 Then
                    column_sum += "sum(t." & column("column_name") & "),"
                    column_name += "t." & column("column_name") & ","
                End If
            Next

            cmd = New OleDbCommand("select t.tahun,t.bulan," & vbCrLf & column_name & vbCrLf & "(select total from t_dealerinfo" & tableRev & " r where t.bulan=r.bulan and t.tahun=r.tahun and t.dealer_kode=r.dealer_kode and keterangan5='total sales' and keterangan2 = 'dealer piloting') as TotalSales,m.dealer_batch,m.dealer_name,md.md_name from (t_h2toh1" & tableRev & " t inner join m_dealer m on t.dealer_kode=m.dealer_kode) inner join m_maindealer md on m.dealer_mdkode=md.md_kode where m.dealer_batch = '" & batch & "' order by t.tahun,t.bulan,dealer_name " & _
                                   "union all select t.tahun,t.bulan," & vbCrLf & column_sum & vbCrLf & "sum(select total from t_dealerinfo" & tableRev & " r where t.bulan=r.bulan and t.tahun=r.tahun and t.dealer_kode=r.dealer_kode and keterangan5='total sales' and keterangan2 = 'dealer piloting'),'" & batch & "','Total','Total' from (t_h2toh1" & tableRev & " t inner join m_dealer m on t.dealer_kode=m.dealer_kode) inner join m_maindealer md on m.dealer_mdkode=md.md_kode where m.dealer_batch = '" & batch & "' group by t.tahun,t.bulan,t.dealer_kode", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                x = 0
                With shXL
                    md = CType(.Cells(10 + (x * 18), 2), Excel.Range).Value()
                    dealer = CType(.Cells(10 + (x * 18), 3), Excel.Range).Value()
                    While CType(.Cells(10 + (x * 18), 2), Excel.Range).Value() <> ""
                        If UCase(CType(.Cells(10 + (x * 18), 3), Excel.Range).Value()) = UCase(reader.Item("dealer_name")) Then
                            md = CType(.Cells(10 + (x * 18), 2), Excel.Range).Value()
                            dealer = CType(.Cells(10 + (x * 18), 3), Excel.Range).Value()
                            row = x
                        End If
                        x += 1
                    End While
                    x -= 1
                    bulan = 0
                    tahun = 0
                    y = 0
                    While IsDate(CType(.Cells(8, 5 + (y * 6)), Excel.Range).Value())
                        If (Month(CType(.Cells(8, 5 + (y * 6)), Excel.Range).Value()) = CInt(reader.Item("bulan").ToString) And _
                             Year(CType(.Cells(8, 5 + (y * 6)), Excel.Range).Value()) = reader.Item("tahun")) Then
                            bulan = Month(CType(.Cells(8, 5 + (y * 6)), Excel.Range).Value())
                            tahun = Year(CType(.Cells(8, 5 + (y * 6)), Excel.Range).Value())
                            col = y
                        End If
                        y += 1
                    End While
                    If bulan <> CInt(reader.Item("bulan").ToString) Or tahun <> reader.Item("tahun") Or UCase(dealer) <> UCase(reader.Item("dealer_name")) Then GoTo lanjut
                    'h1 to h1
                    CType(.Cells(10 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("TotalSales")
                    CType(.Cells(11 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1TotalDataSource")
                    CType(.Cells(12 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1DataFiltering").ToString
                    CType(.Cells(13 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1SMSSent").ToString
                    CType(.Cells(14 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1WorkloadM1").ToString
                    CType(.Cells(15 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1Contacted").ToString
                    CType(.Cells(16 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1Unreachable").ToString
                    CType(.Cells(17 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1Rejected").ToString
                    CType(.Cells(18 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1Workload").ToString
                    CType(.Cells(19 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1ProspectM2").ToString
                    CType(.Cells(20 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1ProspectM1").ToString
                    CType(.Cells(21 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1ProspectM").ToString
                    CType(.Cells(22 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1TotalProspect").ToString
                    CType(.Cells(23 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1HotProspect").ToString
                    CType(.Cells(24 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1LowProspect").ToString
                    CType(.Cells(25 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1NotDeal").ToString
                    CType(.Cells(26 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1DealData").ToString
                    CType(.Cells(27 + (row * 18), 5 + (col * 6)), Excel.Range).Value = reader.Item("H1H1DealSales").ToString

                    CType(.Cells(13 + (row * 18), 6 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-1]C[-1]"
                    CType(.Cells(15 + (row * 18), 6 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-1]C[-1]+R[-3]C[-1])"
                    CType(.Cells(16 + (row * 18), 6 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-2]C[-1]+R[-4]C[-1])"
                    CType(.Cells(17 + (row * 18), 6 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-3]C[-1]+R[-5]C[-1])"
                    CType(.Cells(18 + (row * 18), 6 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-4]C[-1]+R[-6]C[-1])"
                    CType(.Cells(22 + (row * 18), 6 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-10]C[-1]+R[-8]C[-1]+R[-3]C[-1]+R[-2]C[-1])"
                    CType(.Cells(23 + (row * 18), 6 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-11]C[-1]+R[-9]C[-1]+R[-4]C[-1]+R[-3]C[-1])"
                    CType(.Cells(24 + (row * 18), 6 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-12]C[-1]+R[-10]C[-1]+R[-5]C[-1]+R[-4]C[-1])"
                    CType(.Cells(25 + (row * 18), 6 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-13]C[-1]+R[-11]C[-1]+R[-6]C[-1]+R[-5]C[-1])"
                    CType(.Cells(26 + (row * 18), 6 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-14]C[-1]+R[-12]C[-1]+R[-7]C[-1]+R[-6]C[-1])"
                    CType(.Cells(27 + (row * 18), 6 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-17]C[-1]"

                    CType(.Cells(13 + (row * 18), 6 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(15 + (row * 18), 6 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(16 + (row * 18), 6 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(17 + (row * 18), 6 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(18 + (row * 18), 6 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(22 + (row * 18), 6 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(23 + (row * 18), 6 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(24 + (row * 18), 6 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(25 + (row * 18), 6 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(26 + (row * 18), 6 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(27 + (row * 18), 6 + (col * 6)), Excel.Range).Style = "Percent"
                    'h2 to h1 own
                    CType(.Cells(11 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnTotalDataSource")
                    CType(.Cells(12 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnDataFiltering").ToString
                    CType(.Cells(13 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnSMSSent").ToString
                    CType(.Cells(14 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnWorkloadM1").ToString
                    CType(.Cells(15 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnContacted").ToString
                    CType(.Cells(16 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnUnreachable").ToString
                    CType(.Cells(17 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnRejected").ToString
                    CType(.Cells(18 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnWorkload").ToString
                    CType(.Cells(19 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnProspectM2").ToString
                    CType(.Cells(20 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnProspectM1").ToString
                    CType(.Cells(21 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnProspectM").ToString
                    CType(.Cells(22 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnTotalProspect").ToString
                    CType(.Cells(23 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnHotProspect").ToString
                    CType(.Cells(24 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnLowProspect").ToString
                    CType(.Cells(25 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnNotDeal").ToString
                    CType(.Cells(26 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnDealData").ToString
                    CType(.Cells(27 + (row * 18), 7 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OwnDealSales").ToString

                    CType(.Cells(13 + (row * 18), 8 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-1]C[-1]"
                    CType(.Cells(15 + (row * 18), 8 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-1]C[-1]+R[-3]C[-1])"
                    CType(.Cells(16 + (row * 18), 8 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-2]C[-1]+R[-4]C[-1])"
                    CType(.Cells(17 + (row * 18), 8 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-3]C[-1]+R[-5]C[-1])"
                    CType(.Cells(18 + (row * 18), 8 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-4]C[-1]+R[-6]C[-1])"
                    CType(.Cells(22 + (row * 18), 8 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-10]C[-1]+R[-8]C[-1]+R[-3]C[-1]+R[-2]C[-1])"
                    CType(.Cells(23 + (row * 18), 8 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-11]C[-1]+R[-9]C[-1]+R[-4]C[-1]+R[-3]C[-1])"
                    CType(.Cells(24 + (row * 18), 8 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-12]C[-1]+R[-10]C[-1]+R[-5]C[-1]+R[-4]C[-1])"
                    CType(.Cells(25 + (row * 18), 8 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-13]C[-1]+R[-11]C[-1]+R[-6]C[-1]+R[-5]C[-1])"
                    CType(.Cells(26 + (row * 18), 8 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-14]C[-1]+R[-12]C[-1]+R[-7]C[-1]+R[-6]C[-1])"
                    CType(.Cells(27 + (row * 18), 8 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-17]C[-3]"

                    CType(.Cells(13 + (row * 18), 8 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(15 + (row * 18), 8 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(16 + (row * 18), 8 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(17 + (row * 18), 8 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(18 + (row * 18), 8 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(22 + (row * 18), 8 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(23 + (row * 18), 8 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(24 + (row * 18), 8 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(25 + (row * 18), 8 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(26 + (row * 18), 8 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(27 + (row * 18), 8 + (col * 6)), Excel.Range).Style = "Percent"
                    'h2 to h1 other
                    CType(.Cells(11 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherTotalDataSource")
                    CType(.Cells(12 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherDataFiltering").ToString
                    CType(.Cells(13 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherSMSSent").ToString
                    CType(.Cells(14 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherWorkloadM1").ToString
                    CType(.Cells(15 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherContacted").ToString
                    CType(.Cells(16 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherUnreachable").ToString
                    CType(.Cells(17 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherRejected").ToString
                    CType(.Cells(18 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherWorkload").ToString
                    CType(.Cells(19 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherProspectM2").ToString
                    CType(.Cells(20 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherProspectM1").ToString
                    CType(.Cells(21 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherProspectM").ToString
                    CType(.Cells(22 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherTotalProspect").ToString
                    CType(.Cells(23 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherHotProspect").ToString
                    CType(.Cells(24 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherLowProspect").ToString
                    CType(.Cells(25 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherNotDeal").ToString
                    CType(.Cells(26 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherDealData").ToString
                    CType(.Cells(27 + (row * 18), 9 + (col * 6)), Excel.Range).Value = reader.Item("H2H1OtherDealSales").ToString

                    CType(.Cells(13 + (row * 18), 10 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-1]C[-1]"
                    CType(.Cells(15 + (row * 18), 10 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-1]C[-1]+R[-3]C[-1])"
                    CType(.Cells(16 + (row * 18), 10 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-2]C[-1]+R[-4]C[-1])"
                    CType(.Cells(17 + (row * 18), 10 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-3]C[-1]+R[-5]C[-1])"
                    CType(.Cells(18 + (row * 18), 10 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-4]C[-1]+R[-6]C[-1])"
                    CType(.Cells(22 + (row * 18), 10 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-10]C[-1]+R[-8]C[-1]+R[-3]C[-1]+R[-2]C[-1])"
                    CType(.Cells(23 + (row * 18), 10 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-11]C[-1]+R[-9]C[-1]+R[-4]C[-1]+R[-3]C[-1])"
                    CType(.Cells(24 + (row * 18), 10 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-12]C[-1]+R[-10]C[-1]+R[-5]C[-1]+R[-4]C[-1])"
                    CType(.Cells(25 + (row * 18), 10 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-13]C[-1]+R[-11]C[-1]+R[-6]C[-1]+R[-5]C[-1])"
                    CType(.Cells(26 + (row * 18), 10 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/(R[-14]C[-1]+R[-12]C[-1]+R[-7]C[-1]+R[-6]C[-1])"
                    CType(.Cells(27 + (row * 18), 10 + (col * 6)), Excel.Range).FormulaR1C1 = "=R[0]C[-1]/R[-17]C[-5]"

                    CType(.Cells(13 + (row * 18), 10 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(15 + (row * 18), 10 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(16 + (row * 18), 10 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(17 + (row * 18), 10 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(18 + (row * 18), 10 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(22 + (row * 18), 10 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(23 + (row * 18), 10 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(24 + (row * 18), 10 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(25 + (row * 18), 10 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(26 + (row * 18), 10 + (col * 6)), Excel.Range).Style = "Percent"
                    CType(.Cells(27 + (row * 18), 10 + (col * 6)), Excel.Range).Style = "Percent"
                    'merge total sales
                    mergecenter(shXL.Range(.Cells(10 + (row * 18), 5 + (col * 6)), .Cells(10 + (row * 18), 10 + (col * 6))))
                    'garis bawah
                    .Range(.Cells(10 + (row * 18), 5 + (col * 6)), .Cells(10 + (row * 18), 10 + (col * 6))).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
                End With


lanjut:
            End While
            reader.Close()
        Else

            shXL = wbXl.Worksheets("Result")
            'hapus sheet tak terpakai
            wbXl.Application.DisplayAlerts = False
            wbXl.Sheets("145 Dealer").Delete()
            wbXl.Application.DisplayAlerts = True

            cmd = New OleDbCommand("select distinct tahun,bulan from t_h2toh1" & tableRev & " order by tahun desc,bulan desc", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                y = 0
                While IsDate(CType(shXL.Cells(2, 5 + y), Excel.Range).Value())
                    If Year(CType(shXL.Cells(2, 5 + y), Excel.Range).Value()) < reader.Item("tahun") Or _
                       (Year(CType(shXL.Cells(2, 5 + y), Excel.Range).Value()) = reader.Item("tahun") And Month(CType(shXL.Cells(2, 5 + y), Excel.Range).Value()) > CInt(reader.Item("bulan").ToString)) Then
                        With shXL
                            .Columns(5 + y).Insert(Excel.XlInsertShiftDirection.xlShiftToRight, Excel.XlInsertFormatOrigin.xlFormatFromRightOrBelow)
                            CType(.Cells(2, 5 + y), Excel.Range).Value = reader.Item("bulan") & "/01/" & reader("tahun")
                            CType(.Cells(2, 5 + y), Excel.Range).NumberFormat = "[$-409]mmm-yy;@"
                        End With
                        col += 1
                        GoTo sudahada1
                    ElseIf (Year(CType(shXL.Cells(2, 5 + y), Excel.Range).Value()) = reader.Item("tahun") And Month(CType(shXL.Cells(2, 5 + y), Excel.Range).Value()) = CInt(reader.Item("bulan").ToString)) Then
                        GoTo sudahada1
                    End If

                    y += 1
                End While
                With shXL
                    .Columns(5 + y).Insert(Excel.XlInsertShiftDirection.xlShiftToRight, Excel.XlInsertFormatOrigin.xlFormatFromRightOrBelow)
                    CType(.Cells(2, 5 + y), Excel.Range).Value = reader.Item("bulan") & "/01/" & reader("tahun")
                    CType(.Cells(2, 5 + y), Excel.Range).NumberFormat = "[$-409]mmm-yy;@"
                End With
sudahada1:
            End While
            reader.Close()

            'Add baris dealer tab2
            row = 3
            cmd = New OleDbCommand("select distinct m.dealer_batch,md.md_name,m.dealer_name from ((t_h2toh1" & tableRev & " t inner join t_dealerinfo" & tableRev & " r on t.bulan=r.bulan and t.tahun=r.tahun and t.dealer_kode=r.dealer_kode) inner join m_dealer m on t.dealer_kode=m.dealer_kode) inner join m_maindealer md on m.dealer_mdkode=md.md_kode where dealer_batch = '" & batch & "' order by dealer_batch,dealer_name union all select distinct '" & batch & "','Total','Total' from m_dealer", conn2)
            reader = cmd.ExecuteReader
            While reader.Read
                With shXL
                    For i As Integer = 1 To 8
                        .Rows(row + i).Insert(Excel.XlInsertShiftDirection.xlShiftToRight, Excel.XlInsertFormatOrigin.xlFormatFromRightOrBelow)
                    Next

                    If row = 3 Then CType(.Cells(row, 1), Excel.Range).Value = reader("dealer_batch")
                    CType(.Cells(row, 2), Excel.Range).Value = reader("md_name")
                    CType(.Cells(row, 3), Excel.Range).Value = reader("dealer_name")

                    CType(.Cells(row, 4), Excel.Range).Value = "H2 OWN D to H1 RATE"
                    CType(.Cells(row + 1, 4), Excel.Range).Value = "H2 OTHERS D to H1 RATE"
                    CType(.Cells(row + 2, 4), Excel.Range).Value = "H2 to H1 RATE"
                    CType(.Cells(row + 3, 4), Excel.Range).Value = "RETENTION RATE DATA UTILIZATION"
                    CType(.Cells(row + 4, 4), Excel.Range).Value = "H1 TO H1 BUY ONLY"
                    CType(.Cells(row + 5, 4), Excel.Range).Value = "H2 OWN DEALER TO H1"
                    CType(.Cells(row + 6, 4), Excel.Range).Value = "H2 OTHERS DEALER TO H1"
                    CType(.Cells(row + 7, 4), Excel.Range).Value = "TOTAL(SALES)"

                    mergecenter(.Range(.Cells(row, 2), .Cells(row + 7, 2))) 'merge main dealer
                    mergecenter(.Range(.Cells(row, 3), .Cells(row + 7, 3))) 'merga dealer
                    'garis bawah
                    .Range(.Cells(row + 7, 1), .Cells(row + 7, 6 + col)).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous 'insert garis bawah
                    row += 8
                End With
            End While
            reader.Close()
            mergecenter(shXL.Range(shXL.Cells(3, 1), shXL.Cells(row - 1, 1))) 'merge batch
            With shXL
                .Rows(row + 1).delete()
                .Rows(row).delete()
                .Columns(7 + col).delete()
                .Columns(6 + col).delete()
            End With

            cmd = New OleDbCommand("select t.tahun,t.bulan,t.H1H1DealData,t.H2H1OwnDealData,t.H2H1OtherDealData,(select total from t_dealerinfo" & tableRev & " r where t.bulan=r.bulan and t.tahun=r.tahun and t.dealer_kode=r.dealer_kode and keterangan5='total sales' and keterangan2 = 'dealer piloting') as TotalSales,(select total from t_dealerinfo" & tableRev & " r where t.bulan=r.bulan and t.tahun=r.tahun and t.dealer_kode=r.dealer_kode and keterangan5='total unit entry') as TotalUnitEntry,m.dealer_batch,m.dealer_name,md.md_name from (t_h2toh1" & tableRev & " t inner join m_dealer m on t.dealer_kode=m.dealer_kode) inner join m_maindealer md on m.dealer_mdkode=md.md_kode where dealer_batch = '" & batch & "' order by t.tahun,t.bulan,dealer_name union all " & _
                                    "select t.tahun,t.bulan,sum(t.H1H1DealData),sum(t.H2H1OwnDealData),sum(t.H2H1OtherDealData),sum(select total from t_dealerinfo" & tableRev & " r where t.bulan=r.bulan and t.tahun=r.tahun and t.dealer_kode=r.dealer_kode and keterangan5='total sales' and keterangan2 = 'dealer piloting'),(select total from t_dealerinfo" & tableRev & " r where t.bulan=r.bulan and t.tahun=r.tahun and t.dealer_kode=r.dealer_kode and keterangan5='total unit entry'),'" & batch & "','Total','Total' from ((t_h2toh1" & tableRev & " t inner join t_dealerinfo" & tableRev & " r on t.bulan=r.bulan and t.tahun=r.tahun and t.dealer_kode=r.dealer_kode) inner join m_dealer m on t.dealer_kode=m.dealer_kode) inner join m_maindealer md on m.dealer_mdkode=md.md_kode where dealer_batch = '" & batch & "' group by t.tahun,t.bulan,t.dealer_kode", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                x = 0
                With shXL
                    md = ""
                    dealer = ""
                    While CType(.Cells(3 + (x * 8), 3), Excel.Range).Value() <> ""
                        If UCase(CType(.Cells(3 + (x * 8), 3), Excel.Range).Value()) = UCase(reader.Item("dealer_name")) Then
                            dealer = CType(.Cells(3 + (x * 8), 3), Excel.Range).Value()
                            row = x
                        End If
                        x += 1
                    End While
                    x -= 1
                    bulan = 0
                    tahun = 0
                    y = 0
                    While IsDate(CType(.Cells(2, 5 + y), Excel.Range).Value())
                        If (Month(CType(.Cells(2, 5 + y), Excel.Range).Value()) = CInt(reader.Item("bulan").ToString) And _
                             Year(CType(.Cells(2, 5 + y), Excel.Range).Value()) = reader.Item("tahun")) Then
                            bulan = Month(CType(.Cells(2, 5 + y), Excel.Range).Value())
                            tahun = Year(CType(.Cells(2, 5 + y), Excel.Range).Value())
                            col = y
                        End If
                        y += 1
                    End While
                    If bulan <> CInt(reader.Item("bulan").ToString) Or tahun <> reader.Item("tahun") Or UCase(dealer) <> UCase(reader.Item("dealer_name")) Then GoTo lanjutrekap
                    CType(.Cells(7 + (row * 8), 5 + col), Excel.Range).Value = reader.Item("H1H1DealData").ToString
                    CType(.Cells(8 + (row * 8), 5 + col), Excel.Range).Value = reader.Item("H2H1OwnDealData").ToString
                    CType(.Cells(9 + (row * 8), 5 + col), Excel.Range).Value = reader.Item("H2H1OtherDealData").ToString
                    CType(.Cells(10 + (row * 8), 5 + col), Excel.Range).Value = reader.Item("totalsales").ToString


                    CType(.Cells(3 + (row * 8), 5 + col), Excel.Range).FormulaR1C1 = "=R[5]C/R[7]C"
                    CType(.Cells(4 + (row * 8), 5 + col), Excel.Range).FormulaR1C1 = "=R[5]C/R[6]C"
                    CType(.Cells(5 + (row * 8), 5 + col), Excel.Range).FormulaR1C1 = "=(R[3]C+R[4]C)/R[5]C"
                    CType(.Cells(6 + (row * 8), 5 + col), Excel.Range).FormulaR1C1 = "=(R[1]C+R[2]C+R[3]C)/R[4]C"
                    CType(.Cells(3 + (row * 8), 5 + col), Excel.Range).Style = "Percent"
                    CType(.Cells(4 + (row * 8), 5 + col), Excel.Range).Style = "Percent"
                    CType(.Cells(5 + (row * 8), 5 + col), Excel.Range).Style = "Percent"
                    CType(.Cells(6 + (row * 8), 5 + col), Excel.Range).Style = "Percent"

                End With


lanjutrekap:
            End While
            reader.Close()
        End If
        conn2.Close()
        appXL.Visible = True
        wbXl.SaveAs(appXL.GetSaveAsFilename("D:", "Excel Files (*.xlsx), *.xlsx"), Excel.XlFileFormat.xlOpenXMLWorkbook)
        wbXl.Close(False)
        appXL.Quit()

        releaseObject(shXL)
        releaseObject(wbXl)
        releaseObject(appXL)
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message.ToString)
        'End Try
    End Sub
    Public Sub mergecenter(rng As Excel.Range)
        rng.Merge()
        rng.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
        rng.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
        rng.WrapText = True
        rng.Orientation = 0
        rng.AddIndent = False
        rng.IndentLevel = 0
        rng.ShrinkToFit = False
        rng.ReadingOrder = Excel.Constants.xlLTR
        rng.MergeCells = True
    End Sub
    Public Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
End Module
