﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class clsDatabase
    Public Function strPerintah(ByVal strSql As String) As Boolean
        Dim tmpstr() As String
        tmpstr = strSql.Split(CChar(";"))
        For i As Integer = 0 To tmpstr.GetUpperBound(0)
            If conn.State = System.Data.ConnectionState.Open Then conn.Close()
            conn.Open()
            If Trim(tmpstr(i)).Length < 5 Then Exit For
            Dim trans As SqlTransaction = Conn.BeginTransaction
            cmd.Connection = conn
            cmd.Transaction = trans
            Try
                cmd.CommandText = Trim(tmpstr(i))
                cmd.ExecuteNonQuery()
                trans.Commit()
                conn.Close()
            Catch ex As Exception
                trans.Rollback()
                conn.Close()
                Return False
            End Try
        Next
        Return True
    End Function

    Public Sub Open()
        conn.Close()
        If conn.State = System.Data.ConnectionState.Open Then conn.Close()
        Conn.ConnectionString = "Provider=SQLOLEDB.1;Data Source=a5-e5101fcb1d21;Initial Catalog=3pm-elporindo;User ID=sa;Password=3pmadmin;"
        Try
            conn.Open()
        Catch ex As Exception
            conn.Close()
            conn.Open()
        End Try
    End Sub

    Public Sub Close()
        conn.Close()
    End Sub

    Public Function GetReader(ByVal StrSql As String) As SqlDataReader
        Try
            cmd = Conn.CreateCommand()
            cmd.CommandText = StrSql
            Return cmd.ExecuteReader()
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    Public Function Nilai_Dr(ByVal nilaifield As String) As String
        Try
            Return CStr(IIf(IsDBNull(dr(nilaifield)), "", dr(nilaifield)))
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function Ambil_Nilai(ByVal fromField As String, ByVal perintahSql As String) As Collection
        Dim kol As New Collection
        Open()
        dr = GetReader(perintahSql)
        Try
            While dr.Read
                If dr.HasRows = True Then
                    If Nilai_Dr(fromField).ToString <> "" Then kol.Add(Nilai_Dr(fromField).ToString)
                End If
            End While
            Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            Close()
        End Try
        If kol.Count = 0 Then kol.Add("")
        Return kol
    End Function

End Class
