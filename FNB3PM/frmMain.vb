﻿Imports System.Data.OleDb

Imports System.Windows.Forms.DataVisualization.Charting
Imports System.Runtime.InteropServices
Imports System.Text
Imports Excel = Microsoft.Office.Interop.Excel
Public Class frmMain
    'Public masterKasBank As Boolean = False
    'Public masterBiaya As Boolean = False
    'Public pemasukan As Double
    'Public pengeluaran As Double

   
    'Private Sub cekmdi()
    '    For Each ChildForm As Form In Me.MdiChildren
    '        'If ChildForm.Name = "frmMasterKasBank" Then
    '        'masterKasBank = True
    '        'ElseIf ChildForm.Name = "frmMasterBiaya" Then
    '        'masterBiaya = True
    '        'End If
    '    Next
    'End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'menu_off()
        ToolStripLabel1.Text = user
        ToolStripLabel2.Text = servername
        load_mainPrinter()
        'ToolStripLabel3.Text = sGetINI(Application.StartupPath & "\Setting.ini", "login", "printer", ".\sqlexpress")
        ToolStripLabel4.Text = Now.ToLongDateString
        ToolStripLabel5.Text = Now.ToLongTimeString

        'loadchart()
        pRowMax = Get_RowMax(user)
    End Sub

    Public Sub load_mainPrinter()
        
    End Sub

    Private Sub HorizontalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub VerticalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub CascadeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub TutupJendelaAktifToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each ChildForm As Form In Me.MdiChildren
            ChildForm.Close()
        Next
        bringtofrontchart()
    End Sub

    Private Sub frmmain_formclosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If MessageBox.Show("Apakah anda yakin untuk keluar?", "Keluar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            e.Cancel = True
        Else
            CatatLog(user, "Logout", "M_Login", "v")
            frmLogin.Close()

        End If
    End Sub

    Private Sub UserToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim MDIChild As New frmSettingUser
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
        sendtobackchart()
        'MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub GantiPasswordToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim MDIChild As New frmGantiPassword
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
        sendtobackchart()
        'MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub SecurityManagerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim MDIChild As New frmSecurityperGroup
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
        sendtobackchart()
        'MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub


    Sub sendtobackchart()
        'Chart1.SendToBack()
        'Chart2.SendToBack()
    End Sub

    Sub bringtofrontchart()
        'Chart1.BringToFront()
        'Chart2.BringToFront()
    End Sub

    Private Sub mnuMaster1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMaster1.Click
        Dim MDIChild As New frmSettingUser
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub TestImportExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim appXL As Excel.Application
        Dim wbXl As Excel.Workbook
        Dim shXL As Excel.Worksheet
        '      Dim raXL As Excel.Range
        Dim str As String
        'Dim wb As Microsoft.Office.Interop.Excel.Workbook
        appXL = CreateObject("Excel.Application")

        wbXl = appXL.Workbooks.Open(servername + "\Book1.xlsx")
        shXL = wbXl.Worksheets("sheet1")
        For x = 1 To 7
            str = ""
            For y = 1 To 6
                str += shXL.Cells(x, y).Value() & " , "
            Next
            MsgBox(str)
        Next
        appXL.Visible = True

    End Sub

    Private Sub mnuUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuUpload.Click
        Dim MDIChild As New frmUpload
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
        MDIChild.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CatatLog(user, "Upload File", "Menu", "-")
    End Sub

    Private Sub mnuMaster5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuMaster5.Click
        'user setting
        Dim MDIChild As New frmUserSet
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
        MDIChild.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CatatLog(user, "User Setting", "Menu", "-")
    End Sub

    Private Sub mnuExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        Me.Close()
    End Sub

    Private Sub ApprovalToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ApprovalToolStripMenuItem.Click
        Dim MDIChild As New frmViewApprove2
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
        MDIChild.WindowState = System.Windows.Forms.FormWindowState.Maximized

    End Sub

    Private Sub MasterMainDealerToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles MasterMainDealerToolStripMenuItem.Click
        Dim MDIChild As New frmMasterMD
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)

    End Sub

    Private Sub mnuMaster2_Click(sender As System.Object, e As System.EventArgs) Handles mnuMaster2.Click
        Dim MDIChild As New frmMasterDealer
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub mnuMaster3_Click(sender As System.Object, e As System.EventArgs) Handles mnuMaster3.Click
        Dim MDIChild As New frmGantiPassword
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub ExportH1ToH2ToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExportH1ToH2ToolStripMenuItem.Click
        Dim MDIChild As New frmExport
        MDIChild.MdiParent = Me
        MDIChild.btnH2ToH1.Visible = False
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub

    Private Sub ApprovalH1ToH2ToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ApprovalH1ToH2ToolStripMenuItem.Click
        Dim MDIChild As New frmViewApprove
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
        MDIChild.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Private Sub ApprovalDealerInformationToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ApprovalDealerInformationToolStripMenuItem.Click
        Dim MDIChild As New frmViewApprove3
        MDIChild.MdiParent = Me
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
        MDIChild.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Private Sub ExportH2ToH1ToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExportH2ToH1ToolStripMenuItem.Click
        Dim MDIChild As New frmExport
        MDIChild.MdiParent = Me
        MDIChild.btnH1ToH2.Visible = False
        MDIChild.lblKPB.Visible = False
        MDIChild.cmbKPB.Visible = False
        MDIChild.Show()
        MDIChild.Location = New System.Drawing.Point(0, 0)
    End Sub
End Class
