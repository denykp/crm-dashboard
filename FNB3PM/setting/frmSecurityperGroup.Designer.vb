﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSecurityperGroup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtGroup = New System.Windows.Forms.TextBox()
        Me.btnSearchGroup = New System.Windows.Forms.Button()
        Me.tvwMain = New System.Windows.Forms.TreeView()
        Me.btnKeluar = New System.Windows.Forms.Button()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnSimpan = New System.Windows.Forms.Button()
        Me.tabTransaksi = New System.Windows.Forms.TabControl()
        Me.tp1 = New System.Windows.Forms.TabPage()
        Me.tp2 = New System.Windows.Forms.TabPage()
        Me.ckKeluarBayarHutang = New System.Windows.Forms.CheckBox()
        Me.ckKeluarBiaya = New System.Windows.Forms.CheckBox()
        Me.ckKeluarKasbon = New System.Windows.Forms.CheckBox()
        Me.ckKeluarMutasiKas = New System.Windows.Forms.CheckBox()
        Me.ckKeluarGaji = New System.Windows.Forms.CheckBox()
        Me.ckKeluarPrive = New System.Windows.Forms.CheckBox()
        Me.ckMasukPendapatanLain = New System.Windows.Forms.CheckBox()
        Me.ckMasukSetoranModal = New System.Windows.Forms.CheckBox()
        Me.ckMasukMutasiKas = New System.Windows.Forms.CheckBox()
        Me.ckMasukCicilanKasbon = New System.Windows.Forms.CheckBox()
        Me.ckMasukBayarPiutang = New System.Windows.Forms.CheckBox()
        Me.tabTransaksi.SuspendLayout()
        Me.tp1.SuspendLayout()
        Me.tp2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(86, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(11, 13)
        Me.Label3.TabIndex = 56
        Me.Label3.Text = ":"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(14, 15)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(66, 13)
        Me.Label4.TabIndex = 55
        Me.Label4.Text = "Nama Group"
        '
        'txtGroup
        '
        Me.txtGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGroup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtGroup.Location = New System.Drawing.Point(103, 12)
        Me.txtGroup.Name = "txtGroup"
        Me.txtGroup.Size = New System.Drawing.Size(117, 21)
        Me.txtGroup.TabIndex = 54
        '
        'btnSearchGroup
        '
        Me.btnSearchGroup.Location = New System.Drawing.Point(226, 10)
        Me.btnSearchGroup.Name = "btnSearchGroup"
        Me.btnSearchGroup.Size = New System.Drawing.Size(32, 23)
        Me.btnSearchGroup.TabIndex = 116
        Me.btnSearchGroup.Text = "F3"
        Me.btnSearchGroup.UseVisualStyleBackColor = True
        '
        'tvwMain
        '
        Me.tvwMain.CheckBoxes = True
        Me.tvwMain.Location = New System.Drawing.Point(17, 39)
        Me.tvwMain.Name = "tvwMain"
        Me.tvwMain.Size = New System.Drawing.Size(241, 452)
        Me.tvwMain.TabIndex = 119
        '
        'btnKeluar
        '
        Me.btnKeluar.BackColor = System.Drawing.Color.Transparent
        Me.btnKeluar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnKeluar.FlatAppearance.BorderSize = 0
        Me.btnKeluar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnKeluar.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnKeluar.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnKeluar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnKeluar.Location = New System.Drawing.Point(276, 397)
        Me.btnKeluar.Name = "btnKeluar"
        Me.btnKeluar.Size = New System.Drawing.Size(98, 31)
        Me.btnKeluar.TabIndex = 135
        Me.btnKeluar.Text = "Keluar(Esc)"
        Me.btnKeluar.UseVisualStyleBackColor = False
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.Transparent
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnReset.Location = New System.Drawing.Point(276, 360)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(98, 31)
        Me.btnReset.TabIndex = 134
        Me.btnReset.Text = "Reset(Ctrl+R)"
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'btnSimpan
        '
        Me.btnSimpan.BackColor = System.Drawing.Color.Transparent
        Me.btnSimpan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSimpan.FlatAppearance.BorderSize = 0
        Me.btnSimpan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnSimpan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSimpan.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btnSimpan.Location = New System.Drawing.Point(276, 323)
        Me.btnSimpan.Name = "btnSimpan"
        Me.btnSimpan.Size = New System.Drawing.Size(98, 31)
        Me.btnSimpan.TabIndex = 133
        Me.btnSimpan.Text = "Simpan(F2)"
        Me.btnSimpan.UseVisualStyleBackColor = False
        '
        'tabTransaksi
        '
        Me.tabTransaksi.Controls.Add(Me.tp1)
        Me.tabTransaksi.Controls.Add(Me.tp2)
        Me.tabTransaksi.Location = New System.Drawing.Point(276, 39)
        Me.tabTransaksi.Name = "tabTransaksi"
        Me.tabTransaksi.SelectedIndex = 0
        Me.tabTransaksi.Size = New System.Drawing.Size(302, 202)
        Me.tabTransaksi.TabIndex = 136
        '
        'tp1
        '
        Me.tp1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tp1.Controls.Add(Me.ckKeluarPrive)
        Me.tp1.Controls.Add(Me.ckKeluarGaji)
        Me.tp1.Controls.Add(Me.ckKeluarMutasiKas)
        Me.tp1.Controls.Add(Me.ckKeluarKasbon)
        Me.tp1.Controls.Add(Me.ckKeluarBiaya)
        Me.tp1.Controls.Add(Me.ckKeluarBayarHutang)
        Me.tp1.Location = New System.Drawing.Point(4, 22)
        Me.tp1.Name = "tp1"
        Me.tp1.Padding = New System.Windows.Forms.Padding(3)
        Me.tp1.Size = New System.Drawing.Size(294, 176)
        Me.tp1.TabIndex = 0
        Me.tp1.Text = "Kas/Bank Keluar"
        Me.tp1.UseVisualStyleBackColor = True
        '
        'tp2
        '
        Me.tp2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tp2.Controls.Add(Me.ckMasukPendapatanLain)
        Me.tp2.Controls.Add(Me.ckMasukSetoranModal)
        Me.tp2.Controls.Add(Me.ckMasukMutasiKas)
        Me.tp2.Controls.Add(Me.ckMasukCicilanKasbon)
        Me.tp2.Controls.Add(Me.ckMasukBayarPiutang)
        Me.tp2.Location = New System.Drawing.Point(4, 22)
        Me.tp2.Name = "tp2"
        Me.tp2.Padding = New System.Windows.Forms.Padding(3)
        Me.tp2.Size = New System.Drawing.Size(294, 176)
        Me.tp2.TabIndex = 1
        Me.tp2.Text = "Kas/Bank Masuk"
        Me.tp2.UseVisualStyleBackColor = True
        '
        'ckKeluarBayarHutang
        '
        Me.ckKeluarBayarHutang.AutoSize = True
        Me.ckKeluarBayarHutang.Location = New System.Drawing.Point(13, 17)
        Me.ckKeluarBayarHutang.Name = "ckKeluarBayarHutang"
        Me.ckKeluarBayarHutang.Size = New System.Drawing.Size(123, 17)
        Me.ckKeluarBayarHutang.TabIndex = 137
        Me.ckKeluarBayarHutang.Text = "Pembayaran Hutang"
        Me.ckKeluarBayarHutang.UseVisualStyleBackColor = True
        '
        'ckKeluarBiaya
        '
        Me.ckKeluarBiaya.AutoSize = True
        Me.ckKeluarBiaya.Location = New System.Drawing.Point(13, 40)
        Me.ckKeluarBiaya.Name = "ckKeluarBiaya"
        Me.ckKeluarBiaya.Size = New System.Drawing.Size(114, 17)
        Me.ckKeluarBiaya.TabIndex = 138
        Me.ckKeluarBiaya.Text = "Pembayaran Biaya"
        Me.ckKeluarBiaya.UseVisualStyleBackColor = True
        '
        'ckKeluarKasbon
        '
        Me.ckKeluarKasbon.AutoSize = True
        Me.ckKeluarKasbon.Location = New System.Drawing.Point(13, 63)
        Me.ckKeluarKasbon.Name = "ckKeluarKasbon"
        Me.ckKeluarKasbon.Size = New System.Drawing.Size(62, 17)
        Me.ckKeluarKasbon.TabIndex = 139
        Me.ckKeluarKasbon.Text = "Kasbon"
        Me.ckKeluarKasbon.UseVisualStyleBackColor = True
        '
        'ckKeluarMutasiKas
        '
        Me.ckKeluarMutasiKas.AutoSize = True
        Me.ckKeluarMutasiKas.Location = New System.Drawing.Point(13, 86)
        Me.ckKeluarMutasiKas.Name = "ckKeluarMutasiKas"
        Me.ckKeluarMutasiKas.Size = New System.Drawing.Size(78, 17)
        Me.ckKeluarMutasiKas.TabIndex = 140
        Me.ckKeluarMutasiKas.Text = "Mutasi Kas"
        Me.ckKeluarMutasiKas.UseVisualStyleBackColor = True
        '
        'ckKeluarGaji
        '
        Me.ckKeluarGaji.AutoSize = True
        Me.ckKeluarGaji.Location = New System.Drawing.Point(13, 109)
        Me.ckKeluarGaji.Name = "ckKeluarGaji"
        Me.ckKeluarGaji.Size = New System.Drawing.Size(106, 17)
        Me.ckKeluarGaji.TabIndex = 141
        Me.ckKeluarGaji.Text = "Pembayaran Gaji"
        Me.ckKeluarGaji.UseVisualStyleBackColor = True
        '
        'ckKeluarPrive
        '
        Me.ckKeluarPrive.AutoSize = True
        Me.ckKeluarPrive.Location = New System.Drawing.Point(13, 131)
        Me.ckKeluarPrive.Name = "ckKeluarPrive"
        Me.ckKeluarPrive.Size = New System.Drawing.Size(50, 17)
        Me.ckKeluarPrive.TabIndex = 142
        Me.ckKeluarPrive.Text = "Prive"
        Me.ckKeluarPrive.UseVisualStyleBackColor = True
        '
        'ckMasukPendapatanLain
        '
        Me.ckMasukPendapatanLain.AutoSize = True
        Me.ckMasukPendapatanLain.Location = New System.Drawing.Point(16, 109)
        Me.ckMasukPendapatanLain.Name = "ckMasukPendapatanLain"
        Me.ckMasukPendapatanLain.Size = New System.Drawing.Size(122, 17)
        Me.ckMasukPendapatanLain.TabIndex = 147
        Me.ckMasukPendapatanLain.Text = "Pendapatan lain lain"
        Me.ckMasukPendapatanLain.UseVisualStyleBackColor = True
        '
        'ckMasukSetoranModal
        '
        Me.ckMasukSetoranModal.AutoSize = True
        Me.ckMasukSetoranModal.Location = New System.Drawing.Point(16, 86)
        Me.ckMasukSetoranModal.Name = "ckMasukSetoranModal"
        Me.ckMasukSetoranModal.Size = New System.Drawing.Size(95, 17)
        Me.ckMasukSetoranModal.TabIndex = 146
        Me.ckMasukSetoranModal.Text = "Setoran Modal"
        Me.ckMasukSetoranModal.UseVisualStyleBackColor = True
        '
        'ckMasukMutasiKas
        '
        Me.ckMasukMutasiKas.AutoSize = True
        Me.ckMasukMutasiKas.Location = New System.Drawing.Point(16, 63)
        Me.ckMasukMutasiKas.Name = "ckMasukMutasiKas"
        Me.ckMasukMutasiKas.Size = New System.Drawing.Size(78, 17)
        Me.ckMasukMutasiKas.TabIndex = 145
        Me.ckMasukMutasiKas.Text = "Mutasi Kas"
        Me.ckMasukMutasiKas.UseVisualStyleBackColor = True
        '
        'ckMasukCicilanKasbon
        '
        Me.ckMasukCicilanKasbon.AutoSize = True
        Me.ckMasukCicilanKasbon.Location = New System.Drawing.Point(16, 40)
        Me.ckMasukCicilanKasbon.Name = "ckMasukCicilanKasbon"
        Me.ckMasukCicilanKasbon.Size = New System.Drawing.Size(96, 17)
        Me.ckMasukCicilanKasbon.TabIndex = 144
        Me.ckMasukCicilanKasbon.Text = "Cicilan Kasbon"
        Me.ckMasukCicilanKasbon.UseVisualStyleBackColor = True
        '
        'ckMasukBayarPiutang
        '
        Me.ckMasukBayarPiutang.AutoSize = True
        Me.ckMasukBayarPiutang.Location = New System.Drawing.Point(16, 17)
        Me.ckMasukBayarPiutang.Name = "ckMasukBayarPiutang"
        Me.ckMasukBayarPiutang.Size = New System.Drawing.Size(124, 17)
        Me.ckMasukBayarPiutang.TabIndex = 143
        Me.ckMasukBayarPiutang.Text = "Pembayaran Piutang"
        Me.ckMasukBayarPiutang.UseVisualStyleBackColor = True
        '
        'frmSecurityperGroup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(597, 523)
        Me.Controls.Add(Me.tabTransaksi)
        Me.Controls.Add(Me.btnKeluar)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnSimpan)
        Me.Controls.Add(Me.tvwMain)
        Me.Controls.Add(Me.btnSearchGroup)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtGroup)
        Me.KeyPreview = True
        Me.Name = "frmSecurityperGroup"
        Me.Text = "frmSecurityperGroup"
        Me.tabTransaksi.ResumeLayout(False)
        Me.tp1.ResumeLayout(False)
        Me.tp1.PerformLayout()
        Me.tp2.ResumeLayout(False)
        Me.tp2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtGroup As System.Windows.Forms.TextBox
    Friend WithEvents btnSearchGroup As System.Windows.Forms.Button
    Friend WithEvents tvwMain As System.Windows.Forms.TreeView
    Friend WithEvents btnKeluar As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnSimpan As System.Windows.Forms.Button
    Friend WithEvents tabTransaksi As System.Windows.Forms.TabControl
    Friend WithEvents tp1 As System.Windows.Forms.TabPage
    Friend WithEvents tp2 As System.Windows.Forms.TabPage
    Friend WithEvents ckKeluarPrive As System.Windows.Forms.CheckBox
    Friend WithEvents ckKeluarGaji As System.Windows.Forms.CheckBox
    Friend WithEvents ckKeluarMutasiKas As System.Windows.Forms.CheckBox
    Friend WithEvents ckKeluarKasbon As System.Windows.Forms.CheckBox
    Friend WithEvents ckKeluarBiaya As System.Windows.Forms.CheckBox
    Friend WithEvents ckKeluarBayarHutang As System.Windows.Forms.CheckBox
    Friend WithEvents ckMasukPendapatanLain As System.Windows.Forms.CheckBox
    Friend WithEvents ckMasukSetoranModal As System.Windows.Forms.CheckBox
    Friend WithEvents ckMasukMutasiKas As System.Windows.Forms.CheckBox
    Friend WithEvents ckMasukCicilanKasbon As System.Windows.Forms.CheckBox
    Friend WithEvents ckMasukBayarPiutang As System.Windows.Forms.CheckBox
End Class
