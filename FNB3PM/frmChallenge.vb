﻿Imports System.IO

Imports System.Data.OleDb
Public Class frmChallenge
    
    Public LoginSucceeded As Boolean
    ' TODO: Insert code to perform custom authentication using the provided username and password 
    ' (See http://go.microsoft.com/fwlink/?LinkId=35339).  
    ' The custom principal can then be attached to the current thread's principal as follows: 
    '     My.User.CurrentPrincipal = CustomPrincipal
    ' where CustomPrincipal is the IPrincipal implementation used to perform authentication. 
    ' Subsequently, My.User will return identity information encapsulated in the CustomPrincipal object
    ' such as the username, display name, etc.
    Public namagroup As String
    Public keterangan As String
    Public koordinator As Boolean
    Public nomorbill As String
    Public koneksi As String
    Public login1, login2 As Boolean
    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click

        If txtUserName1.Text = "" Then
            MsgBox("User tidak berhak melakukan operasi ini")
        Else
            UserLogin1(txtUserName1.Text, generate_connstring1(txtUserName1.Text, txtPassword1.Text))

            authorize(txtUserName1.Text)
        End If
        Me.Dispose()

    End Sub
    Private Sub authorize(ByVal username1 As String)
        If login1 Then
            LoginSucceeded = True
            executeSQL("insert into t_authorize (tanggal,username,keterangan,nomer_bill) values ('" & Format(Now, "yyyy/MM/dd hh:mm:ss") & "','" & username1 & "','" & keterangan & "','" & nomorbill & "')")

            Me.Hide()
        End If
        Me.Dispose()
    End Sub

    Private Function generate_connstring1(ByVal user1 As String, ByVal pass1 As String) As String
        generate_connstring1 = "Persist Security Info=False;Data Source=" & servername & ";Initial Catalog=" & dbname & ";User ID=" & user1 & ";Password=" & pass1 & ";"
    End Function
    Private Sub UserLogin1(ByVal userlog1 As String, ByVal koneksi As String)
        Dim conn As New oledbConnection(koneksi)
        Dim cmd As oledbCommand
        Dim dr As oledbDataReader
        Dim condition As String = ""

        If namagroup <> "" Then condition &= " nama_group='" & namagroup & "' "
        If condition <> "" Then condition &= " and "
        condition &= " koordinator='" & koordinator & "'"
        Try
            conn.Open()
            cmd = New oledbCommand("select * from user_group where userid='" & userlog1 & "' and modul='POS' and " & condition, conn)
            dr = cmd.ExecuteReader
            If dr.Read Then
                login1 = True
            Else
                MsgBox("User tidak berhak melakukan operasi ini")
            End If
            dr.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
   
    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub
    Private Function Connstring_FINGER() As String
        Connstring_FINGER = "Provider=SQLOLEDB;Data Source=" & servername & ";Initial Catalog=" & dbname & ";User ID=" & user & ";Password=" & pwd & ";"
    End Function

    Private Sub frmChallenge_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        LoginSucceeded = False
    End Sub
    
End Class
