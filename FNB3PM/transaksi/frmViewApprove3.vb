﻿Imports System.Data.OleDb
Public Class frmViewApprove3
    Dim transaction As OleDbTransaction
    Dim revcolor As Color = Color.Chocolate
    Private Sub btnSearchMD_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchMD.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("M_Maindealer")
        f.setColumns("*")

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtKodeMD.Text = f.value
        End If
        f.Dispose()
        load_MD()
    End Sub
    Private Sub load_MD()
        Dim conn2 As New OLEDBConnection(strcon)
        conn2.Open()
        Dim cmd As oledbCommand
        Dim reader As OleDbDataReader = Nothing

        Try
            cmd = New OleDbCommand("select * from M_Maindealer where md_kode = '" & txtKodeMD.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                lblMainDealer.Text = reader("md_name")
            End If
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub
    Private Sub btnSearchDealer_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchDealer.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("M_Dealer")
        f.setColumns("*")
        f.setWhere(IIf(txtKodeMD.Text = "", "", " where dealer_mdkode='" & txtKodeMD.Text & "'"))

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtKodeDlr.Text = f.value
        End If
        f.Dispose()
        load_Dealer()
    End Sub
    Private Sub load_Dealer()
        Dim conn2 As New OleDbConnection(strcon)
        conn2.Open()
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        Try
            cmd = New OleDbCommand("select * from M_Dealer where dealer_kode = '" & txtKodeDlr.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                lblDealer.Text = reader("dealer_name")
            End If
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

    Private Sub txtKodeMD_LostFocus(sender As Object, e As System.EventArgs) Handles txtKodeMD.LostFocus
        load_MD()
    End Sub

    Private Sub txtKodeDlr_LostFocus(sender As Object, e As System.EventArgs) Handles txtKodeDlr.LostFocus
        load_Dealer()
    End Sub

    Private Sub loadData()
        Dim conn2 As New OleDbConnection(strcon)
        conn2.Open()
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing
        Try
            For i = dgvFile.Columns.Count - 1 To 1 Step -1
                dgvFile.Columns.Remove(dgvFile.Columns(i).Name)
            Next
            cmd = New OleDbCommand("select distinct * from (SELECT distinct a.BULAN,a.TAHUN,a.Dealer_Kode,a.MD_Kode,a.keterangan4, " & _
            "total as TotalSales, (select total from t_dealerinfo b where keterangan6 = 'TOTAL UNIT ENTRY' and a.tahun = b.tahun And " & _
            "a.bulan = b.bulan and a.keterangan4 = b.keterangan4) AS TotalUnitEntry FROM t_dealerinfo AS a WHERE " & _
            "(((a.[keterangan5])='TOTAL SALES') AND ((a.dealer_kode)='" & txtKodeDlr.Text & "')) union all SELECT  distinct a.BULAN,a.TAHUN,a.Dealer_Kode, " & _
            "a.MD_Kode,a.keterangan4 ,(select total from t_dealerinfo b where keterangan5 = 'TOTAL SALES' and a.tahun = b.tahun And " & _
            "a.bulan = b.bulan and a.keterangan4 = b.keterangan4) as TotalSales,total AS TotalUnitEntry FROM t_dealerinfo AS a WHERE " & _
            "(((a.[keterangan6])='TOTAL UNIT ENTRY') AND ((a.dealer_kode)='" & txtKodeDlr.Text & "')) ORDER BY a.tahun, a.bulan, a.keterangan4) a ", conn2)
            'cmd = New OleDbCommand("select * from t_dealerinfo where dealer_kode = '" & txtKodeDlr.Text & "' and (keterangan5 ='TOTAL SALES' or keterangan6 = 'TOTAL UNIT ENTRY') order by tahun,bulan,keterangan4", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                With dgvFile
                    .Columns.Add(reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString, reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString)
                    .Columns(.Columns.Count - 1).ReadOnly = True
                    .Columns(.Columns.Count - 1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns(.Columns.Count - 1).Width = 50
                    .Item(.Columns.Count - 1, 0).Value = reader.Item("TotalSales").ToString
                    .Item(.Columns.Count - 1, 1).Value = reader.Item("TotalUnitEntry").ToString
                    .Item(.Columns.Count - 1, 2).Value = reader.Item("keterangan4").ToString
                End With

            End While
            reader.Close()
            For i = dgRev.Columns.Count - 1 To 1 Step -1
                dgRev.Columns.Remove(dgRev.Columns(i).Name)
            Next
            cmd = New OleDbCommand("select distinct * from (SELECT distinct a.BULAN,a.TAHUN,a.Dealer_Kode,a.MD_Kode,a.keterangan4, " & _
            "total as TotalSales, (select total from t_dealerinfo_rev b where keterangan6 = 'TOTAL UNIT ENTRY' and a.tahun = b.tahun And " & _
            "a.bulan = b.bulan and a.keterangan4 = b.keterangan4) AS TotalUnitEntry FROM t_dealerinfo_rev AS a WHERE " & _
            "(((a.[keterangan5])='TOTAL SALES') AND ((a.dealer_kode)='" & txtKodeDlr.Text & "')) union all SELECT  distinct a.BULAN,a.TAHUN,a.Dealer_Kode, " & _
            "a.MD_Kode,a.keterangan4 ,(select total from t_dealerinfo_rev b where keterangan5 = 'TOTAL SALES' and a.tahun = b.tahun And " & _
            "a.bulan = b.bulan and a.keterangan4 = b.keterangan4) as TotalSales,total AS TotalUnitEntry FROM t_dealerinfo_rev AS a WHERE " & _
            "(((a.[keterangan6])='TOTAL UNIT ENTRY') AND ((a.dealer_kode)='" & txtKodeDlr.Text & "')) ORDER BY a.tahun, a.bulan, a.keterangan4) a ", conn2)
            'cmd = New OleDbCommand("select * from t_dealerinfo_rev where dealer_kode = '" & txtKodeDlr.Text & "' order by tahun,bulan", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                With dgRev
                    .Columns.Add(reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString, reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString)
                    .Columns(.Columns.Count - 1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns(.Columns.Count - 1).Width = 50

                    .Item(.Columns.Count - 1, 0).Value = reader.Item("TotalSales").ToString
                    .Item(.Columns.Count - 1, 1).Value = reader.Item("TotalUnitEntry").ToString
                    .Item(.Columns.Count - 1, 2).Value = reader.Item("keterangan4").ToString
                End With

            End While
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

    Private Sub frmViewApprove_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        With dgvFile
            .Rows.Add(3)
            .Item(0, 0).Value = "Total Sales"
            .Item(0, 1).Value = "Total Data Entry"
            .Item(0, 2).Value = "Nama Dealer"
            
        End With
        With dgRev
            .Rows.Add(3)
            .Item(0, 0).Value = "Total Sales"
            .Item(0, 1).Value = "Total Data Entry"
            .Item(0, 2).Value = "Nama Dealer"
        End With
    End Sub

    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Dim conn2 As New OleDbConnection(strcon)
        Try

            conn2.Open()
            transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)

            Dim cmd As OleDbCommand
            Dim reader As OleDbDataReader = Nothing
            Dim fields() As String
            Dim periode() As String
            ReDim fields(2)
            Dim type As String = ""

            fields(0) = "TotalSales"
            fields(1) = "TotalUnitEntry"

            For col As Integer = 1 To dgRev.Columns.Count - 1
                periode = Split(dgRev.Columns(col).HeaderText, "-")
                For row As Integer = 0 To 2
                    If dgRev.Item(col, row).Style.BackColor = revcolor Then
                        'cmd = New OleDbCommand("update t_dealerinfo_rev set " & fields(row) & "='" & dgRev.Item(col, row).Value & "',create_date='" & Format(Now, "yyyy/MM/dd HH:mm:ss") & "',create_user='" & user & "'  where  dealer_kode='" & txtKodeDlr.Text & "' and bulan='" & periode(0) & "' and tahun='" & periode(1) & "'", conn2, transaction)
                        If (dgvFile.Item(col, row).Value Is DBNull.Value Or dgvFile.Item(col, row).Value = "") And dgvFile.Item(col, row).Value <> dgRev.Item(col, row).Value Then
                            MsgBox("Data " & dgRev.Item(0, row).Value & " dengan " & dgRev.Item(0, 2).Value & " " & dgRev.Item(col, 2).Value & " pada bulan " & periode(0) & " tahun " & periode(1) & " tidak dapat ditemukan" & vbCrLf & "Silahkan upload Dealer Information terlebih dahulu sebelum melakukan revisi")
                            Exit Sub
                        End If
                        If row = 0 Then
                            cmd = New OleDbCommand("update t_dealerinfo_rev set total='" & dgRev.Item(col, row).Value & "',create_date='" & Format(Now, "yyyy/MM/dd HH:mm:ss") & "',create_user='" & user & "'  where  dealer_kode='" & txtKodeDlr.Text & "' and bulan='" & periode(0) & "' and tahun='" & periode(1) & "' and keterangan4 ='" & dgRev.Item(col, row + 2).Value & "' and keterangan5 = 'TOTAL SALES' ", conn2, transaction)
                        ElseIf row = 1 Then
                            cmd = New OleDbCommand("update t_dealerinfo_rev set total='" & dgRev.Item(col, row).Value & "',create_date='" & Format(Now, "yyyy/MM/dd HH:mm:ss") & "',create_user='" & user & "'  where  dealer_kode='" & txtKodeDlr.Text & "' and bulan='" & periode(0) & "' and tahun='" & periode(1) & "' and keterangan4 ='" & dgRev.Item(col, row + 1).Value & "' and keterangan6 = 'TOTAL UNIT ENTRY'", conn2, transaction)
                        End If
                        cmd.ExecuteNonQuery()
                    End If
                Next
            Next

            transaction.Commit()
            conn2.Close()
            MsgBox("Data sudah tersimpan")

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()
            transaction.Rollback()
            If conn2.State Then conn2.Close()
        End Try
    End Sub

    Private Sub dgRev_CellValidating(sender As Object, e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgRev.CellValidating
        If dgRev.Item(e.ColumnIndex, e.RowIndex).Value <> e.FormattedValue Then dgRev.Item(e.ColumnIndex, e.RowIndex).Style.BackColor = revcolor

    End Sub

    Private Sub btnLoad_Click(sender As System.Object, e As System.EventArgs) Handles btnLoad.Click
        loadData()
    End Sub
End Class