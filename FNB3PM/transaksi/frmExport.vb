﻿Imports System.Data.OleDb
Public Class frmExport

    Private Sub btnH1ToH2_Click(sender As System.Object, e As System.EventArgs) Handles btnH1ToH2.Click
        syncExcelH1toH2(fileH1toH2_1, cmbKPB.Text, cmbBatch.Text, rbProses.Checked, rbAsli.Checked)
    End Sub

    Private Sub btnH2ToH1_Click(sender As System.Object, e As System.EventArgs) Handles btnH2ToH1.Click
        syncExcelH2toH1(cmbBatch.Text, rbProses.Checked, rbAsli.Checked)
    End Sub

    Private Sub frmExport_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        On Error GoTo err
        Dim conn As New OleDbConnection(strcon)
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing
        conn.Open()
        cmd = New OleDbCommand("select distinct dealer_batch from m_dealer where dealer_batch <> ''", conn)
        reader = cmd.ExecuteReader
        While reader.Read
            cmbBatch.Items.Add(reader(0))
        End While
        reader.Close()
        conn.Close()
        Exit Sub
err:
        MsgBox(Err.Description)
        If Not reader.IsClosed Then reader.Close()
        If conn.State Then conn.Close()
    End Sub
End Class