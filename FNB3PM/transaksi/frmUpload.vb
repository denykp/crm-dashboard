﻿Imports System.Data.OleDb
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.Linq

Public Class frmUpload
    Dim vPathSource As String
    Dim vPathDest As String
    Dim vFileName As String
    Dim vAsal(44, 2) As Integer
    Dim vTuju(44, 2) As Integer
    Dim vNamaMD As String
    Dim vNamaDlr As String
    Public dv1, dv2, dv3, dv4, dv5 As New DataView

    Private Sub frmUpload_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub

    Private Sub frmUpload_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Setting_Grid()
        Setting_ListMD()
        Setting_Path()
        lstMD.Visible = False
        lstMD.BringToFront()
        lstDlr.Visible = False
        lstDlr.BringToFront()
        Panel1.Enabled = True
    End Sub

    Private Sub txtKodeMD_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtKodeMD.KeyDown
        If e.KeyCode = Keys.F5 Then
            Panel1.Enabled = False
            lstMD.Visible = True
            lstMD.Focus()
        ElseIf e.KeyCode = Keys.Enter Then
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub Setting_Path()
        Dim vConn As New OleDbConnection
        vConn.ConnectionString = strcon
        vConn.Open()

        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        pStr = "SELECT TOP 1 * FROM M_UserSetting WHERE User_Name = '" & user & "' ORDER BY ID DESC"

        Try
            cmd = New OleDbCommand(pStr, vConn)
            reader = cmd.ExecuteReader()

            If reader.HasRows Then
                reader.Read()
                vPathSource = reader.Item("Set_SourcePath")
                vPathDest = reader.Item("Set_DestPath")
                txtPath.Text = vPathSource
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
        reader.Close()
        vConn.Close()
    End Sub

    Private Sub Setting_Grid()
        With dgvFile
            .ColumnHeadersDefaultCellStyle.Font = New Font(dgvFile.Font, FontStyle.Bold)
            .ColumnCount = 7
            .Columns(0).Name = "NO."
            .Columns(0).Width = 40
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Name = "DEALER"
            .Columns(1).Width = 150
            .Columns(2).Name = "NAMA FILE"
            .Columns(2).Width = 125
            .Columns(3).Name = "LOCATION"
            .Columns(3).Width = 200
            .Columns(4).Name = "STATUS"
            .Columns(4).Width = 100
            .Columns(5).Name = "TGL.UPLOAD"
            .Columns(5).Width = 100
            .Columns(6).Name = "OLEH"
            .Columns(6).Width = 100

            .AllowUserToResizeColumns = True
            .AllowUserToAddRows = False
            .ReadOnly = True
            '.ColumnHeadersHeight = 0
            .RowHeadersVisible = False
        End With

        DataGridView1.RowHeadersVisible = False
        DataGridView2.RowHeadersVisible = False
        DataGridView3.RowHeadersVisible = False
        DataGridView4.RowHeadersVisible = False
        DataGridView5.RowHeadersVisible = False

        Load_DataFile()
    End Sub

    Private Sub Setting_ListMD()
        Dim vConn As New OleDbConnection
        vConn.ConnectionString = strcon
        vConn.Open()

        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        lstMD.Items.Clear()
        'pStr = "SELECT Dealer_Kode, Dealer_Name FROM M_Dealer WHERE Dealer_MD_YN = YES AND Dealer_AktifYN = YES ORDER BY Dealer_Name"
        pStr = "SELECT MD_Kode, MD_Name FROM M_MainDealer WHERE MD_AktifYN = YES ORDER BY MD_Name"

        Try
            cmd = New OleDbCommand(pStr, vConn)
            reader = cmd.ExecuteReader()

            If reader.HasRows Then
                While reader.Read()
                    lstMD.Items.Add(Strings.Right("    " & Strings.Trim(reader.Item("MD_Kode")), 5) & "  " & Strings.Trim(reader.Item("MD_Name")))
                End While
            End If
            reader.Close()
            vConn.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            reader.Close()
            vConn.Close()
        End Try
    End Sub

    Private Sub txtNamaMD_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNamaMD.KeyPress
        If Not e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub txtNamaDlr_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNamaDlr.KeyPress
        If Not e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub Load_DataFile()
        Dim vConn As New OleDbConnection
        vConn.ConnectionString = strcon
        vConn.Open()

        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        pStr = "SELECT TOP " & pRowMax & " A.*, B.Dealer_Name from T_Files A, M_Dealer B " _
            & "WHERE A.File_DealerKode = B.Dealer_Kode ORDER BY File_UploadDate Desc"
        Try
            cmd = New OleDbCommand(pStr, vConn)
            reader = cmd.ExecuteReader()

            If reader.HasRows Then
                While reader.Read()
                    dgvFile.Rows.Add(New String() {reader.Item("File_ID"), reader.Item("Dealer_Name"), reader.Item("File_Name"), reader.Item("File_Path"), reader.Item("File_Status"), Format(reader.Item("File_UploadDate"), "dd-MMM-yyyy hh:mm"), reader.Item("File_UploadUser")})
                End While
            End If
            reader.Close()
            vConn.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            reader.Close()
            vConn.Close()
        End Try

    End Sub

    Private Sub lstMD_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstMD.DoubleClick
        If lstMD.SelectedIndex <> -1 Then
            txtKodeMD.Text = Strings.Trim(Strings.Left(lstMD.Text, 6))
            txtNamaMD.Text = Strings.Trim(Strings.Mid(lstMD.Text, 8))
            Setting_ListDlr()
        End If
        Panel1.Enabled = True
        txtKodeDlr.Focus()
        lstMD.Visible = False
    End Sub

    Private Sub txtKodeDlr_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtKodeDlr.KeyDown
        If e.KeyCode = Keys.F5 And Strings.Len(txtKodeMD.Text) > 0 Then
            Panel1.Enabled = False
            lstDlr.Visible = True
            lstDlr.Focus()
        ElseIf e.KeyCode = Keys.Enter Then
            SendKeys.Send("{TAB}")
        End If

    End Sub

    Private Sub lstMD_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles lstMD.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If lstMD.SelectedIndex <> -1 Then
                txtKodeMD.Text = Strings.Trim(Strings.Left(lstMD.Text, 6))
                txtNamaMD.Text = Strings.Trim(Strings.Mid(lstMD.Text, 8))
                Setting_ListDlr()
            End If
            Panel1.Enabled = True
            lstMD.Visible = False
            txtKodeDlr.Focus()
        End If
    End Sub

    Private Sub Setting_ListDlr()
        Dim vConn As New OleDbConnection
        vConn.ConnectionString = strcon
        vConn.Open()

        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        lstDlr.Items.Clear()
        pStr = "SELECT Dealer_Kode, Dealer_Name FROM M_Dealer WHERE " _
            & "Dealer_MD_YN = NO AND Dealer_AktifYN = YES AND Dealer_MDKode ='" & txtKodeMD.Text & "' ORDER BY Dealer_Name"

        Try
            cmd = New OleDbCommand(pStr, vConn)
            reader = cmd.ExecuteReader()

            If reader.HasRows Then
                While reader.Read()
                    lstDlr.Items.Add(Strings.Right("     " & Strings.Trim(reader.Item("Dealer_Kode")), 6) & "  " & reader.Item("Dealer_Name"))
                End While
            End If
            reader.Close()
            vConn.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            reader.Close()
            vConn.Close()
        End Try
    End Sub

    Private Sub lstDlr_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstDlr.DoubleClick
        If lstDlr.SelectedIndex <> -1 Then
            txtKodeDlr.Text = Strings.Trim(Strings.Left(lstDlr.Text, 7))
            txtNamaDlr.Text = Strings.Trim(Strings.Mid(lstDlr.Text, 9))
        End If
        Panel1.Enabled = True
        lstDlr.Visible = False
        txtPath.Focus()
    End Sub

    Private Sub lstDlr_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles lstDlr.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then
            If lstDlr.SelectedIndex <> -1 Then
                txtKodeDlr.Text = Strings.Trim(Strings.Left(lstDlr.Text, 7))
                txtNamaDlr.Text = Strings.Trim(Strings.Mid(lstDlr.Text, 9))
            End If
            Panel1.Enabled = True
            lstDlr.Visible = False
            txtPath.Focus()
        End If
    End Sub

    Private Sub txtPath_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPath.KeyPress
        If Not e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter) Then e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Dim myStream As Stream = Nothing
        Dim vDir As New OpenFileDialog()
        Dim tahun As String
        Dim bulan As String
        Dim i As Integer

        If Not Directory.Exists(vPathSource) Then reportLocation = "C:\"
        vDir.InitialDirectory = reportLocation
        vDir.Filter = "excell files (*.xlsx)|*.xls?|All files (*.*)|*.*"
        vDir.FilterIndex = 1
        vDir.RestoreDirectory = True

        If vDir.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                myStream = vDir.OpenFile()
                If (myStream IsNot Nothing) Then
                    reportLocation = vDir.FileName
                    txtPath.Text = (reportLocation)
                End If
            Catch Ex As Exception
                MessageBox.Show("Cannot read the file " & vbCrLf & "Original error: " & Ex.Message)
            Finally
                ' Check this again, since we need to make sure we didn't throw an exception on open.
                If (myStream IsNot Nothing) Then
                    myStream.Close()
                End If
            End Try
            Dim appXL As Excel.Application
            Dim wbXl As Excel.Workbook
            Dim shXL As Excel.Worksheet = Nothing
            Dim vErr As Boolean = False
            Dim vNewXL As String = ""
            
            'ambil struktur xls
            
            appXL = CreateObject("Excel.Application")
            wbXl = appXL.Workbooks.Open(vDir.FileName)

            'Try
            'H1 to H2
            shXL = wbXl.Worksheets("H1 to H2")
            Dim Dealer() As String
            Dim MD() As String
            txtNamaMD.Text = shXL.Range("B3").Text
            txtNamaDlr.Text = shXL.Range("C3").Text

            Dim dt1, dt2, dt3, dt4, dt5 As New DataTable
            dt1.TableName = "dt1"
            dt2.TableName = "dt2"
            dt3.TableName = "dt3"
            dt4.TableName = "dt4"
            dt5.TableName = "dt5"

            dt1.Columns.Add("Check", GetType(Boolean))
            dt1.Columns.Add("Tahun", GetType(String))
            dt1.Columns.Add("Bulan", GetType(String))
            dt2.Columns.Add("Check", GetType(Boolean))
            dt2.Columns.Add("Tahun", GetType(String))
            dt2.Columns.Add("Bulan", GetType(String))
            dt3.Columns.Add("Check", GetType(Boolean))
            dt3.Columns.Add("Tahun", GetType(String))
            dt3.Columns.Add("Bulan", GetType(String))
            dt4.Columns.Add("Check", GetType(Boolean))
            dt4.Columns.Add("Tahun", GetType(String))
            dt4.Columns.Add("Bulan", GetType(String))
            dt5.Columns.Add("Check", GetType(Boolean))
            dt5.Columns.Add("Tahun", GetType(String))
            dt5.Columns.Add("Bulan", GetType(String))

            bulan = formatBulan(CType(shXL.Cells(6, 3), Excel.Range).Value())
            tahun = CType(shXL.Cells(5, 3), Excel.Range).Value()
            i = 0
            While bulan <> ""
                If CType(shXL.Cells(13, 3 + (i * 4)), Excel.Range).Value() <> 0 Then dt1.Rows.Add(New String() {False, tahun, bulan})
                If CType(shXL.Cells(33, 3 + (i * 4)), Excel.Range).Value() <> 0 Then dt2.Rows.Add(New String() {False, tahun, bulan})
                If CType(shXL.Cells(53, 3 + (i * 4)), Excel.Range).Value() <> 0 Then dt3.Rows.Add(New String() {False, tahun, bulan})
                If CType(shXL.Cells(73, 3 + (i * 4)), Excel.Range).Value() <> 0 Then dt4.Rows.Add(New String() {False, tahun, bulan})
                'dt1.Rows.Add(New String() {False, tahun, bulan})
                i += 1
                bulan = formatBulan(CType(shXL.Cells(6, 3 + (i * 4)), Excel.Range).Value())
                If bulan = "01" Then tahun += 1
            End While

            'dt2 = dt1.Copy
            'dt3 = dt1.Copy
            'dt4 = dt1.Copy

            dv1.Table = dt1
            dv2.Table = dt2
            dv3.Table = dt3
            dv4.Table = dt4

            For j As Integer = 1 To 4
                setting_dgv(Me.GroupBox1.Controls("DataGridView" & j), CallByName(Me, "dv" & j, CallType.Get))
            Next

            'H2 to H1
            shXL = wbXl.Worksheets("H2 to H1")
            bulan = formatBulan(CType(shXL.Cells(2, 3), Excel.Range).Value())
            tahun = CType(shXL.Cells(1, 3), Excel.Range).Value()
            i = 0
            While bulan <> ""
                dt5.Rows.Add(New String() {False, tahun, bulan})
                i += 1
                bulan = formatBulan(CType(shXL.Cells(2, 3 + (i * 12)), Excel.Range).Value())
                If bulan = "01" Then tahun += 1
            End While

            dv5.Table = dt5
            setting_dgv(Me.GroupBox2.Controls("DataGridView5"), CallByName(Me, "dv5", CallType.Get))

            Dealer = getfields("M_Dealer", "Dealer_Kode", " where Dealer_Name='" & txtNamaDlr.Text & "'")
            If (Dealer.Length > 0) Then txtKodeDlr.Text = Dealer(0)
            MD = getfields("M_MainDealer", "MD_Kode", " where MD_Name='" & txtNamaMD.Text & "'")
            If (MD.Length > 0) Then txtKodeMD.Text = MD(0)
            wbXl.Close(False)
            appXL.Quit()
            'Catch ex As Exception
            '    MsgBox(ex.Message)
            'End Try

            releaseObject(shXL)
            releaseObject(wbXl)
            releaseObject(appXL)

        End If
    End Sub

    Private Sub setting_dgv(datagrid As DataGridView, dv As DataView)
        dv.Sort = "tahun DESC, bulan DESC"
        datagrid.DataSource = dv
        datagrid.Columns(0).HeaderText = ""
        datagrid.Columns(0).Width = 40
        datagrid.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        datagrid.Columns(0).DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 9, FontStyle.Regular)
        datagrid.Columns(1).DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 9, FontStyle.Regular)
        datagrid.Columns(2).DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 9, FontStyle.Regular)
        datagrid.Columns(1).Width = 50
        datagrid.Columns(2).Width = 50
    End Sub

    Private Sub btnExec_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExec.Click
        'cek file
        If Not System.IO.File.Exists(txtPath.Text) Then
            MsgBox("Tidak ada file yang akan di upload" & vbCrLf & "Silakan di cek kembali", vbInformation, "Error File")
            Exit Sub
        End If
        vPathSource = Path.GetDirectoryName(txtPath.Text)
        vFileName = Path.GetFileName(txtPath.Text)
        reportLocation = txtPath.Text

        'cek dealer
        'If Len(txtKodeDlr.Text) < 2 Then
        '    MsgBox("Kode Dealer tidak valid", vbInformation, "Invalid Input#1")
        '    txtKodeDlr.Focus()
        '    Exit Sub
        'End If

        'If Not Cek_Dealer(txtKodeDlr.Text) = "" Then
        '    MsgBox("Kode Dealer tidak valid", vbInformation, "Invalid Input#2")
        '    txtKodeDlr.Focus()
        '    Exit Sub
        'End If
        'If Not Cek_MainDealer(txtKodeDlr.Text) = "" Then
        '    MsgBox("Kode Main Dealer tidak valid", vbInformation, "Invalid Input#2")
        '    txtKodeMD.Focus()
        '    Exit Sub
        'End If

        'Cek file master 'Master Tab H1H2.xlsx'
        'If Not System.IO.File.Exists("D:\3pmprojects\CRM dashboard\contoh report\M_TabH1H2.xlsx") Then
        '    MsgBox("File Master Tabulasi H1 to H2 not found" & vbCrLf & "Please contact your administrator", vbInformation, "Missing File Master")
        '    Exit Sub
        'End If

        Me.Cursor = Cursors.WaitCursor
        CatatLog(user, "UPLOAD NIGURI START", "BUTTON EXECUTE", reportLocation)
        loadExcel()
        Me.Cursor = Cursors.Default
        MsgBox("Upload Success")
        ProgressBar1.Visible = False
        ProgressBar2.Visible = False
        Label9.Visible = False
    End Sub
    Private Sub loadExcel()
        Dim appXL As Excel.Application
        Dim wbXl As Excel.Workbook


        Dim str As String = ""
        Dim x As Integer = 0
        Dim y As Integer = 0
        Dim vErr As Boolean = False
        Dim vNewXL As String = ""
        Dim vKdDlr As String = txtKodeDlr.Text
        Dim vKdMD As String = txtKodeMD.Text

        ProgressBar1.Maximum = 3
        ProgressBar1.Value = 0
        ProgressBar1.Visible = True
        ProgressBar2.Visible = True
        Label9.Visible = True
        Label9.Text = "Load Excel"
        'ambil struktur xls
        'If Not Ambil_StrukturXLS(vMM) Then Exit Sub

        'cek dulu bila file hasil sudah ada, langsung open dan bila belum ada, copy dulu dari master
        'vNewXL = vPathDest & "\" & vYY & vKdDlr & " " & Strings.Trim(Strings.Left(vNmDlr & "          ", 12)) & ".xlsx"

        appXL = CreateObject("Excel.Application")
        wbXl = appXL.Workbooks.Open(txtPath.Text)
        ProgressBar1.Value += 1
        Load_H1H2(wbXl, vKdDlr, vKdMD)
        ProgressBar1.Value += 1
        Load_H2H1(wbXl, vKdDlr, vKdMD)
        ProgressBar1.Value += 1
        Load_DealerInfo(wbXl, vKdDlr, vKdMD)

        If Not vErr Then CatatLog(user, "UPLOAD NIGURI FINISH", "T_Niguri_Asli", "v")

        'DestroyXL
        'shXL.SaveAs(vNewXL)
        wbXl.Close(False)
        appXL.Quit()

        releaseObject(wbXl)
        releaseObject(appXL)

    End Sub
    Private Function check_dgv(datagrid As DataGridView, bulan As String, tahun As Integer) As Boolean
        check_dgv = False
        For row As Integer = 0 To datagrid.RowCount - 1
            If datagrid.Item("tahun", row).Value = tahun And datagrid.Item("bulan", row).Value = bulan And _
                datagrid.Item("check", row).Value = True Then
                check_dgv = True
                Exit For
            End If
        Next

    End Function
    Private Sub Load_H1H2(wbXL As Excel.Workbook, vKdDlr As String, vKdMD As String)
        Dim reader As DataTableReader
        Dim shXL As Excel.Worksheet = Nothing
        Dim insertQuery As String
        Dim deleteQuery As String
        Dim isi(13) As Decimal
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String = "t_h1toh2"
        ReDim fields(20)
        ReDim nilai(20)
        fields(0) = "bulan"
        fields(1) = "tahun"
        fields(2) = "dealer_kode"
        fields(3) = "MD_kode"
        fields(4) = "total_datasource"
        fields(5) = "PhoneAvailable"
        fields(6) = "SMSSent"
        fields(7) = "1stResult"
        fields(8) = "Contacted"
        fields(9) = "Unreachable"
        fields(10) = "Rejected"
        fields(11) = "Workload"
        fields(12) = "2ndResult"
        fields(13) = "TotalVisitor"
        fields(14) = "TooFar"
        fields(15) = "NoTime"
        fields(16) = "Forget"
        fields(17) = "Create_date"
        fields(18) = "Create_user"
        fields(19) = "KPB"

        Dim bulan As String
        Dim tahun As Integer
        Dim counter As Integer
        Try
            shXL = wbXL.Worksheets("H1 to H2")
            bulan = formatBulan(CType(shXL.Cells(6, 3), Excel.Range).Value())
            tahun = CType(shXL.Cells(5, 3), Excel.Range).Value()
            counter = 0
            While bulan <> ""
                counter += 1
                bulan = formatBulan(CType(shXL.Cells(6, 3 + (counter * 4)), Excel.Range).Value())
            End While
            ProgressBar2.Maximum = counter
            ProgressBar2.Value = 0
            Label9.Text = "H1 to H2"
            bulan = formatBulan(CType(shXL.Cells(6, 3), Excel.Range).Value())
            tahun = CType(shXL.Cells(5, 3), Excel.Range).Value()
            counter = 0
            While bulan <> ""
                For x = 1 To 4
                    isi(0) = CType(shXL.Cells(7 + ((x - 1) * 20), 3 + (counter * 4)), Excel.Range).Value()
                    If isi(0) = 0 Or Not IsNumeric(isi(0)) Then GoTo lanjut
                    isi(1) = CType(shXL.Cells(8 + ((x - 1) * 20), 3 + (counter * 4)), Excel.Range).Value()
                    isi(2) = CType(shXL.Cells(10 + ((x - 1) * 20), 3 + (counter * 4)), Excel.Range).Value()
                    isi(3) = CType(shXL.Cells(13 + ((x - 1) * 20), 3 + (counter * 4)), Excel.Range).Value()
                    isi(4) = CType(shXL.Cells(16 + ((x - 1) * 20), 3 + (counter * 4)), Excel.Range).Value()
                    isi(5) = CType(shXL.Cells(18 + ((x - 1) * 20), 3 + (counter * 4) + 1), Excel.Range).Value()
                    isi(6) = CType(shXL.Cells(18 + ((x - 1) * 20), 3 + (counter * 4) + 2), Excel.Range).Value()
                    isi(7) = CType(shXL.Cells(18 + ((x - 1) * 20), 3 + (counter * 4) + 3), Excel.Range).Value()
                    isi(8) = CType(shXL.Cells(21 + ((x - 1) * 20), 3 + (counter * 4)), Excel.Range).Value()
                    isi(9) = CType(shXL.Cells(25 + ((x - 1) * 20), 3 + (counter * 4)), Excel.Range).Value()
                    isi(10) = CType(shXL.Cells(23 + ((x - 1) * 20), 3 + (counter * 4) + 1), Excel.Range).Value()
                    isi(11) = CType(shXL.Cells(23 + ((x - 1) * 20), 3 + (counter * 4) + 2), Excel.Range).Value()
                    isi(12) = CType(shXL.Cells(23 + ((x - 1) * 20), 3 + (counter * 4) + 3), Excel.Range).Value()

                    nilai(0) = bulan
                    nilai(1) = tahun
                    nilai(2) = vKdDlr
                    nilai(3) = vKdMD
                    nilai(4) = isi(0)
                    nilai(5) = isi(1)
                    nilai(6) = isi(2)
                    nilai(7) = isi(3)
                    nilai(8) = isi(4)
                    nilai(9) = isi(5)
                    nilai(10) = isi(6)
                    nilai(11) = isi(7)
                    nilai(12) = isi(8)
                    nilai(13) = isi(9)
                    nilai(14) = isi(10)
                    nilai(15) = isi(11)
                    nilai(16) = isi(12)
                    nilai(17) = Format(Now, "yyyy/MM/dd HH:mm:ss")
                    nilai(18) = user
                    nilai(19) = "KPB" & x

                    If check_dgv(Me.GroupBox1.Controls("DataGridView" & x), bulan, tahun) Then
                        table_name = "t_h1toh2"
                        reader = getfield(table_name, "*", " where bulan='" & bulan & "' and tahun='" & tahun & "' and dealer_kode='" & vKdDlr & "' and kpb='KPB" & x & "'").CreateDataReader
                        If reader.Read Then
                            If reader.Item(fields(4)) <> isi(0) Or _
                               reader.Item(fields(5)) <> isi(1) Or _
                               reader.Item(fields(6)) <> isi(2) Or _
                               reader.Item(fields(7)) <> isi(3) Or _
                               reader.Item(fields(8)) <> isi(4) Or _
                               reader.Item(fields(9)) <> isi(5) Or _
                               reader.Item(fields(10)) <> isi(6) Or _
                               reader.Item(fields(11)) <> isi(7) Or _
                               reader.Item(fields(12)) <> isi(8) Or _
                               reader.Item(fields(13)) <> isi(9) Or _
                               reader.Item(fields(14)) <> isi(10) Or _
                               reader.Item(fields(15)) <> isi(11) Or _
                               reader.Item(fields(16)) <> isi(12) Then
                                deleteQuery = "delete from " & table_name & " where bulan='" & bulan & "' and tahun='" & tahun & "' and dealer_kode='" & vKdDlr & "' and kpb='KPB" & x & "'"
                                insertQuery = tambah_data2(table_name, fields, nilai)
                                executeSQL(deleteQuery)
                                executeSQL(insertQuery)
                            End If
                        Else
                            insertQuery = tambah_data2(table_name, fields, nilai)
                            executeSQL(insertQuery)
                        End If
                        table_name = "t_h1toh2_rev"
                        reader = getfield(table_name, "*", " where bulan='" & bulan & "' and tahun='" & tahun & "' and dealer_kode='" & vKdDlr & "' and kpb='KPB" & x & "'").CreateDataReader
                        If Not reader.Read() Then
                            deleteQuery = "delete from " & table_name & " where bulan='" & bulan & "' and tahun='" & tahun & "' and dealer_kode='" & vKdDlr & "' and kpb='KPB" & x & "'"
                            insertQuery = tambah_data2(table_name, fields, nilai)
                            executeSQL(deleteQuery)
                            executeSQL(insertQuery)
                        End If
                    End If
lanjut:
                Next
                counter += 1
                bulan = formatBulan(CType(shXL.Cells(6, 3 + (counter * 4)), Excel.Range).Value())
                If bulan = "01" Then tahun += 1
                ProgressBar2.Value += 1
            End While

        Catch ex As Exception
            MessageBox.Show("Error Detected, Upload terminated " & reportLocation & vbCrLf & "Error : " & ex.Message)
            CatatLog(user, "UPLOAD NIGURI FAILED", reportLocation, ex.Message)

        End Try
        releaseObject(shXL)
    End Sub
    Private Sub Load_H2H1(wbXL As Excel.Workbook, vKdDlr As String, vKdMD As String)
        Dim reader As DataTableReader
        Dim shXL As Excel.Worksheet = Nothing
        Dim insertQuery As String
        Dim deleteQuery As String
        Dim isi(51) As Decimal
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String = "t_h2toh1"
        ReDim fields(57)
        ReDim nilai(57)
        fields(0) = "bulan"
        fields(1) = "tahun"
        fields(2) = "dealer_kode"
        fields(3) = "MD_kode"
        fields(4) = "H1H1totaldatasource"
        fields(5) = "H1H1DataFiltering"
        fields(6) = "H1H1SMSSent"
        fields(7) = "H1H1WorkloadM1"
        fields(8) = "H1H1Contacted"
        fields(9) = "H1H1Unreachable"
        fields(10) = "H1H1Rejected"
        fields(11) = "H1H1Workload"
        fields(12) = "H1H1ProspectM2"
        fields(13) = "H1H1ProspectM1"
        fields(14) = "H1H1ProspectM"
        fields(15) = "H1H1TotalProspect"
        fields(16) = "H1H1HotProspect"
        fields(17) = "H1H1LowProspect"
        fields(18) = "H1H1NotDeal"
        fields(19) = "H1H1DealData"
        fields(20) = "H1H1DealSales"

        fields(21) = "H2H1Owntotaldatasource"
        fields(22) = "H2H1OwnDataFiltering"
        fields(23) = "H2H1OwnSMSSent"
        fields(24) = "H2H1OwnWorkloadM1"
        fields(25) = "H2H1OwnContacted"
        fields(26) = "H2H1OwnUnreachable"
        fields(27) = "H2H1OwnRejected"
        fields(28) = "H2H1OwnWorkload"
        fields(29) = "H2H1OwnProspectM2"
        fields(30) = "H2H1OwnProspectM1"
        fields(31) = "H2H1OwnProspectM"
        fields(32) = "H2H1OwnTotalProspect"
        fields(33) = "H2H1OwnHotProspect"
        fields(34) = "H2H1OwnLowProspect"
        fields(35) = "H2H1OwnNotDeal"
        fields(36) = "H2H1OwnDealData"
        fields(37) = "H2H1OwnDealSales"

        fields(38) = "H2H1Othertotaldatasource"
        fields(39) = "H2H1OtherDataFiltering"
        fields(40) = "H2H1OtherSMSSent"
        fields(41) = "H2H1OtherWorkloadM1"
        fields(42) = "H2H1OtherContacted"
        fields(43) = "H2H1OtherUnreachable"
        fields(44) = "H2H1OtherRejected"
        fields(45) = "H2H1OtherWorkload"
        fields(46) = "H2H1OtherProspectM2"
        fields(47) = "H2H1OtherProspectM1"
        fields(48) = "H2H1OtherProspectM"
        fields(49) = "H2H1OtherTotalProspect"
        fields(50) = "H2H1OtherHotProspect"
        fields(51) = "H2H1OtherLowProspect"
        fields(52) = "H2H1OtherNotDeal"
        fields(53) = "H2H1OtherDealData"
        fields(54) = "H2H1OtherDealSales"

        fields(55) = "Create_date"
        fields(56) = "Create_user"


        Dim bulan As String
        Dim tahun As Integer = 2016
        Dim counter As Integer
        Try
            shXL = wbXL.Worksheets("H2 to H1")
            bulan = formatBulan(CType(shXL.Cells(2, 3), Excel.Range).Value())
            tahun = CType(shXL.Cells(1, 3), Excel.Range).Value()
            counter = 0

            While bulan <> ""
                counter += 1
                bulan = formatBulan(CType(shXL.Cells(2, 3 + (counter * 12)), Excel.Range).Value())
            End While
            ProgressBar2.Maximum = counter
            ProgressBar2.Value = 0
            Label9.Text = "H2 to H1"
            bulan = formatBulan(CType(shXL.Cells(2, 3), Excel.Range).Value())
            tahun = CType(shXL.Cells(1, 3), Excel.Range).Value()
            counter = 0
            While bulan <> ""
                isi(0) = CType(shXL.Cells(4, 3 + (counter * 12)), Excel.Range).Value()
                isi(1) = CType(shXL.Cells(6, 3 + (counter * 12)), Excel.Range).Value()
                isi(2) = CType(shXL.Cells(8, 3 + (counter * 12)), Excel.Range).Value()
                isi(3) = CType(shXL.Cells(10, 3 + (counter * 12)), Excel.Range).Value()
                isi(4) = CType(shXL.Cells(14, 3 + (counter * 12)), Excel.Range).Value()
                isi(5) = CType(shXL.Cells(14, 4 + (counter * 12)), Excel.Range).Value()
                isi(6) = CType(shXL.Cells(14, 5 + (counter * 12)), Excel.Range).Value()
                isi(7) = CType(shXL.Cells(14, 6 + (counter * 12)), Excel.Range).Value()
                isi(8) = CType(shXL.Cells(18, 3 + (counter * 12)), Excel.Range).Value()
                isi(9) = CType(shXL.Cells(18, 4 + (counter * 12)), Excel.Range).Value()
                isi(10) = CType(shXL.Cells(18, 5 + (counter * 12)), Excel.Range).Value()
                isi(11) = isi(8) + isi(9) + isi(10)
                isi(12) = CType(shXL.Cells(23, 4 + (counter * 12)), Excel.Range).Value()
                isi(13) = CType(shXL.Cells(23, 5 + (counter * 12)), Excel.Range).Value()
                isi(14) = CType(shXL.Cells(23, 6 + (counter * 12)), Excel.Range).Value()
                isi(15) = CType(shXL.Cells(23, 3 + (counter * 12)), Excel.Range).Value()
                isi(16) = CType(shXL.Cells(26, 3 + (counter * 12)), Excel.Range).Value()

                isi(17) = CType(shXL.Cells(4, 7 + (counter * 12)), Excel.Range).Value()
                isi(18) = CType(shXL.Cells(6, 7 + (counter * 12)), Excel.Range).Value()
                isi(19) = CType(shXL.Cells(8, 7 + (counter * 12)), Excel.Range).Value()
                isi(20) = CType(shXL.Cells(10, 7 + (counter * 12)), Excel.Range).Value()
                isi(21) = CType(shXL.Cells(14, 7 + (counter * 12)), Excel.Range).Value()
                isi(22) = CType(shXL.Cells(14, 8 + (counter * 12)), Excel.Range).Value()
                isi(23) = CType(shXL.Cells(14, 9 + (counter * 12)), Excel.Range).Value()
                isi(24) = CType(shXL.Cells(14, 10 + (counter * 12)), Excel.Range).Value()
                isi(25) = CType(shXL.Cells(18, 7 + (counter * 12)), Excel.Range).Value()
                isi(26) = CType(shXL.Cells(18, 8 + (counter * 12)), Excel.Range).Value()
                isi(27) = CType(shXL.Cells(18, 9 + (counter * 12)), Excel.Range).Value()
                isi(28) = isi(8) + isi(9) + isi(10)
                isi(29) = CType(shXL.Cells(23, 8 + (counter * 12)), Excel.Range).Value()
                isi(30) = CType(shXL.Cells(23, 9 + (counter * 12)), Excel.Range).Value()
                isi(31) = CType(shXL.Cells(23, 10 + (counter * 12)), Excel.Range).Value()
                isi(32) = CType(shXL.Cells(23, 7 + (counter * 12)), Excel.Range).Value()
                isi(33) = CType(shXL.Cells(26, 7 + (counter * 12)), Excel.Range).Value()

                isi(34) = CType(shXL.Cells(4, 11 + (counter * 12)), Excel.Range).Value()
                isi(35) = CType(shXL.Cells(6, 11 + (counter * 12)), Excel.Range).Value()
                isi(36) = CType(shXL.Cells(8, 11 + (counter * 12)), Excel.Range).Value()
                isi(37) = CType(shXL.Cells(10, 11 + (counter * 12)), Excel.Range).Value()
                isi(38) = CType(shXL.Cells(14, 11 + (counter * 12)), Excel.Range).Value()
                isi(39) = CType(shXL.Cells(14, 12 + (counter * 12)), Excel.Range).Value()
                isi(40) = CType(shXL.Cells(14, 13 + (counter * 12)), Excel.Range).Value()
                isi(41) = CType(shXL.Cells(14, 14 + (counter * 12)), Excel.Range).Value()
                isi(42) = CType(shXL.Cells(18, 11 + (counter * 12)), Excel.Range).Value()
                isi(43) = CType(shXL.Cells(18, 12 + (counter * 12)), Excel.Range).Value()
                isi(44) = CType(shXL.Cells(18, 13 + (counter * 12)), Excel.Range).Value()
                isi(45) = isi(8) + isi(9) + isi(10)
                isi(46) = CType(shXL.Cells(23, 12 + (counter * 12)), Excel.Range).Value()
                isi(47) = CType(shXL.Cells(23, 13 + (counter * 12)), Excel.Range).Value()
                isi(48) = CType(shXL.Cells(23, 14 + (counter * 12)), Excel.Range).Value()
                isi(49) = CType(shXL.Cells(23, 11 + (counter * 12)), Excel.Range).Value()
                isi(50) = CType(shXL.Cells(26, 11 + (counter * 12)), Excel.Range).Value()

                nilai(0) = bulan
                nilai(1) = tahun
                nilai(2) = vKdDlr
                nilai(3) = vKdMD
                nilai(4) = isi(0)
                nilai(5) = isi(1)
                nilai(6) = isi(2)
                nilai(7) = isi(3)
                nilai(8) = isi(4)
                nilai(9) = isi(5)
                nilai(10) = isi(6)
                nilai(11) = isi(7)
                nilai(12) = isi(8)
                nilai(13) = isi(9)
                nilai(14) = isi(10)
                nilai(15) = isi(11)
                nilai(16) = isi(12)
                nilai(17) = isi(13)
                nilai(18) = isi(14)
                nilai(19) = isi(15)
                nilai(20) = isi(16)

                nilai(21) = isi(17)
                nilai(22) = isi(18)
                nilai(23) = isi(19)
                nilai(24) = isi(20)
                nilai(25) = isi(21)
                nilai(26) = isi(22)
                nilai(27) = isi(23)
                nilai(28) = isi(24)
                nilai(29) = isi(25)
                nilai(30) = isi(26)
                nilai(31) = isi(27)
                nilai(32) = isi(28)
                nilai(33) = isi(29)
                nilai(34) = isi(30)
                nilai(35) = isi(31)
                nilai(36) = isi(32)
                nilai(37) = isi(33)

                nilai(38) = isi(34)
                nilai(39) = isi(35)
                nilai(40) = isi(36)
                nilai(41) = isi(37)
                nilai(42) = isi(38)
                nilai(43) = isi(39)
                nilai(44) = isi(40)
                nilai(45) = isi(41)
                nilai(46) = isi(42)
                nilai(47) = isi(43)
                nilai(48) = isi(44)
                nilai(49) = isi(45)
                nilai(50) = isi(46)
                nilai(51) = isi(47)
                nilai(52) = isi(48)
                nilai(53) = isi(49)
                nilai(54) = isi(50)
                nilai(55) = Format(Now, "yyyy/MM/dd HH:mm:ss")
                nilai(56) = user

                If check_dgv(Me.GroupBox2.Controls("DataGridView5"), bulan, tahun) Then
                    table_name = "t_h2toh1"
                    reader = getfield(table_name, "*", " where bulan='" & bulan & "' and tahun='" & tahun & "' and dealer_kode='" & vKdDlr & "'").CreateDataReader
                    If reader.Read Then
                        If reader.Item(fields(4)) <> isi(0) Or _
                           reader.Item(fields(5)) <> isi(1) Or _
                           reader.Item(fields(6)) <> isi(2) Or _
                           reader.Item(fields(7)) <> isi(3) Or _
                           reader.Item(fields(8)) <> isi(4) Or _
                           reader.Item(fields(9)) <> isi(5) Or _
                           reader.Item(fields(10)) <> isi(6) Or _
                           reader.Item(fields(11)) <> isi(7) Or _
                           reader.Item(fields(12)) <> isi(8) Or _
                           reader.Item(fields(13)) <> isi(9) Or _
                           reader.Item(fields(14)) <> isi(10) Or _
                           reader.Item(fields(15)) <> isi(11) Or _
                           reader.Item(fields(16)) <> isi(12) Or _
                           reader.Item(fields(17)) <> isi(13) Or _
                           reader.Item(fields(18)) <> isi(14) Or _
                           reader.Item(fields(19)) <> isi(15) Or _
                           reader.Item(fields(20)) <> isi(16) Then
                            deleteQuery = "delete from " & table_name & " where bulan='" & bulan & "' and tahun='" & tahun & "' and dealer_kode='" & vKdDlr & "'"
                            insertQuery = tambah_data2(table_name, fields, nilai)
                            executeSQL(deleteQuery)
                            executeSQL(insertQuery)
                        End If
                    Else
                        insertQuery = tambah_data2(table_name, fields, nilai)

                        executeSQL(insertQuery)
                    End If
                    table_name = "t_h2toh1_rev"
                    reader = getfield(table_name, "*", " where bulan='" & bulan & "' and tahun='" & tahun & "' and dealer_kode='" & vKdDlr & "'").CreateDataReader
                    If Not reader.Read() Then
                        deleteQuery = "delete from " & table_name & " where bulan='" & bulan & "' and tahun='" & tahun & "' and dealer_kode='" & vKdDlr & "'"
                        insertQuery = tambah_data2(table_name, fields, nilai)
                        executeSQL(deleteQuery)
                        executeSQL(insertQuery)
                    End If
                End If
                counter += 1
                bulan = formatBulan(CType(shXL.Cells(2, 3 + (counter * 12)), Excel.Range).Value())
                If bulan = "01" Then tahun += 1
                ProgressBar2.Value += 1
            End While

        Catch ex As Exception
            MessageBox.Show("Error Detected, Upload terminated " & reportLocation & vbCrLf & "Error : " & ex.Message)
            CatatLog(user, "UPLOAD NIGURI FAILED", reportLocation, ex.Message)

        End Try
        releaseObject(shXL)
    End Sub
    Private Sub Load_DealerInfo(wbXL As Excel.Workbook, vKdDlr As String, vKdMD As String)
        Dim shXL As Excel.Worksheet = Nothing
        Dim keterangan1, keterangan2, keterangan3, keterangan4, keterangan5, keterangan6, Total As String
        Dim isi(13) As Decimal
        Dim table_name As String = "t_dealerinfo"
        Dim i, j, k, l, m As Integer

        Dim bulan As String
        Dim tahun As Integer
        Dim counter As Integer
        Try
            shXL = wbXL.Worksheets("Dealer Information")
            bulan = formatBln(CType(shXL.Cells(102, 8), Excel.Range).Value())
            tahun = CType(shXL.Cells(101, 8), Excel.Range).Value()
            counter = 0

            While bulan <> ""
                counter += 1
                bulan = formatBln(CType(shXL.Cells(102, 8 + counter), Excel.Range).Value())
            End While
            ProgressBar2.Maximum = counter
            ProgressBar2.Value = 0
            Label9.Text = "Dealer Information"
            bulan = formatBln(CType(shXL.Cells(102, 8), Excel.Range).Value())
            tahun = CType(shXL.Cells(101, 8), Excel.Range).Value()
            counter = 0

            While bulan <> ""

                'SALES & SERVICE 5X (SALES)
                For i = 0 To 4
                    keterangan1 = CType(shXL.Cells(29, 1), Excel.Range).Value()
                    keterangan2 = CType(shXL.Cells(32, 3), Excel.Range).Value()
                    keterangan3 = CType(shXL.Cells(33 + i, 3), Excel.Range).Value()
                    Total = CType(shXL.Cells(33 + i, 5 + counter), Excel.Range).Value()
                    InsertDealerInformation(bulan, tahun, vKdDlr, vKdMD, keterangan1, keterangan2, keterangan3, "", "", "", Total)
                Next


                'SALES & SERVICE 10X (SALES)
                For i = 0 To 9
                    keterangan1 = CType(shXL.Cells(29, 1), Excel.Range).Value()
                    keterangan2 = CType(shXL.Cells(32, 3), Excel.Range).Value()
                    keterangan3 = CType(shXL.Cells(39, 3), Excel.Range).Value()
                    keterangan4 = CType(shXL.Cells(40 + i, 3), Excel.Range).Value()
                    Total = Format(CType(shXL.Cells(40 + i, 5 + counter), Excel.Range).Value() * 100, "#,###")
                    If Not keterangan4 Is Nothing Then
                        InsertDealerInformation(bulan, tahun, vKdDlr, vKdMD, keterangan1, keterangan2, keterangan3, keterangan4, "", "", Total)
                    End If
                Next

                'SALES & SERVICE (SERVICE) 7X
                For i = 0 To 6
                    keterangan1 = CType(shXL.Cells(29, 1), Excel.Range).Value()
                    keterangan2 = CType(shXL.Cells(52, 3), Excel.Range).Value()
                    keterangan3 = CType(shXL.Cells(53 + i, 3), Excel.Range).Value()
                    Total = CType(shXL.Cells(53 + i, 5 + counter), Excel.Range).Value()
                    If Not keterangan3 Is Nothing Then
                        InsertDealerInformation(bulan, tahun, vKdDlr, vKdMD, keterangan1, keterangan2, keterangan3, "", "", "", Total)
                    End If
                Next

                'SALES & SERVICE (SERVICE) 3X 
                For i = 0 To 2
                    keterangan1 = CType(shXL.Cells(29, 1), Excel.Range).Value()
                    keterangan2 = CType(shXL.Cells(52, 3), Excel.Range).Value()
                    keterangan3 = CType(shXL.Cells(61, 3), Excel.Range).Value()
                    keterangan4 = CType(shXL.Cells(62 + i, 3), Excel.Range).Value()
                    Total = Format(CType(shXL.Cells(62 + i, 5 + counter), Excel.Range).Value() * 100, "#,###")
                    InsertDealerInformation(bulan, tahun, vKdDlr, vKdMD, keterangan1, keterangan2, keterangan3, keterangan4, "", "", Total)
                Next

                'SALES & SERVICE (SERVICE) 3X KPB 1
                For i = 0 To 2
                    keterangan1 = CType(shXL.Cells(29, 1), Excel.Range).Value()
                    keterangan2 = CType(shXL.Cells(52, 3), Excel.Range).Value()
                    keterangan3 = CType(shXL.Cells(66, 3), Excel.Range).Value()
                    keterangan4 = CType(shXL.Cells(67, 3), Excel.Range).Value()
                    keterangan5 = CType(shXL.Cells(68 + i, 3), Excel.Range).Value()
                    Total = Format(CType(shXL.Cells(68 + i, 5 + counter), Excel.Range).Value() * 100, "#,###")
                    InsertDealerInformation(bulan, tahun, vKdDlr, vKdMD, keterangan1, keterangan2, keterangan3, keterangan4, keterangan5, "", Total)
                Next

                'SALES & SERVICE (SERVICE) 3X KPB 2
                For i = 0 To 2
                    keterangan1 = CType(shXL.Cells(29, 1), Excel.Range).Value()
                    keterangan2 = CType(shXL.Cells(52, 3), Excel.Range).Value()
                    keterangan3 = CType(shXL.Cells(66, 3), Excel.Range).Value()
                    keterangan4 = CType(shXL.Cells(72, 3), Excel.Range).Value()
                    keterangan5 = CType(shXL.Cells(73 + i, 3), Excel.Range).Value()
                    Total = Format(CType(shXL.Cells(73 + i, 5 + counter), Excel.Range).Value() * 100, "#,###")
                    InsertDealerInformation(bulan, tahun, vKdDlr, vKdMD, keterangan1, keterangan2, keterangan3, keterangan4, keterangan5, "", Total)
                Next

                'SALES & SERVICE (SERVICE) 3X KPB 3
                For i = 0 To 2
                    keterangan1 = CType(shXL.Cells(29, 1), Excel.Range).Value()
                    keterangan2 = CType(shXL.Cells(52, 3), Excel.Range).Value()
                    keterangan3 = CType(shXL.Cells(66, 3), Excel.Range).Value()
                    keterangan4 = CType(shXL.Cells(77, 3), Excel.Range).Value()
                    keterangan5 = CType(shXL.Cells(78 + i, 3), Excel.Range).Value()
                    Total = Format(CType(shXL.Cells(78 + i, 5 + counter), Excel.Range).Value() * 100, "#,###")
                    InsertDealerInformation(bulan, tahun, vKdDlr, vKdMD, keterangan1, keterangan2, keterangan3, keterangan4, keterangan5, "", Total)
                Next

                'SALES & SERVICE (SERVICE) 3X KPB 4
                For i = 0 To 2
                    keterangan1 = CType(shXL.Cells(29, 1), Excel.Range).Value()
                    keterangan2 = CType(shXL.Cells(52, 3), Excel.Range).Value()
                    keterangan3 = CType(shXL.Cells(66, 3), Excel.Range).Value()
                    keterangan4 = CType(shXL.Cells(82, 3), Excel.Range).Value()
                    keterangan5 = CType(shXL.Cells(83 + i, 3), Excel.Range).Value()
                    Total = Format(CType(shXL.Cells(83 + i, 5 + counter), Excel.Range).Value() * 100, "#,###")
                    InsertDealerInformation(bulan, tahun, vKdDlr, vKdMD, keterangan1, keterangan2, keterangan3, keterangan4, keterangan5, "", Total)
                Next

                'SALES & SERVICE (SERVICE) 3X
                For i = 0 To 2
                    keterangan1 = CType(shXL.Cells(29, 1), Excel.Range).Value()
                    keterangan2 = CType(shXL.Cells(52, 3), Excel.Range).Value()
                    keterangan3 = CType(shXL.Cells(88, 3), Excel.Range).Value()
                    keterangan4 = CType(shXL.Cells(89 + i, 3), Excel.Range).Value()
                    Total = Format(CType(shXL.Cells(89 + i, 5 + counter), Excel.Range).Value() * 100, "#,###")
                    InsertDealerInformation(bulan, tahun, vKdDlr, vKdMD, keterangan1, keterangan2, keterangan3, keterangan4, "", "", Total)
                Next

                'Sales Information 14x
                For i = 0 To 13
                    j = i
                    k = i
                    l = i
                    keterangan1 = CType(shXL.Cells(97, 1), Excel.Range).Value()
                    keterangan2 = CType(shXL.Cells(103 + i, 1), Excel.Range).Value()
                    keterangan3 = CType(shXL.Cells(103 + i, 2), Excel.Range).Value()
                    keterangan4 = CType(shXL.Cells(103 + i, 3), Excel.Range).Value()
                    keterangan5 = CType(shXL.Cells(103 + i, 5), Excel.Range).Value()
                    Total = CType(shXL.Cells(103 + i, 8 + counter), Excel.Range).Value()
                    While keterangan2 Is Nothing
                        j -= 1
                        keterangan2 = CType(shXL.Cells(103 + j, 1), Excel.Range).Value()
                    End While
                    While keterangan3 Is Nothing
                        k -= 1
                        keterangan3 = CType(shXL.Cells(103 + k, 2), Excel.Range).Value()
                    End While
                    While keterangan4 Is Nothing
                        l -= 1
                        keterangan4 = CType(shXL.Cells(103 + l, 3), Excel.Range).Value()
                    End While
                    If Not keterangan4 Is Nothing And keterangan4 <> "…" Then
                        InsertDealerInformation(bulan, tahun, vKdDlr, vKdMD, keterangan1, keterangan2, keterangan3, keterangan4, keterangan5, "", Total)
                    End If
                Next

                'Service Information 52x
                For i = 0 To 51
                    j = i
                    k = i
                    l = i
                    m = i
                    keterangan1 = CType(shXL.Cells(126, 1), Excel.Range).Value()
                    keterangan2 = CType(shXL.Cells(131 + i, 1), Excel.Range).Value()
                    keterangan3 = CType(shXL.Cells(131 + i, 2), Excel.Range).Value()
                    keterangan4 = CType(shXL.Cells(131 + i, 3), Excel.Range).Value()
                    keterangan5 = CType(shXL.Cells(131 + i, 5), Excel.Range).Value()
                    keterangan6 = CType(shXL.Cells(131 + i, 6), Excel.Range).Value()
                    Total = CType(shXL.Cells(131 + i, 8 + counter), Excel.Range).Value()
                    While keterangan2 Is Nothing
                        j -= 1
                        keterangan2 = CType(shXL.Cells(131 + j, 1), Excel.Range).Value()
                    End While
                    While keterangan3 Is Nothing
                        k -= 1
                        keterangan3 = CType(shXL.Cells(131 + k, 2), Excel.Range).Value()
                    End While
                    While keterangan4 Is Nothing
                        l -= 1
                        keterangan4 = CType(shXL.Cells(131 + l, 3), Excel.Range).Value()
                    End While
                    While keterangan5 Is Nothing
                        m -= 1
                        keterangan5 = CType(shXL.Cells(131 + m, 5), Excel.Range).Value()
                    End While
                    If Not keterangan4 Is Nothing And keterangan4 <> "…." Then
                        InsertDealerInformation(bulan, tahun, vKdDlr, vKdMD, keterangan1, keterangan2, keterangan3, keterangan4, keterangan5, keterangan6, Total)
                    End If
                Next
                counter += 1

                bulan = formatBln(CType(shXL.Cells(102, 8 + counter), Excel.Range).Value())
                If bulan = "01" Then tahun += 1
                'tahun = CType(shXL.Cells(101, 8 + counter), Excel.Range).Value()
                ProgressBar2.Value += 1
            End While

        Catch ex As Exception
            MessageBox.Show("Error Detected, Upload terminated " & reportLocation & vbCrLf & "Error : " & ex.Message)
            CatatLog(user, "UPLOAD NIGURI FAILED", reportLocation, ex.Message)

        End Try
        releaseObject(shXL)
    End Sub
    Private Sub InsertDealerInformation(ByVal bulan As String, ByVal tahun As Integer, ByVal vKdDlr As String, ByVal vKdMD As String, ByVal keterangan1 As String, ByVal keterangan2 As String, ByVal keterangan3 As String, ByVal keterangan4 As String, ByVal keterangan5 As String, ByVal keterangan6 As String, ByVal Total As String)
        Dim reader As DataTableReader
        Dim insertQuery As String
        Dim deleteQuery As String
        Dim isi(13) As Decimal
        Dim fields() As String
        Dim nilai() As String
        Dim table_name As String = "t_dealerinfo"
        ReDim fields(13)
        ReDim nilai(13)

        fields(0) = "bulan"
        fields(1) = "tahun"
        fields(2) = "dealer_kode"
        fields(3) = "MD_kode"
        fields(4) = "Create_date"
        fields(5) = "Create_user"
        fields(6) = "keterangan1"
        fields(7) = "keterangan2"
        fields(8) = "keterangan3"
        fields(9) = "keterangan4"
        fields(10) = "keterangan5"
        fields(11) = "keterangan6"
        fields(12) = "Total"

        nilai(0) = bulan
        nilai(1) = tahun
        nilai(2) = vKdDlr
        nilai(3) = vKdMD
        nilai(4) = Format(Now, "yyyy/MM/dd HH:mm:ss")
        nilai(5) = user

        If keterangan1 Is Nothing Then keterangan1 = ""
        If keterangan1 Is Nothing Then keterangan2 = ""
        If keterangan1 Is Nothing Then keterangan3 = ""
        If keterangan1 Is Nothing Then keterangan4 = ""
        If keterangan1 Is Nothing Then keterangan5 = ""
        If keterangan1 Is Nothing Then keterangan6 = ""
        If Total Is Nothing Or Total = "" Then Total = 0

        nilai(6) = keterangan1
        nilai(7) = keterangan2
        nilai(8) = keterangan3
        nilai(9) = keterangan4
        nilai(10) = keterangan5
        nilai(11) = keterangan6
        nilai(12) = Total

        table_name = "t_dealerinfo"
        reader = getfield(table_name, "*", " where bulan='" & bulan & "' and tahun='" & tahun & "' and dealer_kode='" & vKdDlr & "' and MD_kode = '" & vKdMD & "' and keterangan1 = '" & keterangan1 & "' and keterangan2 = '" & keterangan2 & "' and keterangan3 = '" & keterangan3 & "' and keterangan4 = '" & keterangan4 & "' and keterangan5 = '" & keterangan5 & "' and keterangan6 = '" & keterangan6 & "' and total = " & Total & "").CreateDataReader
        If reader.Read Then
            'If reader.Item(fields(4)) <> isi(0) Or _
            '   reader.Item(fields(5)) <> isi(1) Then
            deleteQuery = "delete from " & table_name & " where bulan='" & bulan & "' and tahun='" & tahun & "' and dealer_kode='" & vKdDlr & "' and MD_kode = '" & vKdMD & "' and keterangan1 = '" & keterangan1 & "' and keterangan2 = '" & keterangan2 & "' and keterangan3 = '" & keterangan3 & "' and keterangan4 = '" & keterangan4 & "' and keterangan5 = '" & keterangan5 & "' and keterangan6 = '" & keterangan6 & "' and total = " & Total & ""
            insertQuery = tambah_data2(table_name, fields, nilai)
            executeSQL(deleteQuery)
            executeSQL(insertQuery)
            'End If
        Else
            insertQuery = tambah_data2(table_name, fields, nilai)
            executeSQL(insertQuery)
        End If
        table_name = "t_dealerinfo_rev"
        reader = getfield(table_name, "*", " where bulan='" & bulan & "' and tahun='" & tahun & "' and dealer_kode='" & vKdDlr & "' and MD_kode = '" & vKdMD & "' and keterangan1 = '" & keterangan1 & "' and keterangan2 = '" & keterangan2 & "' and keterangan3 = '" & keterangan3 & "' and keterangan4 = '" & keterangan4 & "' and keterangan5 = '" & keterangan5 & "' and keterangan6 = '" & keterangan6 & "' and total = " & Total & "").CreateDataReader
        If Not reader.Read() Then
            deleteQuery = "delete from " & table_name & " where bulan='" & bulan & "' and tahun='" & tahun & "' and dealer_kode='" & vKdDlr & "' and MD_kode = '" & vKdMD & "' and keterangan1 = '" & keterangan1 & "' and keterangan2 = '" & keterangan2 & "' and keterangan3 = '" & keterangan3 & "' and keterangan4 = '" & keterangan4 & "' and keterangan5 = '" & keterangan5 & "' and keterangan6 = '" & keterangan6 & "' and total = " & Total & ""
            insertQuery = tambah_data2(table_name, fields, nilai)
            executeSQL(deleteQuery)
            executeSQL(insertQuery)
        End If
    End Sub
    Private Function formatBulan(ByVal bln As String) As String
        Select Case LCase(bln)
            Case "januari" : Return "01"
            Case "februari" : Return "02"
            Case "maret" : Return "03"
            Case "april" : Return "04"
            Case "mei" : Return "05"
            Case "juni" : Return "06"
            Case "juli" : Return "07"
            Case "agustus" : Return "08"
            Case "september" : Return "09"
            Case "oktober" : Return "10"
            Case "november" : Return "11"
            Case "desember" : Return "12"
        End Select
    End Function
    Private Function formatBln(ByVal bln As String) As String
        Select Case UCase(bln)
            Case "JAN" : Return "01"
            Case "FEB" : Return "02"
            Case "MAR" : Return "03"
            Case "APR" : Return "04"
            Case "MAY" : Return "05"
            Case "JUNE" : Return "06"
            Case "JUL" : Return "07"
            Case "AUG" : Return "08"
            Case "SEPT" : Return "09"
            Case "OCT" : Return "10"
            Case "NOV" : Return "11"
            Case "DEC" : Return "12"
        End Select
    End Function


    Private Sub txtMM_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Select Case e.KeyChar
            Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ChrW(8)
            Case Else
                e.KeyChar = Strings.ChrW(0)
        End Select
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub txtYY_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Select Case e.KeyChar
            Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ChrW(8)
            Case Else
                e.KeyChar = Strings.ChrW(0)
        End Select
    End Sub

    Private Sub txtKodeDlr_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKodeDlr.LostFocus
        txtKodeDlr.Text = txtKodeDlr.Text.ToUpper
        txtNamaDlr.Text = Cek_Dealer(txtKodeDlr.Text)
    End Sub

    Private Sub txtKodeMD_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKodeMD.LostFocus
        txtKodeMD.Text = txtKodeMD.Text.ToUpper
        txtNamaMD.Text = Cek_MainDealer(txtKodeMD.Text)
    End Sub

    Private Function Cek_Dealer(ByVal vKdDlr As String) As Boolean
        Cek_Dealer = False

        Dim vConn As New OleDbConnection
        vConn.ConnectionString = strcon
        vConn.Open()

        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        pStr = "SELECT Dealer_Kode, Dealer_Name FROM M_Dealer WHERE " _
            & "Dealer_MD_YN = NO AND Dealer_AktifYN = YES AND Dealer_Kode ='" & vKdDlr & "' "

        Try
            cmd = New OleDbCommand(pStr, vConn)
            reader = cmd.ExecuteReader()

            If reader.HasRows Then
                Cek_Dealer = True
                reader.Read()
                vNamaDlr = reader.Item("Dealer_Name")
            End If
            '            Cek_Dealer = reader.HasRows

        Catch ex As Exception
            'MessageBox.Show(ex.Message.ToString)
        End Try
        reader.Close()
        vConn.Close()
    End Function

    Private Function Cek_MainDealer(ByVal vKdMDlr As String) As String
        Cek_MainDealer = ""

        Dim vConn As New OleDbConnection
        vConn.ConnectionString = strcon
        vConn.Open()

        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        pStr = "SELECT MD_kode, MD_Name FROM M_MainDealer WHERE " _
            & " MD_AktifYN = YES AND MD_Kode ='" & vKdMDlr & "' "

        Try
            cmd = New OleDbCommand(pStr, vConn)
            reader = cmd.ExecuteReader()

            If reader.HasRows Then
                Cek_MainDealer = True
                reader.Read()
                Cek_MainDealer = reader.Item("Dealer_Name")
            End If
            '            Cek_MainDealer = reader.HasRows

        Catch ex As Exception
            'MessageBox.Show(ex.Message.ToString)
        End Try
        reader.Close()
        vConn.Close()
    End Function
    Private Function Ambil_StrukturXLS(ByVal vBulan As String) As Boolean
        Dim vConn As New OleDbConnection
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing
        Dim x As Integer = 0

        Ambil_StrukturXLS = False
        vConn.ConnectionString = strcon
        vConn.Open()
        pStr = "SELECT Asal_X, Asal_Y, Tuju_X, Tuju_Y FROM M_StruH1H2_Proses WHERE " _
            & "Bulan = " & CInt(vBulan) & " "

        Try
            cmd = New OleDbCommand(pStr, vConn)
            reader = cmd.ExecuteReader()
            If reader.HasRows Then
                While reader.Read
                    x = x + 1
                    vAsal(x, 1) = reader.Item("Asal_X")
                    vAsal(x, 2) = reader.Item("Asal_Y")
                    vTuju(x, 1) = reader.Item("Tuju_X")
                    vTuju(x, 2) = reader.Item("Tuju_Y")
                End While
            End If
            Ambil_StrukturXLS = True

        Catch ex As Exception
            'MessageBox.Show(ex.Message.ToString)
        End Try
        reader.Close()
        vConn.Close()

    End Function
End Class
