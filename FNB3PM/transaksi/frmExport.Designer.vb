﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnH1ToH2 = New System.Windows.Forms.Button()
        Me.btnH2ToH1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbKPB = New System.Windows.Forms.ComboBox()
        Me.cmbBatch = New System.Windows.Forms.ComboBox()
        Me.lblBatch = New System.Windows.Forms.Label()
        Me.lblKPB = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbResult = New System.Windows.Forms.RadioButton()
        Me.rbProses = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rbRevisi = New System.Windows.Forms.RadioButton()
        Me.rbAsli = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnH1ToH2
        '
        Me.btnH1ToH2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnH1ToH2.Location = New System.Drawing.Point(146, 147)
        Me.btnH1ToH2.Name = "btnH1ToH2"
        Me.btnH1ToH2.Size = New System.Drawing.Size(118, 44)
        Me.btnH1ToH2.TabIndex = 0
        Me.btnH1ToH2.Text = "H1 to H2"
        Me.btnH1ToH2.UseVisualStyleBackColor = True
        '
        'btnH2ToH1
        '
        Me.btnH2ToH1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnH2ToH1.Location = New System.Drawing.Point(146, 147)
        Me.btnH2ToH1.Name = "btnH2ToH1"
        Me.btnH2ToH1.Size = New System.Drawing.Size(118, 44)
        Me.btnH2ToH1.TabIndex = 1
        Me.btnH2ToH1.Text = "H2 to H1"
        Me.btnH2ToH1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(115, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(157, 24)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Synchronize Data"
        '
        'cmbKPB
        '
        Me.cmbKPB.FormattingEnabled = True
        Me.cmbKPB.Items.AddRange(New Object() {"KPB1", "KPB2", "KPB3", "KPB4"})
        Me.cmbKPB.Location = New System.Drawing.Point(79, 97)
        Me.cmbKPB.Name = "cmbKPB"
        Me.cmbKPB.Size = New System.Drawing.Size(118, 21)
        Me.cmbKPB.TabIndex = 2
        '
        'cmbBatch
        '
        Me.cmbBatch.FormattingEnabled = True
        Me.cmbBatch.Location = New System.Drawing.Point(79, 69)
        Me.cmbBatch.Name = "cmbBatch"
        Me.cmbBatch.Size = New System.Drawing.Size(118, 21)
        Me.cmbBatch.TabIndex = 1
        '
        'lblBatch
        '
        Me.lblBatch.AutoSize = True
        Me.lblBatch.Location = New System.Drawing.Point(19, 72)
        Me.lblBatch.Name = "lblBatch"
        Me.lblBatch.Size = New System.Drawing.Size(35, 13)
        Me.lblBatch.TabIndex = 5
        Me.lblBatch.Text = "Batch"
        '
        'lblKPB
        '
        Me.lblKPB.AutoSize = True
        Me.lblKPB.Location = New System.Drawing.Point(19, 100)
        Me.lblKPB.Name = "lblKPB"
        Me.lblKPB.Size = New System.Drawing.Size(28, 13)
        Me.lblKPB.TabIndex = 6
        Me.lblKPB.Text = "KPB"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbResult)
        Me.GroupBox1.Controls.Add(Me.rbProses)
        Me.GroupBox1.Location = New System.Drawing.Point(215, 62)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(137, 28)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'rbResult
        '
        Me.rbResult.AutoSize = True
        Me.rbResult.Location = New System.Drawing.Point(76, 8)
        Me.rbResult.Name = "rbResult"
        Me.rbResult.Size = New System.Drawing.Size(55, 17)
        Me.rbResult.TabIndex = 1
        Me.rbResult.Text = "Result"
        Me.rbResult.UseVisualStyleBackColor = True
        '
        'rbProses
        '
        Me.rbProses.AutoSize = True
        Me.rbProses.Checked = True
        Me.rbProses.Location = New System.Drawing.Point(6, 8)
        Me.rbProses.Name = "rbProses"
        Me.rbProses.Size = New System.Drawing.Size(57, 17)
        Me.rbProses.TabIndex = 0
        Me.rbProses.TabStop = True
        Me.rbProses.Text = "Proses"
        Me.rbProses.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbRevisi)
        Me.GroupBox2.Controls.Add(Me.rbAsli)
        Me.GroupBox2.Location = New System.Drawing.Point(215, 90)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(137, 28)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        '
        'rbRevisi
        '
        Me.rbRevisi.AutoSize = True
        Me.rbRevisi.Location = New System.Drawing.Point(76, 8)
        Me.rbRevisi.Name = "rbRevisi"
        Me.rbRevisi.Size = New System.Drawing.Size(54, 17)
        Me.rbRevisi.TabIndex = 1
        Me.rbRevisi.Text = "Revisi"
        Me.rbRevisi.UseVisualStyleBackColor = True
        '
        'rbAsli
        '
        Me.rbAsli.AutoSize = True
        Me.rbAsli.Checked = True
        Me.rbAsli.Location = New System.Drawing.Point(6, 8)
        Me.rbAsli.Name = "rbAsli"
        Me.rbAsli.Size = New System.Drawing.Size(41, 17)
        Me.rbAsli.TabIndex = 0
        Me.rbAsli.TabStop = True
        Me.rbAsli.Text = "Asli"
        Me.rbAsli.UseVisualStyleBackColor = True
        '
        'frmExport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(409, 216)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblKPB)
        Me.Controls.Add(Me.lblBatch)
        Me.Controls.Add(Me.cmbBatch)
        Me.Controls.Add(Me.cmbKPB)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnH1ToH2)
        Me.Controls.Add(Me.btnH2ToH1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmExport"
        Me.Text = "Export"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnH1ToH2 As System.Windows.Forms.Button
    Friend WithEvents btnH2ToH1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbKPB As System.Windows.Forms.ComboBox
    Friend WithEvents cmbBatch As System.Windows.Forms.ComboBox
    Friend WithEvents lblBatch As System.Windows.Forms.Label
    Friend WithEvents lblKPB As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbResult As System.Windows.Forms.RadioButton
    Friend WithEvents rbProses As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rbRevisi As System.Windows.Forms.RadioButton
    Friend WithEvents rbAsli As System.Windows.Forms.RadioButton
End Class
