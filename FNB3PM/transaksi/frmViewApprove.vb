﻿Imports System.Data.OleDb
Public Class frmViewApprove
    Dim transaction As OleDbTransaction
    Dim revcolor As Color = Color.Chocolate
    Private Sub btnSearchMD_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchMD.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("M_Maindealer")
        f.setColumns("*")

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtKodeMD.Text = f.value
        End If
        f.Dispose()
        load_MD()
    End Sub
    Private Sub load_MD()
        Dim conn2 As New OLEDBConnection(strcon)
        conn2.Open()
        Dim cmd As oledbCommand
        Dim reader As OleDbDataReader = Nothing

        Try
            cmd = New OleDbCommand("select * from M_Maindealer where md_kode = '" & txtKodeMD.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                lblMainDealer.Text = reader("md_name")
            End If
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub
    Private Sub btnSearchDealer_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchDealer.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("M_Dealer")
        f.setColumns("*")
        f.setWhere(IIf(txtKodeMD.Text = "", "", " where dealer_mdkode='" & txtKodeMD.Text & "'"))

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtKodeDlr.Text = f.value
        End If
        f.Dispose()
        load_Dealer()
    End Sub
    Private Sub load_Dealer()
        Dim conn2 As New OleDbConnection(strcon)
        conn2.Open()
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        Try
            cmd = New OleDbCommand("select * from M_Dealer where dealer_kode = '" & txtKodeDlr.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                lblDealer.Text = reader("dealer_name")
            End If
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

    Private Sub txtKodeMD_LostFocus(sender As Object, e As System.EventArgs) Handles txtKodeMD.LostFocus
        load_MD()
    End Sub

    Private Sub txtKodeDlr_LostFocus(sender As Object, e As System.EventArgs) Handles txtKodeDlr.LostFocus
        load_Dealer()
    End Sub

    Private Sub loadKPB()
        Dim conn2 As New OleDbConnection(strcon)
        conn2.Open()
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        Try
            For i = dgvFile.Columns.Count - 1 To 1 Step -1
                dgvFile.Columns.Remove(dgvFile.Columns(i).Name)
            Next
            cmd = New OleDbCommand("select * from t_h1toh2 where dealer_kode = '" & txtKodeDlr.Text & "' and KPB = '" & cmbKPB.Text & "' order by tahun,bulan", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                With dgvFile
                    .Columns.Add(reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString, reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString)
                    .Columns(.Columns.Count - 1).ReadOnly = True
                    .Columns(.Columns.Count - 1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns(.Columns.Count - 1).Width = 50

                    .Columns.Add(reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString & " pct", "%")
                    .Columns(.Columns.Count - 1).ReadOnly = True
                    .Columns(.Columns.Count - 1).Width = 50

                    .Columns(.Columns.Count - 1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Item(.Columns.Count - 2, 0).Value = reader.Item("Total_DataSource").ToString
                    .Item(.Columns.Count - 2, 1).Value = reader.Item("PhoneAvailable").ToString
                    .Item(.Columns.Count - 1, 1).Value = Format((reader.Item("PhoneAvailable") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 2).Value = reader.Item("SMSSent").ToString
                    .Item(.Columns.Count - 1, 2).Value = Format((reader.Item("SMSSent") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 3).Value = reader.Item("1stResult").ToString
                    .Item(.Columns.Count - 1, 3).Value = Format((reader.Item("1stResult") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 4).Value = reader.Item("Contacted").ToString
                    .Item(.Columns.Count - 1, 4).Value = Format((reader.Item("Contacted") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 5).Value = reader.Item("Unreachable").ToString
                    .Item(.Columns.Count - 1, 5).Value = Format((reader.Item("Unreachable") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 6).Value = reader.Item("Rejected").ToString
                    .Item(.Columns.Count - 1, 6).Value = Format((reader.Item("Rejected") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 7).Value = reader.Item("Workload").ToString
                    .Item(.Columns.Count - 1, 7).Value = Format((reader.Item("Workload") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 8).Value = reader.Item("2ndResult").ToString
                    .Item(.Columns.Count - 1, 8).Value = Format((reader.Item("2ndResult") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 9).Value = reader.Item("TotalVisitor").ToString
                    .Item(.Columns.Count - 1, 9).Value = Format((reader.Item("TotalVisitor") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 10).Value = reader.Item("TooFar").ToString
                    .Item(.Columns.Count - 1, 10).Value = Format((reader.Item("TooFar") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 11).Value = reader.Item("NoTime").ToString
                    .Item(.Columns.Count - 1, 11).Value = Format((reader.Item("NoTime") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 12).Value = reader.Item("Forget").ToString
                    .Item(.Columns.Count - 1, 12).Value = Format((reader.Item("Forget") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                End With

            End While
            reader.Close()
            For i = dgRev.Columns.Count - 1 To 1 Step -1
                dgRev.Columns.Remove(dgRev.Columns(i).Name)
            Next
            cmd = New OleDbCommand("select * from t_h1toh2_rev where dealer_kode = '" & txtKodeDlr.Text & "' and KPB = '" & cmbKPB.Text & "' order by tahun,bulan", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                With dgRev
                    .Columns.Add(reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString, reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString)
                    .Columns(.Columns.Count - 1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns(.Columns.Count - 1).Width = 50

                    .Columns.Add(reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString & " pct", "%")
                    .Columns(.Columns.Count - 1).Width = 50
                    .Columns(.Columns.Count - 1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Item(.Columns.Count - 2, 0).Value = reader.Item("Total_DataSource").ToString
                    .Item(.Columns.Count - 2, 1).Value = reader.Item("PhoneAvailable").ToString
                    .Item(.Columns.Count - 1, 1).Value = Format((reader.Item("PhoneAvailable") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 2).Value = reader.Item("SMSSent").ToString
                    .Item(.Columns.Count - 1, 2).Value = Format((reader.Item("SMSSent") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 3).Value = reader.Item("1stResult").ToString
                    .Item(.Columns.Count - 1, 3).Value = Format((reader.Item("1stResult") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 4).Value = reader.Item("Contacted").ToString
                    .Item(.Columns.Count - 1, 4).Value = Format((reader.Item("Contacted") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 5).Value = reader.Item("Unreachable").ToString
                    .Item(.Columns.Count - 1, 5).Value = Format((reader.Item("Unreachable") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 6).Value = reader.Item("Rejected").ToString
                    .Item(.Columns.Count - 1, 6).Value = Format((reader.Item("Rejected") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 7).Value = reader.Item("Workload").ToString
                    .Item(.Columns.Count - 1, 7).Value = Format((reader.Item("Workload") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 8).Value = reader.Item("2ndResult").ToString
                    .Item(.Columns.Count - 1, 8).Value = Format((reader.Item("2ndResult") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 9).Value = reader.Item("TotalVisitor").ToString
                    .Item(.Columns.Count - 1, 9).Value = Format((reader.Item("TotalVisitor") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 10).Value = reader.Item("TooFar").ToString
                    .Item(.Columns.Count - 1, 10).Value = Format((reader.Item("TooFar") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 11).Value = reader.Item("NoTime").ToString
                    .Item(.Columns.Count - 1, 11).Value = Format((reader.Item("NoTime") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 2, 12).Value = reader.Item("Forget").ToString
                    .Item(.Columns.Count - 1, 12).Value = Format((reader.Item("Forget") / reader.Item("Total_DataSource")) * 100, "0.##") & " %"
                End With

            End While
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

    Private Sub frmViewApprove_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        With dgvFile
            .Rows.Add(13)
            .Item(0, 0).Value = "Total data source (sales m)"
            .Item(0, 1).Value = "Phone No Available"
            .Item(0, 2).Value = "SMS Sent"
            .Item(0, 3).Value = "1st  result KPB (after SMS)"
            .Item(0, 4).Value = "Contacted by Call"
            .Item(0, 5).Value = "Unreachable"
            .Item(0, 6).Value = "Rejected"
            .Item(0, 7).Value = "Workload"
            .Item(0, 8).Value = "2nd result (after call)"
            .Item(0, 9).Value = "Total Visitor KPB"
            .Item(0, 10).Value = "Too Far"
            .Item(0, 11).Value = "No Time"
            .Item(0, 12).Value = "Forget"
        End With
        With dgRev
            .Rows.Add(13)
            .Item(0, 0).Value = "Total data source (sales m)"
            .Item(0, 1).Value = "Phone No Available"
            .Item(0, 2).Value = "SMS Sent"
            .Item(0, 3).Value = "1st  result KPB (after SMS)"
            .Item(0, 4).Value = "Contacted by Call"
            .Item(0, 5).Value = "Unreachable"
            .Item(0, 6).Value = "Rejected"
            .Item(0, 7).Value = "Workload"
            .Item(0, 8).Value = "2nd result (after call)"
            .Item(0, 9).Value = "Total Visitor KPB"
            .Item(0, 10).Value = "Too Far"
            .Item(0, 11).Value = "No Time"
            .Item(0, 12).Value = "Forget"
        End With
    End Sub

    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Dim conn2 As New OleDbConnection(strcon)
        Try

            conn2.Open()
            transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)

            Dim cmd As OleDbCommand
            Dim reader As OleDbDataReader = Nothing
            Dim periode() As String
            Dim fields() As String
            ReDim fields(16)

            
            fields(0) = "total_datasource"
            fields(1) = "PhoneAvailable"
            fields(2) = "SMSSent"
            fields(3) = "1stResult"
            fields(4) = "Contacted"
            fields(5) = "Unreachable"
            fields(6) = "Rejected"
            fields(7) = "Workload"
            fields(8) = "2ndResult"
            fields(9) = "TotalVisitor"
            fields(10) = "TooFar"
            fields(11) = "NoTime"
            fields(12) = "Forget"
            fields(13) = "Create_date"
            fields(14) = "Create_user"
            fields(15) = "KPB"

            For col As Integer = 1 To dgRev.Columns.Count - 1 Step 2
                periode = Split(dgRev.Columns(col).HeaderText, "-")
                For row As Integer = 1 To 12
                    If dgRev.Item(col, row).Style.BackColor = revcolor Then
                        cmd = New OleDbCommand("update t_h1toh2_rev set " & fields(row) & "='" & dgRev.Item(col, row).Value & "',create_date='" & Format(Now, "yyyy/MM/dd HH:mm:ss") & "',create_user='" & user & "'  where kpb = '" & cmbKPB.Text & "' and dealer_kode='" & txtKodeDlr.Text & "' and bulan='" & periode(0) & "' and tahun='" & periode(1) & "'", conn2, transaction)
                        cmd.ExecuteNonQuery()
                    End If

                Next
            Next

            transaction.Commit()
            conn2.Close()
            MsgBox("Data sudah tersimpan")

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()
            transaction.Rollback()
            If conn2.State Then conn2.Close()
        End Try
    End Sub
    
    Private Sub cmbKPB_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbKPB.SelectedIndexChanged
        loadKPB()
    End Sub

    Private Sub dgRev_CellValidating(sender As Object, e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgRev.CellValidating
        If dgRev.Item(e.ColumnIndex, e.RowIndex).Value <> e.FormattedValue Then dgRev.Item(e.ColumnIndex, e.RowIndex).Style.BackColor = revcolor

    End Sub

End Class