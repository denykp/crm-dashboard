﻿Imports System.Data.OleDb
Public Class frmViewApprove2
    Dim transaction As OleDbTransaction
    Dim revcolor As Color = Color.Chocolate
    Private Sub btnSearchMD_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchMD.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("M_Maindealer")
        f.setColumns("*")

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtKodeMD.Text = f.value
        End If
        f.Dispose()
        load_MD()
    End Sub
    Private Sub load_MD()
        Dim conn2 As New OLEDBConnection(strcon)
        conn2.Open()
        Dim cmd As oledbCommand
        Dim reader As OleDbDataReader = Nothing

        Try
            cmd = New OleDbCommand("select * from M_Maindealer where md_kode = '" & txtKodeMD.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                lblMainDealer.Text = reader("md_name")
            End If
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub
    Private Sub btnSearchDealer_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchDealer.Click
        Dim f As New frmSearch
        f.setCol(0)
        f.setTableName("M_Dealer")
        f.setColumns("*")
        f.setWhere(IIf(txtKodeMD.Text = "", "", " where dealer_mdkode='" & txtKodeMD.Text & "'"))

        If f.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            txtKodeDlr.Text = f.value
        End If
        f.Dispose()
        load_Dealer()
    End Sub
    Private Sub load_Dealer()
        Dim conn2 As New OleDbConnection(strcon)
        conn2.Open()
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing

        Try
            cmd = New OleDbCommand("select * from M_Dealer where dealer_kode = '" & txtKodeDlr.Text & "'", conn2)
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                lblDealer.Text = reader("dealer_name")
            End If
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

    Private Sub txtKodeMD_LostFocus(sender As Object, e As System.EventArgs) Handles txtKodeMD.LostFocus
        load_MD()
    End Sub

    Private Sub txtKodeDlr_LostFocus(sender As Object, e As System.EventArgs) Handles txtKodeDlr.LostFocus
        load_Dealer()
    End Sub

    Private Sub loadData()
        Dim conn2 As New OleDbConnection(strcon)
        conn2.Open()
        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing
        Dim type As String = ""
        Try
            For i = dgvFile.Columns.Count - 1 To 1 Step -1
                dgvFile.Columns.Remove(dgvFile.Columns(i).Name)
            Next
            Select Case cmbType.Text
                Case "H1 to H1 Buy Only" : Type = "H1H1"
                Case "H2 to H1 Own Dealer" : Type = "H2H1Own"
                Case "H2 to H1 Other Dealer" : Type = "H2H1Other"
            End Select
            cmd = New OleDbCommand("select * from t_h2toh1 where dealer_kode = '" & txtKodeDlr.Text & "' order by tahun,bulan", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                With dgvFile
                    .Columns.Add(reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString, reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString)
                    .Columns(.Columns.Count - 1).ReadOnly = True
                    .Columns(.Columns.Count - 1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns(.Columns.Count - 1).Width = 50

                    .Columns.Add(reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString & " pct", "%")
                    .Columns(.Columns.Count - 1).ReadOnly = True
                    .Columns(.Columns.Count - 1).Width = 50

                    .Columns(.Columns.Count - 1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

                    .Item(.Columns.Count - 2, 0).Value = reader.Item(type & "TotalDataSource").ToString
                    .Item(.Columns.Count - 2, 1).Value = reader.Item(type & "DataFiltering").ToString
                    .Item(.Columns.Count - 2, 2).Value = reader.Item(type & "SMSSent").ToString
                    .Item(.Columns.Count - 2, 3).Value = reader.Item(type & "WorkloadM1").ToString
                    .Item(.Columns.Count - 2, 4).Value = reader.Item(type & "Contacted").ToString
                    .Item(.Columns.Count - 2, 5).Value = reader.Item(type & "Unreachable").ToString
                    .Item(.Columns.Count - 2, 6).Value = reader.Item(type & "Rejected").ToString
                    .Item(.Columns.Count - 2, 7).Value = reader.Item(type & "Workload").ToString
                    .Item(.Columns.Count - 2, 8).Value = reader.Item(type & "ProspectM2").ToString
                    .Item(.Columns.Count - 2, 9).Value = reader.Item(type & "ProspectM1").ToString
                    .Item(.Columns.Count - 2, 10).Value = reader.Item(type & "ProspectM").ToString
                    .Item(.Columns.Count - 2, 11).Value = reader.Item(type & "TotalProspect").ToString
                    .Item(.Columns.Count - 2, 12).Value = reader.Item(type & "HotProspect").ToString
                    .Item(.Columns.Count - 2, 13).Value = reader.Item(type & "LowProspect").ToString
                    .Item(.Columns.Count - 2, 14).Value = reader.Item(type & "NotDeal").ToString
                    .Item(.Columns.Count - 2, 15).Value = reader.Item(type & "DealData").ToString
                    .Item(.Columns.Count - 2, 16).Value = reader.Item(type & "DealSales").ToString
                    .Item(.Columns.Count - 1, 2).Value = Format((reader.Item(type & "SMSSent") / reader.Item(type & "DataFiltering")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 4).Value = Format((reader.Item(type & "Contacted") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 5).Value = Format((reader.Item(type & "Unreachable") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 6).Value = Format((reader.Item(type & "Rejected") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 7).Value = Format((reader.Item(type & "Workload") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1"))) * 100, "0.##") & " %"

                    .Item(.Columns.Count - 1, 11).Value = Format((reader.Item(type & "TotalProspect") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1") + reader.Item(type & "ProspectM2") + reader.Item(type & "ProspectM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 12).Value = Format((reader.Item(type & "HotProspect") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1") + reader.Item(type & "ProspectM2") + reader.Item(type & "ProspectM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 13).Value = Format((reader.Item(type & "LowProspect") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1") + reader.Item(type & "ProspectM2") + reader.Item(type & "ProspectM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 14).Value = Format((reader.Item(type & "NotDeal") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1") + reader.Item(type & "ProspectM2") + reader.Item(type & "ProspectM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 15).Value = Format((reader.Item(type & "DealData") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1") + reader.Item(type & "ProspectM2") + reader.Item(type & "ProspectM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 16).Value = Format((reader.Item(type & "DealSales") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1") + reader.Item(type & "ProspectM2") + reader.Item(type & "ProspectM1"))) * 100, "0.##") & " %"
                End With

            End While
            reader.Close()
            For i = dgRev.Columns.Count - 1 To 1 Step -1
                dgRev.Columns.Remove(dgRev.Columns(i).Name)
            Next
            cmd = New OleDbCommand("select * from t_h2toh1_rev where dealer_kode = '" & txtKodeDlr.Text & "' order by tahun,bulan", conn2)
            reader = cmd.ExecuteReader()
            While reader.Read()
                With dgRev
                    .Columns.Add(reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString, reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString)
                    .Columns(.Columns.Count - 1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns(.Columns.Count - 1).Width = 50

                    .Columns.Add(reader.Item("bulan").ToString & "-" & reader.Item("tahun").ToString & " pct", "%")
                    .Columns(.Columns.Count - 1).Width = 50
                    .Columns(.Columns.Count - 1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Item(.Columns.Count - 2, 0).Value = reader.Item(type & "TotalDataSource").ToString
                    .Item(.Columns.Count - 2, 1).Value = reader.Item(type & "DataFiltering").ToString
                    .Item(.Columns.Count - 2, 2).Value = reader.Item(type & "SMSSent").ToString
                    .Item(.Columns.Count - 2, 3).Value = reader.Item(type & "WorkloadM1").ToString
                    .Item(.Columns.Count - 2, 4).Value = reader.Item(type & "Contacted").ToString
                    .Item(.Columns.Count - 2, 5).Value = reader.Item(type & "Unreachable").ToString
                    .Item(.Columns.Count - 2, 6).Value = reader.Item(type & "Rejected").ToString
                    .Item(.Columns.Count - 2, 7).Value = reader.Item(type & "Workload").ToString
                    .Item(.Columns.Count - 2, 8).Value = reader.Item(type & "ProspectM2").ToString
                    .Item(.Columns.Count - 2, 9).Value = reader.Item(type & "ProspectM1").ToString
                    .Item(.Columns.Count - 2, 10).Value = reader.Item(type & "ProspectM").ToString
                    .Item(.Columns.Count - 2, 11).Value = reader.Item(type & "TotalProspect").ToString
                    .Item(.Columns.Count - 2, 12).Value = reader.Item(type & "HotProspect").ToString
                    .Item(.Columns.Count - 2, 13).Value = reader.Item(type & "LowProspect").ToString
                    .Item(.Columns.Count - 2, 14).Value = reader.Item(type & "NotDeal").ToString
                    .Item(.Columns.Count - 2, 15).Value = reader.Item(type & "DealData").ToString
                    .Item(.Columns.Count - 2, 16).Value = reader.Item(type & "DealSales").ToString
                    .Item(.Columns.Count - 1, 2).Value = Format((reader.Item(type & "SMSSent") / reader.Item(type & "DataFiltering")) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 4).Value = Format((reader.Item(type & "Contacted") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 5).Value = Format((reader.Item(type & "Unreachable") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 6).Value = Format((reader.Item(type & "Rejected") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 7).Value = Format((reader.Item(type & "Workload") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1"))) * 100, "0.##") & " %"

                    .Item(.Columns.Count - 1, 11).Value = Format((reader.Item(type & "TotalProspect") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1") + reader.Item(type & "ProspectM2") + reader.Item(type & "ProspectM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 12).Value = Format((reader.Item(type & "HotProspect") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1") + reader.Item(type & "ProspectM2") + reader.Item(type & "ProspectM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 13).Value = Format((reader.Item(type & "LowProspect") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1") + reader.Item(type & "ProspectM2") + reader.Item(type & "ProspectM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 14).Value = Format((reader.Item(type & "NotDeal") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1") + reader.Item(type & "ProspectM2") + reader.Item(type & "ProspectM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 15).Value = Format((reader.Item(type & "DealData") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1") + reader.Item(type & "ProspectM2") + reader.Item(type & "ProspectM1"))) * 100, "0.##") & " %"
                    .Item(.Columns.Count - 1, 16).Value = Format((reader.Item(type & "DealSales") / (reader.Item(type & "DataFiltering") + reader.Item(type & "WorkloadM1") + reader.Item(type & "ProspectM2") + reader.Item(type & "ProspectM1"))) * 100, "0.##") & " %"

                End With

            End While
            reader.Close()
            conn2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

    Private Sub frmViewApprove_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        With dgvFile
            .Rows.Add(17)
            .Item(0, 0).Value = "Total Data Source"
            .Item(0, 1).Value = "Data Filtering"
            .Item(0, 2).Value = "SMS Sent"
            .Item(0, 3).Value = "Workload from (M-1)"
            .Item(0, 4).Value = "Contacted by Phone"
            .Item(0, 5).Value = "Unreachable"
            .Item(0, 6).Value = "Rejected"
            .Item(0, 7).Value = "Workload"
            .Item(0, 8).Value = "Prospect Customer (M-2)"
            .Item(0, 9).Value = "Prospect Customer (M-1)"
            .Item(0, 10).Value = "Prospect Customer (M)"
            .Item(0, 11).Value = "Total Prospect"
            .Item(0, 12).Value = "Hot Prospect"
            .Item(0, 13).Value = "Low Prospect"
            .Item(0, 14).Value = "Not Deal"
            .Item(0, 15).Value = "Deal / Data analysis"
            .Item(0, 16).Value = "Deal/ Sales"
        End With
        With dgRev
            .Rows.Add(17)
            .Item(0, 0).Value = "Total Data Source"
            .Item(0, 1).Value = "Data Filtering"
            .Item(0, 2).Value = "SMS Sent"
            .Item(0, 3).Value = "Workload from (M-1)"
            .Item(0, 4).Value = "Contacted by Phone"
            .Item(0, 5).Value = "Unreachable"
            .Item(0, 6).Value = "Rejected"
            .Item(0, 7).Value = "Workload"
            .Item(0, 8).Value = "Prospect Customer (M-2)"
            .Item(0, 9).Value = "Prospect Customer (M-1)"
            .Item(0, 10).Value = "Prospect Customer (M)"
            .Item(0, 11).Value = "Total Prospect"
            .Item(0, 12).Value = "Hot Prospect"
            .Item(0, 13).Value = "Low Prospect"
            .Item(0, 14).Value = "Not Deal"
            .Item(0, 15).Value = "Deal / Data analysis"
            .Item(0, 16).Value = "Deal/ Sales"
        End With
    End Sub

    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Dim conn2 As New OleDbConnection(strcon)
        Try

            conn2.Open()
            transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)

            Dim cmd As OleDbCommand
            Dim reader As OleDbDataReader = Nothing
            Dim fields() As String
            Dim periode() As String
            ReDim fields(17)
            Dim type As String = ""
            Select Case cmbType.Text
                Case "H1 to H1 Buy Only" : Type = "H1H1"
                Case "H2 to H1 Own Dealer" : Type = "H2H1Own"
                Case "H2 to H1 Other Dealer" : Type = "H2H1Other"
            End Select

            fields(0) = type & "TotalDataSource"
            fields(1) = type & "DataFiltering"
            fields(2) = type & "SMSSent"
            fields(3) = type & "WorkloadM1"
            fields(4) = type & "Contacted"
            fields(5) = type & "Unreachable"
            fields(6) = type & "Rejected"
            fields(7) = type & "Workload"
            fields(8) = type & "ProspectM2"
            fields(9) = type & "ProspectM1"
            fields(10) = type & "ProspectM"
            fields(11) = type & "TotalProspect"
            fields(12) = type & "HotProspect"
            fields(13) = type & "LowProspect"
            fields(14) = type & "NotDeal"
            fields(15) = type & "DealData"
            fields(16) = type & "DealSales"

            For col As Integer = 1 To dgRev.Columns.Count - 1 Step 2
                periode = Split(dgRev.Columns(col).HeaderText, "-")
                For row As Integer = 1 To 12
                    If dgRev.Item(col, row).Style.BackColor = revcolor Then
                        cmd = New OleDbCommand("update t_h2toh1_rev set " & fields(row) & "='" & dgRev.Item(col, row).Value & "',create_date='" & Format(Now, "yyyy/MM/dd HH:mm:ss") & "',create_user='" & user & "'  where  dealer_kode='" & txtKodeDlr.Text & "' and bulan='" & periode(0) & "' and tahun='" & periode(1) & "'", conn2, transaction)
                        cmd.ExecuteNonQuery()
                    End If

                Next
            Next

            transaction.Commit()
            conn2.Close()
            MsgBox("Data sudah tersimpan")

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            If reader IsNot Nothing Then reader.Close()
            transaction.Rollback()
            If conn2.State Then conn2.Close()
        End Try
    End Sub

    Private Sub cmbKPB_SelectedIndexChanged(sender As System.Object, e As System.EventArgs)
        loadData()
    End Sub

    Private Sub dgRev_CellValidating(sender As Object, e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgRev.CellValidating
        If dgRev.Item(e.ColumnIndex, e.RowIndex).Value <> e.FormattedValue Then dgRev.Item(e.ColumnIndex, e.RowIndex).Style.BackColor = revcolor

    End Sub

    Private Sub btnLoad_Click(sender As System.Object, e As System.EventArgs) Handles btnLoad.Click
        loadData()
    End Sub
End Class