﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewApprove2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtKodeDlr = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtKodeMD = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblDealer = New System.Windows.Forms.Label()
        Me.lblMainDealer = New System.Windows.Forms.Label()
        Me.btnSearchMD = New System.Windows.Forms.Button()
        Me.btnSearchDealer = New System.Windows.Forms.Button()
        Me.dgvFile = New System.Windows.Forms.DataGridView()
        Me.Keterangan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.dgRev = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmbType = New System.Windows.Forms.ComboBox()
        CType(Me.dgvFile, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgRev, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtKodeDlr
        '
        Me.txtKodeDlr.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKodeDlr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtKodeDlr.Location = New System.Drawing.Point(93, 42)
        Me.txtKodeDlr.MaxLength = 10
        Me.txtKodeDlr.Name = "txtKodeDlr"
        Me.txtKodeDlr.Size = New System.Drawing.Size(117, 21)
        Me.txtKodeDlr.TabIndex = 16
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 45)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Dealer"
        '
        'txtKodeMD
        '
        Me.txtKodeMD.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKodeMD.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtKodeMD.Location = New System.Drawing.Point(93, 12)
        Me.txtKodeMD.MaxLength = 5
        Me.txtKodeMD.Name = "txtKodeMD"
        Me.txtKodeMD.Size = New System.Drawing.Size(117, 21)
        Me.txtKodeMD.TabIndex = 14
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 13)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Main Dealer"
        '
        'lblDealer
        '
        Me.lblDealer.AutoSize = True
        Me.lblDealer.Location = New System.Drawing.Point(254, 45)
        Me.lblDealer.Name = "lblDealer"
        Me.lblDealer.Size = New System.Drawing.Size(10, 13)
        Me.lblDealer.TabIndex = 20
        Me.lblDealer.Text = "-"
        '
        'lblMainDealer
        '
        Me.lblMainDealer.AutoSize = True
        Me.lblMainDealer.Location = New System.Drawing.Point(254, 16)
        Me.lblMainDealer.Name = "lblMainDealer"
        Me.lblMainDealer.Size = New System.Drawing.Size(10, 13)
        Me.lblMainDealer.TabIndex = 19
        Me.lblMainDealer.Text = "-"
        '
        'btnSearchMD
        '
        Me.btnSearchMD.ForeColor = System.Drawing.Color.Black
        Me.btnSearchMD.Location = New System.Drawing.Point(216, 11)
        Me.btnSearchMD.Name = "btnSearchMD"
        Me.btnSearchMD.Size = New System.Drawing.Size(32, 23)
        Me.btnSearchMD.TabIndex = 21
        Me.btnSearchMD.Text = "..."
        Me.btnSearchMD.UseVisualStyleBackColor = True
        '
        'btnSearchDealer
        '
        Me.btnSearchDealer.ForeColor = System.Drawing.Color.Black
        Me.btnSearchDealer.Location = New System.Drawing.Point(216, 40)
        Me.btnSearchDealer.Name = "btnSearchDealer"
        Me.btnSearchDealer.Size = New System.Drawing.Size(32, 23)
        Me.btnSearchDealer.TabIndex = 22
        Me.btnSearchDealer.Text = "..."
        Me.btnSearchDealer.UseVisualStyleBackColor = True
        '
        'dgvFile
        '
        Me.dgvFile.AllowUserToAddRows = False
        Me.dgvFile.AllowUserToDeleteRows = False
        Me.dgvFile.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvFile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFile.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Keterangan})
        Me.dgvFile.Location = New System.Drawing.Point(15, 72)
        Me.dgvFile.Name = "dgvFile"
        Me.dgvFile.Size = New System.Drawing.Size(892, 286)
        Me.dgvFile.TabIndex = 74
        '
        'Keterangan
        '
        Me.Keterangan.HeaderText = "Keterangan"
        Me.Keterangan.Name = "Keterangan"
        Me.Keterangan.ReadOnly = True
        Me.Keterangan.Width = 150
        '
        'btnSave
        '
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(672, 40)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(83, 23)
        Me.btnSave.TabIndex = 75
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'dgRev
        '
        Me.dgRev.AllowUserToAddRows = False
        Me.dgRev.AllowUserToDeleteRows = False
        Me.dgRev.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgRev.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgRev.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1})
        Me.dgRev.Location = New System.Drawing.Point(15, 364)
        Me.dgRev.Name = "dgRev"
        Me.dgRev.Size = New System.Drawing.Size(892, 284)
        Me.dgRev.TabIndex = 76
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Keterangan"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 150
        '
        'btnLoad
        '
        Me.btnLoad.ForeColor = System.Drawing.Color.Black
        Me.btnLoad.Location = New System.Drawing.Point(583, 40)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(83, 23)
        Me.btnLoad.TabIndex = 77
        Me.btnLoad.Text = "Load"
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label7.Location = New System.Drawing.Point(503, 15)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(31, 13)
        Me.Label7.TabIndex = 79
        Me.Label7.Text = "Type"
        '
        'cmbType
        '
        Me.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbType.FormattingEnabled = True
        Me.cmbType.Items.AddRange(New Object() {"H1 to H1 Buy Only", "H2 to H1 Own Dealer", "H2 to H1 Other Dealer"})
        Me.cmbType.Location = New System.Drawing.Point(583, 12)
        Me.cmbType.Name = "cmbType"
        Me.cmbType.Size = New System.Drawing.Size(172, 21)
        Me.cmbType.TabIndex = 78
        '
        'frmViewApprove2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(919, 655)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cmbType)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.dgRev)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.dgvFile)
        Me.Controls.Add(Me.btnSearchDealer)
        Me.Controls.Add(Me.btnSearchMD)
        Me.Controls.Add(Me.lblDealer)
        Me.Controls.Add(Me.lblMainDealer)
        Me.Controls.Add(Me.txtKodeDlr)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtKodeMD)
        Me.Controls.Add(Me.Label2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmViewApprove2"
        Me.Text = "Approve Data"
        CType(Me.dgvFile, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgRev, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtKodeDlr As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtKodeMD As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblDealer As System.Windows.Forms.Label
    Friend WithEvents lblMainDealer As System.Windows.Forms.Label
    Friend WithEvents btnSearchMD As System.Windows.Forms.Button
    Friend WithEvents btnSearchDealer As System.Windows.Forms.Button
    Friend WithEvents dgvFile As System.Windows.Forms.DataGridView
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents Keterangan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgRev As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmbType As System.Windows.Forms.ComboBox
End Class
