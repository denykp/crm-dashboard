﻿Imports System.Data.OleDb

Module modUnposting
    Public Sub unposting_pembelian(ByVal no_transaksi As String, ByVal conn2 As oledbConnection, ByVal trans As oledbTransaction)

        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim cmd1 As oledbCommand
        Dim reader1 As oledbDataReader = Nothing
        Dim tanggal As Date
        cmd = New oledbCommand("select * from t_pembelianh where nomer_pembelian = '" & no_transaksi & "'", conn2, trans)
        reader = cmd.ExecuteReader()
        While reader.Read()
            tanggal = reader("tanggal")
            deletekartustock("", no_transaksi, conn2, trans)
            deletebukukas(no_transaksi, conn2, trans)
            If reader("cara_bayar") = "1" Then
                cmd = New oledbCommand("delete from list_hutang where nomer_transaksi = '" & no_transaksi & "'", conn2, trans)
                cmd.ExecuteNonQuery()
                deletekartuhutang(no_transaksi, conn2, trans)
            Else
                cmd = New oledbCommand("update ms_kasbank set saldo = saldo + " & CDbl(reader("total")) & " where kode_kasbank = '" & reader("kode_kasbank") & "' ", conn2, trans)
                cmd.ExecuteNonQuery()
            End If
            cmd = New oledbCommand("delete from t_pembelianh where nomer_pembelian = '" & no_transaksi & "';delete from hst_hpp where nomer_transaksi = '" & no_transaksi & "'", conn2, trans)
            cmd.ExecuteNonQuery()
            cmd1 = New oledbCommand("select * from t_pembeliand where nomer_pembelian = '" & no_transaksi & "'", conn2, trans)
            reader1 = cmd1.ExecuteReader()
            Do While reader1.Read
                updatestock(reader("kode_gudang"), reader1("kode_barang"), reader1("qty_pcs") * -1, tanggal, "", conn2, trans)
            Loop
            cmd = New oledbCommand("delete from t_pembeliand where nomer_pembelian = '" & no_transaksi & "'", conn2, trans)
            cmd.ExecuteNonQuery()
        End While
        reader.Close()
        Dim query As String
        query = "update hpp set hpp=(select top 1 hpp from hst_hpp where kode_barang=hpp.kode_barang and batch=hpp.batch order by tanggal desc,id desc) where kode_barang in (select kode_barang from t_pembeliand where nomer_pembelian='" & no_transaksi & "');"
        query += "update t_billd set hpp= " & _
             "isnull((select top 1 hpp from hst_hpp where kode_barang=t_billd.kode_barang and tanggal<(select tanggal from t_billh where nomer_bill=t_billd.nomer_bill)" & _
             "order by tanggal desc,id desc),0) where nomer_bill in (select nomer_bill from t_billh where  convert(varchar(20),tanggal,120)>'" & Format(tanggal, "yyyy-MM-dd HH:mm:ss") & "') and  kode_barang in (select kode_barang from t_pembeliand where nomer_pembelian='" & no_transaksi & "')"
        cmd = New oledbCommand(query, conn2, trans)
        cmd.ExecuteNonQuery()
    End Sub
    Public Function unposting_kaskeluar(ByVal no_transaksi As String, ByVal conn As oledbConnection, ByVal transaction As oledbTransaction) As Boolean
        Dim cmd As oledbCommand
        Dim dr As oledbDataReader = Nothing

        Dim tanggal As Date
        Dim jenis As String = ""
        Dim carabayar As String
        Dim keterangan As String = ""

        Dim penerima As String

        unposting_kaskeluar = False
        cmd = New oledbCommand("select * from t_kaskeluarh where nomer_kaskeluar='" & no_transaksi & "'", Conn, transaction)
        dr = cmd.ExecuteReader()
        If dr.Read() Then
            tanggal = dr("tanggal")
            carabayar = dr("carabayar")
            jenis = dr("jenis")
            penerima = dr("penerima")
            keterangan = dr("keterangan")
        End If
        dr.Close()
        cmd = New oledbCommand("select t.kode_kasbank,t.jumlah,m.saldo from t_kaskeluar_kas t inner join ms_kasbank m on t.kode_kasbank=m.kode_kasbank where nomer_kaskeluar='" & no_transaksi & "'", Conn, transaction)
        dr = cmd.ExecuteReader
        While dr.Read
            updatekas(dr("kode_kasbank"), dr("jumlah"), Conn, transaction)


        End While
        dr.Close()
        cmd = New oledbCommand("select * from t_kaskeluar_hutang where nomer_kaskeluar='" & no_transaksi & "'", Conn, transaction)
        dr = cmd.ExecuteReader
        If dr.Read Then

            cmd = New oledbCommand("delete from  list_hutang where nomer_transaksi='" & no_transaksi & "'", Conn, transaction)
            cmd.ExecuteNonQuery()
            cmd = New oledbCommand("update ms_supplier set saldo_hutang=(select sum(sisa_hutang) from list_hutang where kode_supplier=ms_supplier.kode_supplier) where kode_supplier='" & dr("kode_supplier") & "'", Conn, transaction)
            cmd.ExecuteNonQuery()
        End If
        dr.Close()
        deletebukukas(no_transaksi, conn, transaction)
        deletekartuhutang(no_transaksi, conn, transaction)
        deletebukukasbon(no_transaksi, conn, transaction)
        cmd = New oledbCommand("update ms_karyawan set kasbon=(select ISNULL(sum(debet-kredit),0) from kartu_kasbon where nik=ms_karyawan.nik)", conn, transaction)
        cmd.ExecuteNonQuery()
        Select Case jenis

            Case "Pembayaran Hutang"
                cmd = New oledbCommand("select * from t_kaskeluar_bayarhutang j inner join list_hutang t on j.nomer_beli=t.nomer_transaksi where nomer_kaskeluar='" & no_transaksi & "'", Conn, transaction)
                dr = cmd.ExecuteReader
                While dr.Read
                    'updatehutangbayar(dr("nomer_beli, dr("kode_supplier, dr("jumlah_bayar, jenis, tanggal, conn, pc)
                    cmd = New oledbCommand("update list_hutang set sudah_bayar=sudah_bayar-" & dr("jumlah_bayar") & " where nomer_transaksi='" & dr("nomer_beli") & "'", conn, transaction)
                    cmd.ExecuteNonQuery()

                    cmd = New oledbCommand("update ms_supplier set saldo_hutang=(select sum(sisa_hutang) from list_hutang where kode_supplier=ms_supplier.kode_supplier) where kode_supplier='" & dr("kode_supplier") & "'", Conn, transaction)
                    cmd.ExecuteNonQuery()
                End While
                dr.Close()

            Case "Mutasi Kas"
                cmd = New oledbCommand("select * from t_kaskeluar_mutasi  where nomer_kaskeluar='" & no_transaksi & "'", Conn, transaction)
                dr = cmd.ExecuteReader
                If dr.Read Then
                    updatekas(dr("kode_kasbank"), -dr("jumlah"), conn, transaction)
                End If
                dr.Close()
        End Select
        unposting_kaskeluar = True
    End Function
    Public Function unposting_kasmasuk(ByVal no_transaksi As String, ByVal conn As oledbConnection, ByVal transaction As oledbTransaction) As Boolean
        Dim cmd As oledbCommand
        Dim dr As oledbDataReader = Nothing

        Dim tanggal As Date
        Dim jenis As String = ""
        Dim carabayar As String
        Dim keterangan As String = ""

        Dim pembayar As String

        unposting_kasmasuk = False
        cmd = New oledbCommand("select * from t_kasmasukh where nomer_kasmasuk='" & no_transaksi & "'", conn, transaction)
        dr = cmd.ExecuteReader()
        If dr.Read() Then
            tanggal = dr("tanggal")
            carabayar = dr("carabayar")
            jenis = dr("jenis")
            pembayar = dr("pembayar")
            keterangan = dr("keterangan")
        End If
        dr.Close()
        cmd = New oledbCommand("select t.kode_kasbank,t.jumlah,m.saldo from t_kasmasuk_kas t inner join ms_kasbank m on t.kode_kasbank=m.kode_kasbank where nomer_kasmasuk='" & no_transaksi & "'", conn, transaction)
        dr = cmd.ExecuteReader
        While dr.Read
            updatekas(dr("kode_kasbank"), -dr("jumlah"), conn, transaction)


        End While
        dr.Close()
        cmd = New oledbCommand("select * from t_kasmasuk_piutang where nomer_kasmasuk='" & no_transaksi & "'", conn, transaction)
        dr = cmd.ExecuteReader
        If dr.Read Then

            cmd = New oledbCommand("delete from  list_piutang where nomer_transaksi='" & no_transaksi & "'", conn, transaction)
            cmd.ExecuteNonQuery()
            'cmd = New oledbCommand("update ms_customer set saldo_piutang=(select sum(sisa_piutang) from list_piutang where kode_customer=ms_customer.kode_customer) where kode_customer='" & dr("kode_customer") & "'", conn, transaction)
            'cmd.ExecuteNonQuery()
        End If
        dr.Close()
        deletebukukas(no_transaksi, conn, transaction)
        deletekartupiutang(no_transaksi, conn, transaction)
        deletebukukasbon(no_transaksi, conn, transaction)
        cmd = New oledbCommand("update ms_karyawan set kasbon=(select ISNULL(sum(debet-kredit),0) from kartu_kasbon where nik=ms_karyawan.nik)", conn, transaction)
        cmd.ExecuteNonQuery()
        Select Case jenis

            Case "Pembayaran piutang"
                cmd = New oledbCommand("select * from t_kasmasuk_bayarpiutang j inner join list_piutang t on j.nomer_beli=t.nomer_transaksi where nomer_kasmasuk='" & no_transaksi & "'", conn, transaction)
                dr = cmd.ExecuteReader
                While dr.Read
                    'updatepiutangbayar(dr("nomer_beli, dr("kode_customer, dr("jumlah_bayar, jenis, tanggal, conn, pc)
                    cmd = New oledbCommand("update list_piutang set sudah_bayar=sudah_bayar-" & dr("jumlah") & " where nomer_transaksi='" & dr("nomer_beli") & "'", conn, transaction)
                    cmd.ExecuteNonQuery()
                End While
                dr.Close()

            Case "Mutasi Kas"
                cmd = New oledbCommand("select * from t_kasmasuk_mutasi  where nomer_kasmasuk='" & no_transaksi & "'", conn, transaction)
                dr = cmd.ExecuteReader
                If dr.Read Then
                    updatekas(dr("kode_kasbank"), dr("jumlah"), conn, transaction)
                End If
                dr.Close()
        End Select
        unposting_kasmasuk = True
    End Function
    Public Sub unposting_opname(ByVal no_transaksi As String, ByVal conn2 As oledbConnection, ByVal transaction As oledbTransaction)
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        'Dim cmd1 As oledbCommand
        'Dim reader1 As oledbDataReader = Nothing

        cmd = New oledbCommand("select * from t_opnamestockh where nomer_opnamestock = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                deletekartustock("", no_transaksi, conn2, transaction)
                cmd = New oledbCommand("delete from t_opnamestockh where nomer_opnamestock = '" & no_transaksi & "'", conn2, transaction)
                cmd.ExecuteNonQuery()

                'cmd1 = New oledbCommand("select * from t_opnamestockd where id = '" & no_transaksi & "'", conn2, transaction)
                'reader1 = cmd1.ExecuteReader()
                'Do While reader1.Read
                '    updatestock(reader("kode_gudang"), reader1("kode_barang"), reader1("selisih") * IIf(reader1("selisih") < 0, 1, -1), conn2, transaction)
                'Loop

                cmd = New oledbCommand("delete from t_opnamestockd where nomer_opnamestock = '" & no_transaksi & "'", conn2, transaction)
                cmd.ExecuteNonQuery()
            End While
        End If
        reader.Close()
        'reader1.Close(

    End Sub

    Public Sub unposting_koreksi(ByVal no_transaksi As String, ByVal conn2 As oledbConnection, ByVal transaction As oledbTransaction)
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim cmd1 As oledbCommand
        Dim reader1 As oledbDataReader = Nothing
        Dim tanggal As Date
        cmd = New oledbCommand("select * from t_koreksistockh where nomer_koreksi = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        If reader.Read() Then
            tanggal = reader("tanggal")
            deletekartustock("", no_transaksi, conn2, transaction)
            cmd = New oledbCommand("delete from t_koreksistockh where nomer_koreksi = '" & no_transaksi & "'", conn2, transaction)
            cmd.ExecuteNonQuery()
            cmd1 = New oledbCommand("select * from t_koreksistockd where nomer_koreksi = '" & no_transaksi & "'", conn2, transaction)
            reader1 = cmd1.ExecuteReader()
            Do While reader1.Read
                updatestock(reader("kode_gudang"), reader1("kode_barang"), reader1("qty_pcs") * -1, tanggal, "", conn2, transaction)
            Loop
            cmd = New oledbCommand("delete from t_koreksistockd where nomer_koreksi = '" & no_transaksi & "'", conn2, transaction)
            cmd.ExecuteNonQuery()

        End If
        reader.Close()
    End Sub

    Public Sub unposting_returbeli(ByVal no_transaksi As String, ByVal conn2 As oledbConnection, ByVal transaction As oledbTransaction)
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim cmd1 As oledbCommand
        Dim reader1 As oledbDataReader = Nothing
        Dim tanggal As Date
        cmd = New oledbCommand("select * from t_returbelih where nomer_returbeli = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        If reader.Read() Then
            tanggal = reader("tanggal")
            deletekartustock("", no_transaksi, conn2, transaction)
            cmd1 = New oledbCommand("select * from t_returbelid where nomer_returbeli = '" & no_transaksi & "'", conn2, transaction)
            reader1 = cmd1.ExecuteReader()
            Do While reader1.Read
                updatestock(reader("kode_gudang"), reader1("kode_barang"), reader1("qty"), tanggal, "", conn2, transaction)
            Loop

            If reader("tipe_tukar") = "Tukar Tunai" Then
                deletebukukas(no_transaksi, conn2, transaction)
                cmd = New oledbCommand("update ms_kasbank set saldo = saldo - " & CDbl(reader("total")) & " where kode_kasbank = '" & reader("kode_kasbank") & "' ", conn2, transaction)
                cmd.ExecuteNonQuery()
            ElseIf reader("tipe_tukar") = "Pengurangan Tagihan" Then
                cmd = New oledbCommand("update list_hutang set koreksi = -(select isnull(sum(total),0) from t_returbelih where nota=list_hutang.nomer_transaksi and tipe_tukar='Pengurangan Tagihan' ) where nomer_transaksi = '" & reader("nota") & "' ", conn2, transaction)
                cmd.ExecuteNonQuery()
                deletekartuhutang(no_transaksi, conn2, transaction)
            ElseIf reader("tipe_tukar") = "Pengurangan Tagihan Lain" Then
                cmd = New oledbCommand("delete from list_hutang where nomer_transaksi = '" & no_transaksi & "' ", conn2, transaction)
                cmd.ExecuteNonQuery()
                deletekartuhutang(no_transaksi, conn2, transaction)
            End If

            cmd = New oledbCommand("delete from t_returbelid where nomer_returbeli = '" & no_transaksi & "'", conn2, transaction)
            cmd.ExecuteNonQuery()
            cmd = New oledbCommand("delete from t_returbelih where nomer_returbeli = '" & no_transaksi & "'", conn2, transaction)
            cmd.ExecuteNonQuery()

        End If
        reader.Close()
    End Sub


    Public Sub unposting_MutasiStock(ByVal no_transaksi As String, ByVal conn2 As oledbConnection, ByVal transaction As oledbTransaction)
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim cmd1 As oledbCommand
        Dim reader1 As oledbDataReader = Nothing
        Dim tanggal As Date
        cmd = New oledbCommand("select * from t_mutasistockh where nomer_mutasistock = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        If reader.Read() Then
            tanggal = reader("tanggal")
            deletekartustock("", no_transaksi, conn2, transaction)
            cmd = New oledbCommand("delete from t_mutasistockh where nomer_mutasistock = '" & no_transaksi & "'", conn2, transaction)
            cmd.ExecuteNonQuery()
            cmd1 = New oledbCommand("select * from t_mutasistockd where nomer_mutasistock = '" & no_transaksi & "'", conn2, transaction)
            reader1 = cmd1.ExecuteReader()
            Do While reader1.Read
                updatestock(reader("gudang_tujuan"), reader1("kode_barang"), reader1("qty_pcs") * -1, tanggal, "", conn2, transaction)
                updatestock(reader("gudang_asal"), reader1("kode_barang"), reader1("qty_pcs"), tanggal, "", conn2, transaction)
            Loop
            cmd = New oledbCommand("delete from t_mutasistockd where nomer_mutasistock = '" & no_transaksi & "'", conn2, transaction)
            cmd.ExecuteNonQuery()

        End If
        reader.Close()
    End Sub

    Public Sub unposting_pemakaian(ByVal no_transaksi As String, ByVal conn2 As oledbConnection, ByVal transaction As oledbTransaction)
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim cmd1 As oledbCommand
        Dim reader1 As oledbDataReader = Nothing
        Dim tanggal As Date
        cmd = New oledbCommand("select * from t_pemakaianstockh where nomer_pemakaianstock = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        If reader.Read() Then
            tanggal = reader("tanggal")
            deletekartustock("", no_transaksi, conn2, transaction)
            cmd = New oledbCommand("delete from t_pemakaianstockh where nomer_pemakaianstock = '" & no_transaksi & "'", conn2, transaction)
            cmd.ExecuteNonQuery()
            cmd1 = New oledbCommand("select * from t_pemakaianstockd where nomer_pemakaianstock = '" & no_transaksi & "'", conn2, transaction)
            reader1 = cmd1.ExecuteReader()
            Do While reader1.Read
                updatestock(reader("kode_gudang"), reader1("kode_barang"), reader1("qty_pcs"), tanggal, "", conn2, transaction)
            Loop
            cmd = New oledbCommand("delete from t_pemakaianstockd where nomer_pemakaianstock = '" & no_transaksi & "'", conn2, transaction)
            cmd.ExecuteNonQuery()

        End If
        reader.Close()
    End Sub

    Public Sub unposting_konversi(ByVal no_transaksi As String, ByVal conn2 As oledbConnection, ByVal transaction As oledbTransaction)
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim cmd1 As oledbCommand
        Dim reader1 As oledbDataReader = Nothing
        Dim tanggal As Date
        cmd = New oledbCommand("select * from t_konversistockh where nomer_konversi = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        If reader.Read Then
            tanggal = reader("tanggal")
            deletekartustock("", no_transaksi, conn2, transaction)
            updatestock(reader("kode_gudang_hasil"), reader("kode_barang"), reader("qty_pcs") * -1, tanggal, "", conn2, transaction)
            cmd = New oledbCommand("delete from t_konversistockh where nomer_konversi = '" & no_transaksi & "'", conn2, transaction)
            cmd.ExecuteNonQuery()
            cmd1 = New oledbCommand("select * from t_konversistockd where nomer_konversi = '" & no_transaksi & "'", conn2, transaction)
            reader1 = cmd1.ExecuteReader()
            Do While reader1.Read
                updatestock(reader("kode_gudang_bahan"), reader1("kode_barang"), reader1("qty_pcs"), tanggal, "", conn2, transaction)
            Loop
            cmd = New oledbCommand("delete from t_konversistockd where nomer_konversi = '" & no_transaksi & "'", conn2, transaction)
            cmd.ExecuteNonQuery()

        End If
        reader.Close()
    End Sub

    Public Sub unposting_stockawal(ByVal no_transaksi As String, ByVal conn2 As oledbConnection, ByVal transaction As oledbTransaction)
        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim reader1 As oledbDataReader = Nothing
        cmd = New oledbCommand("select * from t_stockawalh where nomer_stockawal = '" & no_transaksi & "'", conn2, transaction)
        reader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                deletekartustock("", no_transaksi, conn2, transaction)
                cmd = New oledbCommand("delete from t_stockawalh where nomer_stockawal = '" & no_transaksi & "'", conn2, transaction)
                cmd.ExecuteNonQuery()
                'cmd1 = New oledbCommand("select * from t_stockawald where id = '" & no_transaksi & "'", conn2, transaction)
                'reader1 = cmd1.ExecuteReader()
                'Do While reader1.Read
                'updatestock(reader("kode_gudang"), reader1("kode_barang"), reader1("qty_pcs") * -1, conn2, transaction)
                'Loop
                cmd = New oledbCommand("delete from t_stockawald where nomer_stockawal = '" & no_transaksi & "'", conn2, transaction)
                cmd.ExecuteNonQuery()
            End While
        End If
        reader.Close()
        reader1.Close()
    End Sub
    Public Sub unposting_produksi(ByVal no_transaksi As String, ByVal conn2 As oledbConnection, ByVal trans As oledbTransaction)
        Dim cmd As oledbCommand
        cmd = New oledbCommand("delete from t_produksi_bahan where nomer_produksihasil = '" & no_transaksi & "'", conn2, trans)
        cmd.ExecuteNonQuery()
        cmd = New oledbCommand("delete from t_produksi_hasil where nomer_produksihasil = '" & no_transaksi & "'", conn2, trans)
        cmd.ExecuteNonQuery()
        cmd = New oledbCommand("delete from t_produksih where nomer_produksihasil = '" & no_transaksi & "'", conn2, trans)
        cmd.ExecuteNonQuery()
        cmd = New oledbCommand("delete from hst_hpp where nomer_transaksi = '" & no_transaksi & "'", conn2, trans)
        cmd.ExecuteNonQuery()
    End Sub
    Public Sub unposting_stockproduksi(ByVal no_transaksi As String, ByVal conn2 As oledbConnection, ByVal trans As oledbTransaction)

        Dim cmd As oledbCommand
        Dim reader As oledbDataReader = Nothing
        Dim cmd1 As oledbCommand
        Dim reader1 As oledbDataReader = Nothing
        Dim tanggal As Date

        cmd = New oledbCommand("select * from t_produksih where nomer_produksihasil = '" & no_transaksi & "'", conn2, trans)
        reader = cmd.ExecuteReader()
        While reader.Read()
            deletekartustock("", no_transaksi, conn2, trans)
            tanggal = reader("tanggal")
            'unposting bahan jadi
            cmd1 = New oledbCommand("select * from t_produksi_hasil a left join ms_barang m on a.kode_barang=m.kode_barang where a.nomer_produksihasil = '" & no_transaksi & "'", conn2, trans)
            reader1 = cmd1.ExecuteReader()
            Do While reader1.Read
                updatestock(reader("kode_gudanghasil"), reader1("kode_barang"), reader1("qty_pcs") * -1, reader("tanggal"), reader1("batch"), conn2, trans)
            Loop

            'unposting bahan
            cmd1 = New oledbCommand("select * from t_produksi_bahan a left join ms_barang m on a.kode_barang=m.kode_barang where a.nomer_produksihasil = '" & no_transaksi & "'", conn2, trans)
            reader1 = cmd1.ExecuteReader()
            Do While reader1.Read
                updatestock(reader("kode_gudangbahan"), reader1("kode_barang"), reader1("qty_pcs"), reader("tanggal"), reader1("batch"), conn2, trans)
            Loop
        End While
        reader.Close()
        Dim query As String
        query = "update hpp set hpp=(select top 1 hpp from hst_hpp where kode_barang=hpp.kode_barang and batch=hpp.batch order by tanggal desc,id desc) where kode_barang in (select kode_barang from t_produksi_hasil where nomer_produksihasil='" & no_transaksi & "');"
        query += "update t_billd set hpp= " & _
             "isnull((select top 1 hpp from hst_hpp where kode_barang=t_billd.kode_barang and tanggal<(select tanggal from t_billh where nomer_bill=t_billd.nomer_bill)" & _
             "order by tanggal desc,id desc),0) where nomer_bill in (select nomer_bill from t_billh where  convert(varchar(20),tanggal,120)>'" & Format(tanggal, "yyyy-MM-dd HH:mm:ss") & "') and  kode_barang in (select kode_barang from t_produksi_hasil where nomer_produksihasil='" & no_transaksi & "')"
        cmd = New oledbCommand(query, conn2, trans)
        cmd.ExecuteNonQuery()
        cmd = New oledbCommand("update t_produksih set posting=0 where nomer_produksihasil = '" & no_transaksi & "'", conn2, trans)
        cmd.ExecuteNonQuery()
    End Sub

End Module
