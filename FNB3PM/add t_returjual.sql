USE [fnb3pm]
GO
/****** Object:  Table [dbo].[t_returbelid]    Script Date: 08/21/2013 16:20:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[t_returjuald](
	[ID] [varchar](15) NOT NULL,
	[kode_barang] [varchar](15) NULL,
	[qty] [numeric](18, 0) NULL,
	[satuan] [varchar](25) NULL,
	[isi] [varchar](25) NULL,
	[harga] [numeric](18, 2) NULL,
	[no_urut] [bigint] NULL,
	[total] [numeric](18, 2) NULL,
	[qty_pcs] [numeric](18, 2) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF

USE [fnb3pm]
GO
/****** Object:  Table [dbo].[t_returbelih]    Script Date: 08/21/2013 16:21:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[t_returjualh](
	[ID] [varchar](15) NOT NULL,
	[tanggal] [datetime] NULL,
	[nama_customer] [varchar](50) NULL,
	[keterangan] [varchar](50) NULL,
	[userid] [varchar](20) NULL,
	[kode_kasbank] [varchar](20) NULL,
	[total] [numeric](18, 0) NULL,
	[kode_gudang] [varchar](3) NULL,
	[tipe_tukar] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF


USE [fnb3pm]
GO
/****** Object:  Table [dbo].[t_returbelid]    Script Date: 08/21/2013 16:20:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[t_mutasistockd](
	[ID] [varchar](15) NOT NULL,
	[kode_barang] [varchar](15) NULL,
	[qty] [numeric](18, 0) NULL,
	[satuan] [varchar](25) NULL,
	[isi] [varchar](25) NULL,
	[no_urut] [bigint] NULL,
	[qty_pcs] [numeric](18, 2) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF

USE [fnb3pm]
GO
/****** Object:  Table [dbo].[t_returbelih]    Script Date: 08/21/2013 16:21:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[t_mutasistockh](
	[ID] [varchar](15) NOT NULL,
	[tanggal] [datetime] NULL,
	[keterangan] [varchar](50) NULL,
	[userid] [varchar](20) NULL,
	[gudang_asal] [varchar](3) NULL,
	[gudang_tujuan] [varchar](3) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF