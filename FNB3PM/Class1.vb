﻿Public Class DBClass
    Private con As ADODB.Connection
    Private com As ADODB.Command
    Private rs As New ADODB.Recordset
    Private UserName As String
    Private Password As String
    Private ServerName As String
    Private DatabaseName As String
    Private DatabaseType As String
    Private constr As String
    Private oleconstr As String
    Private connectionTimeOut As Integer = 0
    Private commandTimeOut As Integer = 0
    Public Sub New()
        'constr = new string
        'UserName = New String
    End Sub

    Public Function openConnection() As String
        con = New ADODB.Connection
        com = New ADODB.Command
        If connectionTimeOut > 0 Then con.ConnectionTimeout = connectionTimeOut
        If commandTimeOut > 0 Then
            con.CommandTimeout = commandTimeOut
            com.CommandTimeout = commandTimeOut
        End If
        'constr = "Provider=" & DatabaseType & ";Persist Security Info=False;Initial Catalog=" & DatabaseName & ";Data Source=" & ServerName & ";User ID=" & UserName & ";Password=" & Password
        'constr = "Provider=" & DatabaseType & ";Persist Security Info=False;Data Source=" & ServerName
        Try
            con.Open(constr)
            com.ActiveConnection = con
        Catch ex As Exception
            MsgBox(ex.Message)
            Return ex.Message
        End Try
        Return ""
    End Function
    Public Function openConnection(ByVal tUsername As String, ByVal tPassword As String) As String
        Dim msg As String
        con = New ADODB.Connection
        com = New ADODB.Command
        msg = ""
        If connectionTimeOut > 0 Then con.ConnectionTimeout = connectionTimeOut
        If commandTimeOut > 0 Then
            con.CommandTimeout = commandTimeOut
            com.CommandTimeout = commandTimeOut
        End If
        Try
            con.Open("Provider=" & DatabaseType & ";Persist Security Info=False;Initial Catalog=" & DatabaseName & ";Data Source=" & ServerName & ";User ID=" & tUsername & ";Password=" & tPassword)
            com.ActiveConnection = con
            UserName = tUsername
            Password = tPassword
        Catch ex As Exception
            MsgBox(ex.Message)
            msg = ex.Message
        End Try
        Return msg
    End Function

    Public Function openConnection(ByVal tDatabaseName As String, ByVal tdatabasetype As String, ByVal tservername As String, ByVal tUsername As String, ByVal tPassword As String) As String
        DatabaseName = tDatabaseName
        DatabaseType = tdatabasetype
        ServerName = tservername
        UserName = tUsername
        Password = tPassword
        Dim msg As String
        con = New ADODB.Connection
        com = New ADODB.Command
        msg = ""
        If connectionTimeOut > 0 Then con.ConnectionTimeout = connectionTimeOut
        If commandTimeOut > 0 Then
            con.CommandTimeout = commandTimeOut
            com.CommandTimeout = commandTimeOut
        End If
        constr = "Provider=" & tdatabasetype & ";Persist Security Info=False;Initial Catalog=" & tDatabaseName & ";Data Source=" & tservername & ";User ID=" & tUsername & ";Password=" & tPassword '& IIf(Len(tExtended) > 0, ";Extended Properties=" & tExtended, "")
        oleconstr = "Persist Security Info=False;Initial Catalog=" & tDatabaseName & ";Data Source=" & tservername & ";User ID=" & tUsername & ";Password=" & tPassword '& IIf(Len(tExtended) > 0, ";Extended Properties=" & tExtended, "")
        'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\folder;Extended Properties=dBASE IV;User ID=Admin;Password=;
        Try
            con.Open(constr)
            com.ActiveConnection = con
        Catch ex As Exception
            MsgBox(ex.Message)
            msg = ex.Message

        End Try
        Return msg
    End Function
    Public Function openConnection(ByVal tConnectionString As String) As String
        'DatabaseName = tDatabaseName
        'DatabaseType = tdatabasetype
        'ServerName = tservername
        'UserName = tUsername
        'Password = tPassword
        Dim msg As String
        con = New ADODB.Connection
        com = New ADODB.Command
        msg = ""
        constr = tConnectionString '"Provider=" & tdatabasetype & ";Persist Security Info=False;Initial Catalog=" & tDatabaseName & ";Data Source=" & tservername & ";User ID=" & tUsername & ";Password=" & tPassword '& IIf(Len(tExtended) > 0, ";Extended Properties=" & tExtended, "")
        oleconstr = tConnectionString ' "Persist Security Info=False;Initial Catalog=" & tDatabaseName & ";Data Source=" & tservername & ";User ID=" & tUsername & ";Password=" & tPassword '& IIf(Len(tExtended) > 0, ";Extended Properties=" & tExtended, "")
        'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\folder;Extended Properties=dBASE IV;User ID=Admin;Password=;
        If connectionTimeOut > 0 Then con.ConnectionTimeout = connectionTimeOut
        If commandTimeOut > 0 Then
            con.CommandTimeout = commandTimeOut
            com.CommandTimeout = commandTimeOut
        End If
        Try
            con.Open(constr)
            com.ActiveConnection = con
        Catch ex As Exception
            MsgBox(ex.Message)
            msg = ex.Message

        End Try
        Return msg
    End Function
    Public Sub closeConnection()
        'com = Nothing
        If con.State Then con.Close()
        'con = Nothing
    End Sub
    Public Sub begintrans()
        Try
            con.BeginTrans()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub committrans()
        Try
            con.CommitTrans()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub rollbacktrans()
        Try
            con.RollbackTrans()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub execute(ByVal commandString As String)
        Try
            com.CommandType = ADODB.CommandTypeEnum.adCmdText
            com.CommandText = commandString
            com.Execute()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub query(ByVal text As String, ByRef rSet As ADODB.Recordset)
        Try
            rSet.Open(text, con, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockOptimistic)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        'query = rSet
    End Sub
    Public Sub querycount(ByVal text As String, ByRef rSet As ADODB.Recordset)
        Try
            rSet.Open(text, con, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        'query = rSet
    End Sub
    'Get and set space
    Public Sub setTimeOut(ByVal timeout As Integer)

        CommandTimeout = timeout
        ConnectionTimeout = timeout
    End Sub
    Public Sub setUsername(ByVal text As String)
        UserName = text
    End Sub
    Public Sub setPassword(ByVal text As String)
        Password = text
    End Sub
    Public Sub setServerName(ByVal text As String)
        ServerName = text
    End Sub
    Public Sub setDatabaseName(ByVal text As String)
        DatabaseName = text
    End Sub
    Public Sub setDatabaseType(ByVal text As String)
        DatabaseType = text
    End Sub
    Public Function getUsername() As String
        getUsername = UserName
    End Function
    Public Function getPassword() As String
        getPassword = Password
    End Function
    Public Function getServerName() As String
        getServerName = ServerName
    End Function

    Public Function getDatabaseName() As String
        getDatabaseName = DatabaseName
    End Function
    Public Function getDatabaseType() As String
        getDatabaseType = DatabaseType
    End Function
    Public Function getConnectionString() As String
        getConnectionString = constr  'con.ConnectionString
    End Function
    Public Function getOLEConnectionString() As String
        getOLEConnectionString = oleconstr  'con.ConnectionString
    End Function
    Public Function getActiveStatus() As String
        getActiveStatus = con.State
    End Function
    Public Sub tambah_data(ByVal table_name As String, ByVal field() As String, ByVal nilai() As String)
        Dim query As String
        Dim i As Integer
        query = "insert into " & table_name & " ("
        For i = 0 To UBound(field) - 1
            query = query & field(i) & ","
        Next
        query = Left(query, Len(query) - 1) & ") values ("
        For i = 0 To UBound(nilai) - 1
            query = query & "'" & nilai(i) & "',"
        Next
        query = Left(query, Len(query) - 1) & ")"
        con.Execute(query)

    End Sub
    Public Function query_tambah_data(ByVal table_name As String, ByVal field() As String, ByVal nilai() As String) As String
        Dim query As String
        Dim i As Integer
        query = "insert into " & table_name & " ("
        For i = 0 To UBound(field) - 1
            If field(i) <> "" Then query = query & field(i) & ","
        Next
        query = Left(query, Len(query) - 1) & ") values ("
        For i = 0 To UBound(nilai) - 1
            If field(i) <> "" Then
                query = query & nilai(i) & ","
            End If
        Next
        query = Left(query, Len(query) - 1) & ")"
        query_tambah_data = query

    End Function
End Class
