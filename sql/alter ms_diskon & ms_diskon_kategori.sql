select * from ms_diskon

alter table ms_diskon add [tipe] [varchar](3) NULL
update ms_diskon set tipe='%' where tipe is null

alter table ms_diskon
add [syarat] [bit] NULL,
	[jenis_syarat] [varchar](20) NULL,
	[qty_syarat] [numeric](18, 0) NULL,
	[qty_item] [numeric](18, 0) NULL


update ms_diskon set syarat=0,jenis_syarat='',qty_syarat=0,qty_item=0 where syarat is null

alter table ms_diskon_kategori
add tipe varchar(20)

update ms_diskon_kategori set tipe='Diskon' where tipe is null