USE [fnb3pm]
GO

/****** Object:  Table [dbo].[t_returjualh]    Script Date: 12/28/2015 15:33:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tmp_returjualh](
	[nomer_returjual] [varchar](15) NOT NULL,
	[tanggal] [datetime] NULL,
	[nama_customer] [varchar](50) NULL,
	[keterangan] [varchar](50) NULL,
	[userid] [varchar](20) NULL,
	[kode_kasbank] [varchar](20) NULL,
	[total] [numeric](18, 0) NULL,
	[kode_gudang] [varchar](3) NULL,
	[tipe_tukar] [varchar](20) NULL,
	[nomer_bill] [varchar](20) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


