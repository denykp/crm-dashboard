USE [fnb3pm]
GO
/****** Object:  StoredProcedure [dbo].[generate_produksi]    Script Date: 11/23/2015 09:49:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--alter table ms_barang add qty_hasilproduksi numeric(18,2)
--update ms_barang set qty_hasilproduksi=0
--SELEct * from tmp_produksi

ALTER procedure [dbo].[generate_produksi] 
as
begin
	declare @kode_barang varchar(15),
			@qty_dibutuhkan		numeric(18,3),
			@qty_os		numeric(18,3),
			@qty		numeric(18,3),
			@batch varchar(20),
			@stock		numeric(18,3),
			@qty_bom		numeric(18,6),
			@kode_bahan varchar(15),
			@jml_batch numeric(18,0),
			@i numeric(18,0)=0,
			@kebutuhan		numeric(18,6)
	
	delete from tmp_produksiBahan
	delete from tmp_stockProduksi
	insert into tmp_stockProduksi select kode_barang,sum(stockawal),batch from tmp_stockawalbatch group by kode_barang,batch 
	--Hasil
	declare crsr cursor local for select kode_barang,qty from tmp_produksi order by kode_barang
	open crsr
	fetch next from crsr INTO @kode_barang,@qty
	WHILE @@FETCH_STATUS = 0
	BEGIN
		--Bahan
		declare crsr1 cursor local for select b.kode_bahan,b.qty/m.qty_hasilproduksi from ms_barang_bom b left join ms_barang m
		on m.kode_barang=b.kode_barang where b.kode_barang=@kode_barang 
		open crsr1
		fetch next from crsr1 INTO @kode_bahan,@qty_bom
		WHILE @@FETCH_STATUS = 0
		BEGIN
			print 'Qty_bom  '+cast(@kode_bahan as varchar) +'-'+cast(@qty_bom as varchar)
			print 'Qty'+cast(@kode_bahan as varchar) +'-'+cast(@qty as varchar)
    		set @qty_dibutuhkan=	@qty*@qty_bom
			set @kebutuhan=@qty_dibutuhkan
			print 'kebutuhan '+cast(@kode_bahan as varchar) +'-'+cast(@kebutuhan as varchar)+'dari barang '+cast(@kode_barang as varchar)
			set @jml_batch= (select COUNT(kode_barang) from tmp_stockProduksi  where kode_barang=@kode_bahan)
			set @i=0
			print 'Jumlah batch '+cast(@kode_bahan as varchar)+'-'+cast(@jml_batch as varchar)
				--Stock
				declare crsr2 cursor local for select batch,stock from tmp_stockProduksi where kode_barang=@kode_bahan  order by batch
				open crsr2
				fetch next from crsr2 INTO @batch,@stock
				WHILE @@FETCH_STATUS = 0 and @qty_dibutuhkan>0
				BEGIN
					set @i=@i+1
					print 'Stock '+cast(@kode_bahan as varchar) +'-'+cast(@stock as varchar)+'-'+cast(@batch as varchar)
					if @qty_dibutuhkan>0 and @stock>0
							begin
							if exists(select * from tmp_produksiBahan where kode_barang=@kode_bahan and batch=@batch)
								begin
								if @jml_batch=@i begin update tmp_produksiBahan set qty=qty+@qty_dibutuhkan,kebutuhan=kebutuhan+@kebutuhan where kode_barang=@kode_bahan and batch=@batch end
								if @jml_batch<>@i begin update tmp_produksiBahan set qty=qty+@stock,kebutuhan=kebutuhan+@kebutuhan where kode_barang=@kode_bahan and batch=@batch end
								end
							else
								begin
								if @jml_batch=@i begin insert into tmp_produksiBahan (kode_barang,kebutuhan,qty,batch,stock) values (@kode_bahan,@kebutuhan,@qty_dibutuhkan,@batch,@stock) end
								if @jml_batch<>@i begin insert into tmp_produksiBahan (kode_barang,kebutuhan,qty,batch,stock) values (@kode_bahan,@kebutuhan,@stock,@batch,@stock) end
								end
							update tmp_stockProduksi set stock=0 where kode_barang=@kode_bahan and batch=@batch
							set @qty_dibutuhkan= @qty_dibutuhkan-@stock
							END
					else if @jml_batch=@i
							BEGIN
							if exists(select * from tmp_produksiBahan where kode_barang=@kode_bahan and batch=@batch)
								begin
								update tmp_produksiBahan set qty=qty+@qty_dibutuhkan,kebutuhan=kebutuhan+@kebutuhan where kode_barang=@kode_bahan and batch=@batch
								end
							else
								begin
								insert into tmp_produksiBahan (kode_barang,kebutuhan,qty,batch,stock) values
								(@kode_bahan,@kebutuhan,@qty_dibutuhkan,@batch,@stock)
								end
							update tmp_stockProduksi set stock=stock-@qty_dibutuhkan where kode_barang=@kode_bahan and batch=@batch
							set @qty_dibutuhkan=0
   							END
   					--End			
   								
				fetch next from crsr2 INTO @batch,@stock
				END
				CLOSE crsr2
				DEALLOCATE crsr2
		
		fetch next from crsr1 INTO @kode_bahan,@qty_bom
		END
		DEALLOCATE crsr1
			    
	fetch next from crsr INTO @kode_barang,@qty
	END
    CLOSE crsr
    DEALLOCATE crsr
	
end