USE [fnb3pm]
GO
/****** Object:  StoredProcedure [dbo].[sp_generate_diskon]    Script Date: 02/04/2016 14:52:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER proc [dbo].[sp_generate_diskon]( 	
	@nomer_bill varchar(15)='', 
	@kode_diskon varchar(10)=''
	)
as

declare @total	numeric(18,2),
			@tipe  varchar(15), 
			@jenis_syarat  varchar(20),
			@qty_syarat  numeric(18,2), 
			@cek_qtydiskon  numeric(18,2),
			@diskon  numeric(18,0),
			@sum_harga  numeric(18,0),
			@qty_item numeric(18,0),
			@total_item  numeric(18,0),
			@i int,
			@qty_diskon  numeric(18,0),
			@qty_disc numeric(10,0),
			@total_disc numeric(18,0),
			@qty		numeric(18,4),
			@harga		numeric(18,4)
	set @tipe =(select isnull(tipe,'') from ms_diskon where kode_diskon=@kode_diskon)
	print 'Nomer bill : '+@nomer_bill
	set @jenis_syarat =(select isnull(jenis_syarat,'') from ms_diskon where kode_diskon=@kode_diskon)
	print 'Jenis Syarat : aaa'+ @jenis_syarat
	set @qty_syarat= (select qty_syarat from ms_diskon where kode_diskon=@kode_diskon)	
	print 'Qty syarat : '+ cast(@qty_syarat as varchar(50))					
	set @cek_qtydiskon =(select isnull(SUM(qty),0) from t_billd d inner join ms_barang m on m.kode_barang=d.kode_barang 
						inner join ms_diskon_kategori md on md.kode_kategori=m.kode_kategori inner join ms_diskon sk 
						on md.kode_diskon=sk.kode_diskon  where nomer_bill=@nomer_bill and sk.kode_diskon=@kode_diskon and md.tipe='Syarat')				
	set @diskon=(select diskon from ms_diskon where kode_diskon=@kode_diskon)
	set @sum_harga =(select isnull(SUM(d.jumlah),0) from t_billd d inner join ms_barang m on m.kode_barang=d.kode_barang 
						inner join ms_diskon_kategori md on md.kode_kategori=m.kode_kategori inner join ms_diskon sk on md.kode_diskon=sk.kode_diskon  
						where nomer_bill=@nomer_bill and sk.kode_diskon=@kode_diskon and md.tipe='Diskon')		
	
	print 'Total nota : '+ cast(@sum_harga as varchar(50))		
	set @qty_item=(select qty_item from ms_diskon where kode_diskon=@kode_diskon)
	set @qty_diskon=(select isnull(SUM(d.qty*d.harga),0) from t_billd d inner join ms_barang m on m.kode_barang=d.kode_barang 
						inner join ms_diskon_kategori md on md.kode_kategori=m.kode_kategori inner join ms_diskon sk on md.kode_diskon=sk.kode_diskon  
						where nomer_bill=@nomer_bill and sk.kode_diskon=@kode_diskon and md.tipe='Diskon')
	set @total_item= 0	
	set @i= 0		
IF @tipe='%'
BEGIN
if @jenis_syarat='Kelipatan' and @qty_item>0
	begin 
			declare crsr cursor local for select d.qty,d.harga from t_billd d inner join ms_barang m on m.kode_barang=d.kode_barang 
							inner join ms_diskon_kategori md on md.kode_kategori=m.kode_kategori inner join ms_diskon sk on md.kode_diskon=sk.kode_diskon  
							where nomer_bill=@nomer_bill and sk.kode_diskon=@kode_diskon and md.tipe='Diskon'
			open crsr
				fetch next from crsr INTO @qty,@harga
				set @qty_disc = (floor(@cek_qtydiskon / @qty_syarat))*(@qty_item)
				set @total_disc = 0
				WHILE @@FETCH_STATUS = 0 AND @qty_disc > 0
				begin
					if @qty <= @qty_item
						begin
						set @qty_disc = @qty_disc - @qty
						set @total_disc = @total_disc + (@qty*@harga)*@diskon/100
						fetch next from crsr INTO @qty,@harga
						end
					else
						begin
						set @total_disc = @total_disc + (@qty_disc*@harga)*@diskon/100
						set @qty_disc = @qty_disc - @qty
						fetch next from crsr INTO @qty,@harga
						end
				end
			CLOSE crsr
			DEALLOCATE crsr
		select @total_disc as discount
		print 'Diskon : '+ cast(@total_disc as varchar(50))	
	end
	
else if @jenis_syarat='Kelipatan' and @qty_item=0
	begin
	select (@sum_harga*@diskon)/100 as discount
	print 'Diskon : '+ cast((@sum_harga*@diskon)/100 as varchar(50))
	end
else if @jenis_syarat='Lebih Dari'
	begin
	if @cek_qtydiskon >@qty_syarat and @qty_item>0
		begin
			declare crsr cursor local for select d.qty,d.harga from t_billd d inner join ms_barang m on m.kode_barang=d.kode_barang 
							inner join ms_diskon_kategori md on md.kode_kategori=m.kode_kategori inner join ms_diskon sk on md.kode_diskon=sk.kode_diskon  
							where nomer_bill=@nomer_bill and sk.kode_diskon=@kode_diskon and md.tipe='Diskon' order by harga
			open crsr
				fetch next from crsr INTO @qty,@harga
				set @qty_disc = @qty_item
				set @total_disc = 0
				WHILE @@FETCH_STATUS = 0 AND @qty_disc > 0
					begin
						if @qty <= @qty_item
							begin
							set @qty_disc = @qty_disc-@qty
							set @total_disc = @total_disc + (@qty*@harga)*@diskon/100  
							fetch next from crsr INTO @qty,@harga
							end
						else
							begin
							set @total_disc = @total_disc + (@qty_disc*@harga)*@diskon/100  
							set @qty_disc = @qty_disc-@qty_disc
							fetch next from crsr INTO @qty,@harga
							end
					end 
				select @total_disc as discount
				print 'Diskon : '+ cast(@total_disc as varchar(50))	
		end
	end
else  if @jenis_syarat=''	
	begin
	select (@sum_harga*@diskon)/100 as discount
	print 'Diskon : '+ cast((@sum_harga*@diskon)/100 as varchar(50))	
	end
else if @diskon=0
	begin
	select 0 as discount
	print 'Diskon : '+ cast(0 as varchar(50))	
	end
END
ELSE IF @tipe='Rp'
BEGIN
SELECT @diskon as discount
print 'Diskon : '+ cast(0 as varchar(50))
END	 