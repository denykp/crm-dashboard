USE [fnb3pm]
GO

CREATE TABLE [dbo].[tmp_stockawalbatch](
	[kode_gudang] [varchar](5) NULL,
	[kode_barang] [varchar](15) NULL,
	[batch] [varchar](15) NULL,
	[stockawal] [numeric](18, 3) NULL
) ON [PRIMARY]

GO



create proc [dbo].[sp_stockawalbatch]( 	

	@kode_barang varchar(15)='', 
	@kode_gudang varchar(5)='',
	@periode varchar(8)=''
	)
as
	delete from tmp_stockawalbatch
	
if (@periode ='')
	insert into tmp_stockawalbatch
	select  ms_gudang.kode_gudang, ms_barang.kode_barang,batch, 0 as stockawal from ms_gudang,ms_barang left join stock on ms_barang.kode_barang=stock.kode_barang 
			group by ms_gudang.kode_gudang,ms_barang.kode_barang,batch
else

	if (@kode_barang ='')
		if (@kode_gudang  ='')
			insert into tmp_stockawalbatch
			select  barang.kode_gudang, barang.kode_barang,batch, isnull(SUM(masuk-keluar),0) as stockawal from (select kode_barang,kode_gudang from ms_barang ,ms_gudang) barang left join kartu_stock  	on barang.kode_barang=kartu_stock.kode_barang and barang.kode_gudang=kartu_stock.kode_gudang
			and CONVERT(varchar(8),tanggal,112)<@periode 
				group by barang.kode_gudang,barang.kode_barang,batch
		else
			insert into tmp_stockawalbatch
			select  barang.kode_gudang, barang.kode_barang,batch, isnull(SUM(masuk-keluar),0) as stockawal from (select kode_barang,kode_gudang from ms_barang ,ms_gudang) barang left join kartu_stock  	on barang.kode_barang=kartu_stock.kode_barang and barang.kode_gudang=kartu_stock.kode_gudang
			and CONVERT(varchar(8),tanggal,112)<@periode  where barang.kode_gudang=@kode_gudang 
				group by barang.kode_gudang,barang.kode_barang,batch
	else
		if (@kode_gudang  ='')
			insert into tmp_stockawalbatch
			select  barang.kode_gudang, barang.kode_barang,batch, isnull(SUM(masuk-keluar),0) as stockawal from (select kode_barang,kode_gudang from ms_barang ,ms_gudang) barang left join kartu_stock  	on barang.kode_barang=kartu_stock.kode_barang and barang.kode_gudang=kartu_stock.kode_gudang
			and CONVERT(varchar(8),tanggal,112)<@periode  where barang.kode_barang=@kode_barang 
				group by barang.kode_gudang,barang.kode_barang,batch
		else
			insert into tmp_stockawalbatch
			select  barang.kode_gudang, barang.kode_barang,batch, isnull(SUM(masuk-keluar),0) as stockawal from (select kode_barang,kode_gudang from ms_barang ,ms_gudang) barang left join kartu_stock  	on barang.kode_barang=kartu_stock.kode_barang and barang.kode_gudang=kartu_stock.kode_gudang
			and CONVERT(varchar(8),tanggal,112)<@periode  where barang.kode_gudang=@kode_gudang and barang.kode_barang=@kode_barang 
				group by barang.kode_gudang,barang.kode_barang,batch



GO


