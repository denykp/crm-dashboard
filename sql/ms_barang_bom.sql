USE [fnb3pm]
GO

/****** Object:  Table [dbo].[ms_barang_bom]    Script Date: 10/30/2015 16:14:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ms_barang_bom](
	[kode_barang] [varchar](7) NOT NULL,
	[kode_bahan] [varchar](7) NOT NULL,
	[qty] [numeric](18, 2) NULL,
	[satuan] [varchar](10) NULL,
	[no_urut] [int] NOT NULL,
 CONSTRAINT [PK_ms_barang_bom] PRIMARY KEY CLUSTERED 
(
	[kode_barang] ASC,
	[kode_bahan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


