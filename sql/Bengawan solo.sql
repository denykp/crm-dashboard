select * from ms_barang

alter view [dbo].[vw_rekapjual] as
select convert(varchar(20),h.tanggal,105) tanggal,d.kode_barang,SUM(d.qty) qty from t_billh h left join t_billd d on h.nomer_bill=d.nomer_bill
group by convert(varchar(20),h.tanggal,105),d.kode_barang 

GO
select * 

 ALTER TABLE t_pembelianh DROP COLUMN grand_total
 GO
 alter table t_pembelianh add grand_total  AS ([total]-[disc]+[biaya])
 
alter table t_pembelianh add biaya numeric(18,2),ket_biaya varchar(50)
update t_pembelianh set biaya=0, ket_biaya=''
update t_pembelianh set disc=0 where disc is null


ALTER view [dbo].[vw_billd] as
select * from t_billd
union all
select nomer_reservasi,kode_barang,qty,urut,'',harga,'',0,0 from t_reservasid

GO

CREATE TABLE [dbo].[t_purchaseorderd](
	[nomer_purchaseorder] [varchar](15) NULL,
	[kode_barang] [varchar](15) NULL,
	[qty] [numeric](18, 2) NULL,
	[satuan] [varchar](15) NULL,
	[no_urut] [numeric](3, 0) NULL,
	[qty_pembelian] [numeric](18, 2) NULL,
	[harga] [numeric](18, 10) NULL,
	[status] [varchar](15) NULL
	) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_purchaseorderh]    Script Date: 12/11/2015 15:49:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_purchaseorderh](
	[nomer_purchaseorder] [varchar](15) NOT NULL,
	[tanggal] [datetime] NULL,
	[kode_supplier] [varchar](15) NULL,
	[status] [varchar](15) NULL,
	[userid] [varchar](15) NULL,
	[keterangan] [varchar](900) NULL,
	[discount] [numeric](18, 0) NULL,
	CONSTRAINT [PK_t_purchaseorderh] PRIMARY KEY CLUSTERED 
(
	[nomer_purchaseorder] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


