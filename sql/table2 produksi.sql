USE [fnb3pm]
GO

alter table t_produksih add operator varchar(15)
update t_produksih set operator=userid

sp_RENAME 't_produksih.kode_gudang', 'kode_gudanghasil' , 'COLUMN'

alter table t_produksih add kode_gudangbahan varchar(15)
update t_produksih set kode_gudangbahan=kode_gudanghasil

alter table t_produksih add posting bit
update t_produksih set posting=1

CREATE TABLE [dbo].[t_produksi_bahan](
	[nomer_produksihasil] [varchar](15) NOT NULL,
	[kode_barang] [varchar](15) NULL,
	[qty_teori] [numeric](18, 3) NULL,
	[qty] [numeric](18, 3) NULL,
	[batch] [varchar](20) NULL,
	[no_urut] [numeric](18, 0) NULL,
	[hpp] [numeric](18, 3) NULL,
	[isi] [numeric](18, 0) NULL,
	[qty_pcs]  AS ([qty]*[isi])
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[t_produksih](
	[nomer_produksihasil] [varchar](15) NOT NULL,
	[tanggal] [datetime] NULL,
	[userid] [varchar](15) NULL,
	[keterangan] [varchar](250) NULL,
	[kode_gudang] [varchar](3) NULL,
	[totalhpp_bahan] [numeric](18, 3) NULL,
 CONSTRAINT [PK_t_produksihasilh] PRIMARY KEY CLUSTERED 
(
	[nomer_produksihasil] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[t_produksi_hasil](
	[nomer_produksihasil] [varchar](15) NOT NULL,
	[kode_barang] [varchar](15) NULL,
	[qty] [numeric](18, 2) NULL,
	[tanggal_expired] [datetime] NULL,
	[batch] [varchar](20) NULL,
	[no_urut] [numeric](18, 0) NULL,
	[isi] [numeric](18, 0) NULL,
	[qty_pcs]  AS ([qty]*[isi]),
	[harga] [numeric](18, 2) NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[tmp_stockProduksi](
	[kode_barang] [varchar](15) NULL,
	[stock] [numeric](18, 3) NULL,
	[batch] [varchar](20) NULL
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[tmp_produksi](
	[kode_barang] [varchar](15) NULL,
	[qty] [numeric](18, 0) NULL,
	[satuan] [varchar](15) NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[tmp_produksiBahan](
	[kode_barang] [varchar](15) NULL,
	[kebutuhan] [numeric](18, 6) NULL,
	[qty] [numeric](18, 6) NULL,
	[batch] [varchar](20) NULL,
	[stock] [numeric](18, 6) NULL
) ON [PRIMARY]

GO