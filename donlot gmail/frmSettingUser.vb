﻿Imports System.Data.OleDb

Public Class frmSettingUser
    Dim transaction As oledbTransaction

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim result As String = ""
        If txtUserid.Text = "" Then
            MsgBox("Username tidak boleh kosong")
            txtUserid.Focus()
            Exit Sub
        End If

        

        Dim reader As oledbDataReader = Nothing
        Dim conn2 As New oledbConnection(strcon)
        conn2.ConnectionString = strcon
        conn2.Open()

        transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)
        Try
            cmd = New OleDbCommand("select * from ms_user where username='" & txtUserid.Text & "'", conn2, transaction)
            reader = cmd.ExecuteReader()


            If reader.Read() Then
                cmd = New OleDbCommand("delete from ms_user where username='" & txtUserid.Text & "'", conn2, transaction)
                cmd.ExecuteNonQuery()
            End If
            reader.Close()

            cmd = New OleDbCommand("insert into [ms_user] ([username],[password]) values ('" & txtUserid.Text & "','" & txtUserid.Text & "')", conn2, transaction)
            cmd.ExecuteNonQuery()

            transaction.Commit()
            conn2.Close()
            If result = "1" Or result = "" Then
                MsgBox("New User Added")
                reset_form()
            Else
                MsgBox(result)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            Try
                transaction.Rollback()
            Finally

            End Try
            If conn2.State Then
                'If reader.IsClosed = False Then
                reader.Close()
                'End If
                conn2.Close()
            End If
        Finally
            If reader IsNot Nothing Then reader.Close()
        End Try
    End Sub

    Private Sub frmSettingUser_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then Me.Close()

        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")

    End Sub

    Private Sub frmSettingUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reset_form()
    End Sub

    Private Sub reset_form()
        'dgv1.Rows.Clear()
        refresh_grid()
        txtUserid.Text = ""

    End Sub

    Private Sub refresh_grid()
        Dim da As New oledbDataAdapter()
        Dim ds As New DataSet()
        Dim ds1 As New DataSet()
        Dim dt As DataTable
        Dim dt1 As DataTable
        Dim conn2 As New oledbConnection(strcon)
        conn2.Open()

        cmd = New OleDbCommand("select [username] from ms_user order by username", conn2)
        da.SelectCommand = cmd
        da.Fill(ds, "ms_user")
        dt = ds.Tables("ms_user")
        dgv1.DataSource = dt
        
        'For i As Integer = 0 To dt.Rows.Count - 1
        '    dgv1.Rows.Add(New String() {dt.Rows(i).Item(0), dt.Rows(i).Item(1), dt.Rows(i).Item(2)})
        'Next

        conn2.Close()
    End Sub

    Private Sub dgv1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv1.CellDoubleClick
        txtUserid.Text = dgv1.Item("username", e.RowIndex).Value

    End Sub

    Private Sub dgv1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv1.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtUserid.Text = dgv1.Item(0, dgv1.CurrentCell.RowIndex).Value
        End If
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        If txtUserid.Text = "" Then
            MsgBox("Username tidak boleh kosong, pilih(double klik) username yang akan dihapus, di tabel yang disediakan")
            txtUserid.Focus()
            Exit Sub
        End If
        Dim conn2 As New oledbConnection(strcon)
        conn2.Open()
        transaction = conn2.BeginTransaction(IsolationLevel.ReadCommitted)
        Try

            cmd = New OleDbCommand("delete from ms_user where username ='" & txtUserid.Text & "'", conn2, transaction)
            cmd.ExecuteNonQuery()

            transaction.Commit()
            conn2.Close()
            MsgBox("Login sudah dihapus")
            reset_form()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
            transaction.Rollback()

        Finally
            If reader IsNot Nothing Then reader.Close()
        End Try
        If conn2.State Then conn2.Close()
    End Sub

    Private Sub btnKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKeluar.Click
        Me.Close()
    End Sub

    Private Sub btnAdd_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.MouseHover
        btnAdd.Font = New Font(btnAdd.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnAdd_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.MouseLeave
        btnAdd.Font = New Font(btnAdd.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub btnHapus_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHapus.MouseHover
        btnHapus.Font = New Font(btnHapus.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnHapus_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHapus.MouseLeave
        btnHapus.Font = New Font(btnHapus.Text, 8, FontStyle.Regular)
    End Sub

    Private Sub btnKeluar_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKeluar.MouseHover
        btnKeluar.Font = New Font(btnKeluar.Text, 10, FontStyle.Underline)
    End Sub

    Private Sub btnKeluar_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKeluar.MouseLeave
        btnKeluar.Font = New Font(btnKeluar.Text, 8, FontStyle.Regular)
    End Sub

   

    Private Sub txtKode_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.F5 Then
            Dim btn As Button = Me.Controls("btnSearch")
            btn.PerformClick()
        End If
    End Sub

    Private Function cmbGroup() As Object
        Throw New NotImplementedException
    End Function

End Class